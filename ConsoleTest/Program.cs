﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Win32;

namespace ConsoleTest
{
    class Program
    {
        public class VSsoftware
        {
            public string name=null;
            public string location=null;
        }
        public static string[] GetVSTools()
        {
            string vsTool = null;
            List<string> vsTools = new List<string>();
            for (int i = 60; i < 150; i = i + 10)
            {
                vsTool= System.Environment.GetEnvironmentVariable(string.Format("VS{0}COMNTOOLS",i), EnvironmentVariableTarget.Machine);
                if (!string.IsNullOrEmpty(vsTool))
                {
                    vsTools.Add(vsTool);
                }
            }
            return vsTools.ToArray();
        }
        public static void RewriteXSD(string root)
        {
            string[] vsTools = GetVSTools();
            string commonPath = null;
            string xsdFolder = null;
            for (int i = 0; i < vsTools.Length; i++)
            {
                commonPath = System.IO.Path.GetDirectoryName(System.IO.Path.GetDirectoryName(vsTools[i]));
                xsdFolder = System.IO.Path.Combine(commonPath, "Packages/schemas/html");
                System.IO.File.Copy(System.IO.Path.Combine(root, "NFinal/Resource/schemas/html/html_401.xsd"), System.IO.Path.Combine(xsdFolder, "html_401.xsd"), true);
                System.IO.File.Copy(System.IO.Path.Combine(root, "NFinal/Resource/schemas/html/html_401.xsd"), System.IO.Path.Combine(xsdFolder, "html_45.xsd"), true);
                System.IO.File.Copy(System.IO.Path.Combine(root, "NFinal/Resource/schemas/html/html_401.xsd"), System.IO.Path.Combine(xsdFolder, "nfinal.xsd"), true);
                System.IO.File.Copy(System.IO.Path.Combine(root, "NFinal/Resource/schemas/html/html_401.xsd"), System.IO.Path.Combine(xsdFolder, "xhtml_5.xsd"), true);
            }
        }
        public static string[] GetStringFromCode(string csharpCode,bool withDoubleQuotes)
        {
            List<string> stringList = new List<string>();
            char[] csharpCodeArray = csharpCode.ToCharArray();
            int start = 0;
            int end = 0;
            int num = 0;
            for (int i = 1; i < csharpCodeArray.Length; i++)
            {
                if (i==0 && csharpCodeArray[0] == '\"')
                {
                    start = 0;
                    num++;
                }
                if (csharpCodeArray[i] == '\"' && csharpCodeArray[i - 1] != '\\')
                {
                    if ((num & 1) == 1 && num !=0)
                    {
                        end = i;
                        if (withDoubleQuotes)
                        {
                            stringList.Add(csharpCode.Substring(start, end - start));
                        }
                        else
                        {
                            stringList.Add(csharpCode.Substring(start+1,end -start -1));
                        }
                    }
                    else
                    {
                        start = i;
                    }
                    num++;
                }
            }
            return stringList.ToArray();
        }
        static void Main(string[] args)
        {
            //string[] str = GetStringFromCode("[RewriteFile(\"123\",\"sdfsdfsdf\")]",false);

            //System.Data.SqlClient.SqlConnection con = new System.Data.SqlClient.SqlConnection(@"Server=.\SQLEXPRESS;Database=test;Uid=sa;Pwd=hayiaf;");
            //con.Open();
            //System.Data.SqlClient.SqlCommand cmd = new System.Data.SqlClient.SqlCommand("select * from fields", con);
            //System.Data.SqlClient.SqlDataReader reader = cmd.ExecuteReader();
            //System.Data.DataTable dt = reader.GetSchemaTable();
            //con.Close();


            //System.Data.SQLite.SQLiteConnection con = new System.Data.SQLite.SQLiteConnection(@"Data Source=D:\workspace\personal\NFinal\WebMvc\App_Data\Common.db;Pooling=true;FailIfMissing=false");
            //con.Open();
            //System.Data.SQLite.SQLiteCommand cmd = new System.Data.SQLite.SQLiteCommand("select * from fields", con);
            //System.Data.SQLite.SQLiteDataReader reader = cmd.ExecuteReader();
            //System.Data.DataTable dt = reader.GetSchemaTable();
            //con.Close();
            //Oracle.ManagedDataAccess.Client.OracleConnection con = new Oracle.ManagedDataAccess.Client.OracleConnection("Data Source==(DESCRIPTION=(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=127.0.0.1)(PORT=1521)))(CONNECT_DATA=(SERVICE_NAME=ORCL)));User Id=system;Password=hayiaf;");
            //con.Open();
            //Oracle.ManagedDataAccess.Client.OracleCommand cmd = new Oracle.ManagedDataAccess.Client.OracleCommand("select * from fields", con);
            //Oracle.ManagedDataAccess.Client.OracleDataReader reader = cmd.ExecuteReader();
            //System.Data.DataTable dt = reader.GetSchemaTable();
            //con.Close();
            //NpgsqlTypes.NpgsqlDbType
            //Npgsql.NpgsqlConnection con = new Npgsql.NpgsqlConnection("Server=localhost;DataBase=postgres;Uid=postgres;Pwd=hayiaf;");
            //con.Open();
            //Npgsql.NpgsqlCommand cmd = new Npgsql.NpgsqlCommand("select * from fields", con);
            //Npgsql.NpgsqlDataReader reader = cmd.ExecuteReader();
            //System.Data.DataTable dt = reader.GetSchemaTable();
            //con.Close();
            MySql.Data.MySqlClient.MySqlConnection con = new MySql.Data.MySqlClient.MySqlConnection("Server=localhost;Database=test;Uid=root;Pwd=root;");
            con.Open();
            MySql.Data.MySqlClient.MySqlCommand cmd = new MySql.Data.MySqlClient.MySqlCommand("select * from fields;", con);
            MySql.Data.MySqlClient.MySqlDataReader reader = cmd.ExecuteReader(System.Data.CommandBehavior.SchemaOnly);
            System.Data.DataTable dt = reader.GetSchemaTable();
            con.Close();
            string title = string.Empty;
            for (int i = 0; i < dt.Columns.Count; i++)
            {
                title += '\t' + dt.Columns[i].ColumnName;
            }
            Console.ReadKey ();
        }
    }
}
