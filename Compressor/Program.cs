﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml;
using Yahoo.Yui.Compressor;

namespace Compressor
{
    class Program
    {
        static void Main(string[] args)
        {
            System.Text.Encoding encoding = System.Text.Encoding.UTF8;
            string root = new DirectoryInfo(AppDomain.CurrentDomain.BaseDirectory).Parent.FullName;
            string webconfig =Path.Combine(root,"Web.config");
            XmlDocument doc = new XmlDocument();
            doc.Load(webconfig);
            XmlNode node = doc.DocumentElement.SelectSingleNode("appSettings/add[@key='Apps']");
            if (node != null && node.Attributes != null && node.Attributes["value"] != null)
            {
                string[] Apps = node.Attributes["value"].Value.Split(',');
                for (int i = 0; i < Apps.Length; i++)
                {
                    string dir = Path.Combine(root, Apps[i]);
                    dir = Path.Combine(dir, "Content");
                    if (Directory.Exists(dir))
                    {
                        Console.WriteLine("开始压缩" + Apps[i]);
                        Compress(dir, encoding);
                        Console.WriteLine("压缩结束" + Apps[i]);
                    }
                    else
                    {
                        Console.WriteLine("模块"+Apps[i]+"不存在");
                    }
                }
            }
            Console.ReadKey();
        }
        public static void Compress(string dir,System.Text.Encoding encoding)
        {
            List<FileInfo> fileList = new List<FileInfo>();
            GetDirList(new DirectoryInfo(dir), ref fileList);
            if (fileList.Count == 0)
            {
                Console.WriteLine("未找到js或css文件！");
                return;
            }

            for (int i = 0; i < fileList.Count; i++)
            {
                FileInfo finfo = fileList[i];
                try
                {
                    //读取文件内容
                    string javaScript = File.ReadAllText(finfo.FullName, encoding);
                    if (!finfo.Name.Contains(".min."))
                    {
                        //取消文件只读
                        File.SetAttributes(finfo.FullName, FileAttributes.Normal);
                        if (finfo.Extension.ToLower() == ".js")
                        {
                            //初始化JS压缩类
                            var js = new JavaScriptCompressor();
                            js.CompressionType = CompressionType.Standard;//压缩类型
                            js.Encoding = encoding;//编码s
                            js.IgnoreEval = false;//大小写转换
                            js.ThreadCulture = System.Globalization.CultureInfo.CurrentCulture;
                            //压缩该js
                            javaScript = js.Compress(javaScript);
                        }
                        else if (finfo.Extension.ToLower() == ".css")
                        {
                            //进行CSS压缩
                            CssCompressor css = new CssCompressor();
                            javaScript = css.Compress(javaScript);
                        }
                        string filePath = finfo.FullName.Replace(finfo.Name, "");
                        // 没有扩展名的文件名 “Default”
                        string fimeName = filePath + Path.GetFileNameWithoutExtension(finfo.FullName) + ".min" + finfo.Extension.ToLower(); ;
                        //写入文件内容
                        File.WriteAllText(fimeName, javaScript);
                        Console.WriteLine(finfo.FullName.Substring(dir.Length) + "压缩完成");
                    }
                    else //跳过
                    {
                        Console.WriteLine(finfo.FullName.Substring(dir.Length) + "跳过");
                    }
                }
                catch (Exception)
                {
                    Console.WriteLine(finfo.FullName.Substring(dir.Length) + "出错");
                }
            }
        }
        public static void GetDirList(DirectoryInfo dir, ref List<FileInfo> fileList)
        {
            foreach (FileInfo info in dir.GetFiles("*.js"))
            {
                fileList.Add(info);
            }
            foreach (FileInfo info2 in dir.GetFiles("*.css"))
            {
                fileList.Add(info2);
            }
            DirectoryInfo[] directories = dir.GetDirectories();
            if (directories.Length > 0)
            {
                foreach (DirectoryInfo info3 in directories)
                {
                    GetDirList(info3, ref fileList);
                }
            }
        }
    }
}
