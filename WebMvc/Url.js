﻿﻿function StringFormat() {
         if (arguments.length == 0)
             return null;
         var str = arguments[0];
         for (var i = 1; i < arguments.length; i++) {
             var re = new RegExp('\\{' + (i - 1) + '\\}', 'gm');
             str = str.replace(re, arguments[i]);
         }
         return str;
     }
String.prototype.endWith = function (s) {
    if (s == null || s == "" || this.length == 0 || s.length > this.length)
        return false;
    if (this.substring(this.length - s.length) == s)
        return true;
    else
        return false;
    return true;
}
String.prototype.startWith = function (s) {
    if (s == null || s == "" || this.length == 0 || s.length > this.length)
        return false;
    if (this.substr(0, s.length) == s)
        return true;
    else
        return false;
    return true;
}
function __ParseUrl__(__url__)
{
	this.next=function()
    {
		return __url__;
	}
	return __url__;
}
var Url=new function()
{
		this.App_IndexController_Index= function()
        {
            var __url__=StringFormat("/App/Index.html");
			__url__ = __ParseUrl__(__url__);
            return __url__;
        };
		this.App_Mysql_update= function(name,pwd)
        {
            var __url__=StringFormat("/App/Mysql/update/{0}/{1}17.php",name,pwd);
			__url__ = __ParseUrl__(__url__);
            return __url__;
        };
		this.App_Mysql_insert= function(name,pwd)
        {
            var __url__=StringFormat("/App/Mysql/insert/{0}/{1}17.php",name,pwd);
			__url__ = __ParseUrl__(__url__);
            return __url__;
        };
		this.App_Mysql_delete= function(id)
        {
            var __url__=StringFormat("/App/Mysql/delete/{0}17.php",id);
			__url__ = __ParseUrl__(__url__);
            return __url__;
        };
		this.App_Mysql_page= function(p)
        {
            var __url__=StringFormat("/App/Mysql/page/{0}15.php",p);
			__url__ = __ParseUrl__(__url__);
            return __url__;
        };
		this.App_Mysql_queryAll= function()
        {
            var __url__=StringFormat("/App/Mysql/queryAll.php");
			__url__ = __ParseUrl__(__url__);
            return __url__;
        };
		this.App_Mysql_queryObject= function()
        {
            var __url__=StringFormat("/App/Mysql/queryObject.php");
			__url__ = __ParseUrl__(__url__);
            return __url__;
        };
		this.App_Mysql_queryRandom= function()
        {
            var __url__=StringFormat("/App/Mysql/queryRandom.php");
			__url__ = __ParseUrl__(__url__);
            return __url__;
        };
		this.App_Mysql_queryRow= function(id)
        {
            var __url__=StringFormat("/App/Mysql/queryRow/{0}19.php",id);
			__url__ = __ParseUrl__(__url__);
            return __url__;
        };
		this.App_Mysql_queryTop= function(top)
        {
            var __url__=StringFormat("/App/Mysql/queryTop/{0}19.php",top);
			__url__ = __ParseUrl__(__url__);
            return __url__;
        };
		this.App_Sqlite_update= function()
        {
            var __url__=StringFormat("/App/Sqlite/update.html");
			__url__ = __ParseUrl__(__url__);
            return __url__;
        };
		this.App_Sqlite_insert= function(name,pwd)
        {
            var __url__=StringFormat("/App/Sqlite/insert/{0}/{1}18.php",name,pwd);
			__url__ = __ParseUrl__(__url__);
            return __url__;
        };
		this.App_Sqlite_delete= function(id)
        {
            var __url__=StringFormat("/App/Sqlite/delete/{0}18.php",id);
			__url__ = __ParseUrl__(__url__);
            return __url__;
        };
		this.App_Sqlite_page= function(p)
        {
            var __url__=StringFormat("/App/Sqlite/page-{0}16.json",p);
			__url__ = __ParseUrl__(__url__);
            return __url__;
        };
		this.App_Sqlite_queryAll= function()
        {
            var __url__=StringFormat("/App/Sqlite/queryAll.php");
			__url__ = __ParseUrl__(__url__);
            return __url__;
        };
		this.App_Sqlite_queryObject= function()
        {
            var __url__=StringFormat("/App/Sqlite/queryObject.php");
			__url__ = __ParseUrl__(__url__);
            return __url__;
        };
		this.App_Sqlite_queryRandom= function()
        {
            var __url__=StringFormat("/App/Sqlite/queryRandom.php");
			__url__ = __ParseUrl__(__url__);
            return __url__;
        };
		this.App_Sqlite_queryRow= function(id)
        {
            var __url__=StringFormat("/App/Sqlite/queryRow/{0}20.php",id);
			__url__ = __ParseUrl__(__url__);
            return __url__;
        };
		this.App_Sqlite_queryTop= function(top)
        {
            var __url__=StringFormat("/App/Sqlite/queryTop/{0}20.php",top);
			__url__ = __ParseUrl__(__url__);
            return __url__;
        };
		this.App_SqliteBLL_queryAll= function()
        {
            var __url__=StringFormat("/App/SqliteBLL/queryAll.php");
			__url__ = __ParseUrl__(__url__);
            return __url__;
        };
		this.App_SqliteBLL_queryRandom= function()
        {
            var __url__=StringFormat("/App/SqliteBLL/queryRandom.php");
			__url__ = __ParseUrl__(__url__);
            return __url__;
        };
		this.App_SqliteCache_queryAll= function()
        {
            var __url__=StringFormat("/App/SqliteCache/queryAll.php");
			__url__ = __ParseUrl__(__url__);
            return __url__;
        };
		this.App_SqliteCache_queryRandom= function()
        {
            var __url__=StringFormat("/App/SqliteCache/queryRandom.php");
			__url__ = __ParseUrl__(__url__);
            return __url__;
        };
		this.App_SqliteCache_queryTop= function()
        {
            var __url__=StringFormat("/App/SqliteCache/queryTop.php");
			__url__ = __ParseUrl__(__url__);
            return __url__;
        };
		this.App_SqliteCache_queryRow= function(id)
        {
            var __url__=StringFormat("/App/SqliteCache/queryRow/{0}25.php",id);
			__url__ = __ParseUrl__(__url__);
            return __url__;
        };
		this.App_SqliteCache_Page= function(p)
        {
            var __url__=StringFormat("/App/SqliteCache/Page/{0}21.php",p);
			__url__ = __ParseUrl__(__url__);
            return __url__;
        };
		this.App_SqliteStruct_update= function(name,pwd)
        {
            var __url__=StringFormat("/App/SqliteStruct/update/{0}/{1}24.php",name,pwd);
			__url__ = __ParseUrl__(__url__);
            return __url__;
        };
		this.App_SqliteStruct_insert= function(name,pwd)
        {
            var __url__=StringFormat("/App/SqliteStruct/insert/{0}/{1}24.php",name,pwd);
			__url__ = __ParseUrl__(__url__);
            return __url__;
        };
		this.App_SqliteStruct_delete= function(id)
        {
            var __url__=StringFormat("/App/SqliteStruct/delete/{0}24.php",id);
			__url__ = __ParseUrl__(__url__);
            return __url__;
        };
		this.App_SqliteStruct_page= function(p)
        {
            var __url__=StringFormat("/App/SqliteStruct/page/{0}22.php",p);
			__url__ = __ParseUrl__(__url__);
            return __url__;
        };
		this.App_SqliteStruct_queryAll= function()
        {
            var __url__=StringFormat("/App/SqliteStruct/queryAll.php");
			__url__ = __ParseUrl__(__url__);
            return __url__;
        };
		this.App_SqliteStruct_queryObject= function()
        {
            var __url__=StringFormat("/App/SqliteStruct/queryObject.php");
			__url__ = __ParseUrl__(__url__);
            return __url__;
        };
		this.App_SqliteStruct_queryRandom= function()
        {
            var __url__=StringFormat("/App/SqliteStruct/queryRandom.php");
			__url__ = __ParseUrl__(__url__);
            return __url__;
        };
		this.App_SqliteStruct_queryRow= function(id)
        {
            var __url__=StringFormat("/App/SqliteStruct/queryRow/{0}26.php",id);
			__url__ = __ParseUrl__(__url__);
            return __url__;
        };
		this.App_SqliteStruct_queryTop= function(top)
        {
            var __url__=StringFormat("/App/SqliteStruct/queryTop/{0}26.php",top);
			__url__ = __ParseUrl__(__url__);
            return __url__;
        };
		this.App_SqliteTrans_update= function(name,pwd)
        {
            var __url__=StringFormat("/App/SqliteTrans/update/{0}/{1}23.php",name,pwd);
			__url__ = __ParseUrl__(__url__);
            return __url__;
        };
		this.App_SqliteTrans_insert= function(name,pwd)
        {
            var __url__=StringFormat("/App/SqliteTrans/insert/{0}/{1}23.php",name,pwd);
			__url__ = __ParseUrl__(__url__);
            return __url__;
        };
		this.App_SqliteTrans_delete= function(id)
        {
            var __url__=StringFormat("/App/SqliteTrans/delete/{0}23.php",id);
			__url__ = __ParseUrl__(__url__);
            return __url__;
        };
		this.App_SqliteTrans_page= function(p)
        {
            var __url__=StringFormat("/App/SqliteTrans/page/{0}21.php",p);
			__url__ = __ParseUrl__(__url__);
            return __url__;
        };
		this.App_SqliteTrans_queryAll= function()
        {
            var __url__=StringFormat("/App/SqliteTrans/queryAll.php");
			__url__ = __ParseUrl__(__url__);
            return __url__;
        };
		this.App_SqliteTrans_queryObject= function()
        {
            var __url__=StringFormat("/App/SqliteTrans/queryObject.php");
			__url__ = __ParseUrl__(__url__);
            return __url__;
        };
		this.App_SqliteTrans_queryRandom= function()
        {
            var __url__=StringFormat("/App/SqliteTrans/queryRandom.php");
			__url__ = __ParseUrl__(__url__);
            return __url__;
        };
		this.App_SqliteTrans_queryRow= function(id)
        {
            var __url__=StringFormat("/App/SqliteTrans/queryRow/{0}25.php",id);
			__url__ = __ParseUrl__(__url__);
            return __url__;
        };
		this.App_SqliteTrans_queryTop= function(top)
        {
            var __url__=StringFormat("/App/SqliteTrans/queryTop/{0}25.php",top);
			__url__ = __ParseUrl__(__url__);
            return __url__;
        };
		this.App_SqliteTransStruct_update= function(name,pwd)
        {
            var __url__=StringFormat("/App/SqliteTransStruct/update/{0}/{1}29.php",name,pwd);
			__url__ = __ParseUrl__(__url__);
            return __url__;
        };
		this.App_SqliteTransStruct_insert= function(name,pwd)
        {
            var __url__=StringFormat("/App/SqliteTransStruct/insert/{0}/{1}29.php",name,pwd);
			__url__ = __ParseUrl__(__url__);
            return __url__;
        };
		this.App_SqliteTransStruct_delete= function(id)
        {
            var __url__=StringFormat("/App/SqliteTransStruct/delete/{0}29.php",id);
			__url__ = __ParseUrl__(__url__);
            return __url__;
        };
		this.App_SqliteTransStruct_page= function(p)
        {
            var __url__=StringFormat("/App/SqliteTransStruct/page/{0}27.php",p);
			__url__ = __ParseUrl__(__url__);
            return __url__;
        };
		this.App_SqliteTransStruct_queryAll= function()
        {
            var __url__=StringFormat("/App/SqliteTransStruct/queryAll.php");
			__url__ = __ParseUrl__(__url__);
            return __url__;
        };
		this.App_SqliteTransStruct_queryObject= function()
        {
            var __url__=StringFormat("/App/SqliteTransStruct/queryObject.php");
			__url__ = __ParseUrl__(__url__);
            return __url__;
        };
		this.App_SqliteTransStruct_queryRandom= function()
        {
            var __url__=StringFormat("/App/SqliteTransStruct/queryRandom.php");
			__url__ = __ParseUrl__(__url__);
            return __url__;
        };
		this.App_SqliteTransStruct_queryRow= function(id)
        {
            var __url__=StringFormat("/App/SqliteTransStruct/queryRow/{0}31.php",id);
			__url__ = __ParseUrl__(__url__);
            return __url__;
        };
		this.App_SqliteTransStruct_queryTop= function(top)
        {
            var __url__=StringFormat("/App/SqliteTransStruct/queryTop/{0}31.php",top);
			__url__ = __ParseUrl__(__url__);
            return __url__;
        };
		this.App_SqlServer_update= function(name,pwd)
        {
            var __url__=StringFormat("/App/SqlServer/update/{0}/{1}21.php",name,pwd);
			__url__ = __ParseUrl__(__url__);
            return __url__;
        };
		this.App_SqlServer_insert= function(name,pwd)
        {
            var __url__=StringFormat("/App/SqlServer/insert/{0}/{1}21.php",name,pwd);
			__url__ = __ParseUrl__(__url__);
            return __url__;
        };
		this.App_SqlServer_delete= function(id)
        {
            var __url__=StringFormat("/App/SqlServer/delete/{0}21.php",id);
			__url__ = __ParseUrl__(__url__);
            return __url__;
        };
		this.App_SqlServer_page= function(p)
        {
            var __url__=StringFormat("/App/SqlServer/page/{0}19.php",p);
			__url__ = __ParseUrl__(__url__);
            return __url__;
        };
		this.App_SqlServer_queryAll= function()
        {
            var __url__=StringFormat("/App/SqlServer/queryAll.php");
			__url__ = __ParseUrl__(__url__);
            return __url__;
        };
		this.App_SqlServer_queryObject= function()
        {
            var __url__=StringFormat("/App/SqlServer/queryObject.php");
			__url__ = __ParseUrl__(__url__);
            return __url__;
        };
		this.App_SqlServer_queryRandom= function()
        {
            var __url__=StringFormat("/App/SqlServer/queryRandom.php");
			__url__ = __ParseUrl__(__url__);
            return __url__;
        };
		this.App_SqlServer_queryRow= function(id)
        {
            var __url__=StringFormat("/App/SqlServer/queryRow/{0}23.php",id);
			__url__ = __ParseUrl__(__url__);
            return __url__;
        };
		this.App_SqlServer_queryTop= function(top)
        {
            var __url__=StringFormat("/App/SqlServer/queryTop/{0}23.php",top);
			__url__ = __ParseUrl__(__url__);
            return __url__;
        };
		this.App_SqlServerStruct_update= function(name,pwd)
        {
            var __url__=StringFormat("/App/SqlServerStruct/update/{0}/{1}27.php",name,pwd);
			__url__ = __ParseUrl__(__url__);
            return __url__;
        };
		this.App_SqlServerStruct_insert= function(name,pwd)
        {
            var __url__=StringFormat("/App/SqlServerStruct/insert/{0}/{1}27.php",name,pwd);
			__url__ = __ParseUrl__(__url__);
            return __url__;
        };
		this.App_SqlServerStruct_delete= function(id)
        {
            var __url__=StringFormat("/App/SqlServerStruct/delete/{0}27.php",id);
			__url__ = __ParseUrl__(__url__);
            return __url__;
        };
		this.App_SqlServerStruct_page= function(p)
        {
            var __url__=StringFormat("/App/SqlServerStruct/page/{0}25.php",p);
			__url__ = __ParseUrl__(__url__);
            return __url__;
        };
		this.App_SqlServerStruct_queryAll= function()
        {
            var __url__=StringFormat("/App/SqlServerStruct/queryAll.php");
			__url__ = __ParseUrl__(__url__);
            return __url__;
        };
		this.App_SqlServerStruct_queryObject= function()
        {
            var __url__=StringFormat("/App/SqlServerStruct/queryObject.php");
			__url__ = __ParseUrl__(__url__);
            return __url__;
        };
		this.App_SqlServerStruct_queryRandom= function()
        {
            var __url__=StringFormat("/App/SqlServerStruct/queryRandom.php");
			__url__ = __ParseUrl__(__url__);
            return __url__;
        };
		this.App_SqlServerStruct_queryRow= function(id)
        {
            var __url__=StringFormat("/App/SqlServerStruct/queryRow/{0}29.php",id);
			__url__ = __ParseUrl__(__url__);
            return __url__;
        };
		this.App_SqlServerStruct_queryTop= function(top)
        {
            var __url__=StringFormat("/App/SqlServerStruct/queryTop/{0}29.php",top);
			__url__ = __ParseUrl__(__url__);
            return __url__;
        };
		this.App_SqlServerTrans_update= function(name,pwd)
        {
            var __url__=StringFormat("/App/SqlServerTrans/update/{0}/{1}26.php",name,pwd);
			__url__ = __ParseUrl__(__url__);
            return __url__;
        };
		this.App_SqlServerTrans_insert= function(name,pwd)
        {
            var __url__=StringFormat("/App/SqlServerTrans/insert/{0}/{1}26.php",name,pwd);
			__url__ = __ParseUrl__(__url__);
            return __url__;
        };
		this.App_SqlServerTrans_delete= function(id)
        {
            var __url__=StringFormat("/App/SqlServerTrans/delete/{0}26.php",id);
			__url__ = __ParseUrl__(__url__);
            return __url__;
        };
		this.App_SqlServerTrans_page= function(p)
        {
            var __url__=StringFormat("/App/SqlServerTrans/page/{0}24.php",p);
			__url__ = __ParseUrl__(__url__);
            return __url__;
        };
		this.App_SqlServerTrans_queryAll= function()
        {
            var __url__=StringFormat("/App/SqlServerTrans/queryAll.php");
			__url__ = __ParseUrl__(__url__);
            return __url__;
        };
		this.App_SqlServerTrans_queryObject= function()
        {
            var __url__=StringFormat("/App/SqlServerTrans/queryObject.php");
			__url__ = __ParseUrl__(__url__);
            return __url__;
        };
		this.App_SqlServerTrans_queryRandom= function()
        {
            var __url__=StringFormat("/App/SqlServerTrans/queryRandom.php");
			__url__ = __ParseUrl__(__url__);
            return __url__;
        };
		this.App_SqlServerTrans_queryRow= function(id)
        {
            var __url__=StringFormat("/App/SqlServerTrans/queryRow/{0}28.php",id);
			__url__ = __ParseUrl__(__url__);
            return __url__;
        };
		this.App_SqlServerTrans_queryTop= function(top)
        {
            var __url__=StringFormat("/App/SqlServerTrans/queryTop/{0}28.php",top);
			__url__ = __ParseUrl__(__url__);
            return __url__;
        };
		this.App_Public_Success= function(message,url,second)
        {
            var __url__=StringFormat("/App/Public/Success/{0}/{1}/{2}19.php",message,url,second);
			__url__ = __ParseUrl__(__url__);
            return __url__;
        };
		this.App_Public_Error= function(message,url,second)
        {
            var __url__=StringFormat("/App/Public/Error/{0}/{1}/{2}17.php",message,url,second);
			__url__ = __ParseUrl__(__url__);
            return __url__;
        };
		this.App_Public_Header= function()
        {
            var __url__=StringFormat("/App/Public/Header.php");
			__url__ = __ParseUrl__(__url__);
            return __url__;
        };
		this.App_Public_Footer= function()
        {
            var __url__=StringFormat("/App/Public/Footer.php");
			__url__ = __ParseUrl__(__url__);
            return __url__;
        };
		this.App_Public_Navigator= function()
        {
            var __url__=StringFormat("/App/Public/Navigator.php");
			__url__ = __ParseUrl__(__url__);
            return __url__;
        };
		this.App_VCode_GetVerifyImage= function()
        {
            var __url__=StringFormat("/App/VCode/GetVerifyImage.php");
			__url__ = __ParseUrl__(__url__);
            return __url__;
        };
		this.App_VCode_VCheck= function()
        {
            var __url__=StringFormat("/App/VCode/VCheck.php");
			__url__ = __ParseUrl__(__url__);
            return __url__;
        };
}