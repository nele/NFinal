﻿﻿//======================================================================
//
//        Copyright : Zhengzhou Strawberry Computer Technology Co.,LTD.
//        All rights reserved
//        
//        Application:NFinal MVC framework
//        Filename :Handler.cs
//        Description :HttpHandler
//
//        created by Lucas at  2015-6-30`
//     
//        WebSite:http://www.nfinal.com
//
//======================================================================
using System;
using System.Collections.Generic;
using System.Web;
using System.Threading;
using System.Globalization;
using System.Collections.Specialized;
using System.Text.RegularExpressions;
namespace NFinal
{
    /// <summary>
    /// NFinal框架重写的HttpHandler类
    /// </summary>
    public class Handler : IHttpAsyncHandler
    {
        public bool IsReusable { get { return false; } }
        public Handler()
        {
        }
        public IAsyncResult BeginProcessRequest(HttpContext context, AsyncCallback cb, Object extraData)
        {
			//初始化配置
			NFinal.ConfigurationManager.Load(context.Server.MapPath("/"));
            AsynchOperation asynch = new AsynchOperation(cb, context, extraData);
            asynch.StartAsyncWork();
            return asynch;
        }
        public void EndProcessRequest(IAsyncResult result)
        {
        }
        public void ProcessRequest(HttpContext context)
        {
            throw new InvalidOperationException();
        }
    }
    internal class HandlerFactory : IHttpHandlerFactory
    {
        private IHttpHandler _handler;
        private Type _handlerType;
        internal HandlerFactory(IHttpHandler handler, Type handlerType)
        {
            this._handler = handler;
            this._handlerType = handlerType;
        }
        public IHttpHandler GetHandler(HttpContext context,
                                    string requestType, string url, string pathTranslated)
        {
            if (this._handler == null)
                this._handler = new Handler();
            return this._handler;
        }
        public void ReleaseHandler(IHttpHandler handler)
        {
            // 一个HttpHandler是否能重用，这里就是一个典型的实现方式
            if (!this._handler.IsReusable)
                this._handler = null;
        }
    }
    public class AsynchOperation : IAsyncResult
    {
        private bool _completed;
        private Object _state;
        private AsyncCallback _callback;
        private HttpContext _context;
        bool IAsyncResult.IsCompleted { get { return _completed; } }
        WaitHandle IAsyncResult.AsyncWaitHandle { get { return null; } }
        Object IAsyncResult.AsyncState { get { return _state; } }
        bool IAsyncResult.CompletedSynchronously { get { return false; } }
        public AsynchOperation(AsyncCallback callback, HttpContext context, Object state)
        {
            _callback = callback;
            _context = context;
            _state = state;
            _completed = false;
        }
        public void StartAsyncWork()
        {
            ThreadPool.QueueUserWorkItem(new WaitCallback(StartAsyncTask), null);
        }
        private void StartAsyncTask(Object workItemState)
        {
			//URL路由模式，1为旧模式，2为新模式
            int urlMode = 2;
            try
            {
                if (urlMode == 1)
                {
                    WebMvc.Main.Run(_context, _context.Request.Path);
                }
                else if (urlMode == 2)
                {
                    WebMvc.Router.Run(_context, _context.Request.Path);
                }
            }
            catch (Exception e)
            {
                _context.Response.Write(e.Message);
                _context.Response.Write(e.Source);
                _context.Response.End();
            }
            _completed = true;
            _callback(this);
        }
    }
}