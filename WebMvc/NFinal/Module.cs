﻿﻿﻿//======================================================================
//
//        Copyright : Zhengzhou Strawberry Computer Technology Co.,LTD.
//        All rights reserved
//        
//        Application:NFinal MVC framework
//        Filename :Module.cs
//        Description :路由类
//
//        created by Lucas at  2015-10-15`
//     
//        WebSite:http://www.nfinal.com
//
//======================================================================
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
namespace NFinal
{
    public class Module:NFinal.BaseModule
    {
        protected override void Rewrite(string requestedPath, HttpApplication app)
        {
			//去掉Server头，没有任何作用，且浪费资源
			app.Context.Response.Headers.Remove("Server");
            string requestPath = app.Context.Request.Path;
            string pathInfo = app.Context.Request.PathInfo;
            string paramers = app.Context.Request.Params.ToString();
            string filePath = app.Context.Request.PhysicalPath;
			string domainName = app.Context.Request.Url.Host;
            //执行自定义重写函数
            if (!RewriteUrl.Rewrite(requestedPath,app.Context))
            {
				//重写虚拟目录
                //重写虚拟路径,也可以把此部分放到find中
				switch (requestedPath)
                {
					default: break;
                }
                //获取actionUrl
                string actionUrl = GetActionUrl(requestedPath);
                int find = FindAction(domainName, actionUrl, requestedPath);
                if (find > 0)
                {
                    //重写url使其能够让NFinal.Handler进行处理
                    app.Context.RewritePath(string.Format("{0}{1}{2}", requestedPath, find.ToString("00000"), ".nf"));
                }
            }
        }
        /// <summary>
        /// 计算actionUrl
        /// </summary>
        /// <param name="requestedPath"></param>
        /// <returns></returns>
        private string GetActionUrl(string requestedPath)
        {
			//找到最后一个.的位置
            int position = requestedPath.LastIndexOf('.');
            bool hasExtension = false;
            char ch = requestedPath[position + 1];
            //判断是否有后缀（后缀首字母不能为数字，且长度必须小于4）
            //.后面不是数字
            if (ch > '9' || ch < '0')
            {
                //.后缀长度必须小于4
                if( requestedPath.Length-position<6)
                {
                    hasExtension = true;
                }
            }
			//指向ActionUrl结尾的位置
            int pointer = 0;
            string actionUrl = requestedPath;
            bool hasGet = false;
			//如果有后缀
            if (hasExtension)
            {
                hasGet = int.TryParse(requestedPath.Substring(position - 2, 2), out pointer);
            }
            //如果没有后缀
            else
            {
                hasGet = int.TryParse(requestedPath.Substring(requestedPath.Length - 2, 2), out pointer);
            }
			//取出ActionUrl
            if (hasGet)
            {
                if (pointer > -1 && pointer < requestedPath.Length)
                {
                    actionUrl = requestedPath.Substring(0, pointer + 1);
                }
            }
            return actionUrl;
        }
        /// <summary>
        /// 查询actionUrl
        /// </summary>
        /// <param name="app"></param>
        /// <param name="actionUrl"></param>
        /// <param name="requestedPath"></param>
        /// <returns></returns>
        public int FindAction(string domainName, string actionUrl, string requestedPath)
        {
            int find = 0;
            //无参数
            switch (requestedPath)
            {
				case "/App/Index.html":
                    find = 1; break;
				case "/App/Mysql/queryAll.php":
                    find = 6; break;
				case "/App/Mysql/queryObject.php":
                    find = 7; break;
				case "/App/Mysql/queryRandom.php":
                    find = 8; break;
				case "/App/Sqlite/queryAll.php":
                    find = 15; break;
				case "/App/Sqlite/queryObject.php":
                    find = 16; break;
				case "/App/Sqlite/queryRandom.php":
                    find = 17; break;
				case "/App/SqliteBLL/queryAll.php":
                    find = 20; break;
				case "/App/SqliteBLL/queryRandom.php":
                    find = 21; break;
				case "/App/SqliteCache/queryAll.php":
                    find = 22; break;
				case "/App/SqliteCache/queryRandom.php":
                    find = 23; break;
				case "/App/SqliteCache/queryTop.php":
                    find = 24; break;
				case "/App/SqliteStruct/queryAll.php":
                    find = 31; break;
				case "/App/SqliteStruct/queryObject.php":
                    find = 32; break;
				case "/App/SqliteStruct/queryRandom.php":
                    find = 33; break;
				case "/App/SqliteTrans/queryAll.php":
                    find = 40; break;
				case "/App/SqliteTrans/queryObject.php":
                    find = 41; break;
				case "/App/SqliteTrans/queryRandom.php":
                    find = 42; break;
				case "/App/SqliteTransStruct/queryAll.php":
                    find = 49; break;
				case "/App/SqliteTransStruct/queryObject.php":
                    find = 50; break;
				case "/App/SqliteTransStruct/queryRandom.php":
                    find = 51; break;
				case "/App/SqlServer/queryAll.php":
                    find = 58; break;
				case "/App/SqlServer/queryObject.php":
                    find = 59; break;
				case "/App/SqlServer/queryRandom.php":
                    find = 60; break;
				case "/App/SqlServerStruct/queryAll.php":
                    find = 67; break;
				case "/App/SqlServerStruct/queryObject.php":
                    find = 68; break;
				case "/App/SqlServerStruct/queryRandom.php":
                    find = 69; break;
				case "/App/SqlServerTrans/queryAll.php":
                    find = 76; break;
				case "/App/SqlServerTrans/queryObject.php":
                    find = 77; break;
				case "/App/SqlServerTrans/queryRandom.php":
                    find = 78; break;
				case "/App/Public/Header.php":
                    find = 83; break;
				case "/App/Public/Footer.php":
                    find = 84; break;
				case "/App/Public/Navigator.php":
                    find = 85; break;
				case "/App/VCode/GetVerifyImage.php":
                    find = 86; break;
				case "/App/VCode/VCheck.php":
                    find = 87; break;
                default: find = 0; break;
            }
			//有参数
			if(find==0)
			{
				switch (actionUrl)
				{
					case "/App/Mysql/update/":
						find = 2; break;
					case "/App/Mysql/insert/":
						find = 3; break;
					case "/App/Mysql/delete/":
						find = 4; break;
					case "/App/Mysql/page/":
						find = 5; break;
					case "/App/Mysql/queryRow/":
						find = 9; break;
					case "/App/Mysql/queryTop/":
						find = 10; break;
					case "/App/Sqlite/update.html":
						find = 11; break;
					case "/App/Sqlite/insert/":
						find = 12; break;
					case "/App/Sqlite/delete/":
						find = 13; break;
					case "/App/Sqlite/page-":
						find = 14; break;
					case "/App/Sqlite/queryRow/":
						find = 18; break;
					case "/App/Sqlite/queryTop/":
						find = 19; break;
					case "/App/SqliteCache/queryRow/":
						find = 25; break;
					case "/App/SqliteCache/Page/":
						find = 26; break;
					case "/App/SqliteStruct/update/":
						find = 27; break;
					case "/App/SqliteStruct/insert/":
						find = 28; break;
					case "/App/SqliteStruct/delete/":
						find = 29; break;
					case "/App/SqliteStruct/page/":
						find = 30; break;
					case "/App/SqliteStruct/queryRow/":
						find = 34; break;
					case "/App/SqliteStruct/queryTop/":
						find = 35; break;
					case "/App/SqliteTrans/update/":
						find = 36; break;
					case "/App/SqliteTrans/insert/":
						find = 37; break;
					case "/App/SqliteTrans/delete/":
						find = 38; break;
					case "/App/SqliteTrans/page/":
						find = 39; break;
					case "/App/SqliteTrans/queryRow/":
						find = 43; break;
					case "/App/SqliteTrans/queryTop/":
						find = 44; break;
					case "/App/SqliteTransStruct/update/":
						find = 45; break;
					case "/App/SqliteTransStruct/insert/":
						find = 46; break;
					case "/App/SqliteTransStruct/delete/":
						find = 47; break;
					case "/App/SqliteTransStruct/page/":
						find = 48; break;
					case "/App/SqliteTransStruct/queryRow/":
						find = 52; break;
					case "/App/SqliteTransStruct/queryTop/":
						find = 53; break;
					case "/App/SqlServer/update/":
						find = 54; break;
					case "/App/SqlServer/insert/":
						find = 55; break;
					case "/App/SqlServer/delete/":
						find = 56; break;
					case "/App/SqlServer/page/":
						find = 57; break;
					case "/App/SqlServer/queryRow/":
						find = 61; break;
					case "/App/SqlServer/queryTop/":
						find = 62; break;
					case "/App/SqlServerStruct/update/":
						find = 63; break;
					case "/App/SqlServerStruct/insert/":
						find = 64; break;
					case "/App/SqlServerStruct/delete/":
						find = 65; break;
					case "/App/SqlServerStruct/page/":
						find = 66; break;
					case "/App/SqlServerStruct/queryRow/":
						find = 70; break;
					case "/App/SqlServerStruct/queryTop/":
						find = 71; break;
					case "/App/SqlServerTrans/update/":
						find = 72; break;
					case "/App/SqlServerTrans/insert/":
						find = 73; break;
					case "/App/SqlServerTrans/delete/":
						find = 74; break;
					case "/App/SqlServerTrans/page/":
						find = 75; break;
					case "/App/SqlServerTrans/queryRow/":
						find = 79; break;
					case "/App/SqlServerTrans/queryTop/":
						find = 80; break;
					case "/App/Public/Success/":
						find = 81; break;
					case "/App/Public/Error/":
						find = 82; break;
					default: find = 0; break;
				}
			}
            return find;
        }
    }
}