﻿//======================================================================
//
//        Copyright : Zhengzhou Strawberry Computer Technology Co.,LTD.
//        All rights reserved
//        
//        Application:NFinal MVC framework
//        Filename :MagicViewBag.cs
//        Description :控制器父类
//
//        created by Lucas at  2015-6-30`
//     
//        WebSite:http://www.nfinal.com
//
//======================================================================
using System;
using System.Collections.Generic;
using System.Web;
using System.Collections.Specialized;
using System.IO;

namespace NFinal
{
    //魔法函数,不允许继承.
    public class MagicAction
    {
        public delegate void __Render__(NameValueCollection nvc);
        public __Render__ __render__ = null;
        public virtual string postEventArgumentID { get { return null; } }
        public virtual string postEventSourceID { get { return null; } }
        /// <summary>
        /// 插入Csharp代码段
        /// </summary>
        /// <param name="textFilePath">含有csharp代码的文本文件路径</param>
        public virtual void InsertCodeSegment(string textFilePath) { }
        /// <summary>
        /// 插入Csharp类文件函数中的代码段
        /// </summary>
        /// <param name="classFilePath">csharp类文件路径</param>
        /// <param name="methodName">类文件中的函数名</param>
        public virtual void InsertCodeSegment(string classFilePath, string methodName) { }
        /// <summary>
        /// 渲染html模板
        /// </summary>
        /// <param name="tplPath">模板相对路径,一般相对于App/Views/Default目录</param>
        public virtual void View(string tplPath) { }
        /// <summary>
        /// 渲染html模板
        /// </summary>
        /// <param name="name">模板样式</param>
        /// <param name="tplPath">模板相对路径,一般相对于App/Views/Default目录</param>
        public virtual void View(string name, string tplPath) { }
        public virtual void Redirect(string url)
        { 
            
        }
        public virtual bool Verify()
        {
            return true;
        }
    }
    /// <summary>
    /// 请求类型
    /// </summary>
    public enum MethodType
    {
        NONE = 0,
        POST = 1,
        GET = 2,
        PUT = 3,
        DELETE = 4,
        AJAX = 5
    }
    public struct urlInfo
    {
        public string subdomain;
        public string action;
        public string controller;
        public string app;
        public string url;
    }
    //Controller基类
    public class BaseAction : MagicAction
    {
        public HttpContext _context = null;
        public NameValueCollection _get = null;
        public TextWriter _tw = null;
        public urlInfo urlInfo;
        public string _subdomain;//二级域名
        public string _action;
        public string _controller;
        public string _app;
        public string _url;
        protected bool _isStatic = false;
        protected string _salt = "nfinal";
        public NFinal.MethodType _methodType = NFinal.MethodType.NONE;
        public bool isMobile {
            get { return CheckAgent(); }
        }
        /// <summary>
        /// 是否是手机端
        /// </summary>
        /// <returns></returns>
        public static bool CheckAgent()
        {
            bool flag = false;

            string agent = HttpContext.Current.Request.UserAgent;
            string[] keywords = { "Android", "iPhone", "iPod", "iPad", "Windows Phone", "MQQBrowser" };

            //排除 Windows 桌面系统  
            if (!agent.Contains("Windows NT") || (agent.Contains("Windows NT") && agent.Contains("compatible; MSIE 9.0;")))
            {
                //排除 苹果桌面系统  
                if (!agent.Contains("Windows NT") && !agent.Contains("Macintosh"))
                {
                    foreach (string item in keywords)
                    {
                        if (agent.Contains(item))
                        {
                            flag = true;
                            break;
                        }
                    }
                }
            }

            return flag;
        }
        /// <summary>
        /// 插入Csharp代码段
        /// </summary>
        /// <param name="textFilePath">含有csharp代码的文本文件路径</param>
        public sealed override void InsertCodeSegment(string textFilePath)
        {
        }
        /// <summary>
        /// 插入Csharp类文件函数中的代码段
        /// </summary>
        /// <param name="classFilePath">csharp类文件路径</param>
        /// <param name="methodName">类文件中的函数名</param>
        public sealed override void InsertCodeSegment(string classFilePath, string methodName)
        { 
        }

        public virtual void Before()
        {
            
        }
        public virtual void After()
        { 
            
        }

        #region 初始化函数
        public BaseAction() { }
        
        public string MapPath(string url)
        {
            return new DirectoryInfo(AppDomain.CurrentDomain.BaseDirectory).FullName + "\\" + url.Trim('/').Replace('/', '\\');
        }
        public BaseAction(string fileName)
        {
            this._isStatic = true;
            System.IO.StreamWriter sw = new System.IO.StreamWriter(MapPath(fileName), false, System.Text.Encoding.UTF8);
            this._tw = sw;
        }
        public void Close()
        {
            this._tw.Dispose();
        }
        public BaseAction(System.IO.TextWriter tw)
        {
            this._tw = tw;
        }
        public BaseAction(System.Web.HttpContext context)
        {
            this._tw = context.Response.Output;
            this._context = context;
            this._isStatic = false;
        }
        #endregion
        #region 输出函数
        public void WriteLine(string val)
        {
            _tw.Write(val);
            _tw.Write("<br/>");
        }
        public void WriteLine(object val)
        {
            _tw.Write(val);
            _tw.Write("<br/>");
        }
        public void Write(object obj)
        {
            _tw.Write(obj.ToString());
        }
        public void Write(byte[] buffer)
        {
            if (!_isStatic)
            {
                _context.Response.BinaryWrite(buffer);
            }
            else
            {
                _tw.Write(buffer);
            }
        }
        public void Write(string val)
        {
            _tw.Write(val);
        }
        #endregion
        #region Ajax返回数据
        public void AjaxReturn(dynamic obj)
        {
            AjaxReturn(obj.ToJson());
        }
        public void AjaxReturn(Struct str)
        {
            if (str == null)
            {
                AjaxReturn("null");
            }
            else
            {
                AjaxReturn(str.ToJson());
            }
        }
        public void AjaxReturn(Struct str, int code)
        {
            if (str == null)
            {
                AjaxReturn("null", code);
            }
            else
            {
                AjaxReturn(str.ToJson(), code);
            }
        }
        public void AjaxReturn(Struct str,int code,string msg)
        {
            if (str == null)
            {
                AjaxReturn("null",code,msg);
            }
            else
            {
                AjaxReturn(str.ToJson(),code,msg);
            }
        }
        public void AjaxReturn(string json)
        {
            AjaxReturn(json, 1, "");
        }
        public void AjaxReturn(int code)
        {
            this._context.Response.ContentType = "application/json";
            this._context.Response.Write(string.Format("{{\"code\":{0}}}", code));
        }
        public void AjaxReturn(string json, int code)
        {
            AjaxReturn(json, code, "");
        }
        public void AjaxReturn(string json, int code, string msg)
        {
            this._context.Response.ContentType = "application/json";
            this._context.Response.Write(string.Format("{{\"code\":{0},\"msg\":\"{1}\",\"result\":{2}}}", code, msg, json));
        }
        public void AjaxReturn(int code, string msg)
        {
            this._context.Response.ContentType = "application/json";
            this._context.Response.Write(string.Format("{{\"code\":{0},\"msg\":\"{1}\"}}", code, msg));
        }
        public void AjaxReturn(bool obj)
        {
            this._context.Response.ContentType = "application/json";
            this._context.Response.Write(obj ? "true" : "false");
        }
        public void AjaxReturn<T>(NFinal.List<T> obj)
        {
            this.AjaxReturn(obj.ToJson(), obj ? 1: 0);
        }
        #endregion
        //魔法函数
        public sealed override void View(string tplPath)
        { 
            
        }
        public sealed override void View(string name, string tplPath)
        { 
            
        }
        public sealed override void Redirect(string url)
        {
            if (!_isStatic)
            {
                _context.Response.Redirect(url);
            }
        }
        //验证码验证
        public sealed override bool Verify()
        {
            if (_isStatic)
            {
                return true;
            }
            else
            {
                string _vcode= _context.Request.Form["_vcode"];

                string _vcheck = null;
                if (_context.Request.Cookies["vcheck"] != null)
                {
                    _vcheck = _context.Request.Cookies["vcheck"].Value;
                }
                else
                {
                    return false;
                }
                using (System.Security.Cryptography.MD5CryptoServiceProvider md5 = new System.Security.Cryptography.MD5CryptoServiceProvider())
                {
                    if (BitConverter.ToString(md5.ComputeHash(System.Text.Encoding.UTF8.GetBytes(_vcode + _salt))).Replace("-", "") == _vcheck)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
        }
    }
}