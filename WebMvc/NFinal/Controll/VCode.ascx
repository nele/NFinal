﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="VCode.ascx.cs" Inherits="NFinal.Controll.VCode" %>
<style>
    .vcode {
        height:22px;
        width:150px;
    }
    .vcode input{
        height:20px;
        border:1px solid #ddd;
        outline:0;
    }
    .vcode img{
        height:22px;
        width:50px;
    }
</style>
<span class="vcode">
    <input type="text" id="_vcode" name="_vcode" onkeyup="if (this.value != this.value.toUpperCase()) this.value=this.value.toUpperCase();">
    <img src="<%=_app %>/Common/VCodeController/GetVerifyImage.htm" onclick="this.src='<%=_app %>/Common/VCodeController/GetVerifyImage.htm'+'?_:'+Math.random();">
</span>