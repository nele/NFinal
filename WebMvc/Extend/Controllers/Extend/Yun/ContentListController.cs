﻿
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using WebMvc.App.Code.Extend.Yun;

namespace WebMvc.App.Controllers.Extend.Yun
{
	public class ContentListController : Controller
	{
		public void Index()
		{
			Models.contentConnStr.OpenConnection();
			var logList = Models.contentConnStr.QueryAll("select * from content where isDeleted=1");
			Models.contentConnStr.CloseConnection();
			View("Index.aspx");
		}

		public void ReUp(int id, string key,string module, string file, string filetype, string yun, string domain, string bucketName, string accessId, string accessKey, string sceretKey)
		{
			bool isSuccess = false;
			int flag = 1;
			string fileName = key.Split('/')[key.Split('/').Length-1];
			Models.contentConnStr.OpenConnection();
			if (yun == "1")//阿里云
			{
				IUploadable yuntype = Base.GetYun(yunType.ALi);
				yuntype.Initial(bucketName, key, file, accessId, accessKey, "");
				isSuccess = yuntype.Upload(bucketName, key, file, accessId, accessKey, "", 2 * 1024 * 1024);
				//bool isSuccess = UpDownload.ResumablePutFile("testapp", key + keyGUID, file);
				if (isSuccess)
				{
					flag++;
					//则将此条记录的上传标识设置为1，
					string localUrl = "/" + key;
					string yunUrl = domain + "/" + key;
					var updateCount = Models.contentConnStr.Update("update content set module=@module,yun=1,domain=@domain,fileType=@filetype,isSuccess=1,isDeleted=1,localUrl=@localUrl,yunUrl=@yunUrl,keyStr=@key,file=@file,bucketName=@bucketName,accessId=@accessId,accessKey=@accessKey,sceretKey=@sceretKey where id=@id");
					AjaxReturn(1, "");
				}
				else if (!isSuccess)
				{
					//上传失败，也将此条记录记录到数据库中，-1
					string localUrl = "/" + key;
					string yunUrl = "";
					var updateCount = Models.contentConnStr.Update("update content set (module=@module,yun=1,domain=@domain,fileType=@filetype,isSuccess=-1,isDeleted=1,localUrl=@localUrl,keyStr=@key,file=@file,bucketName=@bucketName,accessId=@accessId,accessKey=@accessKey,sceretKey=@sceretKey where id=@id");
					AjaxReturn(0, "");
				}
				else
				{
					//出现未知错误
				}

			}
			else if (yun == "2")
			{
				//七牛云
				IUploadable yuntype = Base.GetYun(yunType.QiNiu);
				yuntype.Initial("", "", "", "", accessKey, sceretKey);
				isSuccess = yuntype.Upload(bucketName, key, file, "", accessKey, sceretKey, 0);
				//bool isSuccess = UpDownload.ResumablePutFile("testapp", key + keyGUID, file);
				if (isSuccess)
				{
					flag++;
					//则将此条记录的上传标识设置为1，
					string localUrl = "/" + key;
					string yunUrl = domain + "/" + key;

					var table = Models.contentConnStr.QueryRow("select * from content where localUrl=@localUrl");

					//List<Models> userList = Models.contentConnStr.QueryRow("select * from content where localUrl=@localUrl");

					var updateCount = Models.contentConnStr.Update("update content set module=@module,yun=2,domain=@domain,fileType=@filetype,isSuccess=1,isDeleted=1,localUrl=@localUrl,yunUrl=@yunUrl,keyStr=@key,file=@file,bucketName=@bucketName,accessId=@accessId,accessKey=@accessKey,sceretKey=@sceretKey where id=@id");
					AjaxReturn(1, "");
				}
				else if (!isSuccess)
				{
					//上传失败，也将此条记录记录到数据库中，-1
					string localUrl = "/" + key;

					var table = Models.contentConnStr.QueryRow("select * from content where localUrl=@localUrl");
					string yunUrl = "";
					
					var updateCount = Models.contentConnStr.Update("update content set module=@module,yun=2,domain=@domain,fileType=@filetype,isSuccess=-1,isDeleted=1,localUrl=@localUrl,keyStr=@key,file=@file,bucketName=@bucketName,accessId=@accessId,accessKey=@accessKey,sceretKey=@sceretKey where id=@id");
					AjaxReturn(0, "");
				}
				else
				{
					//出现未知错误
				}

			}
			Models.contentConnStr.CloseConnection();
		}
	}
}