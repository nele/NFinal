﻿
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using WebMvc.App.Code.Extend.Yun;

namespace WebMvc.App.Web.Default.Extend.Yun.ContentListController
{
	public class ReUpAction  : Controller
	{
		public ReUpAction(System.IO.TextWriter tw):base(tw){}
		public ReUpAction(string fileName) : base(fileName) {}
		

		public class __ReUp_table__:NFinal.DB.Struct
		{
			public System.Int32 id;
			public System.String module;
			public System.String yun;
			public System.String domain;
			public System.String fileType;
			public System.Boolean isSuccess;
			public System.Boolean isDeleted;
			public System.String localUrl;
			public System.String yunUrl;
			public System.String keyStr;
			public System.String file;
			public System.String bucketName;
			public System.String accessId;
			public System.String accessKey;
			public System.String sceretKey;
			#region 写Json字符串
			public override void WriteJson(System.IO.TextWriter tw)
			{
				tw.Write("{");
						tw.Write("\"id\":");
						tw.Write(id.ToString());
						tw.Write(",");
						tw.Write("\"module\":");//
						tw.Write("\"");
						WriteString(module==null?"null":module.ToString(),tw);
						tw.Write("\"");
						tw.Write(",");
						tw.Write("\"yun\":");//
						tw.Write("\"");
						WriteString(yun==null?"null":yun.ToString(),tw);
						tw.Write("\"");
						tw.Write(",");
						tw.Write("\"domain\":");//
						tw.Write("\"");
						WriteString(domain==null?"null":domain.ToString(),tw);
						tw.Write("\"");
						tw.Write(",");
						tw.Write("\"fileType\":");//
						tw.Write("\"");
						WriteString(fileType==null?"null":fileType.ToString(),tw);
						tw.Write("\"");
						tw.Write(",");
						tw.Write("\"isSuccess\":");
						tw.Write(isSuccess?"true":"false");
						tw.Write(",");
						tw.Write("\"isDeleted\":");
						tw.Write(isDeleted?"true":"false");
						tw.Write(",");
						tw.Write("\"localUrl\":");//
						tw.Write("\"");
						WriteString(localUrl==null?"null":localUrl.ToString(),tw);
						tw.Write("\"");
						tw.Write(",");
						tw.Write("\"yunUrl\":");//
						tw.Write("\"");
						WriteString(yunUrl==null?"null":yunUrl.ToString(),tw);
						tw.Write("\"");
						tw.Write(",");
						tw.Write("\"keyStr\":");//
						tw.Write("\"");
						WriteString(keyStr==null?"null":keyStr.ToString(),tw);
						tw.Write("\"");
						tw.Write(",");
						tw.Write("\"file\":");//
						tw.Write("\"");
						WriteString(file==null?"null":file.ToString(),tw);
						tw.Write("\"");
						tw.Write(",");
						tw.Write("\"bucketName\":");//
						tw.Write("\"");
						WriteString(bucketName==null?"null":bucketName.ToString(),tw);
						tw.Write("\"");
						tw.Write(",");
						tw.Write("\"accessId\":");//
						tw.Write("\"");
						WriteString(accessId==null?"null":accessId.ToString(),tw);
						tw.Write("\"");
						tw.Write(",");
						tw.Write("\"accessKey\":");//
						tw.Write("\"");
						WriteString(accessKey==null?"null":accessKey.ToString(),tw);
						tw.Write("\"");
						tw.Write(",");
						tw.Write("\"sceretKey\":");//
						tw.Write("\"");
						WriteString(sceretKey==null?"null":sceretKey.ToString(),tw);
						tw.Write("\"");
				tw.Write("}");
			}
			#endregion
		}
		public void ReUp(int id,string key,string module,string file,string filetype,string yun,string domain,string bucketName,string accessId,string accessKey,string sceretKey)
		{
			bool isSuccess = false;
			int flag = 1;
			string fileName = key.Split('/')[key.Split('/').Length-1];
			var __ReUp_contentConnStr_con__ = new MySql.Data.MySqlClient.MySqlConnection(Models.ConnectionStrings.contentConnStr);
			__ReUp_contentConnStr_con__.Open();
			if (yun == "1")//阿里云
			{
				IUploadable yuntype = Base.GetYun(yunType.ALi);
				yuntype.Initial(bucketName, key, file, accessId, accessKey, "");
				isSuccess = yuntype.Upload(bucketName, key, file, accessId, accessKey, "", 2 * 1024 * 1024);
				//bool isSuccess = UpDownload.ResumablePutFile("testapp", key + keyGUID, file);
				if (isSuccess)
				{
					flag++;
					//则将此条记录的上传标识设置为1，
					string localUrl = "/" + key;
					string yunUrl = domain + "/" + key;
						#region	var updateCount; 更新表,并返回受影响行数
			var __ReUp_updateCount_command__ = new MySql.Data.MySqlClient.MySqlCommand("update content set module=?module,yun=1,domain=?domain,fileType=?filetype,isSuccess=1,isDeleted=1,localUrl=?localUrl,yunUrl=?yunUrl,keyStr=?key,file=?file,bucketName=?bucketName,accessId=?accessId,accessKey=?accessKey,sceretKey=?sceretKey where id=?id;", __ReUp_contentConnStr_con__);
			var __ReUp_updateCount_parameters__=new MySql.Data.MySqlClient.MySqlParameter[12];
					__ReUp_updateCount_parameters__[0] = new MySql.Data.MySqlClient.MySqlParameter("?module",MySql.Data.MySqlClient.MySqlDbType.VarChar,20);
					__ReUp_updateCount_parameters__[0].Value = module;
					__ReUp_updateCount_parameters__[1] = new MySql.Data.MySqlClient.MySqlParameter("?domain",MySql.Data.MySqlClient.MySqlDbType.VarChar,200);
					__ReUp_updateCount_parameters__[1].Value = domain;
					__ReUp_updateCount_parameters__[2] = new MySql.Data.MySqlClient.MySqlParameter("?filetype",MySql.Data.MySqlClient.MySqlDbType.VarChar,20);
					__ReUp_updateCount_parameters__[2].Value = filetype;
					__ReUp_updateCount_parameters__[3] = new MySql.Data.MySqlClient.MySqlParameter("?localUrl",MySql.Data.MySqlClient.MySqlDbType.VarChar,200);
					__ReUp_updateCount_parameters__[3].Value = localUrl;
					__ReUp_updateCount_parameters__[4] = new MySql.Data.MySqlClient.MySqlParameter("?yunUrl",MySql.Data.MySqlClient.MySqlDbType.VarChar,200);
					__ReUp_updateCount_parameters__[4].Value = yunUrl;
					__ReUp_updateCount_parameters__[5] = new MySql.Data.MySqlClient.MySqlParameter("?key",MySql.Data.MySqlClient.MySqlDbType.VarChar,200);
					__ReUp_updateCount_parameters__[5].Value = key;
					__ReUp_updateCount_parameters__[6] = new MySql.Data.MySqlClient.MySqlParameter("?file",MySql.Data.MySqlClient.MySqlDbType.VarChar,200);
					__ReUp_updateCount_parameters__[6].Value = file;
					__ReUp_updateCount_parameters__[7] = new MySql.Data.MySqlClient.MySqlParameter("?bucketName",MySql.Data.MySqlClient.MySqlDbType.VarChar,200);
					__ReUp_updateCount_parameters__[7].Value = bucketName;
					__ReUp_updateCount_parameters__[8] = new MySql.Data.MySqlClient.MySqlParameter("?accessId",MySql.Data.MySqlClient.MySqlDbType.VarChar,200);
					__ReUp_updateCount_parameters__[8].Value = accessId;
					__ReUp_updateCount_parameters__[9] = new MySql.Data.MySqlClient.MySqlParameter("?accessKey",MySql.Data.MySqlClient.MySqlDbType.VarChar,200);
					__ReUp_updateCount_parameters__[9].Value = accessKey;
					__ReUp_updateCount_parameters__[10] = new MySql.Data.MySqlClient.MySqlParameter("?sceretKey",MySql.Data.MySqlClient.MySqlDbType.VarChar,200);
					__ReUp_updateCount_parameters__[10].Value = sceretKey;
					__ReUp_updateCount_parameters__[11] = new MySql.Data.MySqlClient.MySqlParameter("?id",MySql.Data.MySqlClient.MySqlDbType.Int32);
					__ReUp_updateCount_parameters__[11].Value = id;
			__ReUp_updateCount_command__.Parameters.AddRange(__ReUp_updateCount_parameters__);
			var updateCount = __ReUp_updateCount_command__.ExecuteNonQuery();
			__ReUp_updateCount_command__.Dispose();
	#endregion
			
					AjaxReturn(1, "");
				}
				else if (!isSuccess)
				{
					//上传失败，也将此条记录记录到数据库中，-1
					string localUrl = "/" + key;
					string yunUrl = "";
						#region	var updateCount; 更新表,并返回受影响行数
			var __ReUp_updateCount_command__ = new MySql.Data.MySqlClient.MySqlCommand("update content set (module=?module,yun=1,domain=?domain,fileType=?filetype,isSuccess=-1,isDeleted=1,localUrl=?localUrl,keyStr=?key,file=?file,bucketName=?bucketName,accessId=?accessId,accessKey=?accessKey,sceretKey=?sceretKey where id=?id;", __ReUp_contentConnStr_con__);
			var __ReUp_updateCount_parameters__=new MySql.Data.MySqlClient.MySqlParameter[11];
					__ReUp_updateCount_parameters__[0] = new MySql.Data.MySqlClient.MySqlParameter("?module",MySql.Data.MySqlClient.MySqlDbType.VarChar,20);
					__ReUp_updateCount_parameters__[0].Value = module;
					__ReUp_updateCount_parameters__[1] = new MySql.Data.MySqlClient.MySqlParameter("?domain",MySql.Data.MySqlClient.MySqlDbType.VarChar,200);
					__ReUp_updateCount_parameters__[1].Value = domain;
					__ReUp_updateCount_parameters__[2] = new MySql.Data.MySqlClient.MySqlParameter("?filetype",MySql.Data.MySqlClient.MySqlDbType.VarChar,20);
					__ReUp_updateCount_parameters__[2].Value = filetype;
					__ReUp_updateCount_parameters__[3] = new MySql.Data.MySqlClient.MySqlParameter("?localUrl",MySql.Data.MySqlClient.MySqlDbType.VarChar,200);
					__ReUp_updateCount_parameters__[3].Value = localUrl;
					__ReUp_updateCount_parameters__[4] = new MySql.Data.MySqlClient.MySqlParameter("?key",MySql.Data.MySqlClient.MySqlDbType.VarChar,200);
					__ReUp_updateCount_parameters__[4].Value = key;
					__ReUp_updateCount_parameters__[5] = new MySql.Data.MySqlClient.MySqlParameter("?file",MySql.Data.MySqlClient.MySqlDbType.VarChar,200);
					__ReUp_updateCount_parameters__[5].Value = file;
					__ReUp_updateCount_parameters__[6] = new MySql.Data.MySqlClient.MySqlParameter("?bucketName",MySql.Data.MySqlClient.MySqlDbType.VarChar,200);
					__ReUp_updateCount_parameters__[6].Value = bucketName;
					__ReUp_updateCount_parameters__[7] = new MySql.Data.MySqlClient.MySqlParameter("?accessId",MySql.Data.MySqlClient.MySqlDbType.VarChar,200);
					__ReUp_updateCount_parameters__[7].Value = accessId;
					__ReUp_updateCount_parameters__[8] = new MySql.Data.MySqlClient.MySqlParameter("?accessKey",MySql.Data.MySqlClient.MySqlDbType.VarChar,200);
					__ReUp_updateCount_parameters__[8].Value = accessKey;
					__ReUp_updateCount_parameters__[9] = new MySql.Data.MySqlClient.MySqlParameter("?sceretKey",MySql.Data.MySqlClient.MySqlDbType.VarChar,200);
					__ReUp_updateCount_parameters__[9].Value = sceretKey;
					__ReUp_updateCount_parameters__[10] = new MySql.Data.MySqlClient.MySqlParameter("?id",MySql.Data.MySqlClient.MySqlDbType.Int32);
					__ReUp_updateCount_parameters__[10].Value = id;
			__ReUp_updateCount_command__.Parameters.AddRange(__ReUp_updateCount_parameters__);
			var updateCount = __ReUp_updateCount_command__.ExecuteNonQuery();
			__ReUp_updateCount_command__.Dispose();
	#endregion
			
					AjaxReturn(0, "");
				}
				else
				{
					//出现未知错误
				}

			}
			else if (yun == "2")
			{
				//七牛云
				IUploadable yuntype = Base.GetYun(yunType.QiNiu);
				yuntype.Initial("", "", "", "", accessKey, sceretKey);
				isSuccess = yuntype.Upload(bucketName, key, file, "", accessKey, sceretKey, 0);
				//bool isSuccess = UpDownload.ResumablePutFile("testapp", key + keyGUID, file);
				if (isSuccess)
				{
					flag++;
					//则将此条记录的上传标识设置为1，
					string localUrl = "/" + key;
					string yunUrl = domain + "/" + key;

						#region	var table; 选取一行
			var table = new __ReUp_table__();
			var __ReUp_table_command__ = new MySql.Data.MySqlClient.MySqlCommand("select * from content where localUrl=?localUrl", __ReUp_contentConnStr_con__);
			var __ReUp_table_parameters__=new MySql.Data.MySqlClient.MySqlParameter[1];
			__ReUp_table_parameters__[0] = new MySql.Data.MySqlClient.MySqlParameter("?localUrl",MySql.Data.MySqlClient.MySqlDbType.VarChar,200);
			__ReUp_table_parameters__[0].Value = localUrl;
			__ReUp_table_command__.Parameters.AddRange(__ReUp_table_parameters__);
			var __ReUp_table_reader__= __ReUp_table_command__.ExecuteReader();
			if (__ReUp_table_reader__.HasRows)
			{
				__ReUp_table_reader__.Read();
				table = new __ReUp_table__();
				if(!__ReUp_table_reader__.IsDBNull(0)){table.id = __ReUp_table_reader__.GetInt32(0);}
				if(!__ReUp_table_reader__.IsDBNull(1)){table.module = __ReUp_table_reader__.GetString(1);}
				if(!__ReUp_table_reader__.IsDBNull(2)){table.yun = __ReUp_table_reader__.GetString(2);}
				if(!__ReUp_table_reader__.IsDBNull(3)){table.domain = __ReUp_table_reader__.GetString(3);}
				if(!__ReUp_table_reader__.IsDBNull(4)){table.fileType = __ReUp_table_reader__.GetString(4);}
				if(!__ReUp_table_reader__.IsDBNull(5)){table.isSuccess = __ReUp_table_reader__.GetBoolean(5);}
				if(!__ReUp_table_reader__.IsDBNull(6)){table.isDeleted = __ReUp_table_reader__.GetBoolean(6);}
				if(!__ReUp_table_reader__.IsDBNull(7)){table.localUrl = __ReUp_table_reader__.GetString(7);}
				if(!__ReUp_table_reader__.IsDBNull(8)){table.yunUrl = __ReUp_table_reader__.GetString(8);}
				if(!__ReUp_table_reader__.IsDBNull(9)){table.keyStr = __ReUp_table_reader__.GetString(9);}
				if(!__ReUp_table_reader__.IsDBNull(10)){table.file = __ReUp_table_reader__.GetString(10);}
				if(!__ReUp_table_reader__.IsDBNull(11)){table.bucketName = __ReUp_table_reader__.GetString(11);}
				if(!__ReUp_table_reader__.IsDBNull(12)){table.accessId = __ReUp_table_reader__.GetString(12);}
				if(!__ReUp_table_reader__.IsDBNull(13)){table.accessKey = __ReUp_table_reader__.GetString(13);}
				if(!__ReUp_table_reader__.IsDBNull(14)){table.sceretKey = __ReUp_table_reader__.GetString(14);}
			}
			else
			{
				table = null;
			}
			__ReUp_table_reader__.Dispose();
			__ReUp_table_command__.Dispose();
	#endregion
			

					//List<Models> userList = Models.contentConnStr.QueryRow("select * from content where localUrl=@localUrl");

						#region	var updateCount; 更新表,并返回受影响行数
			var __ReUp_updateCount_command__ = new MySql.Data.MySqlClient.MySqlCommand("update content set module=?module,yun=2,domain=?domain,fileType=?filetype,isSuccess=1,isDeleted=1,localUrl=?localUrl,yunUrl=?yunUrl,keyStr=?key,file=?file,bucketName=?bucketName,accessId=?accessId,accessKey=?accessKey,sceretKey=?sceretKey where id=?id;", __ReUp_contentConnStr_con__);
			var __ReUp_updateCount_parameters__=new MySql.Data.MySqlClient.MySqlParameter[12];
					__ReUp_updateCount_parameters__[0] = new MySql.Data.MySqlClient.MySqlParameter("?module",MySql.Data.MySqlClient.MySqlDbType.VarChar,20);
					__ReUp_updateCount_parameters__[0].Value = module;
					__ReUp_updateCount_parameters__[1] = new MySql.Data.MySqlClient.MySqlParameter("?domain",MySql.Data.MySqlClient.MySqlDbType.VarChar,200);
					__ReUp_updateCount_parameters__[1].Value = domain;
					__ReUp_updateCount_parameters__[2] = new MySql.Data.MySqlClient.MySqlParameter("?filetype",MySql.Data.MySqlClient.MySqlDbType.VarChar,20);
					__ReUp_updateCount_parameters__[2].Value = filetype;
					__ReUp_updateCount_parameters__[3] = new MySql.Data.MySqlClient.MySqlParameter("?localUrl",MySql.Data.MySqlClient.MySqlDbType.VarChar,200);
					__ReUp_updateCount_parameters__[3].Value = localUrl;
					__ReUp_updateCount_parameters__[4] = new MySql.Data.MySqlClient.MySqlParameter("?yunUrl",MySql.Data.MySqlClient.MySqlDbType.VarChar,200);
					__ReUp_updateCount_parameters__[4].Value = yunUrl;
					__ReUp_updateCount_parameters__[5] = new MySql.Data.MySqlClient.MySqlParameter("?key",MySql.Data.MySqlClient.MySqlDbType.VarChar,200);
					__ReUp_updateCount_parameters__[5].Value = key;
					__ReUp_updateCount_parameters__[6] = new MySql.Data.MySqlClient.MySqlParameter("?file",MySql.Data.MySqlClient.MySqlDbType.VarChar,200);
					__ReUp_updateCount_parameters__[6].Value = file;
					__ReUp_updateCount_parameters__[7] = new MySql.Data.MySqlClient.MySqlParameter("?bucketName",MySql.Data.MySqlClient.MySqlDbType.VarChar,200);
					__ReUp_updateCount_parameters__[7].Value = bucketName;
					__ReUp_updateCount_parameters__[8] = new MySql.Data.MySqlClient.MySqlParameter("?accessId",MySql.Data.MySqlClient.MySqlDbType.VarChar,200);
					__ReUp_updateCount_parameters__[8].Value = accessId;
					__ReUp_updateCount_parameters__[9] = new MySql.Data.MySqlClient.MySqlParameter("?accessKey",MySql.Data.MySqlClient.MySqlDbType.VarChar,200);
					__ReUp_updateCount_parameters__[9].Value = accessKey;
					__ReUp_updateCount_parameters__[10] = new MySql.Data.MySqlClient.MySqlParameter("?sceretKey",MySql.Data.MySqlClient.MySqlDbType.VarChar,200);
					__ReUp_updateCount_parameters__[10].Value = sceretKey;
					__ReUp_updateCount_parameters__[11] = new MySql.Data.MySqlClient.MySqlParameter("?id",MySql.Data.MySqlClient.MySqlDbType.Int32);
					__ReUp_updateCount_parameters__[11].Value = id;
			__ReUp_updateCount_command__.Parameters.AddRange(__ReUp_updateCount_parameters__);
			var updateCount = __ReUp_updateCount_command__.ExecuteNonQuery();
			__ReUp_updateCount_command__.Dispose();
	#endregion
			
					AjaxReturn(1, "");
				}
				else if (!isSuccess)
				{
					//上传失败，也将此条记录记录到数据库中，-1
					string localUrl = "/" + key;

						#region	var table; 选取一行
			var table = new __ReUp_table__();
			var __ReUp_table_command__ = new MySql.Data.MySqlClient.MySqlCommand("select * from content where localUrl=?localUrl", __ReUp_contentConnStr_con__);
			var __ReUp_table_parameters__=new MySql.Data.MySqlClient.MySqlParameter[1];
			__ReUp_table_parameters__[0] = new MySql.Data.MySqlClient.MySqlParameter("?localUrl",MySql.Data.MySqlClient.MySqlDbType.VarChar,200);
			__ReUp_table_parameters__[0].Value = localUrl;
			__ReUp_table_command__.Parameters.AddRange(__ReUp_table_parameters__);
			var __ReUp_table_reader__= __ReUp_table_command__.ExecuteReader();
			if (__ReUp_table_reader__.HasRows)
			{
				__ReUp_table_reader__.Read();
				table = new __ReUp_table__();
				if(!__ReUp_table_reader__.IsDBNull(0)){table.id = __ReUp_table_reader__.GetInt32(0);}
				if(!__ReUp_table_reader__.IsDBNull(1)){table.module = __ReUp_table_reader__.GetString(1);}
				if(!__ReUp_table_reader__.IsDBNull(2)){table.yun = __ReUp_table_reader__.GetString(2);}
				if(!__ReUp_table_reader__.IsDBNull(3)){table.domain = __ReUp_table_reader__.GetString(3);}
				if(!__ReUp_table_reader__.IsDBNull(4)){table.fileType = __ReUp_table_reader__.GetString(4);}
				if(!__ReUp_table_reader__.IsDBNull(5)){table.isSuccess = __ReUp_table_reader__.GetBoolean(5);}
				if(!__ReUp_table_reader__.IsDBNull(6)){table.isDeleted = __ReUp_table_reader__.GetBoolean(6);}
				if(!__ReUp_table_reader__.IsDBNull(7)){table.localUrl = __ReUp_table_reader__.GetString(7);}
				if(!__ReUp_table_reader__.IsDBNull(8)){table.yunUrl = __ReUp_table_reader__.GetString(8);}
				if(!__ReUp_table_reader__.IsDBNull(9)){table.keyStr = __ReUp_table_reader__.GetString(9);}
				if(!__ReUp_table_reader__.IsDBNull(10)){table.file = __ReUp_table_reader__.GetString(10);}
				if(!__ReUp_table_reader__.IsDBNull(11)){table.bucketName = __ReUp_table_reader__.GetString(11);}
				if(!__ReUp_table_reader__.IsDBNull(12)){table.accessId = __ReUp_table_reader__.GetString(12);}
				if(!__ReUp_table_reader__.IsDBNull(13)){table.accessKey = __ReUp_table_reader__.GetString(13);}
				if(!__ReUp_table_reader__.IsDBNull(14)){table.sceretKey = __ReUp_table_reader__.GetString(14);}
			}
			else
			{
				table = null;
			}
			__ReUp_table_reader__.Dispose();
			__ReUp_table_command__.Dispose();
	#endregion
			
					string yunUrl = "";
					
						#region	var updateCount; 更新表,并返回受影响行数
			var __ReUp_updateCount_command__ = new MySql.Data.MySqlClient.MySqlCommand("update content set module=?module,yun=2,domain=?domain,fileType=?filetype,isSuccess=-1,isDeleted=1,localUrl=?localUrl,keyStr=?key,file=?file,bucketName=?bucketName,accessId=?accessId,accessKey=?accessKey,sceretKey=?sceretKey where id=?id;", __ReUp_contentConnStr_con__);
			var __ReUp_updateCount_parameters__=new MySql.Data.MySqlClient.MySqlParameter[11];
					__ReUp_updateCount_parameters__[0] = new MySql.Data.MySqlClient.MySqlParameter("?module",MySql.Data.MySqlClient.MySqlDbType.VarChar,20);
					__ReUp_updateCount_parameters__[0].Value = module;
					__ReUp_updateCount_parameters__[1] = new MySql.Data.MySqlClient.MySqlParameter("?domain",MySql.Data.MySqlClient.MySqlDbType.VarChar,200);
					__ReUp_updateCount_parameters__[1].Value = domain;
					__ReUp_updateCount_parameters__[2] = new MySql.Data.MySqlClient.MySqlParameter("?filetype",MySql.Data.MySqlClient.MySqlDbType.VarChar,20);
					__ReUp_updateCount_parameters__[2].Value = filetype;
					__ReUp_updateCount_parameters__[3] = new MySql.Data.MySqlClient.MySqlParameter("?localUrl",MySql.Data.MySqlClient.MySqlDbType.VarChar,200);
					__ReUp_updateCount_parameters__[3].Value = localUrl;
					__ReUp_updateCount_parameters__[4] = new MySql.Data.MySqlClient.MySqlParameter("?key",MySql.Data.MySqlClient.MySqlDbType.VarChar,200);
					__ReUp_updateCount_parameters__[4].Value = key;
					__ReUp_updateCount_parameters__[5] = new MySql.Data.MySqlClient.MySqlParameter("?file",MySql.Data.MySqlClient.MySqlDbType.VarChar,200);
					__ReUp_updateCount_parameters__[5].Value = file;
					__ReUp_updateCount_parameters__[6] = new MySql.Data.MySqlClient.MySqlParameter("?bucketName",MySql.Data.MySqlClient.MySqlDbType.VarChar,200);
					__ReUp_updateCount_parameters__[6].Value = bucketName;
					__ReUp_updateCount_parameters__[7] = new MySql.Data.MySqlClient.MySqlParameter("?accessId",MySql.Data.MySqlClient.MySqlDbType.VarChar,200);
					__ReUp_updateCount_parameters__[7].Value = accessId;
					__ReUp_updateCount_parameters__[8] = new MySql.Data.MySqlClient.MySqlParameter("?accessKey",MySql.Data.MySqlClient.MySqlDbType.VarChar,200);
					__ReUp_updateCount_parameters__[8].Value = accessKey;
					__ReUp_updateCount_parameters__[9] = new MySql.Data.MySqlClient.MySqlParameter("?sceretKey",MySql.Data.MySqlClient.MySqlDbType.VarChar,200);
					__ReUp_updateCount_parameters__[9].Value = sceretKey;
					__ReUp_updateCount_parameters__[10] = new MySql.Data.MySqlClient.MySqlParameter("?id",MySql.Data.MySqlClient.MySqlDbType.Int32);
					__ReUp_updateCount_parameters__[10].Value = id;
			__ReUp_updateCount_command__.Parameters.AddRange(__ReUp_updateCount_parameters__);
			var updateCount = __ReUp_updateCount_command__.ExecuteNonQuery();
			__ReUp_updateCount_command__.Dispose();
	#endregion
			
					AjaxReturn(0, "");
				}
				else
				{
					//出现未知错误
				}

			}
			__ReUp_contentConnStr_con__.Close();
		}
	}
}