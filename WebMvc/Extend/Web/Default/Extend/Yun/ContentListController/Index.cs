﻿
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using WebMvc.App.Code.Extend.Yun;

namespace WebMvc.App.Web.Default.Extend.Yun.ContentListController
{
	public class IndexAction  : Controller
	{
		public IndexAction(System.IO.TextWriter tw):base(tw){}
		public IndexAction(string fileName) : base(fileName) {}
		public class __Index_logList__:NFinal.DB.Struct
		{
			public System.Int32 id;
			public System.String module;
			public System.String yun;
			public System.String domain;
			public System.String fileType;
			public System.Boolean isSuccess;
			public System.Boolean isDeleted;
			public System.String localUrl;
			public System.String yunUrl;
			public System.String keyStr;
			public System.String file;
			public System.String bucketName;
			public System.String accessId;
			public System.String accessKey;
			public System.String sceretKey;
			#region 写Json字符串
			public override void WriteJson(System.IO.TextWriter tw)
			{
				tw.Write("{");
						tw.Write("\"id\":");
						tw.Write(id.ToString());
						tw.Write(",");
						tw.Write("\"module\":");//
						tw.Write("\"");
						WriteString(module==null?"null":module.ToString(),tw);
						tw.Write("\"");
						tw.Write(",");
						tw.Write("\"yun\":");//
						tw.Write("\"");
						WriteString(yun==null?"null":yun.ToString(),tw);
						tw.Write("\"");
						tw.Write(",");
						tw.Write("\"domain\":");//
						tw.Write("\"");
						WriteString(domain==null?"null":domain.ToString(),tw);
						tw.Write("\"");
						tw.Write(",");
						tw.Write("\"fileType\":");//
						tw.Write("\"");
						WriteString(fileType==null?"null":fileType.ToString(),tw);
						tw.Write("\"");
						tw.Write(",");
						tw.Write("\"isSuccess\":");
						tw.Write(isSuccess?"true":"false");
						tw.Write(",");
						tw.Write("\"isDeleted\":");
						tw.Write(isDeleted?"true":"false");
						tw.Write(",");
						tw.Write("\"localUrl\":");//
						tw.Write("\"");
						WriteString(localUrl==null?"null":localUrl.ToString(),tw);
						tw.Write("\"");
						tw.Write(",");
						tw.Write("\"yunUrl\":");//
						tw.Write("\"");
						WriteString(yunUrl==null?"null":yunUrl.ToString(),tw);
						tw.Write("\"");
						tw.Write(",");
						tw.Write("\"keyStr\":");//
						tw.Write("\"");
						WriteString(keyStr==null?"null":keyStr.ToString(),tw);
						tw.Write("\"");
						tw.Write(",");
						tw.Write("\"file\":");//
						tw.Write("\"");
						WriteString(file==null?"null":file.ToString(),tw);
						tw.Write("\"");
						tw.Write(",");
						tw.Write("\"bucketName\":");//
						tw.Write("\"");
						WriteString(bucketName==null?"null":bucketName.ToString(),tw);
						tw.Write("\"");
						tw.Write(",");
						tw.Write("\"accessId\":");//
						tw.Write("\"");
						WriteString(accessId==null?"null":accessId.ToString(),tw);
						tw.Write("\"");
						tw.Write(",");
						tw.Write("\"accessKey\":");//
						tw.Write("\"");
						WriteString(accessKey==null?"null":accessKey.ToString(),tw);
						tw.Write("\"");
						tw.Write(",");
						tw.Write("\"sceretKey\":");//
						tw.Write("\"");
						WriteString(sceretKey==null?"null":sceretKey.ToString(),tw);
						tw.Write("\"");
				tw.Write("}");
			}
			#endregion
		}
		public void Index()
		{
			var __Index_contentConnStr_con__ = new MySql.Data.MySqlClient.MySqlConnection(Models.ConnectionStrings.contentConnStr);
			__Index_contentConnStr_con__.Open();
				#region	var logList;选取所有记录
			var logList = new NFinal.DB.NList<__Index_logList__>();
			var __Index_logList_command__ = new MySql.Data.MySqlClient.MySqlCommand("select * from content where isDeleted=1", __Index_contentConnStr_con__);
			var __Index_logList_reader__= __Index_logList_command__.ExecuteReader();
			if (__Index_logList_reader__.HasRows)
			{
				while (__Index_logList_reader__.Read())
				{
					var __Index_logList_temp__ = new __Index_logList__();
					if(!__Index_logList_reader__.IsDBNull(0)){__Index_logList_temp__.id = __Index_logList_reader__.GetInt32(0);}
					if(!__Index_logList_reader__.IsDBNull(1)){__Index_logList_temp__.module = __Index_logList_reader__.GetString(1);}
					if(!__Index_logList_reader__.IsDBNull(2)){__Index_logList_temp__.yun = __Index_logList_reader__.GetString(2);}
					if(!__Index_logList_reader__.IsDBNull(3)){__Index_logList_temp__.domain = __Index_logList_reader__.GetString(3);}
					if(!__Index_logList_reader__.IsDBNull(4)){__Index_logList_temp__.fileType = __Index_logList_reader__.GetString(4);}
					if(!__Index_logList_reader__.IsDBNull(5)){__Index_logList_temp__.isSuccess = __Index_logList_reader__.GetBoolean(5);}
					if(!__Index_logList_reader__.IsDBNull(6)){__Index_logList_temp__.isDeleted = __Index_logList_reader__.GetBoolean(6);}
					if(!__Index_logList_reader__.IsDBNull(7)){__Index_logList_temp__.localUrl = __Index_logList_reader__.GetString(7);}
					if(!__Index_logList_reader__.IsDBNull(8)){__Index_logList_temp__.yunUrl = __Index_logList_reader__.GetString(8);}
					if(!__Index_logList_reader__.IsDBNull(9)){__Index_logList_temp__.keyStr = __Index_logList_reader__.GetString(9);}
					if(!__Index_logList_reader__.IsDBNull(10)){__Index_logList_temp__.file = __Index_logList_reader__.GetString(10);}
					if(!__Index_logList_reader__.IsDBNull(11)){__Index_logList_temp__.bucketName = __Index_logList_reader__.GetString(11);}
					if(!__Index_logList_reader__.IsDBNull(12)){__Index_logList_temp__.accessId = __Index_logList_reader__.GetString(12);}
					if(!__Index_logList_reader__.IsDBNull(13)){__Index_logList_temp__.accessKey = __Index_logList_reader__.GetString(13);}
					if(!__Index_logList_reader__.IsDBNull(14)){__Index_logList_temp__.sceretKey = __Index_logList_reader__.GetString(14);}
					logList.Add(__Index_logList_temp__);
				}
			}
			__Index_logList_reader__.Dispose();
			__Index_logList_command__.Dispose();
	#endregion
			
			__Index_contentConnStr_con__.Close();
			
			Write("\r\n<!DOCTYPE html>\r\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\r\n<head runat=\"server\">\r\n    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />\r\n    <title></title>\r\n    <script src=\"http://aliyun/ContentApp/Content/js/jquery-1.11.2.min.js\" ></script>\r\n</head>\r\n<body>\r\n    <div>\r\n        <table>\r\n            <tr>\r\n                <td>module</td>\r\n                <td>domain</td>\r\n                <td>yun</td>\r\n                <td>fileType</td>\r\n                <td>isSuccess</td>\r\n                <td>localUrl</td>\r\n                <td>yunUrl</td>\r\n                <td>key</td>\r\n                <td>file</td>\r\n                <td>bucketName</td>\r\n                <td>accessId</td>\r\n                <td>accessKey</td>\r\n                <td>sceretKey</td>\r\n            </tr>\r\n");
			var c=logList.GetEnumerator(); while(c.MoveNext()){
				Write("\r\n            <tr>\r\n                <td>");
				Write(c.Current.module);
				Write("</td>\r\n                <td>");
				Write(c.Current.domain);
				Write("</td>\r\n");
				if(c.Current.yun=="1" ){
					Write("\r\n                    <td>阿里云</td>\r\n                <elseif condition=\"");
					Write(c.Current.yun=="2");
					Write("\" /> \r\n                    <td>七牛云</td>\r\n");
				}else{
					Write("\r\n                    <td>未知</td>\r\n");
				}
				Write("\r\n                <td>");
				Write(c.Current.fileType);
				Write("</td>\r\n");
				if(c.Current.isSuccess){
					Write("\r\n                    <td>上传成功</td>\r\n");
				}else{
					Write("\r\n                    <td><a id=\"upFalse\" paramid=\"");
					Write(c.Current.id);
					Write("\" paramkey=\"");
					Write(c.Current.keyStr);
					Write("\" parammodule=\"");
					Write(c.Current.module);
					Write("\" paramfile=\"");
					Write(c.Current.file);
					Write("\" paramfiletype=\"");
					Write(c.Current.fileType);
					Write("\" paramyun=\"");
					Write(c.Current.yun);
					Write("\" paramdomain=\"");
					Write(c.Current.domain);
					Write("\" parambucketName=\"");
					Write(c.Current.bucketName);
					Write("\" paramaccessId=\"");
					Write(c.Current.accessId);
					Write("\" paramaccessKey=\"");
					Write(c.Current.accessKey);
					Write("\" paramsecretKey=\"");
					Write(c.Current.sceretKey);
					Write("\">上传失败，重新上传</a></td>\r\n");
				}
				Write("\r\n                <td>");
				Write(c.Current.localUrl);
				Write("</td>\r\n                <td>");
				Write(c.Current.yunUrl);
				Write("</td>\r\n                <td>");
				Write(c.Current.keyStr);
				Write("</td>\r\n                <td>");
				Write(c.Current.file);
				Write("</td>\r\n                <td>");
				Write(c.Current.bucketName);
				Write("</td>\r\n                <td>");
				Write(c.Current.accessId);
				Write("</td>\r\n                <td>");
				Write(c.Current.accessKey);
				Write("</td>\r\n                <td>");
				Write(c.Current.sceretKey);
				Write("</td>\r\n            </tr> \r\n");
			}
			Write("\r\n        </table>\r\n    </div>\r\n    <script type=\"text/javascript\">\r\n        $(\"#upFalse\").click(function () {\r\n            ReUpload($(this).attr(\"paramid\"), $(this).attr(\"paramkey\"), $(this).attr(\"parammodule\"), $(this).attr(\"paramfile\"), $(this).attr(\"paramfiletype\"), $(this).attr(\"paramyun\"), $(this), $(this).attr(\"paramdomain\"), $(this).attr(\"parambucketName\"), $(this).attr(\"paramaccessId\"), $(this).attr(\"paramaccessKey\"), $(this).attr(\"paramsecretKey\"));\r\n        });\r\n        function ReUpload(id, key, module, file, filetype, yun, control, domain, bucketName, accessId, accessKey, sceretKey) {\r\n            $.post(\"/ContentApp/ContentListController/ReUp.htm\",\r\n                {\r\n                    \"id\": id,\r\n                    \"key\": key,\r\n                    \"module\": module,\r\n                    \"file\": file,\r\n                    \"filetype\": filetype,\r\n                    \"yun\": yun,\r\n                    \"domain\": domain,\r\n                    \"bucketName\": bucketName,\r\n                    \"accessId\": accessId,\r\n                    \"accessKey\": accessKey,\r\n                    \"sceretKey\": sceretKey\r\n                },\r\n                function (data) {\r\n                    if (data.code == 1) {\r\n                        alert(\"上传成功\");\r\n                        window.location.reload();\r\n                    } else {\r\n                        alert(\"上传失败\");\r\n                    }\r\n                });\r\n        } </script>\r\n</body>\r\n</html>");
		}

		
	}
}