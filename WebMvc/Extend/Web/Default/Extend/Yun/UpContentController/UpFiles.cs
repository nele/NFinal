﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Web.UI;
using System.Data;
using NFinal;

namespace WebMvc.App.Web.Default.Extend.Yun.UpContentController
{
    public class UpFilesAction  : Controller
	{
		public UpFilesAction(System.IO.TextWriter tw):base(tw){}
		public UpFilesAction(string fileName) : base(fileName) {}
        

        public void UpFiles(string accessKey,string sceretKey,string bucketName,string chooseYun,string accessId,string domain,string module)
        {
            string path = _context.Server.MapPath(_app + "/Content");
            Models.DAL.Extend.Yun.UpCommon dalUpCommon = new Models.DAL.Extend.Yun.UpCommon();
            bool f = dalUpCommon.UpToYun(path, accessKey, sceretKey, bucketName.ToLower(), chooseYun, accessId, domain, module);
            if (f)
            {
                AjaxReturn(1, "");
            }
            else
            {
                AjaxReturn(0, "");
            }
        }
    }
}