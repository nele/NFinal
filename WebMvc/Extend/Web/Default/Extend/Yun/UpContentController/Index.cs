﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Web.UI;
using System.Data;
using NFinal;

namespace WebMvc.App.Web.Default.Extend.Yun.UpContentController
{
    public class IndexAction  : Controller
	{
		public IndexAction(System.IO.TextWriter tw):base(tw){}
		public IndexAction(string fileName) : base(fileName) {}
        public void Index()
        {
            
			Write("\r\n\r\n<!DOCTYPE html>\r\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\r\n<head runat=\"server\">\r\n    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />\r\n    <title>上传</title>\r\n    <script src=\"/App/Content/js/jquery-1.11.2.min.js\"></script>\r\n    <script src=\"/Url.js\"></script>\r\n    <script type=\"text/javascript\">\r\n        $(function () {\r\n            $(\"#aLi\").css(\"display\", \"block\");\r\n            $(\"#qiNiu\").css(\"display\", \"none\");\r\n\r\n            $(\"#toUp\").click(function () {\r\n                $.post(Url.App_UpContentController_UpFiles(), $(\"#form1\").serialize(), function (data) {\r\n                    if (data.code == 1) {\r\n                        alert(\"上传成功\");\r\n                    }\r\n                    else {\r\n                        alert(\"上传失败\");\r\n                        window.location.href = Url.App_ContentListController_Index();\r\n                    }\r\n                });\r\n            });\r\n        });\r\n        function ChooseYun() {\r\n            var yun = $(\"#chooseYun option:selected\").val();\r\n            if (yun == \"1\") {\r\n                $(\"#aLi\").css(\"display\", \"block\");\r\n                $(\"#qiNiu\").css(\"display\", \"none\");\r\n            }\r\n            else {\r\n                $(\"#aLi\").css(\"display\", \"none\");\r\n                $(\"#qiNiu\").css(\"display\", \"block\");\r\n            }\r\n        }\r\n    </script>\r\n</head>\r\n<body>\r\n    <form id=\"form1\">\r\n        <div>\r\n            chooseYun:<br />\r\n            <select id=\"chooseYun\" name=\"chooseYun\" style=\"width: 150px\" onchange=\"ChooseYun()\">\r\n                <option value=\"1\">阿里云</option>\r\n                <option value=\"2\">七牛云</option>\r\n            </select><br />\r\n            <br />\r\n            module:<br />\r\n            <input type=\"text\" name=\"module\" /><br />\r\n            <br />\r\n            domain:<br />\r\n            <input type=\"text\" name=\"domain\" /><br />\r\n            <br />\r\n            bucket:<br />\r\n            <input type=\"text\" name=\"bucketName\" /><br />\r\n            <br />\r\n            accessKey:<br />\r\n            <input type=\"text\" name=\"accessKey\" /><br />\r\n            <br />\r\n            <div id=\"qiNiu\">\r\n                sceretKey:<br />\r\n                <input type=\"text\" name=\"sceretKey\" /><br />\r\n                <br />\r\n            </div>\r\n            <div id=\"aLi\">\r\n                accessId:<br />\r\n                <input type=\"text\" name=\"accessId\" /><br />\r\n                <br />\r\n            </div>\r\n\r\n            <input type=\"button\" value=\"上传\" id=\"toUp\" />\r\n        </div>\r\n    </form>\r\n</body>\r\n</html>\r\n");
        }

        
    }
}