﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Index.aspx.cs" Inherits="WebMvc.App.Views.Default.Extend.Yun.UpContentController.Index" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>上传</title>
    <script src="/App/Content/js/jquery-1.11.2.min.js"></script>
    <script src="/Url.js"></script>
    <script type="text/javascript">
        $(function () {
            $("#aLi").css("display", "block");
            $("#qiNiu").css("display", "none");

            $("#toUp").click(function () {
                $.post(Url.App_UpContentController_UpFiles(), $("#form1").serialize(), function (data) {
                    if (data.code == 1) {
                        alert("上传成功");
                    }
                    else {
                        alert("上传失败");
                        window.location.href = Url.App_ContentListController_Index();
                    }
                });
            });
        });
        function ChooseYun() {
            var yun = $("#chooseYun option:selected").val();
            if (yun == "1") {
                $("#aLi").css("display", "block");
                $("#qiNiu").css("display", "none");
            }
            else {
                $("#aLi").css("display", "none");
                $("#qiNiu").css("display", "block");
            }
        }
    </script>
</head>
<body>
    <form id="form1">
        <div>
            chooseYun:<br />
            <select id="chooseYun" name="chooseYun" style="width: 150px" onchange="ChooseYun()">
                <option value="1">阿里云</option>
                <option value="2">七牛云</option>
            </select><br />
            <br />
            module:<br />
            <input type="text" name="module" /><br />
            <br />
            domain:<br />
            <input type="text" name="domain" /><br />
            <br />
            bucket:<br />
            <input type="text" name="bucketName" /><br />
            <br />
            accessKey:<br />
            <input type="text" name="accessKey" /><br />
            <br />
            <div id="qiNiu">
                sceretKey:<br />
                <input type="text" name="sceretKey" /><br />
                <br />
            </div>
            <div id="aLi">
                accessId:<br />
                <input type="text" name="accessId" /><br />
                <br />
            </div>

            <input type="button" value="上传" id="toUp" />
        </div>
    </form>
</body>
</html>
