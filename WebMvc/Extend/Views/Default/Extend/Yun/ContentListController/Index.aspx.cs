﻿﻿//======================================================================
//
//        Copyright : Zhengzhou Strawberry Computer Technology Co.,LTD.
//        All rights reserved
//        
//        Application:NFinal MVC framework
//        Filename :{Index}.cs
//        Description :模板提示类
//
//        created by Lucas at  2015-6-30`
//     
//        WebSite:http://www.nfinal.com
//
//======================================================================
using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
namespace WebMvc.App.Views.Default.Extend.Yun.ContentListController
{
    public partial class Index : System.Web.UI.Page
    {
		//数据库模型
        public class __Index_logList__:NFinal.DB.Struct
		{
			public System.Int32 id;
			public System.String module;
			public System.String yun;
			public System.String domain;
			public System.String fileType;
			public System.Boolean isSuccess;
			public System.Boolean isDeleted;
			public System.String localUrl;
			public System.String yunUrl;
			public System.String keyStr;
			public System.String file;
			public System.String bucketName;
			public System.String accessId;
			public System.String accessKey;
			public System.String sceretKey;
			#region 写Json字符串
			public override void WriteJson(System.IO.TextWriter tw)
			{
				tw.Write("{");
						tw.Write("\"id\":");
						tw.Write(id.ToString());
						tw.Write(",");
						tw.Write("\"module\":");//
						tw.Write("\"");
						WriteString(module==null?"null":module.ToString(),tw);
						tw.Write("\"");
						tw.Write(",");
						tw.Write("\"yun\":");//
						tw.Write("\"");
						WriteString(yun==null?"null":yun.ToString(),tw);
						tw.Write("\"");
						tw.Write(",");
						tw.Write("\"domain\":");//
						tw.Write("\"");
						WriteString(domain==null?"null":domain.ToString(),tw);
						tw.Write("\"");
						tw.Write(",");
						tw.Write("\"fileType\":");//
						tw.Write("\"");
						WriteString(fileType==null?"null":fileType.ToString(),tw);
						tw.Write("\"");
						tw.Write(",");
						tw.Write("\"isSuccess\":");
						tw.Write(isSuccess?"true":"false");
						tw.Write(",");
						tw.Write("\"isDeleted\":");
						tw.Write(isDeleted?"true":"false");
						tw.Write(",");
						tw.Write("\"localUrl\":");//
						tw.Write("\"");
						WriteString(localUrl==null?"null":localUrl.ToString(),tw);
						tw.Write("\"");
						tw.Write(",");
						tw.Write("\"yunUrl\":");//
						tw.Write("\"");
						WriteString(yunUrl==null?"null":yunUrl.ToString(),tw);
						tw.Write("\"");
						tw.Write(",");
						tw.Write("\"keyStr\":");//
						tw.Write("\"");
						WriteString(keyStr==null?"null":keyStr.ToString(),tw);
						tw.Write("\"");
						tw.Write(",");
						tw.Write("\"file\":");//
						tw.Write("\"");
						WriteString(file==null?"null":file.ToString(),tw);
						tw.Write("\"");
						tw.Write(",");
						tw.Write("\"bucketName\":");//
						tw.Write("\"");
						WriteString(bucketName==null?"null":bucketName.ToString(),tw);
						tw.Write("\"");
						tw.Write(",");
						tw.Write("\"accessId\":");//
						tw.Write("\"");
						WriteString(accessId==null?"null":accessId.ToString(),tw);
						tw.Write("\"");
						tw.Write(",");
						tw.Write("\"accessKey\":");//
						tw.Write("\"");
						WriteString(accessKey==null?"null":accessKey.ToString(),tw);
						tw.Write("\"");
						tw.Write(",");
						tw.Write("\"sceretKey\":");//
						tw.Write("\"");
						WriteString(sceretKey==null?"null":sceretKey.ToString(),tw);
						tw.Write("\"");
				tw.Write("}");
			}
			#endregion
		}
		//函数内变量
        public class Index_AutoComplete:Controller
        {
			//参数变量
			//数据库变量
						public NFinal.DB.NList<__Index_logList__> logList;
			//一般变量
			//DAL函数声明变量
        }
		//变量存储类,用于自动完成.
        public Index_AutoComplete ViewBag = new Index_AutoComplete();
    }
}