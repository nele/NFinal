﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Index.aspx.cs" Inherits="WebMvc.App.Views.Default.Extend.Yun.ContentListController.Index" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
    <script src="http://aliyun/ContentApp/Content/js/jquery-1.11.2.min.js"></script>
</head>
<body>
    <div>
        <table>
            <tr>
                <td>module</td>
                <td>domain</td>
                <td>yun</td>
                <td>fileType</td>
                <td>isSuccess</td>
                <td>localUrl</td>
                <td>yunUrl</td>
                <td>key</td>
                <td>file</td>
                <td>bucketName</td>
                <td>accessId</td>
                <td>accessKey</td>
                <td>sceretKey</td>
            </tr>
            <foreach enumerator="<%var c = ViewBag.logList.GetEnumerator();%>">
                <tr>
                    <td><%=c.Current.module%></td>
                    <td><%=c.Current.domain%></td>
                    <if condition="<%=c.Current.yun=="1" %>">
                        <td>阿里云</td>
                        <elseif condition="<%=c.Current.yun=="2" %>" />
                        <td>七牛云</td>
                        <else />
                        <td>未知</td>
                    </if>
                    <td><%=c.Current.fileType%></td>
                    <if condition="<%=c.Current.isSuccess%>">
                        <td>上传成功</td>
                        <else />
                        <td><a id="upFalse" paramid="<%=c.Current.id%>" paramkey="<%=c.Current.keyStr%>" parammodule="<%=c.Current.module%>" paramfile="<%=c.Current.file%>" paramfiletype="<%=c.Current.fileType%>" paramyun="<%=c.Current.yun%>" paramdomain="<%=c.Current.domain%>" parambucketname="<%=c.Current.bucketName%>" paramaccessid="<%=c.Current.accessId%>" paramaccesskey="<%=c.Current.accessKey%>" paramsecretkey="<%=c.Current.sceretKey%>">上传失败，重新上传</a></td>
                    </if>
                    <td><%=c.Current.localUrl%></td>
                    <td><%=c.Current.yunUrl%></td>
                    <td><%=c.Current.keyStr%></td>
                    <td><%=c.Current.file%></td>
                    <td><%=c.Current.bucketName%></td>
                    <td><%=c.Current.accessId%></td>
                    <td><%=c.Current.accessKey%></td>
                    <td><%=c.Current.sceretKey%></td>
                </tr>
            </foreach>
        </table>
    </div>
    <script type="text/javascript">
        $("#upFalse").click(function () {
            ReUpload($(this).attr("paramid"), $(this).attr("paramkey"), $(this).attr("parammodule"), $(this).attr("paramfile"), $(this).attr("paramfiletype"), $(this).attr("paramyun"), $(this), $(this).attr("paramdomain"), $(this).attr("parambucketName"), $(this).attr("paramaccessId"), $(this).attr("paramaccessKey"), $(this).attr("paramsecretKey"));
        });
        function ReUpload(id, key, module, file, filetype, yun, control, domain, bucketName, accessId, accessKey, sceretKey) {
            $.post("/ContentApp/ContentListController/ReUp.htm",
                {
                    "id": id,
                    "key": key,
                    "module": module,
                    "file": file,
                    "filetype": filetype,
                    "yun": yun,
                    "domain": domain,
                    "bucketName": bucketName,
                    "accessId": accessId,
                    "accessKey": accessKey,
                    "sceretKey": sceretKey
                },
                function (data) {
                    if (data.code == 1) {
                        alert("上传成功");
                        window.location.reload();
                    } else {
                        alert("上传失败");
                    }
                });
        } </script>
</body>
</html>
