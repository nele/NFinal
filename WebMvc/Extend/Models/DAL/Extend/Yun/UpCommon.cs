﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using WebMvc.App.Code.Extend.Yun;

namespace WebMvc.App.Models.DAL.Extend.Yun
{
	public class UpCommon
	{
		public class __UpToYun_table__:NFinal.DB.Struct
		{
			public System.Int32 id;
			public System.String module;
			public System.String yun;
			public System.String domain;
			public System.String fileType;
			public System.Boolean isSuccess;
			public System.Boolean isDeleted;
			public System.String localUrl;
			public System.String yunUrl;
			public System.String keyStr;
			public System.String file;
			public System.String bucketName;
			public System.String accessId;
			public System.String accessKey;
			public System.String sceretKey;
			#region 写Json字符串
			public override void WriteJson(System.IO.TextWriter tw)
			{
				tw.Write("{");
						tw.Write("\"id\":");
						tw.Write(id.ToString());
						tw.Write(",");
						tw.Write("\"module\":");//
						tw.Write("\"");
						WriteString(module==null?"null":module.ToString(),tw);
						tw.Write("\"");
						tw.Write(",");
						tw.Write("\"yun\":");//
						tw.Write("\"");
						WriteString(yun==null?"null":yun.ToString(),tw);
						tw.Write("\"");
						tw.Write(",");
						tw.Write("\"domain\":");//
						tw.Write("\"");
						WriteString(domain==null?"null":domain.ToString(),tw);
						tw.Write("\"");
						tw.Write(",");
						tw.Write("\"fileType\":");//
						tw.Write("\"");
						WriteString(fileType==null?"null":fileType.ToString(),tw);
						tw.Write("\"");
						tw.Write(",");
						tw.Write("\"isSuccess\":");
						tw.Write(isSuccess?"true":"false");
						tw.Write(",");
						tw.Write("\"isDeleted\":");
						tw.Write(isDeleted?"true":"false");
						tw.Write(",");
						tw.Write("\"localUrl\":");//
						tw.Write("\"");
						WriteString(localUrl==null?"null":localUrl.ToString(),tw);
						tw.Write("\"");
						tw.Write(",");
						tw.Write("\"yunUrl\":");//
						tw.Write("\"");
						WriteString(yunUrl==null?"null":yunUrl.ToString(),tw);
						tw.Write("\"");
						tw.Write(",");
						tw.Write("\"keyStr\":");//
						tw.Write("\"");
						WriteString(keyStr==null?"null":keyStr.ToString(),tw);
						tw.Write("\"");
						tw.Write(",");
						tw.Write("\"file\":");//
						tw.Write("\"");
						WriteString(file==null?"null":file.ToString(),tw);
						tw.Write("\"");
						tw.Write(",");
						tw.Write("\"bucketName\":");//
						tw.Write("\"");
						WriteString(bucketName==null?"null":bucketName.ToString(),tw);
						tw.Write("\"");
						tw.Write(",");
						tw.Write("\"accessId\":");//
						tw.Write("\"");
						WriteString(accessId==null?"null":accessId.ToString(),tw);
						tw.Write("\"");
						tw.Write(",");
						tw.Write("\"accessKey\":");//
						tw.Write("\"");
						WriteString(accessKey==null?"null":accessKey.ToString(),tw);
						tw.Write("\"");
						tw.Write(",");
						tw.Write("\"sceretKey\":");//
						tw.Write("\"");
						WriteString(sceretKey==null?"null":sceretKey.ToString(),tw);
						tw.Write("\"");
				tw.Write("}");
			}
			#endregion
		}
		public bool UpToYun(string path, string accessKey, string sceretKey, string bucketName, string chooseYun, string accessId, string domain, string module)
		{
			string[] filePaths = Directory.GetDirectories(path);
            var __UpToYun_contentConnStr_con__ = new MySql.Data.MySqlClient.MySqlConnection(Models.ConnectionStrings.contentConnStr);
			__UpToYun_contentConnStr_con__.Open();
            foreach (string filePath in filePaths)
			{
				string[] ffPath = Directory.GetDirectories(filePath);

				string[] ffFiles = Directory.GetFiles(filePath);

				if (ffPath.Length <= 0)
				{
					string[] files = Directory.GetFiles(filePath);
					foreach (string file in files)
					{
						string fileName = Path.GetFileName(file);//11.css
						string strFileType = string.Empty;
						foreach (string preFloder in file.Substring(filePath.IndexOf(module + "\\")).Split('\\'))
						{
							strFileType = strFileType + "/" + preFloder;
						}
						//string fileType = filePath.Split('\\')[filePath.Split('\\').Length - 1];
						string key = strFileType.TrimStart('/');

						string keyGUID = "/" + Guid.NewGuid().ToString();

						bool isSuccess = false;
						
						if (chooseYun == "1")//阿里云
						{
							IUploadable yun = Base.GetYun(yunType.ALi);
							yun.Initial(bucketName, key, file, accessId, accessKey, "");
							isSuccess = yun.Upload(bucketName, key, file, accessId, accessKey, "",2 * 1024 * 1024);
							//bool isSuccess = UpDownload.ResumablePutFile("testapp", key + keyGUID, file);
							if (isSuccess)
							{
								//则将此条记录的上传标识设置为1，
								string localUrl = "/" + key;
								string yunUrl = domain + "/" + key;

									#region	var table; 选取一行
			var table = new __UpToYun_table__();
			var __UpToYun_table_command__ = new MySql.Data.MySqlClient.MySqlCommand("select * from content where localUrl=?localUrl and yun=1", __UpToYun_contentConnStr_con__);
			var __UpToYun_table_parameters__=new MySql.Data.MySqlClient.MySqlParameter[1];
			__UpToYun_table_parameters__[0] = new MySql.Data.MySqlClient.MySqlParameter("?localUrl",MySql.Data.MySqlClient.MySqlDbType.VarChar,200);
			__UpToYun_table_parameters__[0].Value = localUrl;
			__UpToYun_table_command__.Parameters.AddRange(__UpToYun_table_parameters__);
			var __UpToYun_table_reader__= __UpToYun_table_command__.ExecuteReader();
			if (__UpToYun_table_reader__.HasRows)
			{
				__UpToYun_table_reader__.Read();
				table = new __UpToYun_table__();
				if(!__UpToYun_table_reader__.IsDBNull(0)){table.id = __UpToYun_table_reader__.GetInt32(0);}
				if(!__UpToYun_table_reader__.IsDBNull(1)){table.module = __UpToYun_table_reader__.GetString(1);}
				if(!__UpToYun_table_reader__.IsDBNull(2)){table.yun = __UpToYun_table_reader__.GetString(2);}
				if(!__UpToYun_table_reader__.IsDBNull(3)){table.domain = __UpToYun_table_reader__.GetString(3);}
				if(!__UpToYun_table_reader__.IsDBNull(4)){table.fileType = __UpToYun_table_reader__.GetString(4);}
				if(!__UpToYun_table_reader__.IsDBNull(5)){table.isSuccess = __UpToYun_table_reader__.GetBoolean(5);}
				if(!__UpToYun_table_reader__.IsDBNull(6)){table.isDeleted = __UpToYun_table_reader__.GetBoolean(6);}
				if(!__UpToYun_table_reader__.IsDBNull(7)){table.localUrl = __UpToYun_table_reader__.GetString(7);}
				if(!__UpToYun_table_reader__.IsDBNull(8)){table.yunUrl = __UpToYun_table_reader__.GetString(8);}
				if(!__UpToYun_table_reader__.IsDBNull(9)){table.keyStr = __UpToYun_table_reader__.GetString(9);}
				if(!__UpToYun_table_reader__.IsDBNull(10)){table.file = __UpToYun_table_reader__.GetString(10);}
				if(!__UpToYun_table_reader__.IsDBNull(11)){table.bucketName = __UpToYun_table_reader__.GetString(11);}
				if(!__UpToYun_table_reader__.IsDBNull(12)){table.accessId = __UpToYun_table_reader__.GetString(12);}
				if(!__UpToYun_table_reader__.IsDBNull(13)){table.accessKey = __UpToYun_table_reader__.GetString(13);}
				if(!__UpToYun_table_reader__.IsDBNull(14)){table.sceretKey = __UpToYun_table_reader__.GetString(14);}
			}
			else
			{
				table = null;
			}
			__UpToYun_table_reader__.Dispose();
			__UpToYun_table_command__.Dispose();
	#endregion
			

								if (table.id == 0)
								{
										#region	var insertId; 插入并返回ID
			var __UpToYun_insertId_command__ = new MySql.Data.MySqlClient.MySqlCommand("insert into content(module,yun,domain,fileType,isSuccess,isDeleted,localUrl,yunUrl,keyStr,file,bucketName,accessId,accessKey,sceretKey) values(?module,1,?domain,?strFileType,1,1,?localUrl,?yunUrl,?key,?file,?bucketName,?accessId,?accessKey,?sceretKey);select @@IDENTITY;", __UpToYun_contentConnStr_con__);
			var __UpToYun_insertId_parameters__=new MySql.Data.MySqlClient.MySqlParameter[11];
			__UpToYun_insertId_parameters__[0] = new MySql.Data.MySqlClient.MySqlParameter("?module",MySql.Data.MySqlClient.MySqlDbType.VarChar,20);
			__UpToYun_insertId_parameters__[0].Value = module;
			__UpToYun_insertId_parameters__[1] = new MySql.Data.MySqlClient.MySqlParameter("?domain",MySql.Data.MySqlClient.MySqlDbType.VarChar,200);
			__UpToYun_insertId_parameters__[1].Value = domain;
			__UpToYun_insertId_parameters__[2] = new MySql.Data.MySqlClient.MySqlParameter("?strFileType",MySql.Data.MySqlClient.MySqlDbType.VarChar,20);
			__UpToYun_insertId_parameters__[2].Value = strFileType;
			__UpToYun_insertId_parameters__[3] = new MySql.Data.MySqlClient.MySqlParameter("?localUrl",MySql.Data.MySqlClient.MySqlDbType.VarChar,200);
			__UpToYun_insertId_parameters__[3].Value = localUrl;
			__UpToYun_insertId_parameters__[4] = new MySql.Data.MySqlClient.MySqlParameter("?yunUrl",MySql.Data.MySqlClient.MySqlDbType.VarChar,200);
			__UpToYun_insertId_parameters__[4].Value = yunUrl;
			__UpToYun_insertId_parameters__[5] = new MySql.Data.MySqlClient.MySqlParameter("?key",MySql.Data.MySqlClient.MySqlDbType.VarChar,200);
			__UpToYun_insertId_parameters__[5].Value = key;
			__UpToYun_insertId_parameters__[6] = new MySql.Data.MySqlClient.MySqlParameter("?file",MySql.Data.MySqlClient.MySqlDbType.VarChar,200);
			__UpToYun_insertId_parameters__[6].Value = file;
			__UpToYun_insertId_parameters__[7] = new MySql.Data.MySqlClient.MySqlParameter("?bucketName",MySql.Data.MySqlClient.MySqlDbType.VarChar,200);
			__UpToYun_insertId_parameters__[7].Value = bucketName;
			__UpToYun_insertId_parameters__[8] = new MySql.Data.MySqlClient.MySqlParameter("?accessId",MySql.Data.MySqlClient.MySqlDbType.VarChar,200);
			__UpToYun_insertId_parameters__[8].Value = accessId;
			__UpToYun_insertId_parameters__[9] = new MySql.Data.MySqlClient.MySqlParameter("?accessKey",MySql.Data.MySqlClient.MySqlDbType.VarChar,200);
			__UpToYun_insertId_parameters__[9].Value = accessKey;
			__UpToYun_insertId_parameters__[10] = new MySql.Data.MySqlClient.MySqlParameter("?sceretKey",MySql.Data.MySqlClient.MySqlDbType.VarChar,200);
			__UpToYun_insertId_parameters__[10].Value = sceretKey;
			__UpToYun_insertId_command__.Parameters.AddRange(__UpToYun_insertId_parameters__);
			var insertId = int.Parse(__UpToYun_insertId_command__.ExecuteScalar().ToString());
			__UpToYun_insertId_command__.Dispose();
	#endregion
			
								}
								else
								{
									var id = table.id;// Convert.ToInt32(table.Rows[0]["id"]);
										#region	var updateCount; 更新表,并返回受影响行数
			var __UpToYun_updateCount_command__ = new MySql.Data.MySqlClient.MySqlCommand("update content set module=?module,yun=?chooseYun,domain=?domain,fileType=?strFileType,isSuccess=1,isDeleted=1,localUrl=?localUrl,yunUrl=?yunUrl,keyStr=?key,file=?file,bucketName=?bucketName,accessId=?accessId,accessKey=?accessKey,sceretKey=?sceretKey where id=?id;", __UpToYun_contentConnStr_con__);
			var __UpToYun_updateCount_parameters__=new MySql.Data.MySqlClient.MySqlParameter[13];
					__UpToYun_updateCount_parameters__[0] = new MySql.Data.MySqlClient.MySqlParameter("?module",MySql.Data.MySqlClient.MySqlDbType.VarChar,20);
					__UpToYun_updateCount_parameters__[0].Value = module;
					__UpToYun_updateCount_parameters__[1] = new MySql.Data.MySqlClient.MySqlParameter("?chooseYun",MySql.Data.MySqlClient.MySqlDbType.VarChar,2);
					__UpToYun_updateCount_parameters__[1].Value = chooseYun;
					__UpToYun_updateCount_parameters__[2] = new MySql.Data.MySqlClient.MySqlParameter("?domain",MySql.Data.MySqlClient.MySqlDbType.VarChar,200);
					__UpToYun_updateCount_parameters__[2].Value = domain;
					__UpToYun_updateCount_parameters__[3] = new MySql.Data.MySqlClient.MySqlParameter("?strFileType",MySql.Data.MySqlClient.MySqlDbType.VarChar,20);
					__UpToYun_updateCount_parameters__[3].Value = strFileType;
					__UpToYun_updateCount_parameters__[4] = new MySql.Data.MySqlClient.MySqlParameter("?localUrl",MySql.Data.MySqlClient.MySqlDbType.VarChar,200);
					__UpToYun_updateCount_parameters__[4].Value = localUrl;
					__UpToYun_updateCount_parameters__[5] = new MySql.Data.MySqlClient.MySqlParameter("?yunUrl",MySql.Data.MySqlClient.MySqlDbType.VarChar,200);
					__UpToYun_updateCount_parameters__[5].Value = yunUrl;
					__UpToYun_updateCount_parameters__[6] = new MySql.Data.MySqlClient.MySqlParameter("?key",MySql.Data.MySqlClient.MySqlDbType.VarChar,200);
					__UpToYun_updateCount_parameters__[6].Value = key;
					__UpToYun_updateCount_parameters__[7] = new MySql.Data.MySqlClient.MySqlParameter("?file",MySql.Data.MySqlClient.MySqlDbType.VarChar,200);
					__UpToYun_updateCount_parameters__[7].Value = file;
					__UpToYun_updateCount_parameters__[8] = new MySql.Data.MySqlClient.MySqlParameter("?bucketName",MySql.Data.MySqlClient.MySqlDbType.VarChar,200);
					__UpToYun_updateCount_parameters__[8].Value = bucketName;
					__UpToYun_updateCount_parameters__[9] = new MySql.Data.MySqlClient.MySqlParameter("?accessId",MySql.Data.MySqlClient.MySqlDbType.VarChar,200);
					__UpToYun_updateCount_parameters__[9].Value = accessId;
					__UpToYun_updateCount_parameters__[10] = new MySql.Data.MySqlClient.MySqlParameter("?accessKey",MySql.Data.MySqlClient.MySqlDbType.VarChar,200);
					__UpToYun_updateCount_parameters__[10].Value = accessKey;
					__UpToYun_updateCount_parameters__[11] = new MySql.Data.MySqlClient.MySqlParameter("?sceretKey",MySql.Data.MySqlClient.MySqlDbType.VarChar,200);
					__UpToYun_updateCount_parameters__[11].Value = sceretKey;
					__UpToYun_updateCount_parameters__[12] = new MySql.Data.MySqlClient.MySqlParameter("?id",MySql.Data.MySqlClient.MySqlDbType.Int32);
					__UpToYun_updateCount_parameters__[12].Value = id;
			__UpToYun_updateCount_command__.Parameters.AddRange(__UpToYun_updateCount_parameters__);
			var updateCount = __UpToYun_updateCount_command__.ExecuteNonQuery();
			__UpToYun_updateCount_command__.Dispose();
	#endregion
			
								}
							}
							else if (!isSuccess)
							{
								//上传失败，也将此条记录记录到数据库中，-1
								string localUrl = "/" + key;

									#region	var table; 选取一行
			var table = new __UpToYun_table__();
			var __UpToYun_table_command__ = new MySql.Data.MySqlClient.MySqlCommand("select * from content where localUrl=?localUrl and yun=2", __UpToYun_contentConnStr_con__);
			var __UpToYun_table_parameters__=new MySql.Data.MySqlClient.MySqlParameter[1];
			__UpToYun_table_parameters__[0] = new MySql.Data.MySqlClient.MySqlParameter("?localUrl",MySql.Data.MySqlClient.MySqlDbType.VarChar,200);
			__UpToYun_table_parameters__[0].Value = localUrl;
			__UpToYun_table_command__.Parameters.AddRange(__UpToYun_table_parameters__);
			var __UpToYun_table_reader__= __UpToYun_table_command__.ExecuteReader();
			if (__UpToYun_table_reader__.HasRows)
			{
				__UpToYun_table_reader__.Read();
				table = new __UpToYun_table__();
				if(!__UpToYun_table_reader__.IsDBNull(0)){table.id = __UpToYun_table_reader__.GetInt32(0);}
				if(!__UpToYun_table_reader__.IsDBNull(1)){table.module = __UpToYun_table_reader__.GetString(1);}
				if(!__UpToYun_table_reader__.IsDBNull(2)){table.yun = __UpToYun_table_reader__.GetString(2);}
				if(!__UpToYun_table_reader__.IsDBNull(3)){table.domain = __UpToYun_table_reader__.GetString(3);}
				if(!__UpToYun_table_reader__.IsDBNull(4)){table.fileType = __UpToYun_table_reader__.GetString(4);}
				if(!__UpToYun_table_reader__.IsDBNull(5)){table.isSuccess = __UpToYun_table_reader__.GetBoolean(5);}
				if(!__UpToYun_table_reader__.IsDBNull(6)){table.isDeleted = __UpToYun_table_reader__.GetBoolean(6);}
				if(!__UpToYun_table_reader__.IsDBNull(7)){table.localUrl = __UpToYun_table_reader__.GetString(7);}
				if(!__UpToYun_table_reader__.IsDBNull(8)){table.yunUrl = __UpToYun_table_reader__.GetString(8);}
				if(!__UpToYun_table_reader__.IsDBNull(9)){table.keyStr = __UpToYun_table_reader__.GetString(9);}
				if(!__UpToYun_table_reader__.IsDBNull(10)){table.file = __UpToYun_table_reader__.GetString(10);}
				if(!__UpToYun_table_reader__.IsDBNull(11)){table.bucketName = __UpToYun_table_reader__.GetString(11);}
				if(!__UpToYun_table_reader__.IsDBNull(12)){table.accessId = __UpToYun_table_reader__.GetString(12);}
				if(!__UpToYun_table_reader__.IsDBNull(13)){table.accessKey = __UpToYun_table_reader__.GetString(13);}
				if(!__UpToYun_table_reader__.IsDBNull(14)){table.sceretKey = __UpToYun_table_reader__.GetString(14);}
			}
			else
			{
				table = null;
			}
			__UpToYun_table_reader__.Dispose();
			__UpToYun_table_command__.Dispose();
	#endregion
			
								string yunUrl = "";
								if (table.id == 0)
								{
										#region	var insertId; 插入并返回ID
			var __UpToYun_insertId_command__ = new MySql.Data.MySqlClient.MySqlCommand("insert into content(module,yun,domain,fileType,isSuccess,isDeleted,localUrl,yunUrl,keyStr,file,bucketName,accessId,accessKey,sceretKey) values(?module,1,?domain,?strFileType,-1,1,?localUrl,?yunUrl,?key,?file,?bucketName,?accessId,?accessKey,?sceretKey);select @@IDENTITY;", __UpToYun_contentConnStr_con__);
			var __UpToYun_insertId_parameters__=new MySql.Data.MySqlClient.MySqlParameter[11];
			__UpToYun_insertId_parameters__[0] = new MySql.Data.MySqlClient.MySqlParameter("?module",MySql.Data.MySqlClient.MySqlDbType.VarChar,20);
			__UpToYun_insertId_parameters__[0].Value = module;
			__UpToYun_insertId_parameters__[1] = new MySql.Data.MySqlClient.MySqlParameter("?domain",MySql.Data.MySqlClient.MySqlDbType.VarChar,200);
			__UpToYun_insertId_parameters__[1].Value = domain;
			__UpToYun_insertId_parameters__[2] = new MySql.Data.MySqlClient.MySqlParameter("?strFileType",MySql.Data.MySqlClient.MySqlDbType.VarChar,20);
			__UpToYun_insertId_parameters__[2].Value = strFileType;
			__UpToYun_insertId_parameters__[3] = new MySql.Data.MySqlClient.MySqlParameter("?localUrl",MySql.Data.MySqlClient.MySqlDbType.VarChar,200);
			__UpToYun_insertId_parameters__[3].Value = localUrl;
			__UpToYun_insertId_parameters__[4] = new MySql.Data.MySqlClient.MySqlParameter("?yunUrl",MySql.Data.MySqlClient.MySqlDbType.VarChar,200);
			__UpToYun_insertId_parameters__[4].Value = yunUrl;
			__UpToYun_insertId_parameters__[5] = new MySql.Data.MySqlClient.MySqlParameter("?key",MySql.Data.MySqlClient.MySqlDbType.VarChar,200);
			__UpToYun_insertId_parameters__[5].Value = key;
			__UpToYun_insertId_parameters__[6] = new MySql.Data.MySqlClient.MySqlParameter("?file",MySql.Data.MySqlClient.MySqlDbType.VarChar,200);
			__UpToYun_insertId_parameters__[6].Value = file;
			__UpToYun_insertId_parameters__[7] = new MySql.Data.MySqlClient.MySqlParameter("?bucketName",MySql.Data.MySqlClient.MySqlDbType.VarChar,200);
			__UpToYun_insertId_parameters__[7].Value = bucketName;
			__UpToYun_insertId_parameters__[8] = new MySql.Data.MySqlClient.MySqlParameter("?accessId",MySql.Data.MySqlClient.MySqlDbType.VarChar,200);
			__UpToYun_insertId_parameters__[8].Value = accessId;
			__UpToYun_insertId_parameters__[9] = new MySql.Data.MySqlClient.MySqlParameter("?accessKey",MySql.Data.MySqlClient.MySqlDbType.VarChar,200);
			__UpToYun_insertId_parameters__[9].Value = accessKey;
			__UpToYun_insertId_parameters__[10] = new MySql.Data.MySqlClient.MySqlParameter("?sceretKey",MySql.Data.MySqlClient.MySqlDbType.VarChar,200);
			__UpToYun_insertId_parameters__[10].Value = sceretKey;
			__UpToYun_insertId_command__.Parameters.AddRange(__UpToYun_insertId_parameters__);
			var insertId = int.Parse(__UpToYun_insertId_command__.ExecuteScalar().ToString());
			__UpToYun_insertId_command__.Dispose();
	#endregion
			
								}
								else
								{
									var id = table.id;//Convert.ToInt32(table.Rows[0]["id"]);
										#region	var updateCount; 更新表,并返回受影响行数
			var __UpToYun_updateCount_command__ = new MySql.Data.MySqlClient.MySqlCommand("update content set (module=?module,yun=?chooseYun,domain=?domain,fileType=?strFileType,isSuccess=-1,isDeleted=1,localUrl=?localUrl,keyStr=?key,file=?file,bucketName=?bucketName,accessId=?accessId,accessKey=?accessKey,sceretKey=?sceretKey where id=?id;", __UpToYun_contentConnStr_con__);
			var __UpToYun_updateCount_parameters__=new MySql.Data.MySqlClient.MySqlParameter[12];
					__UpToYun_updateCount_parameters__[0] = new MySql.Data.MySqlClient.MySqlParameter("?module",MySql.Data.MySqlClient.MySqlDbType.VarChar,20);
					__UpToYun_updateCount_parameters__[0].Value = module;
					__UpToYun_updateCount_parameters__[1] = new MySql.Data.MySqlClient.MySqlParameter("?chooseYun",MySql.Data.MySqlClient.MySqlDbType.VarChar,2);
					__UpToYun_updateCount_parameters__[1].Value = chooseYun;
					__UpToYun_updateCount_parameters__[2] = new MySql.Data.MySqlClient.MySqlParameter("?domain",MySql.Data.MySqlClient.MySqlDbType.VarChar,200);
					__UpToYun_updateCount_parameters__[2].Value = domain;
					__UpToYun_updateCount_parameters__[3] = new MySql.Data.MySqlClient.MySqlParameter("?strFileType",MySql.Data.MySqlClient.MySqlDbType.VarChar,20);
					__UpToYun_updateCount_parameters__[3].Value = strFileType;
					__UpToYun_updateCount_parameters__[4] = new MySql.Data.MySqlClient.MySqlParameter("?localUrl",MySql.Data.MySqlClient.MySqlDbType.VarChar,200);
					__UpToYun_updateCount_parameters__[4].Value = localUrl;
					__UpToYun_updateCount_parameters__[5] = new MySql.Data.MySqlClient.MySqlParameter("?key",MySql.Data.MySqlClient.MySqlDbType.VarChar,200);
					__UpToYun_updateCount_parameters__[5].Value = key;
					__UpToYun_updateCount_parameters__[6] = new MySql.Data.MySqlClient.MySqlParameter("?file",MySql.Data.MySqlClient.MySqlDbType.VarChar,200);
					__UpToYun_updateCount_parameters__[6].Value = file;
					__UpToYun_updateCount_parameters__[7] = new MySql.Data.MySqlClient.MySqlParameter("?bucketName",MySql.Data.MySqlClient.MySqlDbType.VarChar,200);
					__UpToYun_updateCount_parameters__[7].Value = bucketName;
					__UpToYun_updateCount_parameters__[8] = new MySql.Data.MySqlClient.MySqlParameter("?accessId",MySql.Data.MySqlClient.MySqlDbType.VarChar,200);
					__UpToYun_updateCount_parameters__[8].Value = accessId;
					__UpToYun_updateCount_parameters__[9] = new MySql.Data.MySqlClient.MySqlParameter("?accessKey",MySql.Data.MySqlClient.MySqlDbType.VarChar,200);
					__UpToYun_updateCount_parameters__[9].Value = accessKey;
					__UpToYun_updateCount_parameters__[10] = new MySql.Data.MySqlClient.MySqlParameter("?sceretKey",MySql.Data.MySqlClient.MySqlDbType.VarChar,200);
					__UpToYun_updateCount_parameters__[10].Value = sceretKey;
					__UpToYun_updateCount_parameters__[11] = new MySql.Data.MySqlClient.MySqlParameter("?id",MySql.Data.MySqlClient.MySqlDbType.Int32);
					__UpToYun_updateCount_parameters__[11].Value = id;
			__UpToYun_updateCount_command__.Parameters.AddRange(__UpToYun_updateCount_parameters__);
			var updateCount = __UpToYun_updateCount_command__.ExecuteNonQuery();
			__UpToYun_updateCount_command__.Dispose();
	#endregion
			
								}
							}
							else
							{
								//出现未知错误
							}

						}
						else if (chooseYun == "2")
						{
							IUploadable yun = Base.GetYun(yunType.QiNiu);
							yun.Initial("", "", "", "", accessKey, sceretKey);
							isSuccess = yun.Upload(bucketName, key + keyGUID, file, "", accessKey, sceretKey, 0);
							//bool isSuccess = UpDownload.ResumablePutFile("testapp", key + keyGUID, file);
							if (isSuccess)
							{
								//则将此条记录的上传标识设置为1，
								string localUrl = "/" + key;
								string yunUrl = domain + "/" + key + keyGUID;

									#region	var table; 选取一行
			var table = new __UpToYun_table__();
			var __UpToYun_table_command__ = new MySql.Data.MySqlClient.MySqlCommand("select * from content where localUrl=?localUrl", __UpToYun_contentConnStr_con__);
			var __UpToYun_table_parameters__=new MySql.Data.MySqlClient.MySqlParameter[1];
			__UpToYun_table_parameters__[0] = new MySql.Data.MySqlClient.MySqlParameter("?localUrl",MySql.Data.MySqlClient.MySqlDbType.VarChar,200);
			__UpToYun_table_parameters__[0].Value = localUrl;
			__UpToYun_table_command__.Parameters.AddRange(__UpToYun_table_parameters__);
			var __UpToYun_table_reader__= __UpToYun_table_command__.ExecuteReader();
			if (__UpToYun_table_reader__.HasRows)
			{
				__UpToYun_table_reader__.Read();
				table = new __UpToYun_table__();
				if(!__UpToYun_table_reader__.IsDBNull(0)){table.id = __UpToYun_table_reader__.GetInt32(0);}
				if(!__UpToYun_table_reader__.IsDBNull(1)){table.module = __UpToYun_table_reader__.GetString(1);}
				if(!__UpToYun_table_reader__.IsDBNull(2)){table.yun = __UpToYun_table_reader__.GetString(2);}
				if(!__UpToYun_table_reader__.IsDBNull(3)){table.domain = __UpToYun_table_reader__.GetString(3);}
				if(!__UpToYun_table_reader__.IsDBNull(4)){table.fileType = __UpToYun_table_reader__.GetString(4);}
				if(!__UpToYun_table_reader__.IsDBNull(5)){table.isSuccess = __UpToYun_table_reader__.GetBoolean(5);}
				if(!__UpToYun_table_reader__.IsDBNull(6)){table.isDeleted = __UpToYun_table_reader__.GetBoolean(6);}
				if(!__UpToYun_table_reader__.IsDBNull(7)){table.localUrl = __UpToYun_table_reader__.GetString(7);}
				if(!__UpToYun_table_reader__.IsDBNull(8)){table.yunUrl = __UpToYun_table_reader__.GetString(8);}
				if(!__UpToYun_table_reader__.IsDBNull(9)){table.keyStr = __UpToYun_table_reader__.GetString(9);}
				if(!__UpToYun_table_reader__.IsDBNull(10)){table.file = __UpToYun_table_reader__.GetString(10);}
				if(!__UpToYun_table_reader__.IsDBNull(11)){table.bucketName = __UpToYun_table_reader__.GetString(11);}
				if(!__UpToYun_table_reader__.IsDBNull(12)){table.accessId = __UpToYun_table_reader__.GetString(12);}
				if(!__UpToYun_table_reader__.IsDBNull(13)){table.accessKey = __UpToYun_table_reader__.GetString(13);}
				if(!__UpToYun_table_reader__.IsDBNull(14)){table.sceretKey = __UpToYun_table_reader__.GetString(14);}
			}
			else
			{
				table = null;
			}
			__UpToYun_table_reader__.Dispose();
			__UpToYun_table_command__.Dispose();
	#endregion
			
								if (table.id == 0)
								{
										#region	var insertId; 插入并返回ID
			var __UpToYun_insertId_command__ = new MySql.Data.MySqlClient.MySqlCommand("insert into content(module,yun,domain,fileType,isSuccess,isDeleted,localUrl,yunUrl,keyStr,file,bucketName,accessId,accessKey,sceretKey) values(?module,2,?domain,?strFileType,1,1,?localUrl,?yunUrl,?key,?file,?bucketName,?accessId,?accessKey,?sceretKey);select @@IDENTITY;", __UpToYun_contentConnStr_con__);
			var __UpToYun_insertId_parameters__=new MySql.Data.MySqlClient.MySqlParameter[11];
			__UpToYun_insertId_parameters__[0] = new MySql.Data.MySqlClient.MySqlParameter("?module",MySql.Data.MySqlClient.MySqlDbType.VarChar,20);
			__UpToYun_insertId_parameters__[0].Value = module;
			__UpToYun_insertId_parameters__[1] = new MySql.Data.MySqlClient.MySqlParameter("?domain",MySql.Data.MySqlClient.MySqlDbType.VarChar,200);
			__UpToYun_insertId_parameters__[1].Value = domain;
			__UpToYun_insertId_parameters__[2] = new MySql.Data.MySqlClient.MySqlParameter("?strFileType",MySql.Data.MySqlClient.MySqlDbType.VarChar,20);
			__UpToYun_insertId_parameters__[2].Value = strFileType;
			__UpToYun_insertId_parameters__[3] = new MySql.Data.MySqlClient.MySqlParameter("?localUrl",MySql.Data.MySqlClient.MySqlDbType.VarChar,200);
			__UpToYun_insertId_parameters__[3].Value = localUrl;
			__UpToYun_insertId_parameters__[4] = new MySql.Data.MySqlClient.MySqlParameter("?yunUrl",MySql.Data.MySqlClient.MySqlDbType.VarChar,200);
			__UpToYun_insertId_parameters__[4].Value = yunUrl;
			__UpToYun_insertId_parameters__[5] = new MySql.Data.MySqlClient.MySqlParameter("?key",MySql.Data.MySqlClient.MySqlDbType.VarChar,200);
			__UpToYun_insertId_parameters__[5].Value = key;
			__UpToYun_insertId_parameters__[6] = new MySql.Data.MySqlClient.MySqlParameter("?file",MySql.Data.MySqlClient.MySqlDbType.VarChar,200);
			__UpToYun_insertId_parameters__[6].Value = file;
			__UpToYun_insertId_parameters__[7] = new MySql.Data.MySqlClient.MySqlParameter("?bucketName",MySql.Data.MySqlClient.MySqlDbType.VarChar,200);
			__UpToYun_insertId_parameters__[7].Value = bucketName;
			__UpToYun_insertId_parameters__[8] = new MySql.Data.MySqlClient.MySqlParameter("?accessId",MySql.Data.MySqlClient.MySqlDbType.VarChar,200);
			__UpToYun_insertId_parameters__[8].Value = accessId;
			__UpToYun_insertId_parameters__[9] = new MySql.Data.MySqlClient.MySqlParameter("?accessKey",MySql.Data.MySqlClient.MySqlDbType.VarChar,200);
			__UpToYun_insertId_parameters__[9].Value = accessKey;
			__UpToYun_insertId_parameters__[10] = new MySql.Data.MySqlClient.MySqlParameter("?sceretKey",MySql.Data.MySqlClient.MySqlDbType.VarChar,200);
			__UpToYun_insertId_parameters__[10].Value = sceretKey;
			__UpToYun_insertId_command__.Parameters.AddRange(__UpToYun_insertId_parameters__);
			var insertId = int.Parse(__UpToYun_insertId_command__.ExecuteScalar().ToString());
			__UpToYun_insertId_command__.Dispose();
	#endregion
			
								}
								else
								{
									var id = table.id;//Models.contentConnStr.QueryRow("select id from content");//Convert.ToInt32(table.Rows[0]["id"]);
										#region	var updateCount; 更新表,并返回受影响行数
			var __UpToYun_updateCount_command__ = new MySql.Data.MySqlClient.MySqlCommand("update content set module=?module,yun=?chooseYun,domain=?domain,fileType=?strFileType,isSuccess=1,isDeleted=1,localUrl=?localUrl,yunUrl=?yunUrl,keyStr=?key,file=?file,bucketName=?bucketName,accessId=?accessId,accessKey=?accessKey,sceretKey=?sceretKey where id=?id;", __UpToYun_contentConnStr_con__);
			var __UpToYun_updateCount_parameters__=new MySql.Data.MySqlClient.MySqlParameter[13];
					__UpToYun_updateCount_parameters__[0] = new MySql.Data.MySqlClient.MySqlParameter("?module",MySql.Data.MySqlClient.MySqlDbType.VarChar,20);
					__UpToYun_updateCount_parameters__[0].Value = module;
					__UpToYun_updateCount_parameters__[1] = new MySql.Data.MySqlClient.MySqlParameter("?chooseYun",MySql.Data.MySqlClient.MySqlDbType.VarChar,2);
					__UpToYun_updateCount_parameters__[1].Value = chooseYun;
					__UpToYun_updateCount_parameters__[2] = new MySql.Data.MySqlClient.MySqlParameter("?domain",MySql.Data.MySqlClient.MySqlDbType.VarChar,200);
					__UpToYun_updateCount_parameters__[2].Value = domain;
					__UpToYun_updateCount_parameters__[3] = new MySql.Data.MySqlClient.MySqlParameter("?strFileType",MySql.Data.MySqlClient.MySqlDbType.VarChar,20);
					__UpToYun_updateCount_parameters__[3].Value = strFileType;
					__UpToYun_updateCount_parameters__[4] = new MySql.Data.MySqlClient.MySqlParameter("?localUrl",MySql.Data.MySqlClient.MySqlDbType.VarChar,200);
					__UpToYun_updateCount_parameters__[4].Value = localUrl;
					__UpToYun_updateCount_parameters__[5] = new MySql.Data.MySqlClient.MySqlParameter("?yunUrl",MySql.Data.MySqlClient.MySqlDbType.VarChar,200);
					__UpToYun_updateCount_parameters__[5].Value = yunUrl;
					__UpToYun_updateCount_parameters__[6] = new MySql.Data.MySqlClient.MySqlParameter("?key",MySql.Data.MySqlClient.MySqlDbType.VarChar,200);
					__UpToYun_updateCount_parameters__[6].Value = key;
					__UpToYun_updateCount_parameters__[7] = new MySql.Data.MySqlClient.MySqlParameter("?file",MySql.Data.MySqlClient.MySqlDbType.VarChar,200);
					__UpToYun_updateCount_parameters__[7].Value = file;
					__UpToYun_updateCount_parameters__[8] = new MySql.Data.MySqlClient.MySqlParameter("?bucketName",MySql.Data.MySqlClient.MySqlDbType.VarChar,200);
					__UpToYun_updateCount_parameters__[8].Value = bucketName;
					__UpToYun_updateCount_parameters__[9] = new MySql.Data.MySqlClient.MySqlParameter("?accessId",MySql.Data.MySqlClient.MySqlDbType.VarChar,200);
					__UpToYun_updateCount_parameters__[9].Value = accessId;
					__UpToYun_updateCount_parameters__[10] = new MySql.Data.MySqlClient.MySqlParameter("?accessKey",MySql.Data.MySqlClient.MySqlDbType.VarChar,200);
					__UpToYun_updateCount_parameters__[10].Value = accessKey;
					__UpToYun_updateCount_parameters__[11] = new MySql.Data.MySqlClient.MySqlParameter("?sceretKey",MySql.Data.MySqlClient.MySqlDbType.VarChar,200);
					__UpToYun_updateCount_parameters__[11].Value = sceretKey;
					__UpToYun_updateCount_parameters__[12] = new MySql.Data.MySqlClient.MySqlParameter("?id",MySql.Data.MySqlClient.MySqlDbType.Int32);
					__UpToYun_updateCount_parameters__[12].Value = id;
			__UpToYun_updateCount_command__.Parameters.AddRange(__UpToYun_updateCount_parameters__);
			var updateCount = __UpToYun_updateCount_command__.ExecuteNonQuery();
			__UpToYun_updateCount_command__.Dispose();
	#endregion
			
								}
							}
							else if (!isSuccess)
							{
								//上传失败，也将此条记录记录到数据库中，-1
								string localUrl = "/" + key;

									#region	var table; 选取一行
			var table = new __UpToYun_table__();
			var __UpToYun_table_command__ = new MySql.Data.MySqlClient.MySqlCommand("select * from content where localUrl=?localUrl", __UpToYun_contentConnStr_con__);
			var __UpToYun_table_parameters__=new MySql.Data.MySqlClient.MySqlParameter[1];
			__UpToYun_table_parameters__[0] = new MySql.Data.MySqlClient.MySqlParameter("?localUrl",MySql.Data.MySqlClient.MySqlDbType.VarChar,200);
			__UpToYun_table_parameters__[0].Value = localUrl;
			__UpToYun_table_command__.Parameters.AddRange(__UpToYun_table_parameters__);
			var __UpToYun_table_reader__= __UpToYun_table_command__.ExecuteReader();
			if (__UpToYun_table_reader__.HasRows)
			{
				__UpToYun_table_reader__.Read();
				table = new __UpToYun_table__();
				if(!__UpToYun_table_reader__.IsDBNull(0)){table.id = __UpToYun_table_reader__.GetInt32(0);}
				if(!__UpToYun_table_reader__.IsDBNull(1)){table.module = __UpToYun_table_reader__.GetString(1);}
				if(!__UpToYun_table_reader__.IsDBNull(2)){table.yun = __UpToYun_table_reader__.GetString(2);}
				if(!__UpToYun_table_reader__.IsDBNull(3)){table.domain = __UpToYun_table_reader__.GetString(3);}
				if(!__UpToYun_table_reader__.IsDBNull(4)){table.fileType = __UpToYun_table_reader__.GetString(4);}
				if(!__UpToYun_table_reader__.IsDBNull(5)){table.isSuccess = __UpToYun_table_reader__.GetBoolean(5);}
				if(!__UpToYun_table_reader__.IsDBNull(6)){table.isDeleted = __UpToYun_table_reader__.GetBoolean(6);}
				if(!__UpToYun_table_reader__.IsDBNull(7)){table.localUrl = __UpToYun_table_reader__.GetString(7);}
				if(!__UpToYun_table_reader__.IsDBNull(8)){table.yunUrl = __UpToYun_table_reader__.GetString(8);}
				if(!__UpToYun_table_reader__.IsDBNull(9)){table.keyStr = __UpToYun_table_reader__.GetString(9);}
				if(!__UpToYun_table_reader__.IsDBNull(10)){table.file = __UpToYun_table_reader__.GetString(10);}
				if(!__UpToYun_table_reader__.IsDBNull(11)){table.bucketName = __UpToYun_table_reader__.GetString(11);}
				if(!__UpToYun_table_reader__.IsDBNull(12)){table.accessId = __UpToYun_table_reader__.GetString(12);}
				if(!__UpToYun_table_reader__.IsDBNull(13)){table.accessKey = __UpToYun_table_reader__.GetString(13);}
				if(!__UpToYun_table_reader__.IsDBNull(14)){table.sceretKey = __UpToYun_table_reader__.GetString(14);}
			}
			else
			{
				table = null;
			}
			__UpToYun_table_reader__.Dispose();
			__UpToYun_table_command__.Dispose();
	#endregion
			
								string yunUrl = "";
								if (table.id == 0)
								{
										#region	var insertId; 插入并返回ID
			var __UpToYun_insertId_command__ = new MySql.Data.MySqlClient.MySqlCommand("insert into content(module,yun,domain,fileType,isSuccess,isDeleted,localUrl,yunUrl,keyStr,file,bucketName,accessId,accessKey,sceretKey) values(?module,2,?domain,?strFileType,-1,1,?localUrl,?yunUrl,?key,?file,?bucketName,?accessId,?accessKey,?sceretKey);select @@IDENTITY;", __UpToYun_contentConnStr_con__);
			var __UpToYun_insertId_parameters__=new MySql.Data.MySqlClient.MySqlParameter[11];
			__UpToYun_insertId_parameters__[0] = new MySql.Data.MySqlClient.MySqlParameter("?module",MySql.Data.MySqlClient.MySqlDbType.VarChar,20);
			__UpToYun_insertId_parameters__[0].Value = module;
			__UpToYun_insertId_parameters__[1] = new MySql.Data.MySqlClient.MySqlParameter("?domain",MySql.Data.MySqlClient.MySqlDbType.VarChar,200);
			__UpToYun_insertId_parameters__[1].Value = domain;
			__UpToYun_insertId_parameters__[2] = new MySql.Data.MySqlClient.MySqlParameter("?strFileType",MySql.Data.MySqlClient.MySqlDbType.VarChar,20);
			__UpToYun_insertId_parameters__[2].Value = strFileType;
			__UpToYun_insertId_parameters__[3] = new MySql.Data.MySqlClient.MySqlParameter("?localUrl",MySql.Data.MySqlClient.MySqlDbType.VarChar,200);
			__UpToYun_insertId_parameters__[3].Value = localUrl;
			__UpToYun_insertId_parameters__[4] = new MySql.Data.MySqlClient.MySqlParameter("?yunUrl",MySql.Data.MySqlClient.MySqlDbType.VarChar,200);
			__UpToYun_insertId_parameters__[4].Value = yunUrl;
			__UpToYun_insertId_parameters__[5] = new MySql.Data.MySqlClient.MySqlParameter("?key",MySql.Data.MySqlClient.MySqlDbType.VarChar,200);
			__UpToYun_insertId_parameters__[5].Value = key;
			__UpToYun_insertId_parameters__[6] = new MySql.Data.MySqlClient.MySqlParameter("?file",MySql.Data.MySqlClient.MySqlDbType.VarChar,200);
			__UpToYun_insertId_parameters__[6].Value = file;
			__UpToYun_insertId_parameters__[7] = new MySql.Data.MySqlClient.MySqlParameter("?bucketName",MySql.Data.MySqlClient.MySqlDbType.VarChar,200);
			__UpToYun_insertId_parameters__[7].Value = bucketName;
			__UpToYun_insertId_parameters__[8] = new MySql.Data.MySqlClient.MySqlParameter("?accessId",MySql.Data.MySqlClient.MySqlDbType.VarChar,200);
			__UpToYun_insertId_parameters__[8].Value = accessId;
			__UpToYun_insertId_parameters__[9] = new MySql.Data.MySqlClient.MySqlParameter("?accessKey",MySql.Data.MySqlClient.MySqlDbType.VarChar,200);
			__UpToYun_insertId_parameters__[9].Value = accessKey;
			__UpToYun_insertId_parameters__[10] = new MySql.Data.MySqlClient.MySqlParameter("?sceretKey",MySql.Data.MySqlClient.MySqlDbType.VarChar,200);
			__UpToYun_insertId_parameters__[10].Value = sceretKey;
			__UpToYun_insertId_command__.Parameters.AddRange(__UpToYun_insertId_parameters__);
			var insertId = int.Parse(__UpToYun_insertId_command__.ExecuteScalar().ToString());
			__UpToYun_insertId_command__.Dispose();
	#endregion
			
								}
								else
								{
									var id = table.id;//Convert.ToInt32(table.Rows[0]["id"]);
										#region	var updateCount; 更新表,并返回受影响行数
			var __UpToYun_updateCount_command__ = new MySql.Data.MySqlClient.MySqlCommand("update content set module=?module,yun=?chooseYun,domain=?domain,fileType=?strFileType,isSuccess=-1,isDeleted=1,localUrl=?localUrl,keyStr=?key,file=?file,bucketName=?bucketName,accessId=?accessId,accessKey=?accessKey,sceretKey=?sceretKey where id=?id;", __UpToYun_contentConnStr_con__);
			var __UpToYun_updateCount_parameters__=new MySql.Data.MySqlClient.MySqlParameter[12];
					__UpToYun_updateCount_parameters__[0] = new MySql.Data.MySqlClient.MySqlParameter("?module",MySql.Data.MySqlClient.MySqlDbType.VarChar,20);
					__UpToYun_updateCount_parameters__[0].Value = module;
					__UpToYun_updateCount_parameters__[1] = new MySql.Data.MySqlClient.MySqlParameter("?chooseYun",MySql.Data.MySqlClient.MySqlDbType.VarChar,2);
					__UpToYun_updateCount_parameters__[1].Value = chooseYun;
					__UpToYun_updateCount_parameters__[2] = new MySql.Data.MySqlClient.MySqlParameter("?domain",MySql.Data.MySqlClient.MySqlDbType.VarChar,200);
					__UpToYun_updateCount_parameters__[2].Value = domain;
					__UpToYun_updateCount_parameters__[3] = new MySql.Data.MySqlClient.MySqlParameter("?strFileType",MySql.Data.MySqlClient.MySqlDbType.VarChar,20);
					__UpToYun_updateCount_parameters__[3].Value = strFileType;
					__UpToYun_updateCount_parameters__[4] = new MySql.Data.MySqlClient.MySqlParameter("?localUrl",MySql.Data.MySqlClient.MySqlDbType.VarChar,200);
					__UpToYun_updateCount_parameters__[4].Value = localUrl;
					__UpToYun_updateCount_parameters__[5] = new MySql.Data.MySqlClient.MySqlParameter("?key",MySql.Data.MySqlClient.MySqlDbType.VarChar,200);
					__UpToYun_updateCount_parameters__[5].Value = key;
					__UpToYun_updateCount_parameters__[6] = new MySql.Data.MySqlClient.MySqlParameter("?file",MySql.Data.MySqlClient.MySqlDbType.VarChar,200);
					__UpToYun_updateCount_parameters__[6].Value = file;
					__UpToYun_updateCount_parameters__[7] = new MySql.Data.MySqlClient.MySqlParameter("?bucketName",MySql.Data.MySqlClient.MySqlDbType.VarChar,200);
					__UpToYun_updateCount_parameters__[7].Value = bucketName;
					__UpToYun_updateCount_parameters__[8] = new MySql.Data.MySqlClient.MySqlParameter("?accessId",MySql.Data.MySqlClient.MySqlDbType.VarChar,200);
					__UpToYun_updateCount_parameters__[8].Value = accessId;
					__UpToYun_updateCount_parameters__[9] = new MySql.Data.MySqlClient.MySqlParameter("?accessKey",MySql.Data.MySqlClient.MySqlDbType.VarChar,200);
					__UpToYun_updateCount_parameters__[9].Value = accessKey;
					__UpToYun_updateCount_parameters__[10] = new MySql.Data.MySqlClient.MySqlParameter("?sceretKey",MySql.Data.MySqlClient.MySqlDbType.VarChar,200);
					__UpToYun_updateCount_parameters__[10].Value = sceretKey;
					__UpToYun_updateCount_parameters__[11] = new MySql.Data.MySqlClient.MySqlParameter("?id",MySql.Data.MySqlClient.MySqlDbType.Int32);
					__UpToYun_updateCount_parameters__[11].Value = id;
			__UpToYun_updateCount_command__.Parameters.AddRange(__UpToYun_updateCount_parameters__);
			var updateCount = __UpToYun_updateCount_command__.ExecuteNonQuery();
			__UpToYun_updateCount_command__.Dispose();
	#endregion
			
								}
							}
							else
							{
								//出现未知错误
							}

						}
						else
						{
							//有默认云或其他
						}
					}
				}
				else
				{
					UpToYun(filePath, accessKey, sceretKey, bucketName, chooseYun, accessId, domain, module);
				}
			}
			var c = Models.contentConnStr.QueryAll("select * from content where isSuccess=-1").Count;
			__UpToYun_contentConnStr_con__.Close();
			if (c<=0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
	}
}