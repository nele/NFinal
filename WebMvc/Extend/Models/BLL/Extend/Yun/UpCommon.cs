﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using WebMvc.App.Code.Extend.Yun;

namespace WebMvc.App.Models.BLL.Extend.Yun
{
	public class UpCommon
	{
		public bool UpToYun(string path, string accessKey, string sceretKey, string bucketName, string chooseYun, string accessId, string domain, string module)
		{
			string[] filePaths = Directory.GetDirectories(path);
            Models.contentConnStr.OpenConnection();
            foreach (string filePath in filePaths)
			{
				string[] ffPath = Directory.GetDirectories(filePath);

				string[] ffFiles = Directory.GetFiles(filePath);

				if (ffPath.Length <= 0)
				{
					string[] files = Directory.GetFiles(filePath);
					foreach (string file in files)
					{
						string fileName = Path.GetFileName(file);//11.css
						string strFileType = string.Empty;
						foreach (string preFloder in file.Substring(filePath.IndexOf(module + "\\")).Split('\\'))
						{
							strFileType = strFileType + "/" + preFloder;
						}
						//string fileType = filePath.Split('\\')[filePath.Split('\\').Length - 1];
						string key = strFileType.TrimStart('/');

						string keyGUID = "/" + Guid.NewGuid().ToString();

						bool isSuccess = false;
						
						if (chooseYun == "1")//阿里云
						{
							IUploadable yun = Base.GetYun(yunType.ALi);
							yun.Initial(bucketName, key, file, accessId, accessKey, "");
							isSuccess = yun.Upload(bucketName, key, file, accessId, accessKey, "",2 * 1024 * 1024);
							//bool isSuccess = UpDownload.ResumablePutFile("testapp", key + keyGUID, file);
							if (isSuccess)
							{
								//则将此条记录的上传标识设置为1，
								string localUrl = "/" + key;
								string yunUrl = domain + "/" + key;

								var table = Models.contentConnStr.QueryRow("select * from content where localUrl=@localUrl and yun=1");

								if (table.id == 0)
								{
									var insertId = Models.contentConnStr.Insert("insert into content(module,yun,domain,fileType,isSuccess,isDeleted,localUrl,yunUrl,keyStr,file,bucketName,accessId,accessKey,sceretKey) values(@module,1,@domain,@strFileType,1,1,@localUrl,@yunUrl,@key,@file,@bucketName,@accessId,@accessKey,@sceretKey)");
								}
								else
								{
									var id = table.id;// Convert.ToInt32(table.Rows[0]["id"]);
									var updateCount = Models.contentConnStr.Update("update content set module=@module,yun=@chooseYun,domain=@domain,fileType=@strFileType,isSuccess=1,isDeleted=1,localUrl=@localUrl,yunUrl=@yunUrl,keyStr=@key,file=@file,bucketName=@bucketName,accessId=@accessId,accessKey=@accessKey,sceretKey=@sceretKey where id=@id");
								}
							}
							else if (!isSuccess)
							{
								//上传失败，也将此条记录记录到数据库中，-1
								string localUrl = "/" + key;

								var table = Models.contentConnStr.QueryRow("select * from content where localUrl=@localUrl and yun=2");
								string yunUrl = "";
								if (table.id == 0)
								{
									var insertId = Models.contentConnStr.Insert("insert into content(module,yun,domain,fileType,isSuccess,isDeleted,localUrl,yunUrl,keyStr,file,bucketName,accessId,accessKey,sceretKey) values(@module,1,@domain,@strFileType,-1,1,@localUrl,@yunUrl,@key,@file,@bucketName,@accessId,@accessKey,@sceretKey)");
								}
								else
								{
									var id = table.id;//Convert.ToInt32(table.Rows[0]["id"]);
									var updateCount = Models.contentConnStr.Update("update content set (module=@module,yun=@chooseYun,domain=@domain,fileType=@strFileType,isSuccess=-1,isDeleted=1,localUrl=@localUrl,keyStr=@key,file=@file,bucketName=@bucketName,accessId=@accessId,accessKey=@accessKey,sceretKey=@sceretKey where id=@id");
								}
							}
							else
							{
								//出现未知错误
							}

						}
						else if (chooseYun == "2")
						{
							IUploadable yun = Base.GetYun(yunType.QiNiu);
							yun.Initial("", "", "", "", accessKey, sceretKey);
							isSuccess = yun.Upload(bucketName, key + keyGUID, file, "", accessKey, sceretKey, 0);
							//bool isSuccess = UpDownload.ResumablePutFile("testapp", key + keyGUID, file);
							if (isSuccess)
							{
								//则将此条记录的上传标识设置为1，
								string localUrl = "/" + key;
								string yunUrl = domain + "/" + key + keyGUID;

								var table = Models.contentConnStr.QueryRow("select * from content where localUrl=@localUrl");
								if (table.id == 0)
								{
									var insertId = Models.contentConnStr.Insert("insert into content(module,yun,domain,fileType,isSuccess,isDeleted,localUrl,yunUrl,keyStr,file,bucketName,accessId,accessKey,sceretKey) values(@module,2,@domain,@strFileType,1,1,@localUrl,@yunUrl,@key,@file,@bucketName,@accessId,@accessKey,@sceretKey)");
								}
								else
								{
									var id = table.id;//Models.contentConnStr.QueryRow("select id from content");//Convert.ToInt32(table.Rows[0]["id"]);
									var updateCount = Models.contentConnStr.Update("update content set module=@module,yun=@chooseYun,domain=@domain,fileType=@strFileType,isSuccess=1,isDeleted=1,localUrl=@localUrl,yunUrl=@yunUrl,keyStr=@key,file=@file,bucketName=@bucketName,accessId=@accessId,accessKey=@accessKey,sceretKey=@sceretKey where id=@id");
								}
							}
							else if (!isSuccess)
							{
								//上传失败，也将此条记录记录到数据库中，-1
								string localUrl = "/" + key;

								var table = Models.contentConnStr.QueryRow("select * from content where localUrl=@localUrl");
								string yunUrl = "";
								if (table.id == 0)
								{
									var insertId = Models.contentConnStr.Insert("insert into content(module,yun,domain,fileType,isSuccess,isDeleted,localUrl,yunUrl,keyStr,file,bucketName,accessId,accessKey,sceretKey) values(@module,2,@domain,@strFileType,-1,1,@localUrl,@yunUrl,@key,@file,@bucketName,@accessId,@accessKey,@sceretKey)");
								}
								else
								{
									var id = table.id;//Convert.ToInt32(table.Rows[0]["id"]);
									var updateCount = Models.contentConnStr.Update("update content set module=@module,yun=@chooseYun,domain=@domain,fileType=@strFileType,isSuccess=-1,isDeleted=1,localUrl=@localUrl,keyStr=@key,file=@file,bucketName=@bucketName,accessId=@accessId,accessKey=@accessKey,sceretKey=@sceretKey where id=@id");
								}
							}
							else
							{
								//出现未知错误
							}

						}
						else
						{
							//有默认云或其他
						}
					}
				}
				else
				{
					UpToYun(filePath, accessKey, sceretKey, bucketName, chooseYun, accessId, domain, module);
				}
			}
			var c = Models.contentConnStr.QueryAll("select * from content where isSuccess=-1").Count;
			Models.contentConnStr.CloseConnection();
			if (c<=0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
	}
}