﻿using Qiniu.IO.Resumable;
using Qiniu.RPC;
using Qiniu.RS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WebMvc.App.Code.Extend.Yun
{
	class QiNiuUp:IUploadable
	{
		public void Initial(string bucket, string key, string accessKey,string secretKey, string fname, string accessId)
		{
			Qiniu.Conf.Config.ACCESS_KEY = accessKey;// "2Dp4hve4VAvAk41wIgZa3aED7_LaLmZ3gbLElCp_";
			Qiniu.Conf.Config.SECRET_KEY = secretKey;// "oSrBN0qPF7MNMCvYCYc5ZhZJxWzkcmfECeMb4_fb";
		}

		public bool Upload(string bucket, string key, string fname, string accessId, string accessKey, string secretKey, int partSize)
		{
			Initial(bucket, key, accessKey,secretKey,fname,accessId);
			PutPolicy policy = new PutPolicy(bucket, 3600);
			string upToken = policy.Token();
			Settings setting = new Settings();
			ResumablePutExtra extra = new ResumablePutExtra();
			ResumablePut client = new ResumablePut(setting, extra);
			CallRet ret = client.PutFile(upToken, fname, key);
			if (ret.OK)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
	}
}
