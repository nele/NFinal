﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WebMvc.App.Code.Extend.Yun
{
	public class Base
	{
		public static IUploadable GetYun(yunType t)
		{
			if (t == yunType.ALi)
			{
				return new ALiUp();
			}
			else if (t==yunType.QiNiu)
			{
				return new QiNiuUp();
			}
			else
			{
				return new QiNiuUp();
			}

		}

		public static IUploadable GetYun()
		{
			yunType t=yunType.QiNiu;
			return GetYun(t);
		}
	}
}
