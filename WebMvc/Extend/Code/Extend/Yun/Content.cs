﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WebMvc.App.Code.Extend.Yun
{
	public class Content
	{
		public int Id { get; set; }
		public string Module { get; set; }
		public string Yun { get; set; }
		public string Domain { get; set; }
		public string FileType { get; set; }
		public bool IsSuccess { get; set; }
		public bool IsDeleted { get; set; }

		public string YunUrl { get; set; }
		public string LocalUrl { get; set; }
		public string KeyStr { get; set; }
		public string File { get; set; }
		public string BucketName { get; set; }
		public string AccessId { get; set; }
		public string AccessKey { get; set; }
		public string SceretKey { get; set; }


	}
}
