﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WebMvc.App.Code.Extend.Yun
{
	public class UpInfo
	{
		public string path
		{
			get;
			set;
		}
		public int count
		{
			get;
			set;
		}
		public int flag
		{
			get;
			set;
		}
		public string accessKey
		{
			get;
			set;
		}
		public string sceretKey
		{
			get;
			set;
		}
		public string bucketName
		{
			get;
			set;
		}
		public string chooseYun
		{
			get;
			set;
		}
		public string accessId
		{
			get;
			set;
		}
		public string domain
		{
			get;
			set;
		}
		public string module
		{
			get;
			set;
		}
		
	}
}
