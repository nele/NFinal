﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace WebMvc.App.Code.Extend.Yun
{
	public class ParseUrl
	{
		//找到Views文件夹，找到.aspx文件
		//用正则匹配script得到数组，再循环数组匹配src得到第2个分组中的内容进行替换
		public static bool GetAspx(string path, string yunUrl)
		{
			int count = 0;
			int flag = 0;
			string[] filePaths = Directory.GetDirectories(path);
			

			foreach (string filePath in filePaths)
			{
				string[] ffPath = Directory.GetDirectories(filePath);

				//string[] ffFiles = Directory.GetFiles(filePath);

				#region ffFiles.Length>0
				//if (ffFiles.Length > 0)
				//{
				//	foreach (string file in ffFiles)
				//	{
				//		count++;
				//		string fileName = Path.GetFileName(file);
				//		string strFileType = Path.GetExtension(file);
				//		if (strFileType == ".aspx")
				//		{
				//			StreamReader sr = new StreamReader(file, Encoding.Default);							
				//			string aspxCode = sr.ReadToEnd();
				//			sr.Close();
							
				//			string scriptRegStr = @"<\s*script[^>]*>\s*<\s*/\s*script>";
				//			string srcRegStr = @"\s+src\s*=\s*""([^""]+)""(?:\s|>)";
				//				//"\s+src\s*=\s*" + @"([^" + "]+)" + @"(?:\s|>)";\s+src\s*=\s*"([^"]+)"(?:\s|>)
				//			string linkRegStr = @"<\s*link[^>]*\s*>";
				//			string hrefRegStr = @"\s+href\s*=\s*""([^""]+)""(?:\s|>)";
				//			//<link\s+[^>]+>	href="([^"]+)"
				//			int scriptNum = 0;
				//			List<string> scriptList = new List<string>();
				//			int linkNum = 0;
				//			List<string> linkList = new List<string>();
				//			//string aspxCodeR = aspxCode;
				//			while(true)
				//			{
				//				scriptNum++;
				//				linkNum++;
				//				Regex scriptReg = new Regex(scriptRegStr);
				//				Match scriptMatch = scriptReg.Match(aspxCode);
				//				//int scriptStart = scriptMatch.Index;

				//				Regex linkReg = new Regex(linkRegStr);
				//				Match linkMatch = linkReg.Match(aspxCode);
				//				//int linkStart = linkMatch.Index;

				//				if (!scriptMatch.Success && !linkMatch.Success)
				//				{
				//					break;
				//				}

				//				if (scriptMatch.Success)
				//				{
				//					string scriptCode = scriptMatch.Groups[0].Value;
				//					Regex srcReg = new Regex(srcRegStr);
				//					Match srcMatch = srcReg.Match(scriptCode);
				//					if (srcMatch.Success)
				//					{
				//						string srcCode = srcMatch.Groups[0].Value;
				//						string srcContent = srcMatch.Groups[1].Value;
				//						string newSrcContent = srcContent;
				//						//int srcStart = srcMatch.Index;
				//						if(!srcContent.Contains("http://"))
				//						{
				//							newSrcContent = yunUrl + srcContent;
				//						}
				//						//scriptList.Add("<script src='" + newSrcContent + "'></script>");
				//						scriptList.Add(string.Format(@"<script src=""{0}"" ></script>", newSrcContent));
				//						aspxCode = aspxCode.Replace(scriptCode, "scriptList[" + scriptNum + "]");
				//						//aspxCodeR = aspxCode.Replace(scriptReg.Match(aspxCode).Groups[0].Value, "scriptCode");
				//					}
				//				}

				//				if (linkMatch.Success)
				//				{
				//					string linkCode = linkMatch.Groups[0].Value;
				//					Regex hrefReg = new Regex(hrefRegStr);
				//					Match hrefMatch = hrefReg.Match(linkCode);
				//					if (hrefMatch.Success)
				//					{
				//						string hrefCode = hrefMatch.Groups[0].Value;
				//						string hrefContent = hrefMatch.Groups[1].Value;
				//						//int hrefStart = hrefMatch.Index;
				//						string newHrefContent = hrefContent;
				//						if(!hrefContent.Contains("http://"))
				//						{
				//							newHrefContent = yunUrl + hrefContent;
				//						}
				//						//linkList.Add(newHrefContent);
				//						//linkList.Add(@"<link rel=""stylesheet"" href='" + newHrefContent + "'>");
				//						linkList.Add(string.Format(@"<link rel=""{0}"" href=""{1}"">", "stylesheet", newHrefContent));
				//						aspxCode = aspxCode.Replace(linkCode, "linkList[" + linkNum + "]");
				//						//aspxCodeR = aspxCode.Replace(linkReg.Match(aspxCode).Groups[0].Value, "linkCode");
				//					}
				//				}								
				//			}
				//			for (int i = 1; i <= scriptList.Count; i++)
				//			{
				//				aspxCode = aspxCode.Replace("scriptList[" + i + "]", scriptList[i - 1]);
				//			}
				//			for (int i = 1; i <= linkList.Count; i++)
				//			{
				//				aspxCode = aspxCode.Replace("linkList[" + i + "]", linkList[i - 1]);
				//			}
				//			using (FileStream fsWrite = new FileStream(file, FileMode.OpenOrCreate, FileAccess.Write))
				//			{
				//				byte[] buffer = Encoding.Default.GetBytes(aspxCode);//new byte[1024 * 1024 * 1];
				//				fsWrite.Write(buffer, 0, buffer.Length);
				//			}
				//		}
				//	}
				//}
				#endregion


				#region ffPath.Length <= 0

				if (ffPath.Length <= 0)
				{
					string[] files = Directory.GetFiles(filePath);
					foreach (string file in files)
					{
						count++;
						string fileName = Path.GetFileName(file);
						string strFileType = Path.GetExtension(file);
						if (strFileType == ".aspx")
						{
							StreamReader sr = new StreamReader(file,Encoding.Default);
							
							string aspxCode = sr.ReadToEnd();
							sr.Close();
							
							string scriptRegStr = @"<\s*script[^>]*>\s*<\s*/\s*script>";
							string srcRegStr = @"\s+src\s*=\s*""([^""]+)""(?:\s|>)";
							string linkRegStr = @"<\s*link[^>]*/\s*>";
							string hrefRegStr = @"\s+href\s*=\s*""([^""]+)""(?:\s|>)";
							//string charsetStr = @"charset\s*=\s*([^""]*)";//charset\s*=\s*([^"]*)


							//Regex charsetReg = new Regex(charsetStr);
							//Match charsetMatch = charsetReg.Match(aspxCode);
							//Encoding charSetCode;
							//Encoding.charsetMatch.Groups[1].Value

							//if(charsetMatch.Success)
							//{
							//	byte b = Convert.ToByte(charsetMatch.Groups[1].Value);
							//	charSetCode=Encoding.Convert()
							//}

							int scriptNum = 0;
							List<string> scriptList = new List<string>();
							int linkNum = 0;
							List<string> linkList = new List<string>();

							while(true)
							{
								scriptNum++;
								linkNum++;
								Regex scriptReg = new Regex(scriptRegStr);
								Match scriptMatch = scriptReg.Match(aspxCode);

								Regex linkReg = new Regex(linkRegStr);
								Match linkMatch = linkReg.Match(aspxCode);

								if (!scriptMatch.Success && !linkMatch.Success)
								{
									break;
								}


								if (scriptMatch.Success)
								{
									string scriptCode = scriptMatch.Groups[0].Value;
									Regex srcReg = new Regex(srcRegStr);
									Match srcMatch = srcReg.Match(scriptCode);
									if (srcMatch.Success)
									{
										string srcCode = srcMatch.Groups[0].Value;
										string srcContent = srcMatch.Groups[1].Value;
										string newSrcContent = srcContent;
										if (!srcContent.Contains("http://"))
										{
											newSrcContent = yunUrl + srcContent;
										}
										scriptList.Add(string.Format(@"<script src=""{0}"" ></script>", newSrcContent));
										aspxCode = aspxCode.Replace(scriptCode, "scriptList[" + scriptNum + "]");
									}
								}

								if (linkMatch.Success)
								{
									string linkCode = linkMatch.Groups[0].Value;
									Regex hrefReg = new Regex(hrefRegStr);
									Match hrefMatch = hrefReg.Match(linkCode);
									if (hrefMatch.Success)
									{
										string hrefCode = hrefMatch.Groups[0].Value;
										string hrefContent = hrefMatch.Groups[1].Value;
										string newHrefContent = hrefContent;
										if (!hrefContent.Contains("http://"))
										{
											newHrefContent = yunUrl + hrefContent;
										}
										linkList.Add(string.Format(@"<link rel=""{0}"" href=""{1}"">", "stylesheet", newHrefContent));
										aspxCode = aspxCode.Replace(linkCode, "linkList[" + linkNum + "]");
									}
								}
							}

							for (int i = 1; i <= scriptList.Count; i++)
							{
								aspxCode = aspxCode.Replace("scriptList[" + i + "]", scriptList[i-1]);
							}
							for (int i = 1; i <= linkList.Count; i++)
							{
								aspxCode = aspxCode.Replace("linkList[" + i + "]", linkList[i - 1]);
							}

							using (FileStream fsWrite = new FileStream(file, FileMode.OpenOrCreate, FileAccess.Write))
							{
								byte[] buffer = Encoding.Default.GetBytes(aspxCode);
								fsWrite.Write(buffer, 0, buffer.Length);
							}
						}
					}
					
				}
				#endregion

				else
				{
					GetAspx(filePath, yunUrl);
				}
			}
			
			if (flag == count)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
	}
}
