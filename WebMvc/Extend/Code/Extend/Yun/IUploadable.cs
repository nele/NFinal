﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;

namespace WebMvc.App.Code.Extend.Yun
{
	public interface IUploadable
	{
		void Initial(string bucket, string key, string fname, string accessId, string accessKey, string secretKey);
		bool Upload(string bucket, string key, string fname, string accessId, string accessKey, string secretKey, int partSize);

		//void Initial(string bucket, string key);
		//bool Upload(string bucket, string key, string localFile, string secretKey, string accessKey);
		
	}
}
