﻿﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
namespace WebMvc
{
    public class Url
    {
		private static string __ParseUrl__(string __url__)
        {
            switch (__url__)
            {
				default: break;
            }
            return __url__;
        }
		public static string App_IndexController_Index()
        {
            string __url__=string.Format("/App/Index.html");
			__url__ = __ParseUrl__(__url__);
            return __url__;
        }
		public static string App_Mysql_update(string name,string pwd)
        {
            string __url__=string.Format("/App/Mysql/update/{0}/{1}17.php",name,pwd);
			__url__ = __ParseUrl__(__url__);
            return __url__;
        }
		public static string App_Mysql_insert(string name,string pwd)
        {
            string __url__=string.Format("/App/Mysql/insert/{0}/{1}17.php",name,pwd);
			__url__ = __ParseUrl__(__url__);
            return __url__;
        }
		public static string App_Mysql_delete(int id)
        {
            string __url__=string.Format("/App/Mysql/delete/{0}17.php",id);
			__url__ = __ParseUrl__(__url__);
            return __url__;
        }
		public static string App_Mysql_page(int p)
        {
            string __url__=string.Format("/App/Mysql/page/{0}15.php",p);
			__url__ = __ParseUrl__(__url__);
            return __url__;
        }
		public static string App_Mysql_queryAll()
        {
            string __url__=string.Format("/App/Mysql/queryAll.php");
			__url__ = __ParseUrl__(__url__);
            return __url__;
        }
		public static string App_Mysql_queryObject()
        {
            string __url__=string.Format("/App/Mysql/queryObject.php");
			__url__ = __ParseUrl__(__url__);
            return __url__;
        }
		public static string App_Mysql_queryRandom()
        {
            string __url__=string.Format("/App/Mysql/queryRandom.php");
			__url__ = __ParseUrl__(__url__);
            return __url__;
        }
		public static string App_Mysql_queryRow(int id)
        {
            string __url__=string.Format("/App/Mysql/queryRow/{0}19.php",id);
			__url__ = __ParseUrl__(__url__);
            return __url__;
        }
		public static string App_Mysql_queryTop(int top)
        {
            string __url__=string.Format("/App/Mysql/queryTop/{0}19.php",top);
			__url__ = __ParseUrl__(__url__);
            return __url__;
        }
		public static string App_Sqlite_update()
        {
            string __url__=string.Format("/App/Sqlite/update.html");
			__url__ = __ParseUrl__(__url__);
            return __url__;
        }
		public static string App_Sqlite_insert(string name,string pwd)
        {
            string __url__=string.Format("/App/Sqlite/insert/{0}/{1}18.php",name,pwd);
			__url__ = __ParseUrl__(__url__);
            return __url__;
        }
		public static string App_Sqlite_delete(int id)
        {
            string __url__=string.Format("/App/Sqlite/delete/{0}18.php",id);
			__url__ = __ParseUrl__(__url__);
            return __url__;
        }
		public static string App_Sqlite_page(int p)
        {
            string __url__=string.Format("/App/Sqlite/page-{0}16.json",p);
			__url__ = __ParseUrl__(__url__);
            return __url__;
        }
		public static string App_Sqlite_queryAll()
        {
            string __url__=string.Format("/App/Sqlite/queryAll.php");
			__url__ = __ParseUrl__(__url__);
            return __url__;
        }
		public static string App_Sqlite_queryObject()
        {
            string __url__=string.Format("/App/Sqlite/queryObject.php");
			__url__ = __ParseUrl__(__url__);
            return __url__;
        }
		public static string App_Sqlite_queryRandom()
        {
            string __url__=string.Format("/App/Sqlite/queryRandom.php");
			__url__ = __ParseUrl__(__url__);
            return __url__;
        }
		public static string App_Sqlite_queryRow(int id)
        {
            string __url__=string.Format("/App/Sqlite/queryRow/{0}20.php",id);
			__url__ = __ParseUrl__(__url__);
            return __url__;
        }
		public static string App_Sqlite_queryTop(int top)
        {
            string __url__=string.Format("/App/Sqlite/queryTop/{0}20.php",top);
			__url__ = __ParseUrl__(__url__);
            return __url__;
        }
		public static string App_SqliteBLL_queryAll()
        {
            string __url__=string.Format("/App/SqliteBLL/queryAll.php");
			__url__ = __ParseUrl__(__url__);
            return __url__;
        }
		public static string App_SqliteBLL_queryRandom()
        {
            string __url__=string.Format("/App/SqliteBLL/queryRandom.php");
			__url__ = __ParseUrl__(__url__);
            return __url__;
        }
		public static string App_SqliteCache_queryAll()
        {
            string __url__=string.Format("/App/SqliteCache/queryAll.php");
			__url__ = __ParseUrl__(__url__);
            return __url__;
        }
		public static string App_SqliteCache_queryRandom()
        {
            string __url__=string.Format("/App/SqliteCache/queryRandom.php");
			__url__ = __ParseUrl__(__url__);
            return __url__;
        }
		public static string App_SqliteCache_queryTop()
        {
            string __url__=string.Format("/App/SqliteCache/queryTop.php");
			__url__ = __ParseUrl__(__url__);
            return __url__;
        }
		public static string App_SqliteCache_queryRow(int id)
        {
            string __url__=string.Format("/App/SqliteCache/queryRow/{0}25.php",id);
			__url__ = __ParseUrl__(__url__);
            return __url__;
        }
		public static string App_SqliteCache_Page(int p)
        {
            string __url__=string.Format("/App/SqliteCache/Page/{0}21.php",p);
			__url__ = __ParseUrl__(__url__);
            return __url__;
        }
		public static string App_SqliteStruct_update(string name,string pwd)
        {
            string __url__=string.Format("/App/SqliteStruct/update/{0}/{1}24.php",name,pwd);
			__url__ = __ParseUrl__(__url__);
            return __url__;
        }
		public static string App_SqliteStruct_insert(string name,string pwd)
        {
            string __url__=string.Format("/App/SqliteStruct/insert/{0}/{1}24.php",name,pwd);
			__url__ = __ParseUrl__(__url__);
            return __url__;
        }
		public static string App_SqliteStruct_delete(int id)
        {
            string __url__=string.Format("/App/SqliteStruct/delete/{0}24.php",id);
			__url__ = __ParseUrl__(__url__);
            return __url__;
        }
		public static string App_SqliteStruct_page(int p)
        {
            string __url__=string.Format("/App/SqliteStruct/page/{0}22.php",p);
			__url__ = __ParseUrl__(__url__);
            return __url__;
        }
		public static string App_SqliteStruct_queryAll()
        {
            string __url__=string.Format("/App/SqliteStruct/queryAll.php");
			__url__ = __ParseUrl__(__url__);
            return __url__;
        }
		public static string App_SqliteStruct_queryObject()
        {
            string __url__=string.Format("/App/SqliteStruct/queryObject.php");
			__url__ = __ParseUrl__(__url__);
            return __url__;
        }
		public static string App_SqliteStruct_queryRandom()
        {
            string __url__=string.Format("/App/SqliteStruct/queryRandom.php");
			__url__ = __ParseUrl__(__url__);
            return __url__;
        }
		public static string App_SqliteStruct_queryRow(int id)
        {
            string __url__=string.Format("/App/SqliteStruct/queryRow/{0}26.php",id);
			__url__ = __ParseUrl__(__url__);
            return __url__;
        }
		public static string App_SqliteStruct_queryTop(int top)
        {
            string __url__=string.Format("/App/SqliteStruct/queryTop/{0}26.php",top);
			__url__ = __ParseUrl__(__url__);
            return __url__;
        }
		public static string App_SqliteTrans_update(string name,string pwd)
        {
            string __url__=string.Format("/App/SqliteTrans/update/{0}/{1}23.php",name,pwd);
			__url__ = __ParseUrl__(__url__);
            return __url__;
        }
		public static string App_SqliteTrans_insert(string name,string pwd)
        {
            string __url__=string.Format("/App/SqliteTrans/insert/{0}/{1}23.php",name,pwd);
			__url__ = __ParseUrl__(__url__);
            return __url__;
        }
		public static string App_SqliteTrans_delete(int id)
        {
            string __url__=string.Format("/App/SqliteTrans/delete/{0}23.php",id);
			__url__ = __ParseUrl__(__url__);
            return __url__;
        }
		public static string App_SqliteTrans_page(int p)
        {
            string __url__=string.Format("/App/SqliteTrans/page/{0}21.php",p);
			__url__ = __ParseUrl__(__url__);
            return __url__;
        }
		public static string App_SqliteTrans_queryAll()
        {
            string __url__=string.Format("/App/SqliteTrans/queryAll.php");
			__url__ = __ParseUrl__(__url__);
            return __url__;
        }
		public static string App_SqliteTrans_queryObject()
        {
            string __url__=string.Format("/App/SqliteTrans/queryObject.php");
			__url__ = __ParseUrl__(__url__);
            return __url__;
        }
		public static string App_SqliteTrans_queryRandom()
        {
            string __url__=string.Format("/App/SqliteTrans/queryRandom.php");
			__url__ = __ParseUrl__(__url__);
            return __url__;
        }
		public static string App_SqliteTrans_queryRow(int id)
        {
            string __url__=string.Format("/App/SqliteTrans/queryRow/{0}25.php",id);
			__url__ = __ParseUrl__(__url__);
            return __url__;
        }
		public static string App_SqliteTrans_queryTop(int top)
        {
            string __url__=string.Format("/App/SqliteTrans/queryTop/{0}25.php",top);
			__url__ = __ParseUrl__(__url__);
            return __url__;
        }
		public static string App_SqliteTransStruct_update(string name,string pwd)
        {
            string __url__=string.Format("/App/SqliteTransStruct/update/{0}/{1}29.php",name,pwd);
			__url__ = __ParseUrl__(__url__);
            return __url__;
        }
		public static string App_SqliteTransStruct_insert(string name,string pwd)
        {
            string __url__=string.Format("/App/SqliteTransStruct/insert/{0}/{1}29.php",name,pwd);
			__url__ = __ParseUrl__(__url__);
            return __url__;
        }
		public static string App_SqliteTransStruct_delete(int id)
        {
            string __url__=string.Format("/App/SqliteTransStruct/delete/{0}29.php",id);
			__url__ = __ParseUrl__(__url__);
            return __url__;
        }
		public static string App_SqliteTransStruct_page(int p)
        {
            string __url__=string.Format("/App/SqliteTransStruct/page/{0}27.php",p);
			__url__ = __ParseUrl__(__url__);
            return __url__;
        }
		public static string App_SqliteTransStruct_queryAll()
        {
            string __url__=string.Format("/App/SqliteTransStruct/queryAll.php");
			__url__ = __ParseUrl__(__url__);
            return __url__;
        }
		public static string App_SqliteTransStruct_queryObject()
        {
            string __url__=string.Format("/App/SqliteTransStruct/queryObject.php");
			__url__ = __ParseUrl__(__url__);
            return __url__;
        }
		public static string App_SqliteTransStruct_queryRandom()
        {
            string __url__=string.Format("/App/SqliteTransStruct/queryRandom.php");
			__url__ = __ParseUrl__(__url__);
            return __url__;
        }
		public static string App_SqliteTransStruct_queryRow(int id)
        {
            string __url__=string.Format("/App/SqliteTransStruct/queryRow/{0}31.php",id);
			__url__ = __ParseUrl__(__url__);
            return __url__;
        }
		public static string App_SqliteTransStruct_queryTop(int top)
        {
            string __url__=string.Format("/App/SqliteTransStruct/queryTop/{0}31.php",top);
			__url__ = __ParseUrl__(__url__);
            return __url__;
        }
		public static string App_SqlServer_update(string name,string pwd)
        {
            string __url__=string.Format("/App/SqlServer/update/{0}/{1}21.php",name,pwd);
			__url__ = __ParseUrl__(__url__);
            return __url__;
        }
		public static string App_SqlServer_insert(string name,string pwd)
        {
            string __url__=string.Format("/App/SqlServer/insert/{0}/{1}21.php",name,pwd);
			__url__ = __ParseUrl__(__url__);
            return __url__;
        }
		public static string App_SqlServer_delete(int id)
        {
            string __url__=string.Format("/App/SqlServer/delete/{0}21.php",id);
			__url__ = __ParseUrl__(__url__);
            return __url__;
        }
		public static string App_SqlServer_page(int p)
        {
            string __url__=string.Format("/App/SqlServer/page/{0}19.php",p);
			__url__ = __ParseUrl__(__url__);
            return __url__;
        }
		public static string App_SqlServer_queryAll()
        {
            string __url__=string.Format("/App/SqlServer/queryAll.php");
			__url__ = __ParseUrl__(__url__);
            return __url__;
        }
		public static string App_SqlServer_queryObject()
        {
            string __url__=string.Format("/App/SqlServer/queryObject.php");
			__url__ = __ParseUrl__(__url__);
            return __url__;
        }
		public static string App_SqlServer_queryRandom()
        {
            string __url__=string.Format("/App/SqlServer/queryRandom.php");
			__url__ = __ParseUrl__(__url__);
            return __url__;
        }
		public static string App_SqlServer_queryRow(int id)
        {
            string __url__=string.Format("/App/SqlServer/queryRow/{0}23.php",id);
			__url__ = __ParseUrl__(__url__);
            return __url__;
        }
		public static string App_SqlServer_queryTop(int top)
        {
            string __url__=string.Format("/App/SqlServer/queryTop/{0}23.php",top);
			__url__ = __ParseUrl__(__url__);
            return __url__;
        }
		public static string App_SqlServerStruct_update(string name,string pwd)
        {
            string __url__=string.Format("/App/SqlServerStruct/update/{0}/{1}27.php",name,pwd);
			__url__ = __ParseUrl__(__url__);
            return __url__;
        }
		public static string App_SqlServerStruct_insert(string name,string pwd)
        {
            string __url__=string.Format("/App/SqlServerStruct/insert/{0}/{1}27.php",name,pwd);
			__url__ = __ParseUrl__(__url__);
            return __url__;
        }
		public static string App_SqlServerStruct_delete(int id)
        {
            string __url__=string.Format("/App/SqlServerStruct/delete/{0}27.php",id);
			__url__ = __ParseUrl__(__url__);
            return __url__;
        }
		public static string App_SqlServerStruct_page(int p)
        {
            string __url__=string.Format("/App/SqlServerStruct/page/{0}25.php",p);
			__url__ = __ParseUrl__(__url__);
            return __url__;
        }
		public static string App_SqlServerStruct_queryAll()
        {
            string __url__=string.Format("/App/SqlServerStruct/queryAll.php");
			__url__ = __ParseUrl__(__url__);
            return __url__;
        }
		public static string App_SqlServerStruct_queryObject()
        {
            string __url__=string.Format("/App/SqlServerStruct/queryObject.php");
			__url__ = __ParseUrl__(__url__);
            return __url__;
        }
		public static string App_SqlServerStruct_queryRandom()
        {
            string __url__=string.Format("/App/SqlServerStruct/queryRandom.php");
			__url__ = __ParseUrl__(__url__);
            return __url__;
        }
		public static string App_SqlServerStruct_queryRow(int id)
        {
            string __url__=string.Format("/App/SqlServerStruct/queryRow/{0}29.php",id);
			__url__ = __ParseUrl__(__url__);
            return __url__;
        }
		public static string App_SqlServerStruct_queryTop(int top)
        {
            string __url__=string.Format("/App/SqlServerStruct/queryTop/{0}29.php",top);
			__url__ = __ParseUrl__(__url__);
            return __url__;
        }
		public static string App_SqlServerTrans_update(string name,string pwd)
        {
            string __url__=string.Format("/App/SqlServerTrans/update/{0}/{1}26.php",name,pwd);
			__url__ = __ParseUrl__(__url__);
            return __url__;
        }
		public static string App_SqlServerTrans_insert(string name,string pwd)
        {
            string __url__=string.Format("/App/SqlServerTrans/insert/{0}/{1}26.php",name,pwd);
			__url__ = __ParseUrl__(__url__);
            return __url__;
        }
		public static string App_SqlServerTrans_delete(int id)
        {
            string __url__=string.Format("/App/SqlServerTrans/delete/{0}26.php",id);
			__url__ = __ParseUrl__(__url__);
            return __url__;
        }
		public static string App_SqlServerTrans_page(int p)
        {
            string __url__=string.Format("/App/SqlServerTrans/page/{0}24.php",p);
			__url__ = __ParseUrl__(__url__);
            return __url__;
        }
		public static string App_SqlServerTrans_queryAll()
        {
            string __url__=string.Format("/App/SqlServerTrans/queryAll.php");
			__url__ = __ParseUrl__(__url__);
            return __url__;
        }
		public static string App_SqlServerTrans_queryObject()
        {
            string __url__=string.Format("/App/SqlServerTrans/queryObject.php");
			__url__ = __ParseUrl__(__url__);
            return __url__;
        }
		public static string App_SqlServerTrans_queryRandom()
        {
            string __url__=string.Format("/App/SqlServerTrans/queryRandom.php");
			__url__ = __ParseUrl__(__url__);
            return __url__;
        }
		public static string App_SqlServerTrans_queryRow(int id)
        {
            string __url__=string.Format("/App/SqlServerTrans/queryRow/{0}28.php",id);
			__url__ = __ParseUrl__(__url__);
            return __url__;
        }
		public static string App_SqlServerTrans_queryTop(int top)
        {
            string __url__=string.Format("/App/SqlServerTrans/queryTop/{0}28.php",top);
			__url__ = __ParseUrl__(__url__);
            return __url__;
        }
		public static string App_Public_Success(string message,string url,int second)
        {
            string __url__=string.Format("/App/Public/Success/{0}/{1}/{2}19.php",message,url,second);
			__url__ = __ParseUrl__(__url__);
            return __url__;
        }
		public static string App_Public_Error(string message,string url,int second)
        {
            string __url__=string.Format("/App/Public/Error/{0}/{1}/{2}17.php",message,url,second);
			__url__ = __ParseUrl__(__url__);
            return __url__;
        }
		public static string App_Public_Header()
        {
            string __url__=string.Format("/App/Public/Header.php");
			__url__ = __ParseUrl__(__url__);
            return __url__;
        }
		public static string App_Public_Footer()
        {
            string __url__=string.Format("/App/Public/Footer.php");
			__url__ = __ParseUrl__(__url__);
            return __url__;
        }
		public static string App_Public_Navigator()
        {
            string __url__=string.Format("/App/Public/Navigator.php");
			__url__ = __ParseUrl__(__url__);
            return __url__;
        }
		public static string App_VCode_GetVerifyImage()
        {
            string __url__=string.Format("/App/VCode/GetVerifyImage.php");
			__url__ = __ParseUrl__(__url__);
            return __url__;
        }
		public static string App_VCode_VCheck()
        {
            string __url__=string.Format("/App/VCode/VCheck.php");
			__url__ = __ParseUrl__(__url__);
            return __url__;
        }
    }
}
