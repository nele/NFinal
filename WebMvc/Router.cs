﻿﻿﻿//======================================================================
//
//        Copyright : Zhengzhou Strawberry Computer Technology Co.,LTD.
//        All rights reserved
//        
//        Application:NFinal MVC framework
//        Filename :Router.cs
//        Description :路由类
//
//        created by Lucas at  2015-10-15`
//     
//        WebSite:http://www.nfinal.com
//
//======================================================================
using System;
using System.Collections.Generic;
using System.Web;
using System.Threading;
using System.Globalization;
using System.Collections.Specialized;
using System.Text.RegularExpressions;
namespace WebMvc
{
    public static class Router
    {
        public static string GetString(string val, string de)
        {
            return string.IsNullOrEmpty(val) ? de : val;
        }
        public static string UCFirst(string val)
        {
            CultureInfo cultureInfo = Thread.CurrentThread.CurrentCulture;
            return cultureInfo.TextInfo.ToTitleCase(val);
        }
		 private static void Callback(IAsyncResult result)
        {
            //结束异步写入
            System.IO.FileStream stream = (System.IO.FileStream)result.AsyncState;
            stream.EndWrite(result);
            stream.Close();
        }
		//设置浏览器缓存
        public static void SetBrowerCache(HttpContext context,int optimizing,int seconds)
        {
            //浏览器缓存
            if ((optimizing & 224) != 0)
            {
                context.Response.Cache.SetCacheability(HttpCacheability.Public);
                //NotModify
                if ((optimizing & 32) != 0)
                {
                    context.Response.Cache.SetLastModifiedFromFileDependencies();
                }
                //Expires
                else if ((optimizing & 64) != 0)
                {
                    context.Response.Cache.SetExpires(DateTime.Now.AddSeconds(seconds));
                }
                //NoExpires
                else if ((optimizing & 128) != 0)
                {
                }
            }
            else
            {
                //不进行缓存
                context.Response.Cache.SetNoStore();
            }
        }
        //设置服务器缓存
        public static void SetServerCacheAndOutPut(HttpContext context, string app, string urlWithOutAppAndExt, string url, byte[] buffer, int optimizing, int seconds)
        {
            //服务器缓存
            if ((optimizing & 14) != 0)
            {
                //FileDependency
                if ((optimizing & 2) != 0)
                {
                    string fileName = context.Server.MapPath(app + "/HTML" + urlWithOutAppAndExt + ".html");
                    //将HTML写入静态文件
                    if (!System.IO.File.Exists(fileName))
                    {
                        string dir = System.IO.Path.GetDirectoryName(fileName);
                        if (!System.IO.Directory.Exists(dir))
                        {
                            System.IO.Directory.CreateDirectory(dir);
                        }
                        System.IO.FileStream file = new System.IO.FileStream(fileName, System.IO.FileMode.Create, System.IO.FileAccess.Write, System.IO.FileShare.ReadWrite, 1024, true);
                        file.BeginWrite(buffer, 0, buffer.Length, new AsyncCallback(Callback), file);
                    }
                    //将HTML插入新的缓存
                    System.Web.Caching.CacheDependency dep = new System.Web.Caching.CacheDependency(fileName);
                    HttpRuntime.Cache.Insert(url, buffer, dep, System.Web.Caching.Cache.NoAbsoluteExpiration, System.Web.Caching.Cache.NoSlidingExpiration);
                }
                //AbsoluteExpiration
                else if ((optimizing & 4) != 0)
                {
                    HttpRuntime.Cache.Insert(url, buffer, null, DateTime.Now.AddSeconds(seconds), System.Web.Caching.Cache.NoSlidingExpiration);
                }
                //SlidingExpiration
                else if ((optimizing & 8) != 0)
                {
                    HttpRuntime.Cache.Insert(url, buffer, null, System.Web.Caching.Cache.NoAbsoluteExpiration, new TimeSpan(0, 0, seconds));
                }
            }
            //直接输出
            context.Response.OutputStream.Write(buffer, 0, buffer.Length);
            context.Response.OutputStream.Close();
        }
        //写入缓存
        public static bool WriteCache(HttpContext context,string url,int optimizing)
        {
            object cache = null;
            //服务器缓存
            if ((optimizing & 14) != 0)
            {
                cache = HttpRuntime.Cache.Get(url);
                //如果缓存存在则直接输出.
                if (cache != null)
                {
                    System.IO.Stream stream = null;
                    if ((optimizing & 1536) != 0)
                    {
                        if ((optimizing & 512) != 0)
                        {
                            context.Response.AppendHeader("Content-encoding", "gzip");
                        }
                        else if ((optimizing & 1024) != 0)
                        {
                            context.Response.AppendHeader("Content-encoding", "deflate");
                        }
                    }
                    byte[] buffer= (byte[])cache;
                    context.Response.OutputStream.Write(buffer, 0, buffer.Length);
                    context.Response.OutputStream.Close();
                    return true;
                }
            }
            return false;
        }
        /// <summary>
        /// 计算actionUrl
        /// </summary>
        /// <param name="requestedPath"></param>
        /// <returns></returns>
        private static string GetActionUrl(string requestedPath)
        {
            int position = requestedPath.LastIndexOf('.');
            int pointer = 0;
            string actionUrl = "";
            if (position > 0)
            {
                bool hasGet = int.TryParse(requestedPath.Substring(position - 2, 2), out pointer);
                if (hasGet)
                {
                    actionUrl = requestedPath.Substring(0, pointer + 1);
                }
                else
                {
                    actionUrl = requestedPath;
                }
            }
            return actionUrl;
        }
        public static void Run(HttpContext context, string requestedPath)
        {
            //取出action标识
            int find = 0;
            string f = requestedPath.Substring(requestedPath.Length - 8, 5);
            int.TryParse(requestedPath.Substring(requestedPath.Length - 8, 5), out find);
            //去掉00000.htm后缀,还原requestPath
            requestedPath = requestedPath.Substring(0, requestedPath.Length - 8);
            string[] hostName = context.Request.Url.Host.ToString().Split('.');
            string subdomain = hostName.Length == 3 ? hostName[0] : "www";
            string[] parameters = null;
            NameValueCollection get = new NameValueCollection();
			//提取form参数
            if (context.Request.Form.Count > 0)
            {
                for (int i = 0; i < context.Request.Form.Count; i++)
                {
                    get.Add(context.Request.Form.Keys[i], context.Request.Form[i]);
                }
            }
			//提取URL?后的参数
			if (context.Request.QueryString.Count > 0)
            {
                for (int i = 0; i < context.Request.QueryString.Count; i++)
                {
                    get.Add(context.Request.QueryString.Keys[i], context.Request.QueryString[i]);
                }
            }
			//提取MethodType
			NFinal.MethodType methodType = NFinal.MethodType.NONE;
            switch (context.Request.HttpMethod)
            {
                case "POST":methodType = NFinal.MethodType.POST; break;
                case "GET":methodType = NFinal.MethodType.GET; break;
                case "DELETE":methodType = NFinal.MethodType.DELETE; break;
                case "PUT":methodType = NFinal.MethodType.PUT; break;
                case "AJAX":methodType = NFinal.MethodType.AJAX; break;
                default:methodType = NFinal.MethodType.NONE; break;
            }
            //获取actionUrl,用于获取参数
            string actionUrl = GetActionUrl(requestedPath);
            switch (find)
            {
					case 1:
					{
						context.Response.ContentType = "text/html;charset=utf-8";
						Regex regParameters = new Regex("/App/Index.html");
                        Match matParameters = regParameters.Match(requestedPath);
						App.Web.Default.IndexController.IndexAction control= new App.Web.Default.IndexController.IndexAction(context);
						control._methodType = methodType;
						control._context = context;
						control._subdomain = subdomain;
						control._url=requestedPath;
						control._get = get;
						control._app="/App";
						control.Before();
						control.Index();
						control.After();
						control.Close();
						context.Response.End();
					}break;
					case 2:
					{
						context.Response.ContentType = "text/html";
						Regex regParameters = new Regex("/App/Mysql/update/([\\S]+)/([\\S]+)17.php");
                        Match matParameters = regParameters.Match(requestedPath);
						if (get["name"] == null)
                        {
                            get.Add("name", matParameters.Groups[1].Value);
                        }
                        else
                        {
                            get["name"] = matParameters.Groups[1].Value;
                        }
						if (get["pwd"] == null)
                        {
                            get.Add("pwd", matParameters.Groups[2].Value);
                        }
                        else
                        {
                            get["pwd"] = matParameters.Groups[2].Value;
                        }
						App.Web.Default.Mysql.updateAction control= new App.Web.Default.Mysql.updateAction(context);
							string name=get["name"]==null?null:get["name"];
							string pwd=get["pwd"]==null?null:get["pwd"];
						control._methodType = methodType;
						control._context = context;
						control._subdomain = subdomain;
						control._url=requestedPath;
						control._get = get;
						control._app="/App";
						control.Before();
						control.update(name,pwd);
						control.After();
						control.Close();
						context.Response.End();
					}break;
					case 3:
					{
						context.Response.ContentType = "text/html";
						Regex regParameters = new Regex("/App/Mysql/insert/([\\S]+)/([\\S]+)17.php");
                        Match matParameters = regParameters.Match(requestedPath);
						if (get["name"] == null)
                        {
                            get.Add("name", matParameters.Groups[1].Value);
                        }
                        else
                        {
                            get["name"] = matParameters.Groups[1].Value;
                        }
						if (get["pwd"] == null)
                        {
                            get.Add("pwd", matParameters.Groups[2].Value);
                        }
                        else
                        {
                            get["pwd"] = matParameters.Groups[2].Value;
                        }
						App.Web.Default.Mysql.insertAction control= new App.Web.Default.Mysql.insertAction(context);
							string name=get["name"]==null?null:get["name"];
							string pwd=get["pwd"]==null?null:get["pwd"];
						control._methodType = methodType;
						control._context = context;
						control._subdomain = subdomain;
						control._url=requestedPath;
						control._get = get;
						control._app="/App";
						control.Before();
						control.insert(name,pwd);
						control.After();
						control.Close();
						context.Response.End();
					}break;
					case 4:
					{
						context.Response.ContentType = "text/html";
						Regex regParameters = new Regex("/App/Mysql/delete/([\\S]+)17.php");
                        Match matParameters = regParameters.Match(requestedPath);
						if (get["id"] == null)
                        {
                            get.Add("id", matParameters.Groups[1].Value);
                        }
                        else
                        {
                            get["id"] = matParameters.Groups[1].Value;
                        }
						App.Web.Default.Mysql.deleteAction control= new App.Web.Default.Mysql.deleteAction(context);
							int id=int.TryParse(get["id"],out id)?id:0;
						control._methodType = methodType;
						control._context = context;
						control._subdomain = subdomain;
						control._url=requestedPath;
						control._get = get;
						control._app="/App";
						control.Before();
						control.delete(id);
						control.After();
						control.Close();
						context.Response.End();
					}break;
					case 5:
					{
						context.Response.ContentType = "text/html";
						Regex regParameters = new Regex("/App/Mysql/page/([\\S]+)15.php");
                        Match matParameters = regParameters.Match(requestedPath);
						if (get["p"] == null)
                        {
                            get.Add("p", matParameters.Groups[1].Value);
                        }
                        else
                        {
                            get["p"] = matParameters.Groups[1].Value;
                        }
						App.Web.Default.Mysql.pageAction control= new App.Web.Default.Mysql.pageAction(context);
							int p=int.TryParse(get["p"],out p)?p:0;
						control._methodType = methodType;
						control._context = context;
						control._subdomain = subdomain;
						control._url=requestedPath;
						control._get = get;
						control._app="/App";
						control.Before();
						control.page(p);
						control.After();
						control.Close();
						context.Response.End();
					}break;
					case 6:
					{
						context.Response.ContentType = "text/html";
						Regex regParameters = new Regex("/App/Mysql/queryAll.php");
                        Match matParameters = regParameters.Match(requestedPath);
						App.Web.Default.Mysql.queryAllAction control= new App.Web.Default.Mysql.queryAllAction(context);
						control._methodType = methodType;
						control._context = context;
						control._subdomain = subdomain;
						control._url=requestedPath;
						control._get = get;
						control._app="/App";
						control.Before();
						control.queryAll();
						control.After();
						control.Close();
						context.Response.End();
					}break;
					case 7:
					{
						context.Response.ContentType = "text/html";
						Regex regParameters = new Regex("/App/Mysql/queryObject.php");
                        Match matParameters = regParameters.Match(requestedPath);
						App.Web.Default.Mysql.queryObjectAction control= new App.Web.Default.Mysql.queryObjectAction(context);
						control._methodType = methodType;
						control._context = context;
						control._subdomain = subdomain;
						control._url=requestedPath;
						control._get = get;
						control._app="/App";
						control.Before();
						control.queryObject();
						control.After();
						control.Close();
						context.Response.End();
					}break;
					case 8:
					{
						context.Response.ContentType = "text/html";
						Regex regParameters = new Regex("/App/Mysql/queryRandom.php");
                        Match matParameters = regParameters.Match(requestedPath);
						App.Web.Default.Mysql.queryRandomAction control= new App.Web.Default.Mysql.queryRandomAction(context);
						control._methodType = methodType;
						control._context = context;
						control._subdomain = subdomain;
						control._url=requestedPath;
						control._get = get;
						control._app="/App";
						control.Before();
						control.queryRandom();
						control.After();
						control.Close();
						context.Response.End();
					}break;
					case 9:
					{
						context.Response.ContentType = "text/html";
						Regex regParameters = new Regex("/App/Mysql/queryRow/([\\S]+)19.php");
                        Match matParameters = regParameters.Match(requestedPath);
						if (get["id"] == null)
                        {
                            get.Add("id", matParameters.Groups[1].Value);
                        }
                        else
                        {
                            get["id"] = matParameters.Groups[1].Value;
                        }
						App.Web.Default.Mysql.queryRowAction control= new App.Web.Default.Mysql.queryRowAction(context);
							int id=int.TryParse(get["id"],out id)?id:0;
						control._methodType = methodType;
						control._context = context;
						control._subdomain = subdomain;
						control._url=requestedPath;
						control._get = get;
						control._app="/App";
						control.Before();
						control.queryRow(id);
						control.After();
						control.Close();
						context.Response.End();
					}break;
					case 10:
					{
						context.Response.ContentType = "text/html";
						Regex regParameters = new Regex("/App/Mysql/queryTop/([\\S]+)19.php");
                        Match matParameters = regParameters.Match(requestedPath);
						if (get["top"] == null)
                        {
                            get.Add("top", matParameters.Groups[1].Value);
                        }
                        else
                        {
                            get["top"] = matParameters.Groups[1].Value;
                        }
						App.Web.Default.Mysql.queryTopAction control= new App.Web.Default.Mysql.queryTopAction(context);
							int top=int.TryParse(get["top"],out top)?top:0;
						control._methodType = methodType;
						control._context = context;
						control._subdomain = subdomain;
						control._url=requestedPath;
						control._get = get;
						control._app="/App";
						control.Before();
						control.queryTop(top);
						control.After();
						control.Close();
						context.Response.End();
					}break;
					case 11:
					{
						context.Response.ContentType = "text/html;charset=utf-8";
						Regex regParameters = new Regex("/App/Sqlite/update.html");
                        Match matParameters = regParameters.Match(requestedPath);
						if(!(  (get["name"].Length <= 12) 
							 && (get["name"]!=null) 
							)){break;}
						App.Web.Default.Sqlite.updateAction control= new App.Web.Default.Sqlite.updateAction(context);
							string name=get["name"]==null?null:get["name"];
							string pwd=get["pwd"]==null?null:get["pwd"];
						control._methodType = methodType;
						control._context = context;
						control._subdomain = subdomain;
						control._url=requestedPath;
						control._get = get;
						control._app="/App";
						control.Before();
						control.update(name,pwd);
						control.After();
						control.Close();
						context.Response.End();
					}break;
					case 12:
					{
						context.Response.ContentType = "text/html";
						Regex regParameters = new Regex("/App/Sqlite/insert/([\\S]+)/([\\S]+)18.php");
                        Match matParameters = regParameters.Match(requestedPath);
						if (get["name"] == null)
                        {
                            get.Add("name", matParameters.Groups[1].Value);
                        }
                        else
                        {
                            get["name"] = matParameters.Groups[1].Value;
                        }
						if (get["pwd"] == null)
                        {
                            get.Add("pwd", matParameters.Groups[2].Value);
                        }
                        else
                        {
                            get["pwd"] = matParameters.Groups[2].Value;
                        }
						App.Web.Default.Sqlite.insertAction control= new App.Web.Default.Sqlite.insertAction(context);
							string name=get["name"]==null?null:get["name"];
							string pwd=get["pwd"]==null?null:get["pwd"];
						control._methodType = methodType;
						control._context = context;
						control._subdomain = subdomain;
						control._url=requestedPath;
						control._get = get;
						control._app="/App";
						control.Before();
						control.insert(name,pwd);
						control.After();
						control.Close();
						context.Response.End();
					}break;
					case 13:
					{
						context.Response.ContentType = "text/html";
						Regex regParameters = new Regex("/App/Sqlite/delete/([\\S]+)18.php");
                        Match matParameters = regParameters.Match(requestedPath);
						if (get["id"] == null)
                        {
                            get.Add("id", matParameters.Groups[1].Value);
                        }
                        else
                        {
                            get["id"] = matParameters.Groups[1].Value;
                        }
						App.Web.Default.Sqlite.deleteAction control= new App.Web.Default.Sqlite.deleteAction(context);
							int id=int.TryParse(get["id"],out id)?id:0;
						control._methodType = methodType;
						control._context = context;
						control._subdomain = subdomain;
						control._url=requestedPath;
						control._get = get;
						control._app="/App";
						control.Before();
						control.delete(id);
						control.After();
						control.Close();
						context.Response.End();
					}break;
					case 14:
					{
						context.Response.ContentType = "text/html;charset=utf-8";
						Regex regParameters = new Regex("/App/Sqlite/page-([\\S]+)16.json");
                        Match matParameters = regParameters.Match(requestedPath);
						if (get["p"] == null)
                        {
                            get.Add("p", matParameters.Groups[1].Value);
                        }
                        else
                        {
                            get["p"] = matParameters.Groups[1].Value;
                        }
						App.Web.Default.Sqlite.pageAction control= new App.Web.Default.Sqlite.pageAction(context);
							int p=int.TryParse(get["p"],out p)?p:0;
						control._methodType = methodType;
						control._context = context;
						control._subdomain = subdomain;
						control._url=requestedPath;
						control._get = get;
						control._app="/App";
						control.Before();
						control.page(p);
						control.After();
						control.Close();
						context.Response.End();
					}break;
					case 15:
					{
						context.Response.ContentType = "text/html";
						Regex regParameters = new Regex("/App/Sqlite/queryAll.php");
                        Match matParameters = regParameters.Match(requestedPath);
						App.Web.Default.Sqlite.queryAllAction control= new App.Web.Default.Sqlite.queryAllAction(context);
						control._methodType = methodType;
						control._context = context;
						control._subdomain = subdomain;
						control._url=requestedPath;
						control._get = get;
						control._app="/App";
						control.Before();
						control.queryAll();
						control.After();
						control.Close();
						context.Response.End();
					}break;
					case 16:
					{
						context.Response.ContentType = "text/html";
						Regex regParameters = new Regex("/App/Sqlite/queryObject.php");
                        Match matParameters = regParameters.Match(requestedPath);
						App.Web.Default.Sqlite.queryObjectAction control= new App.Web.Default.Sqlite.queryObjectAction(context);
						control._methodType = methodType;
						control._context = context;
						control._subdomain = subdomain;
						control._url=requestedPath;
						control._get = get;
						control._app="/App";
						control.Before();
						control.queryObject();
						control.After();
						control.Close();
						context.Response.End();
					}break;
					case 17:
					{
						context.Response.ContentType = "text/html";
						Regex regParameters = new Regex("/App/Sqlite/queryRandom.php");
                        Match matParameters = regParameters.Match(requestedPath);
						App.Web.Default.Sqlite.queryRandomAction control= new App.Web.Default.Sqlite.queryRandomAction(context);
						control._methodType = methodType;
						control._context = context;
						control._subdomain = subdomain;
						control._url=requestedPath;
						control._get = get;
						control._app="/App";
						control.Before();
						control.queryRandom();
						control.After();
						control.Close();
						context.Response.End();
					}break;
					case 18:
					{
						context.Response.ContentType = "text/html";
						Regex regParameters = new Regex("/App/Sqlite/queryRow/([\\S]+)20.php");
                        Match matParameters = regParameters.Match(requestedPath);
						if (get["id"] == null)
                        {
                            get.Add("id", matParameters.Groups[1].Value);
                        }
                        else
                        {
                            get["id"] = matParameters.Groups[1].Value;
                        }
						App.Web.Default.Sqlite.queryRowAction control= new App.Web.Default.Sqlite.queryRowAction(context);
							int id=int.TryParse(get["id"],out id)?id:0;
						control._methodType = methodType;
						control._context = context;
						control._subdomain = subdomain;
						control._url=requestedPath;
						control._get = get;
						control._app="/App";
						control.Before();
						control.queryRow(id);
						control.After();
						control.Close();
						context.Response.End();
					}break;
					case 19:
					{
						context.Response.ContentType = "text/html";
						Regex regParameters = new Regex("/App/Sqlite/queryTop/([\\S]+)20.php");
                        Match matParameters = regParameters.Match(requestedPath);
						if (get["top"] == null)
                        {
                            get.Add("top", matParameters.Groups[1].Value);
                        }
                        else
                        {
                            get["top"] = matParameters.Groups[1].Value;
                        }
						App.Web.Default.Sqlite.queryTopAction control= new App.Web.Default.Sqlite.queryTopAction(context);
							int top=int.TryParse(get["top"],out top)?top:0;
						control._methodType = methodType;
						control._context = context;
						control._subdomain = subdomain;
						control._url=requestedPath;
						control._get = get;
						control._app="/App";
						control.Before();
						control.queryTop(top);
						control.After();
						control.Close();
						context.Response.End();
					}break;
					case 20:
					{
						context.Response.ContentType = "text/html";
						Regex regParameters = new Regex("/App/SqliteBLL/queryAll.php");
                        Match matParameters = regParameters.Match(requestedPath);
						App.Web.Default.SqliteBLL.queryAllAction control= new App.Web.Default.SqliteBLL.queryAllAction(context);
						control._methodType = methodType;
						control._context = context;
						control._subdomain = subdomain;
						control._url=requestedPath;
						control._get = get;
						control._app="/App";
						control.Before();
						control.queryAll();
						control.After();
						control.Close();
						context.Response.End();
					}break;
					case 21:
					{
						context.Response.ContentType = "text/html";
						Regex regParameters = new Regex("/App/SqliteBLL/queryRandom.php");
                        Match matParameters = regParameters.Match(requestedPath);
						App.Web.Default.SqliteBLL.queryRandomAction control= new App.Web.Default.SqliteBLL.queryRandomAction(context);
						control._methodType = methodType;
						control._context = context;
						control._subdomain = subdomain;
						control._url=requestedPath;
						control._get = get;
						control._app="/App";
						control.Before();
						control.queryRandom();
						control.After();
						control.Close();
						context.Response.End();
					}break;
					case 22:
					{
						context.Response.ContentType = "text/html";
						Regex regParameters = new Regex("/App/SqliteCache/queryAll.php");
                        Match matParameters = regParameters.Match(requestedPath);
						App.Web.Default.SqliteCache.queryAllAction control= new App.Web.Default.SqliteCache.queryAllAction(context);
						control._methodType = methodType;
						control._context = context;
						control._subdomain = subdomain;
						control._url=requestedPath;
						control._get = get;
						control._app="/App";
						control.Before();
						control.queryAll();
						control.After();
						control.Close();
						context.Response.End();
					}break;
					case 23:
					{
						context.Response.ContentType = "text/html";
						Regex regParameters = new Regex("/App/SqliteCache/queryRandom.php");
                        Match matParameters = regParameters.Match(requestedPath);
						App.Web.Default.SqliteCache.queryRandomAction control= new App.Web.Default.SqliteCache.queryRandomAction(context);
						control._methodType = methodType;
						control._context = context;
						control._subdomain = subdomain;
						control._url=requestedPath;
						control._get = get;
						control._app="/App";
						control.Before();
						control.queryRandom();
						control.After();
						control.Close();
						context.Response.End();
					}break;
					case 24:
					{
						context.Response.ContentType = "text/html";
						Regex regParameters = new Regex("/App/SqliteCache/queryTop.php");
                        Match matParameters = regParameters.Match(requestedPath);
						App.Web.Default.SqliteCache.queryTopAction control= new App.Web.Default.SqliteCache.queryTopAction(context);
						control._methodType = methodType;
						control._context = context;
						control._subdomain = subdomain;
						control._url=requestedPath;
						control._get = get;
						control._app="/App";
						control.Before();
						control.queryTop();
						control.After();
						control.Close();
						context.Response.End();
					}break;
					case 25:
					{
						context.Response.ContentType = "text/html";
						Regex regParameters = new Regex("/App/SqliteCache/queryRow/([\\S]+)25.php");
                        Match matParameters = regParameters.Match(requestedPath);
						if (get["id"] == null)
                        {
                            get.Add("id", matParameters.Groups[1].Value);
                        }
                        else
                        {
                            get["id"] = matParameters.Groups[1].Value;
                        }
						App.Web.Default.SqliteCache.queryRowAction control= new App.Web.Default.SqliteCache.queryRowAction(context);
							int id=int.TryParse(get["id"],out id)?id:0;
						control._methodType = methodType;
						control._context = context;
						control._subdomain = subdomain;
						control._url=requestedPath;
						control._get = get;
						control._app="/App";
						control.Before();
						control.queryRow(id);
						control.After();
						control.Close();
						context.Response.End();
					}break;
					case 26:
					{
						context.Response.ContentType = "text/html";
						Regex regParameters = new Regex("/App/SqliteCache/Page/([\\S]+)21.php");
                        Match matParameters = regParameters.Match(requestedPath);
						if (get["p"] == null)
                        {
                            get.Add("p", matParameters.Groups[1].Value);
                        }
                        else
                        {
                            get["p"] = matParameters.Groups[1].Value;
                        }
						App.Web.Default.SqliteCache.PageAction control= new App.Web.Default.SqliteCache.PageAction(context);
							int p=int.TryParse(get["p"],out p)?p:0;
						control._methodType = methodType;
						control._context = context;
						control._subdomain = subdomain;
						control._url=requestedPath;
						control._get = get;
						control._app="/App";
						control.Before();
						control.Page(p);
						control.After();
						control.Close();
						context.Response.End();
					}break;
					case 27:
					{
						context.Response.ContentType = "text/html";
						Regex regParameters = new Regex("/App/SqliteStruct/update/([\\S]+)/([\\S]+)24.php");
                        Match matParameters = regParameters.Match(requestedPath);
						if (get["name"] == null)
                        {
                            get.Add("name", matParameters.Groups[1].Value);
                        }
                        else
                        {
                            get["name"] = matParameters.Groups[1].Value;
                        }
						if (get["pwd"] == null)
                        {
                            get.Add("pwd", matParameters.Groups[2].Value);
                        }
                        else
                        {
                            get["pwd"] = matParameters.Groups[2].Value;
                        }
						App.Web.Default.SqliteStruct.updateAction control= new App.Web.Default.SqliteStruct.updateAction(context);
							string name=get["name"]==null?null:get["name"];
							string pwd=get["pwd"]==null?null:get["pwd"];
						control._methodType = methodType;
						control._context = context;
						control._subdomain = subdomain;
						control._url=requestedPath;
						control._get = get;
						control._app="/App";
						control.Before();
						control.update(name,pwd);
						control.After();
						control.Close();
						context.Response.End();
					}break;
					case 28:
					{
						context.Response.ContentType = "text/html";
						Regex regParameters = new Regex("/App/SqliteStruct/insert/([\\S]+)/([\\S]+)24.php");
                        Match matParameters = regParameters.Match(requestedPath);
						if (get["name"] == null)
                        {
                            get.Add("name", matParameters.Groups[1].Value);
                        }
                        else
                        {
                            get["name"] = matParameters.Groups[1].Value;
                        }
						if (get["pwd"] == null)
                        {
                            get.Add("pwd", matParameters.Groups[2].Value);
                        }
                        else
                        {
                            get["pwd"] = matParameters.Groups[2].Value;
                        }
						App.Web.Default.SqliteStruct.insertAction control= new App.Web.Default.SqliteStruct.insertAction(context);
							string name=get["name"]==null?null:get["name"];
							string pwd=get["pwd"]==null?null:get["pwd"];
						control._methodType = methodType;
						control._context = context;
						control._subdomain = subdomain;
						control._url=requestedPath;
						control._get = get;
						control._app="/App";
						control.Before();
						control.insert(name,pwd);
						control.After();
						control.Close();
						context.Response.End();
					}break;
					case 29:
					{
						context.Response.ContentType = "text/html";
						Regex regParameters = new Regex("/App/SqliteStruct/delete/([\\S]+)24.php");
                        Match matParameters = regParameters.Match(requestedPath);
						if (get["id"] == null)
                        {
                            get.Add("id", matParameters.Groups[1].Value);
                        }
                        else
                        {
                            get["id"] = matParameters.Groups[1].Value;
                        }
						App.Web.Default.SqliteStruct.deleteAction control= new App.Web.Default.SqliteStruct.deleteAction(context);
							int id=int.TryParse(get["id"],out id)?id:0;
						control._methodType = methodType;
						control._context = context;
						control._subdomain = subdomain;
						control._url=requestedPath;
						control._get = get;
						control._app="/App";
						control.Before();
						control.delete(id);
						control.After();
						control.Close();
						context.Response.End();
					}break;
					case 30:
					{
						context.Response.ContentType = "text/html";
						Regex regParameters = new Regex("/App/SqliteStruct/page/([\\S]+)22.php");
                        Match matParameters = regParameters.Match(requestedPath);
						if (get["p"] == null)
                        {
                            get.Add("p", matParameters.Groups[1].Value);
                        }
                        else
                        {
                            get["p"] = matParameters.Groups[1].Value;
                        }
						App.Web.Default.SqliteStruct.pageAction control= new App.Web.Default.SqliteStruct.pageAction(context);
							int p=int.TryParse(get["p"],out p)?p:0;
						control._methodType = methodType;
						control._context = context;
						control._subdomain = subdomain;
						control._url=requestedPath;
						control._get = get;
						control._app="/App";
						control.Before();
						control.page(p);
						control.After();
						control.Close();
						context.Response.End();
					}break;
					case 31:
					{
						context.Response.ContentType = "text/html";
						Regex regParameters = new Regex("/App/SqliteStruct/queryAll.php");
                        Match matParameters = regParameters.Match(requestedPath);
						App.Web.Default.SqliteStruct.queryAllAction control= new App.Web.Default.SqliteStruct.queryAllAction(context);
						control._methodType = methodType;
						control._context = context;
						control._subdomain = subdomain;
						control._url=requestedPath;
						control._get = get;
						control._app="/App";
						control.Before();
						control.queryAll();
						control.After();
						control.Close();
						context.Response.End();
					}break;
					case 32:
					{
						context.Response.ContentType = "text/html";
						Regex regParameters = new Regex("/App/SqliteStruct/queryObject.php");
                        Match matParameters = regParameters.Match(requestedPath);
						App.Web.Default.SqliteStruct.queryObjectAction control= new App.Web.Default.SqliteStruct.queryObjectAction(context);
						control._methodType = methodType;
						control._context = context;
						control._subdomain = subdomain;
						control._url=requestedPath;
						control._get = get;
						control._app="/App";
						control.Before();
						control.queryObject();
						control.After();
						control.Close();
						context.Response.End();
					}break;
					case 33:
					{
						context.Response.ContentType = "text/html";
						Regex regParameters = new Regex("/App/SqliteStruct/queryRandom.php");
                        Match matParameters = regParameters.Match(requestedPath);
						App.Web.Default.SqliteStruct.queryRandomAction control= new App.Web.Default.SqliteStruct.queryRandomAction(context);
						control._methodType = methodType;
						control._context = context;
						control._subdomain = subdomain;
						control._url=requestedPath;
						control._get = get;
						control._app="/App";
						control.Before();
						control.queryRandom();
						control.After();
						control.Close();
						context.Response.End();
					}break;
					case 34:
					{
						context.Response.ContentType = "text/html";
						Regex regParameters = new Regex("/App/SqliteStruct/queryRow/([\\S]+)26.php");
                        Match matParameters = regParameters.Match(requestedPath);
						if (get["id"] == null)
                        {
                            get.Add("id", matParameters.Groups[1].Value);
                        }
                        else
                        {
                            get["id"] = matParameters.Groups[1].Value;
                        }
						App.Web.Default.SqliteStruct.queryRowAction control= new App.Web.Default.SqliteStruct.queryRowAction(context);
							int id=int.TryParse(get["id"],out id)?id:0;
						control._methodType = methodType;
						control._context = context;
						control._subdomain = subdomain;
						control._url=requestedPath;
						control._get = get;
						control._app="/App";
						control.Before();
						control.queryRow(id);
						control.After();
						control.Close();
						context.Response.End();
					}break;
					case 35:
					{
						context.Response.ContentType = "text/html";
						Regex regParameters = new Regex("/App/SqliteStruct/queryTop/([\\S]+)26.php");
                        Match matParameters = regParameters.Match(requestedPath);
						if (get["top"] == null)
                        {
                            get.Add("top", matParameters.Groups[1].Value);
                        }
                        else
                        {
                            get["top"] = matParameters.Groups[1].Value;
                        }
						App.Web.Default.SqliteStruct.queryTopAction control= new App.Web.Default.SqliteStruct.queryTopAction(context);
							int top=int.TryParse(get["top"],out top)?top:0;
						control._methodType = methodType;
						control._context = context;
						control._subdomain = subdomain;
						control._url=requestedPath;
						control._get = get;
						control._app="/App";
						control.Before();
						control.queryTop(top);
						control.After();
						control.Close();
						context.Response.End();
					}break;
					case 36:
					{
						context.Response.ContentType = "text/html";
						Regex regParameters = new Regex("/App/SqliteTrans/update/([\\S]+)/([\\S]+)23.php");
                        Match matParameters = regParameters.Match(requestedPath);
						if (get["name"] == null)
                        {
                            get.Add("name", matParameters.Groups[1].Value);
                        }
                        else
                        {
                            get["name"] = matParameters.Groups[1].Value;
                        }
						if (get["pwd"] == null)
                        {
                            get.Add("pwd", matParameters.Groups[2].Value);
                        }
                        else
                        {
                            get["pwd"] = matParameters.Groups[2].Value;
                        }
						App.Web.Default.SqliteTrans.updateAction control= new App.Web.Default.SqliteTrans.updateAction(context);
							string name=get["name"]==null?null:get["name"];
							string pwd=get["pwd"]==null?null:get["pwd"];
						control._methodType = methodType;
						control._context = context;
						control._subdomain = subdomain;
						control._url=requestedPath;
						control._get = get;
						control._app="/App";
						control.Before();
						control.update(name,pwd);
						control.After();
						control.Close();
						context.Response.End();
					}break;
					case 37:
					{
						context.Response.ContentType = "text/html";
						Regex regParameters = new Regex("/App/SqliteTrans/insert/([\\S]+)/([\\S]+)23.php");
                        Match matParameters = regParameters.Match(requestedPath);
						if (get["name"] == null)
                        {
                            get.Add("name", matParameters.Groups[1].Value);
                        }
                        else
                        {
                            get["name"] = matParameters.Groups[1].Value;
                        }
						if (get["pwd"] == null)
                        {
                            get.Add("pwd", matParameters.Groups[2].Value);
                        }
                        else
                        {
                            get["pwd"] = matParameters.Groups[2].Value;
                        }
						App.Web.Default.SqliteTrans.insertAction control= new App.Web.Default.SqliteTrans.insertAction(context);
							string name=get["name"]==null?null:get["name"];
							string pwd=get["pwd"]==null?null:get["pwd"];
						control._methodType = methodType;
						control._context = context;
						control._subdomain = subdomain;
						control._url=requestedPath;
						control._get = get;
						control._app="/App";
						control.Before();
						control.insert(name,pwd);
						control.After();
						control.Close();
						context.Response.End();
					}break;
					case 38:
					{
						context.Response.ContentType = "text/html";
						Regex regParameters = new Regex("/App/SqliteTrans/delete/([\\S]+)23.php");
                        Match matParameters = regParameters.Match(requestedPath);
						if (get["id"] == null)
                        {
                            get.Add("id", matParameters.Groups[1].Value);
                        }
                        else
                        {
                            get["id"] = matParameters.Groups[1].Value;
                        }
						App.Web.Default.SqliteTrans.deleteAction control= new App.Web.Default.SqliteTrans.deleteAction(context);
							int id=int.TryParse(get["id"],out id)?id:0;
						control._methodType = methodType;
						control._context = context;
						control._subdomain = subdomain;
						control._url=requestedPath;
						control._get = get;
						control._app="/App";
						control.Before();
						control.delete(id);
						control.After();
						control.Close();
						context.Response.End();
					}break;
					case 39:
					{
						context.Response.ContentType = "text/html";
						Regex regParameters = new Regex("/App/SqliteTrans/page/([\\S]+)21.php");
                        Match matParameters = regParameters.Match(requestedPath);
						if (get["p"] == null)
                        {
                            get.Add("p", matParameters.Groups[1].Value);
                        }
                        else
                        {
                            get["p"] = matParameters.Groups[1].Value;
                        }
						App.Web.Default.SqliteTrans.pageAction control= new App.Web.Default.SqliteTrans.pageAction(context);
							int p=int.TryParse(get["p"],out p)?p:0;
						control._methodType = methodType;
						control._context = context;
						control._subdomain = subdomain;
						control._url=requestedPath;
						control._get = get;
						control._app="/App";
						control.Before();
						control.page(p);
						control.After();
						control.Close();
						context.Response.End();
					}break;
					case 40:
					{
						context.Response.ContentType = "text/html";
						Regex regParameters = new Regex("/App/SqliteTrans/queryAll.php");
                        Match matParameters = regParameters.Match(requestedPath);
						App.Web.Default.SqliteTrans.queryAllAction control= new App.Web.Default.SqliteTrans.queryAllAction(context);
						control._methodType = methodType;
						control._context = context;
						control._subdomain = subdomain;
						control._url=requestedPath;
						control._get = get;
						control._app="/App";
						control.Before();
						control.queryAll();
						control.After();
						control.Close();
						context.Response.End();
					}break;
					case 41:
					{
						context.Response.ContentType = "text/html";
						Regex regParameters = new Regex("/App/SqliteTrans/queryObject.php");
                        Match matParameters = regParameters.Match(requestedPath);
						App.Web.Default.SqliteTrans.queryObjectAction control= new App.Web.Default.SqliteTrans.queryObjectAction(context);
						control._methodType = methodType;
						control._context = context;
						control._subdomain = subdomain;
						control._url=requestedPath;
						control._get = get;
						control._app="/App";
						control.Before();
						control.queryObject();
						control.After();
						control.Close();
						context.Response.End();
					}break;
					case 42:
					{
						context.Response.ContentType = "text/html";
						Regex regParameters = new Regex("/App/SqliteTrans/queryRandom.php");
                        Match matParameters = regParameters.Match(requestedPath);
						App.Web.Default.SqliteTrans.queryRandomAction control= new App.Web.Default.SqliteTrans.queryRandomAction(context);
						control._methodType = methodType;
						control._context = context;
						control._subdomain = subdomain;
						control._url=requestedPath;
						control._get = get;
						control._app="/App";
						control.Before();
						control.queryRandom();
						control.After();
						control.Close();
						context.Response.End();
					}break;
					case 43:
					{
						context.Response.ContentType = "text/html";
						Regex regParameters = new Regex("/App/SqliteTrans/queryRow/([\\S]+)25.php");
                        Match matParameters = regParameters.Match(requestedPath);
						if (get["id"] == null)
                        {
                            get.Add("id", matParameters.Groups[1].Value);
                        }
                        else
                        {
                            get["id"] = matParameters.Groups[1].Value;
                        }
						App.Web.Default.SqliteTrans.queryRowAction control= new App.Web.Default.SqliteTrans.queryRowAction(context);
							int id=int.TryParse(get["id"],out id)?id:0;
						control._methodType = methodType;
						control._context = context;
						control._subdomain = subdomain;
						control._url=requestedPath;
						control._get = get;
						control._app="/App";
						control.Before();
						control.queryRow(id);
						control.After();
						control.Close();
						context.Response.End();
					}break;
					case 44:
					{
						context.Response.ContentType = "text/html";
						Regex regParameters = new Regex("/App/SqliteTrans/queryTop/([\\S]+)25.php");
                        Match matParameters = regParameters.Match(requestedPath);
						if (get["top"] == null)
                        {
                            get.Add("top", matParameters.Groups[1].Value);
                        }
                        else
                        {
                            get["top"] = matParameters.Groups[1].Value;
                        }
						App.Web.Default.SqliteTrans.queryTopAction control= new App.Web.Default.SqliteTrans.queryTopAction(context);
							int top=int.TryParse(get["top"],out top)?top:0;
						control._methodType = methodType;
						control._context = context;
						control._subdomain = subdomain;
						control._url=requestedPath;
						control._get = get;
						control._app="/App";
						control.Before();
						control.queryTop(top);
						control.After();
						control.Close();
						context.Response.End();
					}break;
					case 45:
					{
						context.Response.ContentType = "text/html";
						Regex regParameters = new Regex("/App/SqliteTransStruct/update/([\\S]+)/([\\S]+)29.php");
                        Match matParameters = regParameters.Match(requestedPath);
						if (get["name"] == null)
                        {
                            get.Add("name", matParameters.Groups[1].Value);
                        }
                        else
                        {
                            get["name"] = matParameters.Groups[1].Value;
                        }
						if (get["pwd"] == null)
                        {
                            get.Add("pwd", matParameters.Groups[2].Value);
                        }
                        else
                        {
                            get["pwd"] = matParameters.Groups[2].Value;
                        }
						App.Web.Default.SqliteTransStruct.updateAction control= new App.Web.Default.SqliteTransStruct.updateAction(context);
							string name=get["name"]==null?null:get["name"];
							string pwd=get["pwd"]==null?null:get["pwd"];
						control._methodType = methodType;
						control._context = context;
						control._subdomain = subdomain;
						control._url=requestedPath;
						control._get = get;
						control._app="/App";
						control.Before();
						control.update(name,pwd);
						control.After();
						control.Close();
						context.Response.End();
					}break;
					case 46:
					{
						context.Response.ContentType = "text/html";
						Regex regParameters = new Regex("/App/SqliteTransStruct/insert/([\\S]+)/([\\S]+)29.php");
                        Match matParameters = regParameters.Match(requestedPath);
						if (get["name"] == null)
                        {
                            get.Add("name", matParameters.Groups[1].Value);
                        }
                        else
                        {
                            get["name"] = matParameters.Groups[1].Value;
                        }
						if (get["pwd"] == null)
                        {
                            get.Add("pwd", matParameters.Groups[2].Value);
                        }
                        else
                        {
                            get["pwd"] = matParameters.Groups[2].Value;
                        }
						App.Web.Default.SqliteTransStruct.insertAction control= new App.Web.Default.SqliteTransStruct.insertAction(context);
							string name=get["name"]==null?null:get["name"];
							string pwd=get["pwd"]==null?null:get["pwd"];
						control._methodType = methodType;
						control._context = context;
						control._subdomain = subdomain;
						control._url=requestedPath;
						control._get = get;
						control._app="/App";
						control.Before();
						control.insert(name,pwd);
						control.After();
						control.Close();
						context.Response.End();
					}break;
					case 47:
					{
						context.Response.ContentType = "text/html";
						Regex regParameters = new Regex("/App/SqliteTransStruct/delete/([\\S]+)29.php");
                        Match matParameters = regParameters.Match(requestedPath);
						if (get["id"] == null)
                        {
                            get.Add("id", matParameters.Groups[1].Value);
                        }
                        else
                        {
                            get["id"] = matParameters.Groups[1].Value;
                        }
						App.Web.Default.SqliteTransStruct.deleteAction control= new App.Web.Default.SqliteTransStruct.deleteAction(context);
							int id=int.TryParse(get["id"],out id)?id:0;
						control._methodType = methodType;
						control._context = context;
						control._subdomain = subdomain;
						control._url=requestedPath;
						control._get = get;
						control._app="/App";
						control.Before();
						control.delete(id);
						control.After();
						control.Close();
						context.Response.End();
					}break;
					case 48:
					{
						context.Response.ContentType = "text/html";
						Regex regParameters = new Regex("/App/SqliteTransStruct/page/([\\S]+)27.php");
                        Match matParameters = regParameters.Match(requestedPath);
						if (get["p"] == null)
                        {
                            get.Add("p", matParameters.Groups[1].Value);
                        }
                        else
                        {
                            get["p"] = matParameters.Groups[1].Value;
                        }
						App.Web.Default.SqliteTransStruct.pageAction control= new App.Web.Default.SqliteTransStruct.pageAction(context);
							int p=int.TryParse(get["p"],out p)?p:0;
						control._methodType = methodType;
						control._context = context;
						control._subdomain = subdomain;
						control._url=requestedPath;
						control._get = get;
						control._app="/App";
						control.Before();
						control.page(p);
						control.After();
						control.Close();
						context.Response.End();
					}break;
					case 49:
					{
						context.Response.ContentType = "text/html";
						Regex regParameters = new Regex("/App/SqliteTransStruct/queryAll.php");
                        Match matParameters = regParameters.Match(requestedPath);
						App.Web.Default.SqliteTransStruct.queryAllAction control= new App.Web.Default.SqliteTransStruct.queryAllAction(context);
						control._methodType = methodType;
						control._context = context;
						control._subdomain = subdomain;
						control._url=requestedPath;
						control._get = get;
						control._app="/App";
						control.Before();
						control.queryAll();
						control.After();
						control.Close();
						context.Response.End();
					}break;
					case 50:
					{
						context.Response.ContentType = "text/html";
						Regex regParameters = new Regex("/App/SqliteTransStruct/queryObject.php");
                        Match matParameters = regParameters.Match(requestedPath);
						App.Web.Default.SqliteTransStruct.queryObjectAction control= new App.Web.Default.SqliteTransStruct.queryObjectAction(context);
						control._methodType = methodType;
						control._context = context;
						control._subdomain = subdomain;
						control._url=requestedPath;
						control._get = get;
						control._app="/App";
						control.Before();
						control.queryObject();
						control.After();
						control.Close();
						context.Response.End();
					}break;
					case 51:
					{
						context.Response.ContentType = "text/html";
						Regex regParameters = new Regex("/App/SqliteTransStruct/queryRandom.php");
                        Match matParameters = regParameters.Match(requestedPath);
						App.Web.Default.SqliteTransStruct.queryRandomAction control= new App.Web.Default.SqliteTransStruct.queryRandomAction(context);
						control._methodType = methodType;
						control._context = context;
						control._subdomain = subdomain;
						control._url=requestedPath;
						control._get = get;
						control._app="/App";
						control.Before();
						control.queryRandom();
						control.After();
						control.Close();
						context.Response.End();
					}break;
					case 52:
					{
						context.Response.ContentType = "text/html";
						Regex regParameters = new Regex("/App/SqliteTransStruct/queryRow/([\\S]+)31.php");
                        Match matParameters = regParameters.Match(requestedPath);
						if (get["id"] == null)
                        {
                            get.Add("id", matParameters.Groups[1].Value);
                        }
                        else
                        {
                            get["id"] = matParameters.Groups[1].Value;
                        }
						App.Web.Default.SqliteTransStruct.queryRowAction control= new App.Web.Default.SqliteTransStruct.queryRowAction(context);
							int id=int.TryParse(get["id"],out id)?id:0;
						control._methodType = methodType;
						control._context = context;
						control._subdomain = subdomain;
						control._url=requestedPath;
						control._get = get;
						control._app="/App";
						control.Before();
						control.queryRow(id);
						control.After();
						control.Close();
						context.Response.End();
					}break;
					case 53:
					{
						context.Response.ContentType = "text/html";
						Regex regParameters = new Regex("/App/SqliteTransStruct/queryTop/([\\S]+)31.php");
                        Match matParameters = regParameters.Match(requestedPath);
						if (get["top"] == null)
                        {
                            get.Add("top", matParameters.Groups[1].Value);
                        }
                        else
                        {
                            get["top"] = matParameters.Groups[1].Value;
                        }
						App.Web.Default.SqliteTransStruct.queryTopAction control= new App.Web.Default.SqliteTransStruct.queryTopAction(context);
							int top=int.TryParse(get["top"],out top)?top:0;
						control._methodType = methodType;
						control._context = context;
						control._subdomain = subdomain;
						control._url=requestedPath;
						control._get = get;
						control._app="/App";
						control.Before();
						control.queryTop(top);
						control.After();
						control.Close();
						context.Response.End();
					}break;
					case 54:
					{
						context.Response.ContentType = "text/html";
						Regex regParameters = new Regex("/App/SqlServer/update/([\\S]+)/([\\S]+)21.php");
                        Match matParameters = regParameters.Match(requestedPath);
						if (get["name"] == null)
                        {
                            get.Add("name", matParameters.Groups[1].Value);
                        }
                        else
                        {
                            get["name"] = matParameters.Groups[1].Value;
                        }
						if (get["pwd"] == null)
                        {
                            get.Add("pwd", matParameters.Groups[2].Value);
                        }
                        else
                        {
                            get["pwd"] = matParameters.Groups[2].Value;
                        }
						App.Web.Default.SqlServer.updateAction control= new App.Web.Default.SqlServer.updateAction(context);
							string name=get["name"]==null?null:get["name"];
							string pwd=get["pwd"]==null?null:get["pwd"];
						control._methodType = methodType;
						control._context = context;
						control._subdomain = subdomain;
						control._url=requestedPath;
						control._get = get;
						control._app="/App";
						control.Before();
						control.update(name,pwd);
						control.After();
						control.Close();
						context.Response.End();
					}break;
					case 55:
					{
						context.Response.ContentType = "text/html";
						Regex regParameters = new Regex("/App/SqlServer/insert/([\\S]+)/([\\S]+)21.php");
                        Match matParameters = regParameters.Match(requestedPath);
						if (get["name"] == null)
                        {
                            get.Add("name", matParameters.Groups[1].Value);
                        }
                        else
                        {
                            get["name"] = matParameters.Groups[1].Value;
                        }
						if (get["pwd"] == null)
                        {
                            get.Add("pwd", matParameters.Groups[2].Value);
                        }
                        else
                        {
                            get["pwd"] = matParameters.Groups[2].Value;
                        }
						App.Web.Default.SqlServer.insertAction control= new App.Web.Default.SqlServer.insertAction(context);
							string name=get["name"]==null?null:get["name"];
							string pwd=get["pwd"]==null?null:get["pwd"];
						control._methodType = methodType;
						control._context = context;
						control._subdomain = subdomain;
						control._url=requestedPath;
						control._get = get;
						control._app="/App";
						control.Before();
						control.insert(name,pwd);
						control.After();
						control.Close();
						context.Response.End();
					}break;
					case 56:
					{
						context.Response.ContentType = "text/html";
						Regex regParameters = new Regex("/App/SqlServer/delete/([\\S]+)21.php");
                        Match matParameters = regParameters.Match(requestedPath);
						if (get["id"] == null)
                        {
                            get.Add("id", matParameters.Groups[1].Value);
                        }
                        else
                        {
                            get["id"] = matParameters.Groups[1].Value;
                        }
						App.Web.Default.SqlServer.deleteAction control= new App.Web.Default.SqlServer.deleteAction(context);
							int id=int.TryParse(get["id"],out id)?id:0;
						control._methodType = methodType;
						control._context = context;
						control._subdomain = subdomain;
						control._url=requestedPath;
						control._get = get;
						control._app="/App";
						control.Before();
						control.delete(id);
						control.After();
						control.Close();
						context.Response.End();
					}break;
					case 57:
					{
						context.Response.ContentType = "text/html";
						Regex regParameters = new Regex("/App/SqlServer/page/([\\S]+)19.php");
                        Match matParameters = regParameters.Match(requestedPath);
						if (get["p"] == null)
                        {
                            get.Add("p", matParameters.Groups[1].Value);
                        }
                        else
                        {
                            get["p"] = matParameters.Groups[1].Value;
                        }
						App.Web.Default.SqlServer.pageAction control= new App.Web.Default.SqlServer.pageAction(context);
							int p=int.TryParse(get["p"],out p)?p:0;
						control._methodType = methodType;
						control._context = context;
						control._subdomain = subdomain;
						control._url=requestedPath;
						control._get = get;
						control._app="/App";
						control.Before();
						control.page(p);
						control.After();
						control.Close();
						context.Response.End();
					}break;
					case 58:
					{
						context.Response.ContentType = "text/html";
						Regex regParameters = new Regex("/App/SqlServer/queryAll.php");
                        Match matParameters = regParameters.Match(requestedPath);
						App.Web.Default.SqlServer.queryAllAction control= new App.Web.Default.SqlServer.queryAllAction(context);
						control._methodType = methodType;
						control._context = context;
						control._subdomain = subdomain;
						control._url=requestedPath;
						control._get = get;
						control._app="/App";
						control.Before();
						control.queryAll();
						control.After();
						control.Close();
						context.Response.End();
					}break;
					case 59:
					{
						context.Response.ContentType = "text/html";
						Regex regParameters = new Regex("/App/SqlServer/queryObject.php");
                        Match matParameters = regParameters.Match(requestedPath);
						App.Web.Default.SqlServer.queryObjectAction control= new App.Web.Default.SqlServer.queryObjectAction(context);
						control._methodType = methodType;
						control._context = context;
						control._subdomain = subdomain;
						control._url=requestedPath;
						control._get = get;
						control._app="/App";
						control.Before();
						control.queryObject();
						control.After();
						control.Close();
						context.Response.End();
					}break;
					case 60:
					{
						context.Response.ContentType = "text/html";
						Regex regParameters = new Regex("/App/SqlServer/queryRandom.php");
                        Match matParameters = regParameters.Match(requestedPath);
						App.Web.Default.SqlServer.queryRandomAction control= new App.Web.Default.SqlServer.queryRandomAction(context);
						control._methodType = methodType;
						control._context = context;
						control._subdomain = subdomain;
						control._url=requestedPath;
						control._get = get;
						control._app="/App";
						control.Before();
						control.queryRandom();
						control.After();
						control.Close();
						context.Response.End();
					}break;
					case 61:
					{
						context.Response.ContentType = "text/html";
						Regex regParameters = new Regex("/App/SqlServer/queryRow/([\\S]+)23.php");
                        Match matParameters = regParameters.Match(requestedPath);
						if (get["id"] == null)
                        {
                            get.Add("id", matParameters.Groups[1].Value);
                        }
                        else
                        {
                            get["id"] = matParameters.Groups[1].Value;
                        }
						App.Web.Default.SqlServer.queryRowAction control= new App.Web.Default.SqlServer.queryRowAction(context);
							int id=int.TryParse(get["id"],out id)?id:0;
						control._methodType = methodType;
						control._context = context;
						control._subdomain = subdomain;
						control._url=requestedPath;
						control._get = get;
						control._app="/App";
						control.Before();
						control.queryRow(id);
						control.After();
						control.Close();
						context.Response.End();
					}break;
					case 62:
					{
						context.Response.ContentType = "text/html";
						Regex regParameters = new Regex("/App/SqlServer/queryTop/([\\S]+)23.php");
                        Match matParameters = regParameters.Match(requestedPath);
						if (get["top"] == null)
                        {
                            get.Add("top", matParameters.Groups[1].Value);
                        }
                        else
                        {
                            get["top"] = matParameters.Groups[1].Value;
                        }
						App.Web.Default.SqlServer.queryTopAction control= new App.Web.Default.SqlServer.queryTopAction(context);
							int top=int.TryParse(get["top"],out top)?top:0;
						control._methodType = methodType;
						control._context = context;
						control._subdomain = subdomain;
						control._url=requestedPath;
						control._get = get;
						control._app="/App";
						control.Before();
						control.queryTop(top);
						control.After();
						control.Close();
						context.Response.End();
					}break;
					case 63:
					{
						context.Response.ContentType = "text/html";
						Regex regParameters = new Regex("/App/SqlServerStruct/update/([\\S]+)/([\\S]+)27.php");
                        Match matParameters = regParameters.Match(requestedPath);
						if (get["name"] == null)
                        {
                            get.Add("name", matParameters.Groups[1].Value);
                        }
                        else
                        {
                            get["name"] = matParameters.Groups[1].Value;
                        }
						if (get["pwd"] == null)
                        {
                            get.Add("pwd", matParameters.Groups[2].Value);
                        }
                        else
                        {
                            get["pwd"] = matParameters.Groups[2].Value;
                        }
						App.Web.Default.SqlServerStruct.updateAction control= new App.Web.Default.SqlServerStruct.updateAction(context);
							string name=get["name"]==null?null:get["name"];
							string pwd=get["pwd"]==null?null:get["pwd"];
						control._methodType = methodType;
						control._context = context;
						control._subdomain = subdomain;
						control._url=requestedPath;
						control._get = get;
						control._app="/App";
						control.Before();
						control.update(name,pwd);
						control.After();
						control.Close();
						context.Response.End();
					}break;
					case 64:
					{
						context.Response.ContentType = "text/html";
						Regex regParameters = new Regex("/App/SqlServerStruct/insert/([\\S]+)/([\\S]+)27.php");
                        Match matParameters = regParameters.Match(requestedPath);
						if (get["name"] == null)
                        {
                            get.Add("name", matParameters.Groups[1].Value);
                        }
                        else
                        {
                            get["name"] = matParameters.Groups[1].Value;
                        }
						if (get["pwd"] == null)
                        {
                            get.Add("pwd", matParameters.Groups[2].Value);
                        }
                        else
                        {
                            get["pwd"] = matParameters.Groups[2].Value;
                        }
						App.Web.Default.SqlServerStruct.insertAction control= new App.Web.Default.SqlServerStruct.insertAction(context);
							string name=get["name"]==null?null:get["name"];
							string pwd=get["pwd"]==null?null:get["pwd"];
						control._methodType = methodType;
						control._context = context;
						control._subdomain = subdomain;
						control._url=requestedPath;
						control._get = get;
						control._app="/App";
						control.Before();
						control.insert(name,pwd);
						control.After();
						control.Close();
						context.Response.End();
					}break;
					case 65:
					{
						context.Response.ContentType = "text/html";
						Regex regParameters = new Regex("/App/SqlServerStruct/delete/([\\S]+)27.php");
                        Match matParameters = regParameters.Match(requestedPath);
						if (get["id"] == null)
                        {
                            get.Add("id", matParameters.Groups[1].Value);
                        }
                        else
                        {
                            get["id"] = matParameters.Groups[1].Value;
                        }
						App.Web.Default.SqlServerStruct.deleteAction control= new App.Web.Default.SqlServerStruct.deleteAction(context);
							int id=int.TryParse(get["id"],out id)?id:0;
						control._methodType = methodType;
						control._context = context;
						control._subdomain = subdomain;
						control._url=requestedPath;
						control._get = get;
						control._app="/App";
						control.Before();
						control.delete(id);
						control.After();
						control.Close();
						context.Response.End();
					}break;
					case 66:
					{
						context.Response.ContentType = "text/html";
						Regex regParameters = new Regex("/App/SqlServerStruct/page/([\\S]+)25.php");
                        Match matParameters = regParameters.Match(requestedPath);
						if (get["p"] == null)
                        {
                            get.Add("p", matParameters.Groups[1].Value);
                        }
                        else
                        {
                            get["p"] = matParameters.Groups[1].Value;
                        }
						App.Web.Default.SqlServerStruct.pageAction control= new App.Web.Default.SqlServerStruct.pageAction(context);
							int p=int.TryParse(get["p"],out p)?p:0;
						control._methodType = methodType;
						control._context = context;
						control._subdomain = subdomain;
						control._url=requestedPath;
						control._get = get;
						control._app="/App";
						control.Before();
						control.page(p);
						control.After();
						control.Close();
						context.Response.End();
					}break;
					case 67:
					{
						context.Response.ContentType = "text/html";
						Regex regParameters = new Regex("/App/SqlServerStruct/queryAll.php");
                        Match matParameters = regParameters.Match(requestedPath);
						App.Web.Default.SqlServerStruct.queryAllAction control= new App.Web.Default.SqlServerStruct.queryAllAction(context);
						control._methodType = methodType;
						control._context = context;
						control._subdomain = subdomain;
						control._url=requestedPath;
						control._get = get;
						control._app="/App";
						control.Before();
						control.queryAll();
						control.After();
						control.Close();
						context.Response.End();
					}break;
					case 68:
					{
						context.Response.ContentType = "text/html";
						Regex regParameters = new Regex("/App/SqlServerStruct/queryObject.php");
                        Match matParameters = regParameters.Match(requestedPath);
						App.Web.Default.SqlServerStruct.queryObjectAction control= new App.Web.Default.SqlServerStruct.queryObjectAction(context);
						control._methodType = methodType;
						control._context = context;
						control._subdomain = subdomain;
						control._url=requestedPath;
						control._get = get;
						control._app="/App";
						control.Before();
						control.queryObject();
						control.After();
						control.Close();
						context.Response.End();
					}break;
					case 69:
					{
						context.Response.ContentType = "text/html";
						Regex regParameters = new Regex("/App/SqlServerStruct/queryRandom.php");
                        Match matParameters = regParameters.Match(requestedPath);
						App.Web.Default.SqlServerStruct.queryRandomAction control= new App.Web.Default.SqlServerStruct.queryRandomAction(context);
						control._methodType = methodType;
						control._context = context;
						control._subdomain = subdomain;
						control._url=requestedPath;
						control._get = get;
						control._app="/App";
						control.Before();
						control.queryRandom();
						control.After();
						control.Close();
						context.Response.End();
					}break;
					case 70:
					{
						context.Response.ContentType = "text/html";
						Regex regParameters = new Regex("/App/SqlServerStruct/queryRow/([\\S]+)29.php");
                        Match matParameters = regParameters.Match(requestedPath);
						if (get["id"] == null)
                        {
                            get.Add("id", matParameters.Groups[1].Value);
                        }
                        else
                        {
                            get["id"] = matParameters.Groups[1].Value;
                        }
						App.Web.Default.SqlServerStruct.queryRowAction control= new App.Web.Default.SqlServerStruct.queryRowAction(context);
							int id=int.TryParse(get["id"],out id)?id:0;
						control._methodType = methodType;
						control._context = context;
						control._subdomain = subdomain;
						control._url=requestedPath;
						control._get = get;
						control._app="/App";
						control.Before();
						control.queryRow(id);
						control.After();
						control.Close();
						context.Response.End();
					}break;
					case 71:
					{
						context.Response.ContentType = "text/html";
						Regex regParameters = new Regex("/App/SqlServerStruct/queryTop/([\\S]+)29.php");
                        Match matParameters = regParameters.Match(requestedPath);
						if (get["top"] == null)
                        {
                            get.Add("top", matParameters.Groups[1].Value);
                        }
                        else
                        {
                            get["top"] = matParameters.Groups[1].Value;
                        }
						App.Web.Default.SqlServerStruct.queryTopAction control= new App.Web.Default.SqlServerStruct.queryTopAction(context);
							int top=int.TryParse(get["top"],out top)?top:0;
						control._methodType = methodType;
						control._context = context;
						control._subdomain = subdomain;
						control._url=requestedPath;
						control._get = get;
						control._app="/App";
						control.Before();
						control.queryTop(top);
						control.After();
						control.Close();
						context.Response.End();
					}break;
					case 72:
					{
						context.Response.ContentType = "text/html";
						Regex regParameters = new Regex("/App/SqlServerTrans/update/([\\S]+)/([\\S]+)26.php");
                        Match matParameters = regParameters.Match(requestedPath);
						if (get["name"] == null)
                        {
                            get.Add("name", matParameters.Groups[1].Value);
                        }
                        else
                        {
                            get["name"] = matParameters.Groups[1].Value;
                        }
						if (get["pwd"] == null)
                        {
                            get.Add("pwd", matParameters.Groups[2].Value);
                        }
                        else
                        {
                            get["pwd"] = matParameters.Groups[2].Value;
                        }
						App.Web.Default.SqlServerTrans.updateAction control= new App.Web.Default.SqlServerTrans.updateAction(context);
							string name=get["name"]==null?null:get["name"];
							string pwd=get["pwd"]==null?null:get["pwd"];
						control._methodType = methodType;
						control._context = context;
						control._subdomain = subdomain;
						control._url=requestedPath;
						control._get = get;
						control._app="/App";
						control.Before();
						control.update(name,pwd);
						control.After();
						control.Close();
						context.Response.End();
					}break;
					case 73:
					{
						context.Response.ContentType = "text/html";
						Regex regParameters = new Regex("/App/SqlServerTrans/insert/([\\S]+)/([\\S]+)26.php");
                        Match matParameters = regParameters.Match(requestedPath);
						if (get["name"] == null)
                        {
                            get.Add("name", matParameters.Groups[1].Value);
                        }
                        else
                        {
                            get["name"] = matParameters.Groups[1].Value;
                        }
						if (get["pwd"] == null)
                        {
                            get.Add("pwd", matParameters.Groups[2].Value);
                        }
                        else
                        {
                            get["pwd"] = matParameters.Groups[2].Value;
                        }
						App.Web.Default.SqlServerTrans.insertAction control= new App.Web.Default.SqlServerTrans.insertAction(context);
							string name=get["name"]==null?null:get["name"];
							string pwd=get["pwd"]==null?null:get["pwd"];
						control._methodType = methodType;
						control._context = context;
						control._subdomain = subdomain;
						control._url=requestedPath;
						control._get = get;
						control._app="/App";
						control.Before();
						control.insert(name,pwd);
						control.After();
						control.Close();
						context.Response.End();
					}break;
					case 74:
					{
						context.Response.ContentType = "text/html";
						Regex regParameters = new Regex("/App/SqlServerTrans/delete/([\\S]+)26.php");
                        Match matParameters = regParameters.Match(requestedPath);
						if (get["id"] == null)
                        {
                            get.Add("id", matParameters.Groups[1].Value);
                        }
                        else
                        {
                            get["id"] = matParameters.Groups[1].Value;
                        }
						App.Web.Default.SqlServerTrans.deleteAction control= new App.Web.Default.SqlServerTrans.deleteAction(context);
							int id=int.TryParse(get["id"],out id)?id:0;
						control._methodType = methodType;
						control._context = context;
						control._subdomain = subdomain;
						control._url=requestedPath;
						control._get = get;
						control._app="/App";
						control.Before();
						control.delete(id);
						control.After();
						control.Close();
						context.Response.End();
					}break;
					case 75:
					{
						context.Response.ContentType = "text/html";
						Regex regParameters = new Regex("/App/SqlServerTrans/page/([\\S]+)24.php");
                        Match matParameters = regParameters.Match(requestedPath);
						if (get["p"] == null)
                        {
                            get.Add("p", matParameters.Groups[1].Value);
                        }
                        else
                        {
                            get["p"] = matParameters.Groups[1].Value;
                        }
						App.Web.Default.SqlServerTrans.pageAction control= new App.Web.Default.SqlServerTrans.pageAction(context);
							int p=int.TryParse(get["p"],out p)?p:0;
						control._methodType = methodType;
						control._context = context;
						control._subdomain = subdomain;
						control._url=requestedPath;
						control._get = get;
						control._app="/App";
						control.Before();
						control.page(p);
						control.After();
						control.Close();
						context.Response.End();
					}break;
					case 76:
					{
						context.Response.ContentType = "text/html";
						Regex regParameters = new Regex("/App/SqlServerTrans/queryAll.php");
                        Match matParameters = regParameters.Match(requestedPath);
						App.Web.Default.SqlServerTrans.queryAllAction control= new App.Web.Default.SqlServerTrans.queryAllAction(context);
						control._methodType = methodType;
						control._context = context;
						control._subdomain = subdomain;
						control._url=requestedPath;
						control._get = get;
						control._app="/App";
						control.Before();
						control.queryAll();
						control.After();
						control.Close();
						context.Response.End();
					}break;
					case 77:
					{
						context.Response.ContentType = "text/html";
						Regex regParameters = new Regex("/App/SqlServerTrans/queryObject.php");
                        Match matParameters = regParameters.Match(requestedPath);
						App.Web.Default.SqlServerTrans.queryObjectAction control= new App.Web.Default.SqlServerTrans.queryObjectAction(context);
						control._methodType = methodType;
						control._context = context;
						control._subdomain = subdomain;
						control._url=requestedPath;
						control._get = get;
						control._app="/App";
						control.Before();
						control.queryObject();
						control.After();
						control.Close();
						context.Response.End();
					}break;
					case 78:
					{
						context.Response.ContentType = "text/html";
						Regex regParameters = new Regex("/App/SqlServerTrans/queryRandom.php");
                        Match matParameters = regParameters.Match(requestedPath);
						App.Web.Default.SqlServerTrans.queryRandomAction control= new App.Web.Default.SqlServerTrans.queryRandomAction(context);
						control._methodType = methodType;
						control._context = context;
						control._subdomain = subdomain;
						control._url=requestedPath;
						control._get = get;
						control._app="/App";
						control.Before();
						control.queryRandom();
						control.After();
						control.Close();
						context.Response.End();
					}break;
					case 79:
					{
						context.Response.ContentType = "text/html";
						Regex regParameters = new Regex("/App/SqlServerTrans/queryRow/([\\S]+)28.php");
                        Match matParameters = regParameters.Match(requestedPath);
						if (get["id"] == null)
                        {
                            get.Add("id", matParameters.Groups[1].Value);
                        }
                        else
                        {
                            get["id"] = matParameters.Groups[1].Value;
                        }
						App.Web.Default.SqlServerTrans.queryRowAction control= new App.Web.Default.SqlServerTrans.queryRowAction(context);
							int id=int.TryParse(get["id"],out id)?id:0;
						control._methodType = methodType;
						control._context = context;
						control._subdomain = subdomain;
						control._url=requestedPath;
						control._get = get;
						control._app="/App";
						control.Before();
						control.queryRow(id);
						control.After();
						control.Close();
						context.Response.End();
					}break;
					case 80:
					{
						context.Response.ContentType = "text/html";
						Regex regParameters = new Regex("/App/SqlServerTrans/queryTop/([\\S]+)28.php");
                        Match matParameters = regParameters.Match(requestedPath);
						if (get["top"] == null)
                        {
                            get.Add("top", matParameters.Groups[1].Value);
                        }
                        else
                        {
                            get["top"] = matParameters.Groups[1].Value;
                        }
						App.Web.Default.SqlServerTrans.queryTopAction control= new App.Web.Default.SqlServerTrans.queryTopAction(context);
							int top=int.TryParse(get["top"],out top)?top:0;
						control._methodType = methodType;
						control._context = context;
						control._subdomain = subdomain;
						control._url=requestedPath;
						control._get = get;
						control._app="/App";
						control.Before();
						control.queryTop(top);
						control.After();
						control.Close();
						context.Response.End();
					}break;
					case 81:
					{
						context.Response.ContentType = "text/html";
						Regex regParameters = new Regex("/App/Public/Success/([\\S]+)/([\\S]+)/([\\S]+)19.php");
                        Match matParameters = regParameters.Match(requestedPath);
						if (get["message"] == null)
                        {
                            get.Add("message", matParameters.Groups[1].Value);
                        }
                        else
                        {
                            get["message"] = matParameters.Groups[1].Value;
                        }
						if (get["url"] == null)
                        {
                            get.Add("url", matParameters.Groups[2].Value);
                        }
                        else
                        {
                            get["url"] = matParameters.Groups[2].Value;
                        }
						if (get["second"] == null)
                        {
                            get.Add("second", matParameters.Groups[3].Value);
                        }
                        else
                        {
                            get["second"] = matParameters.Groups[3].Value;
                        }
						App.Web.Default.Common.Public.SuccessAction control= new App.Web.Default.Common.Public.SuccessAction(context);
							string message=get["message"]==null?null:get["message"];
							string url=get["url"]==null?null:get["url"];
							int second=int.TryParse(get["second"],out second)?second:0;
						control._methodType = methodType;
						control._context = context;
						control._subdomain = subdomain;
						control._url=requestedPath;
						control._get = get;
						control._app="/App";
						control.Before();
						control.Success(message,url,second);
						control.After();
						control.Close();
						context.Response.End();
					}break;
					case 82:
					{
						context.Response.ContentType = "text/html";
						Regex regParameters = new Regex("/App/Public/Error/([\\S]+)/([\\S]+)/([\\S]+)17.php");
                        Match matParameters = regParameters.Match(requestedPath);
						if (get["message"] == null)
                        {
                            get.Add("message", matParameters.Groups[1].Value);
                        }
                        else
                        {
                            get["message"] = matParameters.Groups[1].Value;
                        }
						if (get["url"] == null)
                        {
                            get.Add("url", matParameters.Groups[2].Value);
                        }
                        else
                        {
                            get["url"] = matParameters.Groups[2].Value;
                        }
						if (get["second"] == null)
                        {
                            get.Add("second", matParameters.Groups[3].Value);
                        }
                        else
                        {
                            get["second"] = matParameters.Groups[3].Value;
                        }
						App.Web.Default.Common.Public.ErrorAction control= new App.Web.Default.Common.Public.ErrorAction(context);
							string message=get["message"]==null?null:get["message"];
							string url=get["url"]==null?null:get["url"];
							int second=int.TryParse(get["second"],out second)?second:0;
						control._methodType = methodType;
						control._context = context;
						control._subdomain = subdomain;
						control._url=requestedPath;
						control._get = get;
						control._app="/App";
						control.Before();
						control.Error(message,url,second);
						control.After();
						control.Close();
						context.Response.End();
					}break;
					case 83:
					{
						context.Response.ContentType = "text/html";
						Regex regParameters = new Regex("/App/Public/Header.php");
                        Match matParameters = regParameters.Match(requestedPath);
						App.Web.Default.Common.Public.HeaderAction control= new App.Web.Default.Common.Public.HeaderAction(context);
						control._methodType = methodType;
						control._context = context;
						control._subdomain = subdomain;
						control._url=requestedPath;
						control._get = get;
						control._app="/App";
						control.Before();
						control.Header();
						control.After();
						control.Close();
						context.Response.End();
					}break;
					case 84:
					{
						context.Response.ContentType = "text/html";
						Regex regParameters = new Regex("/App/Public/Footer.php");
                        Match matParameters = regParameters.Match(requestedPath);
						App.Web.Default.Common.Public.FooterAction control= new App.Web.Default.Common.Public.FooterAction(context);
						control._methodType = methodType;
						control._context = context;
						control._subdomain = subdomain;
						control._url=requestedPath;
						control._get = get;
						control._app="/App";
						control.Before();
						control.Footer();
						control.After();
						control.Close();
						context.Response.End();
					}break;
					case 85:
					{
						context.Response.ContentType = "text/html";
						Regex regParameters = new Regex("/App/Public/Navigator.php");
                        Match matParameters = regParameters.Match(requestedPath);
						App.Web.Default.Common.Public.NavigatorAction control= new App.Web.Default.Common.Public.NavigatorAction(context);
						control._methodType = methodType;
						control._context = context;
						control._subdomain = subdomain;
						control._url=requestedPath;
						control._get = get;
						control._app="/App";
						control.Before();
						control.Navigator();
						control.After();
						control.Close();
						context.Response.End();
					}break;
					case 86:
					{
						context.Response.ContentType = "text/html";
						Regex regParameters = new Regex("/App/VCode/GetVerifyImage.php");
                        Match matParameters = regParameters.Match(requestedPath);
						App.Web.Default.Common.VCode.GetVerifyImageAction control= new App.Web.Default.Common.VCode.GetVerifyImageAction(context);
						control._methodType = methodType;
						control._context = context;
						control._subdomain = subdomain;
						control._url=requestedPath;
						control._get = get;
						control._app="/App";
						control.Before();
						control.GetVerifyImage();
						control.After();
						control.Close();
						context.Response.End();
					}break;
					case 87:
					{
						context.Response.ContentType = "text/html";
						Regex regParameters = new Regex("/App/VCode/VCheck.php");
                        Match matParameters = regParameters.Match(requestedPath);
						App.Web.Default.Common.VCode.VCheckAction control= new App.Web.Default.Common.VCode.VCheckAction(context);
						control._methodType = methodType;
						control._context = context;
						control._subdomain = subdomain;
						control._url=requestedPath;
						control._get = get;
						control._app="/App";
						control.Before();
						control.VCheck();
						control.After();
						control.Close();
						context.Response.End();
					}break;
                default: break;
            }
        }
    }
}