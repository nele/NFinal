﻿using System;
using System.Collections.Generic;
using System.Web;
using NFinal.Advanced;


namespace WebMvc.App.Web.Default.Sqlite
{
    public class queryTopAction  : Controller
	{
		public queryTopAction(System.IO.TextWriter tw):base(tw){}
		public queryTopAction(string fileName) : base(fileName) {}
		public queryTopAction(HttpContext context):base(context){}
        Models.Tips.Common.users users = new Models.Tips.Common.users();
        
        
        
        
        
        
        
        
        public class __queryTop_us__:NFinal.Struct
		{
			/// <summary>
            /// 用户表ID
            /// </summary>
			public System.Int64 id;
			/// <summary>
            /// 名称
            /// </summary>
			public System.String name;
			/// <summary>
            /// 密码
            /// </summary>
			public System.String pwd;
			#region 写Json字符串
			public override void WriteJson(System.IO.TextWriter tw)
			{
				tw.Write("{");
						tw.Write("\"id\":");
						tw.Write(id.ToString());
						tw.Write(",");
						tw.Write("\"name\":");
						if(name==null)
						{
							tw.Write("null");
						}
						else
						{
							tw.Write("\"");
							tw.Write(name);
							tw.Write("\"");
						}
						tw.Write(",");
						tw.Write("\"pwd\":");
						if(pwd==null)
						{
							tw.Write("null");
						}
						else
						{
							tw.Write("\"");
							tw.Write(pwd);
							tw.Write("\"");
						}
				tw.Write("}");
			}
			#endregion
		}
		public void queryTop(int top)
        {
            var con = new System.Data.SQLite.SQLiteConnection(NFinal.ConfigurationManager.ConnectionStrings["Common"].ConnectionString);
			con.Open();
            #region	var us; 随机选取前N行
			var us = new NFinal.List<__queryTop_us__>();
			var __queryTop_us_command__ = new System.Data.SQLite.SQLiteCommand(string.Format("select users.id,users.name,users.pwd from users Limit {0}",top), con);
			var __queryTop_us_reader__= __queryTop_us_command__.ExecuteReader();
			if (__queryTop_us_reader__.HasRows)
			{
				while (__queryTop_us_reader__.Read())
				{
					var __queryTop_us_temp__ = new __queryTop_us__();
					__queryTop_us_temp__.id = __queryTop_us_reader__.GetInt64(0);
					__queryTop_us_temp__.name =__queryTop_us_reader__.IsDBNull(1)?null: __queryTop_us_reader__.GetString(1);
					__queryTop_us_temp__.pwd =__queryTop_us_reader__.IsDBNull(2)?null: __queryTop_us_reader__.GetString(2);
					us.Add(__queryTop_us_temp__);
				}
			}
			__queryTop_us_reader__.Dispose();
			__queryTop_us_command__.Dispose();
			#endregion
			
            con.Close();
            AjaxReturn(us);
        }
    }
}