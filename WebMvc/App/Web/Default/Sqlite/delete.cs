﻿using System;
using System.Collections.Generic;
using System.Web;
using NFinal.Advanced;


namespace WebMvc.App.Web.Default.Sqlite
{
    public class deleteAction  : Controller
	{
		public deleteAction(System.IO.TextWriter tw):base(tw){}
		public deleteAction(string fileName) : base(fileName) {}
		public deleteAction(HttpContext context):base(context){}
        Models.Tips.Common.users users = new Models.Tips.Common.users();
        
        
        public void delete(int id)
        {
            var con = new System.Data.SQLite.SQLiteConnection(NFinal.ConfigurationManager.ConnectionStrings["Common"].ConnectionString);
			con.Open();
            #region	var count; 删除记录,返回受影响行数
			var __delete_count_command__ = new System.Data.SQLite.SQLiteCommand("delete from users where users.id=@id", con);
			var __delete_count_parameters__=new System.Data.SQLite.SQLiteParameter[1];
			__delete_count_parameters__[0] = new System.Data.SQLite.SQLiteParameter("@id",System.Data.DbType.Int64);
			__delete_count_parameters__[0].Value = id;
			__delete_count_command__.Parameters.AddRange(__delete_count_parameters__);
			var count = __delete_count_command__.ExecuteNonQuery();
			__delete_count_command__.Dispose();
			#endregion
			
            con.Close();
            Write(count);
        }
        
        
        
        
        
        
    }
}