﻿using System;
using System.Collections.Generic;
using System.Web;
using NFinal.Advanced;


namespace WebMvc.App.Web.Default.Sqlite
{
    public class queryAllAction  : Controller
	{
		public queryAllAction(System.IO.TextWriter tw):base(tw){}
		public queryAllAction(string fileName) : base(fileName) {}
		public queryAllAction(HttpContext context):base(context){}
        Models.Tips.Common.users users = new Models.Tips.Common.users();
        
        
        
        
        public class __queryAll_us__:NFinal.Struct
		{
			/// <summary>
            /// 用户表ID
            /// </summary>
			public System.Int64 id;
			/// <summary>
            /// 名称
            /// </summary>
			public System.String name;
			/// <summary>
            /// 密码
            /// </summary>
			public System.String pwd;
			#region 写Json字符串
			public override void WriteJson(System.IO.TextWriter tw)
			{
				tw.Write("{");
						tw.Write("\"id\":");
						tw.Write(id.ToString());
						tw.Write(",");
						tw.Write("\"name\":");
						if(name==null)
						{
							tw.Write("null");
						}
						else
						{
							tw.Write("\"");
							tw.Write(name);
							tw.Write("\"");
						}
						tw.Write(",");
						tw.Write("\"pwd\":");
						if(pwd==null)
						{
							tw.Write("null");
						}
						else
						{
							tw.Write("\"");
							tw.Write(pwd);
							tw.Write("\"");
						}
				tw.Write("}");
			}
			#endregion
		}
		public void queryAll()
        {
            var con = new System.Data.SQLite.SQLiteConnection(NFinal.ConfigurationManager.ConnectionStrings["Common"].ConnectionString);
			con.Open();
            #region	var us;选取所有记录
			var us = new NFinal.List<__queryAll_us__>();
			var __queryAll_us_command__ = new System.Data.SQLite.SQLiteCommand("select users.id,users.name,users.pwd from users", con);
			var __queryAll_us_reader__= __queryAll_us_command__.ExecuteReader();
			if (__queryAll_us_reader__.HasRows)
			{
				while (__queryAll_us_reader__.Read())
				{
					var __queryAll_us_temp__ = new __queryAll_us__();
					__queryAll_us_temp__.id = __queryAll_us_reader__.GetInt64(0);
					__queryAll_us_temp__.name =__queryAll_us_reader__.IsDBNull(1)?null: __queryAll_us_reader__.GetString(1);
					__queryAll_us_temp__.pwd =__queryAll_us_reader__.IsDBNull(2)?null: __queryAll_us_reader__.GetString(2);
					us.Add(__queryAll_us_temp__);
				}
			}
			__queryAll_us_reader__.Dispose();
			__queryAll_us_command__.Dispose();
			#endregion
			
            con.Close();
            AjaxReturn(us);
        }
        
        
        
        
    }
}