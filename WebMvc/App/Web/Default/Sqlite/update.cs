﻿using System;
using System.Collections.Generic;
using System.Web;
using NFinal.Advanced;


namespace WebMvc.App.Web.Default.Sqlite
{
    public class updateAction  : Controller
	{
		public updateAction(System.IO.TextWriter tw):base(tw){}
		public updateAction(string fileName) : base(fileName) {}
		public updateAction(HttpContext context):base(context){}
        Models.Tips.Common.users users = new Models.Tips.Common.users();
        [GetHtml("/update.html")]
        public void update(string name,string pwd)
        {
            var con = new System.Data.SQLite.SQLiteConnection(NFinal.ConfigurationManager.ConnectionStrings["Common"].ConnectionString);
			con.Open();
            #region	var count; 更新表,并返回受影响行数
			var __update_count_command__ = new System.Data.SQLite.SQLiteCommand("update users set name=@name,pwd=@pwd where users.id=1;", con);
			var __update_count_parameters__=new System.Data.SQLite.SQLiteParameter[2];
					__update_count_parameters__[0] = new System.Data.SQLite.SQLiteParameter("@name",System.Data.DbType.String);
					__update_count_parameters__[0].Value = name;
					__update_count_parameters__[1] = new System.Data.SQLite.SQLiteParameter("@pwd",System.Data.DbType.String);
					__update_count_parameters__[1].Value = pwd;
			__update_count_command__.Parameters.AddRange(__update_count_parameters__);
				var count = __update_count_command__.ExecuteNonQuery();
			__update_count_command__.Dispose();
			#endregion
			
            con.Close();
            Write(count);
        }
        
        
        
        
        
        
        
        
    }
}