﻿using System;
using System.Collections.Generic;
using System.Web;
using NFinal.Advanced;


namespace WebMvc.App.Web.Default.Sqlite
{
    public class pageAction  : Controller
	{
		public pageAction(System.IO.TextWriter tw):base(tw){}
		public pageAction(string fileName) : base(fileName) {}
		public pageAction(HttpContext context):base(context){}
        Models.Tips.Common.users users = new Models.Tips.Common.users();
        
        
        
        public class __page_us__:NFinal.Struct
		{
			/// <summary>
            /// 用户表ID
            /// </summary>
			public System.Int64 id;
			/// <summary>
            /// 名称
            /// </summary>
			public System.String name;
			/// <summary>
            /// 密码
            /// </summary>
			public System.String pwd;
			#region 写Json字符串
			public override void WriteJson(System.IO.TextWriter tw)
			{
				tw.Write("{");
						tw.Write("\"id\":");
						tw.Write(id.ToString());
						tw.Write(",");
						tw.Write("\"name\":");
						if(name==null)
						{
							tw.Write("null");
						}
						else
						{
							tw.Write("\"");
							tw.Write(name);
							tw.Write("\"");
						}
						tw.Write(",");
						tw.Write("\"pwd\":");
						if(pwd==null)
						{
							tw.Write("null");
						}
						else
						{
							tw.Write("\"");
							tw.Write(pwd);
							tw.Write("\"");
						}
				tw.Write("}");
			}
			#endregion
		}
		[GetJson("/page-{p}.json")]
        public void page(int p)
        {
            var con = new System.Data.SQLite.SQLiteConnection(NFinal.ConfigurationManager.ConnectionStrings["Common"].ConnectionString);
			con.Open();
            NFinal.Page page = new NFinal.Page(p, 2);
            #region	var us;分页
			var __page_us_command__ = new System.Data.SQLite.SQLiteCommand("select count(*) from users", con);
			page.recordCount =System.Convert.ToInt32(__page_us_command__.ExecuteScalar());
			__page_us_command__.Dispose();
			page.count = (page.recordCount % page.size==0)? page.recordCount/page.size:page.recordCount/page.size+1;
			//传页码时用的变量名
			if (page.index > page.count)
            {
                page.index = page.count;
            }
            if (page.index < 1)
            {
                page.index = 1;
            }
			var us = new NFinal.List<__page_us__>();
            //计算得到SQL语句
			__page_us_command__ = new System.Data.SQLite.SQLiteCommand(string.Format("select users.id,users.name,users.pwd from users Limit {0} Offset {1}",page.size,(page.index-1)*page.size), con);
			var __page_us_reader__= __page_us_command__.ExecuteReader();
			if (__page_us_reader__.HasRows)
			{
				while (__page_us_reader__.Read())
				{
					var __page_us_temp__ = new __page_us__();
					__page_us_temp__.id = __page_us_reader__.GetInt64(0);
					__page_us_temp__.name =__page_us_reader__.IsDBNull(1)?null: __page_us_reader__.GetString(1);
					__page_us_temp__.pwd =__page_us_reader__.IsDBNull(2)?null: __page_us_reader__.GetString(2);
					us.Add(__page_us_temp__);
				}
			}
			__page_us_reader__.Dispose();
			__page_us_command__.Dispose();
			#endregion
			
            con.Close();
            AjaxReturn(us);
        }
        
        
        
        
        
    }
}