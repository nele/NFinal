﻿using System;
using System.Collections.Generic;
using System.Web;
using NFinal.Advanced;


namespace WebMvc.App.Web.Default.Sqlite
{
    public class queryObjectAction  : Controller
	{
		public queryObjectAction(System.IO.TextWriter tw):base(tw){}
		public queryObjectAction(string fileName) : base(fileName) {}
		public queryObjectAction(HttpContext context):base(context){}
        Models.Tips.Common.users users = new Models.Tips.Common.users();
        
        
        
        
        
        public void queryObject()
        {
            var con = new System.Data.SQLite.SQLiteConnection(NFinal.ConfigurationManager.ConnectionStrings["Common"].ConnectionString);
			con.Open();
            #region	var count; 选取首行首列的值
			var __queryObject_count_command__ = new System.Data.SQLite.SQLiteCommand("select count(*) from users", con);
			var count = __queryObject_count_command__.ExecuteScalar().ToInt32();
			__queryObject_count_command__.Dispose();
			#endregion
			
            con.Close();
            Write(count);
        }
        
        
        
    }
}