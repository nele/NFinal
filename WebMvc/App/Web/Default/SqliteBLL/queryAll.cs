﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebMvc.App.Web.Default.SqliteBLL
{
    public class queryAllAction  : Controller
	{
		public queryAllAction(System.IO.TextWriter tw):base(tw){}
		public queryAllAction(string fileName) : base(fileName) {}
		public queryAllAction(HttpContext context):base(context){}
        public void queryAll()
        {
            var con = new System.Data.SQLite.SQLiteConnection(NFinal.ConfigurationManager.ConnectionStrings["Common"].ConnectionString);
			con.Open();
            Models.DAL.Sqlite sqlite = new Models.DAL.Sqlite();
            var users= sqlite.GetAll(con);
            con.Close();
            AjaxReturn(users);
        }
        
    }
}