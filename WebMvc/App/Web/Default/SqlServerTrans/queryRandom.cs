﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebMvc.App.Web.Default.SqlServerTrans
{
    public class queryRandomAction  : Controller
	{
		public queryRandomAction(System.IO.TextWriter tw):base(tw){}
		public queryRandomAction(string fileName) : base(fileName) {}
		public queryRandomAction(HttpContext context):base(context){}
        
        
        
        
        
        
        public class __queryRandom_users__:NFinal.Struct
		{
			public System.Int32 id;
			public System.String name;
			public System.String pwd;
			#region 写Json字符串
			public override void WriteJson(System.IO.TextWriter tw)
			{
				tw.Write("{");
						tw.Write("\"id\":");
						tw.Write(id.ToString());
						tw.Write(",");
						tw.Write("\"name\":");
						if(name==null)
						{
							tw.Write("null");
						}
						else
						{
							tw.Write("\"");
							tw.Write(name);
							tw.Write("\"");
						}
						tw.Write(",");
						tw.Write("\"pwd\":");
						if(pwd==null)
						{
							tw.Write("null");
						}
						else
						{
							tw.Write("\"");
							tw.Write(pwd);
							tw.Write("\"");
						}
				tw.Write("}");
			}
			#endregion
		}
		public void queryRandom()
        {
            var con = new System.Data.SqlClient.SqlConnection(NFinal.ConfigurationManager.ConnectionStrings["SqlServer"].ConnectionString);
			con.Open();
            var trans = con.BeginTransaction();
            try
            {
                #region	var users; 随机选取前N行
			var users = new NFinal.List<__queryRandom_users__>();
			var __queryRandom_users_command__ = new System.Data.SqlClient.SqlCommand(string.Format("select  top {0} * from users order by newid()",3), con);
			__queryRandom_users_command__.Transaction=trans;
			var __queryRandom_users_reader__= __queryRandom_users_command__.ExecuteReader();
			if (__queryRandom_users_reader__.HasRows)
			{
				while (__queryRandom_users_reader__.Read())
				{
					var __queryRandom_users_temp__ = new __queryRandom_users__();
					__queryRandom_users_temp__.id = __queryRandom_users_reader__.GetInt32(0);
					__queryRandom_users_temp__.name =__queryRandom_users_reader__.IsDBNull(1)?null: __queryRandom_users_reader__.GetString(1);
					__queryRandom_users_temp__.pwd =__queryRandom_users_reader__.IsDBNull(2)?null: __queryRandom_users_reader__.GetString(2);
					users.Add(__queryRandom_users_temp__);
				}
			}
			__queryRandom_users_reader__.Dispose();
			__queryRandom_users_command__.Dispose();
			#endregion
			
                trans.Commit();
                AjaxReturn(users);
            }
            catch
            {
                trans.Rollback();
            }
            con.Close();
        }
        
        
    }
}