﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebMvc.App.Web.Default.SqlServer
{
    public class deleteAction  : Controller
	{
		public deleteAction(System.IO.TextWriter tw):base(tw){}
		public deleteAction(string fileName) : base(fileName) {}
		public deleteAction(HttpContext context):base(context){}
        Models.Tips.SqlServer.users users = new Models.Tips.SqlServer.users();
        
        
        public void delete(int id)
        {
            var con = new System.Data.SqlClient.SqlConnection(NFinal.ConfigurationManager.ConnectionStrings["SqlServer"].ConnectionString);
			con.Open();
            #region	var count; 删除记录,返回受影响行数
			var __delete_count_command__ = new System.Data.SqlClient.SqlCommand("delete from users where users.id=@id", con);
			var __delete_count_parameters__=new System.Data.SqlClient.SqlParameter[1];
			__delete_count_parameters__[0] = new System.Data.SqlClient.SqlParameter("@id",System.Data.SqlDbType.Int);
			__delete_count_parameters__[0].Value = id;
			__delete_count_command__.Parameters.AddRange(__delete_count_parameters__);
			var count = __delete_count_command__.ExecuteNonQuery();
			__delete_count_command__.Dispose();
			#endregion
			
            con.Close();
            Write(count);
        }
        
        
        
        
        
        
    }
}