﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebMvc.App.Web.Default.SqlServer
{
    public class insertAction  : Controller
	{
		public insertAction(System.IO.TextWriter tw):base(tw){}
		public insertAction(string fileName) : base(fileName) {}
		public insertAction(HttpContext context):base(context){}
        Models.Tips.SqlServer.users users = new Models.Tips.SqlServer.users();
        
        public void insert(string name,string pwd)
        {
            var con = new System.Data.SqlClient.SqlConnection(NFinal.ConfigurationManager.ConnectionStrings["SqlServer"].ConnectionString);
			con.Open();
            #region	var id; 插入并返回ID
			var __insert_id_command__ = new System.Data.SqlClient.SqlCommand("insert into users(name,pwd) values(@name,@pwd);select @@IDENTITY;", con);
			var __insert_id_parameters__=new System.Data.SqlClient.SqlParameter[2];
			__insert_id_parameters__[0] = new System.Data.SqlClient.SqlParameter("@name",System.Data.SqlDbType.NVarChar,50);
			__insert_id_parameters__[0].Value = name;
			__insert_id_parameters__[1] = new System.Data.SqlClient.SqlParameter("@pwd",System.Data.SqlDbType.NVarChar,50);
			__insert_id_parameters__[1].Value = pwd;
			__insert_id_command__.Parameters.AddRange(__insert_id_parameters__);
				var id = int.Parse(__insert_id_command__.ExecuteScalar().ToString());
			__insert_id_command__.Dispose();
			#endregion
			
            con.Close();
            Write(id);
        }
        
        
        
        
        
        
        
    }
}