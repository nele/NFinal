﻿using System;
using System.Collections.Generic;
using System.Web;

namespace WebMvc.App.Web.Default.Common.Public
{
    public class NavigatorAction  : Controller
	{
		public NavigatorAction(System.IO.TextWriter tw):base(tw){}
		public NavigatorAction(string fileName) : base(fileName) {}
		public NavigatorAction(HttpContext context):base(context){}
        
        
        
        
        public void Navigator()
        {
            NFinal.Page page = new NFinal.Page(1,2);
            
			Write("当前页:");
			Write(page.index);
			Write("/");
			Write(page.count);
			Write("<a href=\"");
			Write(page.GetUrlFunction(1));
			Write("\">首页</a><a href=\"");
			Write(page.index <= 1 ? "#" : page.GetUrlFunction(page.index - 1));
			Write("\">上一页</a>");
			for(int i = ((page.index - 1) / page.navigatorSize) * page.navigatorSize + 1;i <= page.count && i <= ((page.index - 1) / page.navigatorSize + 1) * page.navigatorSize;i++){
				if(i == page.index){
					Write("<a style=\"color:red;\" href=\"");
					Write(page.GetUrlFunction(i));
					Write("\">");
					Write(i);
					Write("</a>");
				}else{
					Write("<a href=\"");
					Write(page.GetUrlFunction(i));
					Write("\">");
					Write(i);
					Write("</a>");
				}
			}
			Write("<a href=\"");
			Write(page.index >= page.count ? "#" : page.GetUrlFunction(page.index + 1));
			Write("\">下一页</a><a href=\"");
			Write(page.GetUrlFunction(page.count));
			Write("\">末页</a>");
        }
        
    }
}