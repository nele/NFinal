﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebMvc.App.Web.Default.SqliteTransStruct
{
    public class queryAllAction  : Controller
	{
		public queryAllAction(System.IO.TextWriter tw):base(tw){}
		public queryAllAction(string fileName) : base(fileName) {}
		public queryAllAction(HttpContext context):base(context){}
        
        
        
        
        public void queryAll()
        {
            var con = new System.Data.SQLite.SQLiteConnection(NFinal.ConfigurationManager.ConnectionStrings["Common"].ConnectionString);
			con.Open();
            var trans = con.BeginTransaction();
            List<Models.Entity.Common.users> users = new List<Models.Entity.Common.users>();
            try
            {
                #region	var users;选取所有记录
			var __queryAll_users_command__ = new System.Data.SQLite.SQLiteCommand("select * from users", con);
			__queryAll_users_command__.Transaction=trans;
			var __queryAll_users_reader__= __queryAll_users_command__.ExecuteReader();
			if (__queryAll_users_reader__.HasRows)
			{
				while (__queryAll_users_reader__.Read())
				{
					var __queryAll_users_temp__ = new Models.Entity.Common.users();
					__queryAll_users_temp__.id = __queryAll_users_reader__.GetInt64(0);
					__queryAll_users_temp__.name =__queryAll_users_reader__.IsDBNull(1)?null: __queryAll_users_reader__.GetString(1);
					__queryAll_users_temp__.pwd =__queryAll_users_reader__.IsDBNull(2)?null: __queryAll_users_reader__.GetString(2);
					users.Add(__queryAll_users_temp__);
				}
			}
			__queryAll_users_reader__.Dispose();
			__queryAll_users_command__.Dispose();
			#endregion
			
                AjaxReturn(users);
                trans.Rollback();
            }
            catch
            {
                trans.Rollback();
            }
            con.Close();
        }
        
        
        
        
    }
}