﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebMvc.App.Web.Default.SqliteTransStruct
{
    public class pageAction  : Controller
	{
		public pageAction(System.IO.TextWriter tw):base(tw){}
		public pageAction(string fileName) : base(fileName) {}
		public pageAction(HttpContext context):base(context){}
        
        
        
        public void page(int p)
        {
            var con = new System.Data.SQLite.SQLiteConnection(NFinal.ConfigurationManager.ConnectionStrings["Common"].ConnectionString);
			con.Open();
            var trans = con.BeginTransaction();
            List<Models.Entity.Common.users> users = new List<Models.Entity.Common.users>();
            NFinal.Page page = new NFinal.Page(p, 2);
            try
            {
                #region	var users;分页
			var __page_users_command__ = new System.Data.SQLite.SQLiteCommand("select count(*) from users", con);
			__page_users_command__.Transaction=trans;
			page.recordCount =System.Convert.ToInt32(__page_users_command__.ExecuteScalar());
			__page_users_command__.Dispose();
			page.count = (page.recordCount % page.size==0)? page.recordCount/page.size:page.recordCount/page.size+1;
			//传页码时用的变量名
			if (page.index > page.count)
            {
                page.index = page.count;
            }
            if (page.index < 1)
            {
                page.index = 1;
            }
            //计算得到SQL语句
			__page_users_command__ = new System.Data.SQLite.SQLiteCommand(string.Format("select * from users Limit {0} Offset {1}",page.size,(page.index-1)*page.size), con);
			__page_users_command__.Transaction=trans;
			var __page_users_reader__= __page_users_command__.ExecuteReader();
			if (__page_users_reader__.HasRows)
			{
				while (__page_users_reader__.Read())
				{
					var __page_users_temp__ = new Models.Entity.Common.users();
					__page_users_temp__.id = __page_users_reader__.GetInt64(0);
					__page_users_temp__.name =__page_users_reader__.IsDBNull(1)?null: __page_users_reader__.GetString(1);
					__page_users_temp__.pwd =__page_users_reader__.IsDBNull(2)?null: __page_users_reader__.GetString(2);
					users.Add(__page_users_temp__);
				}
			}
			__page_users_reader__.Dispose();
			__page_users_command__.Dispose();
			#endregion
			
                AjaxReturn(users);
                trans.Commit();
            }
            catch
            {
                trans.Rollback();
            }
            con.Close();
        }
        
        
        
        
        
    }
}