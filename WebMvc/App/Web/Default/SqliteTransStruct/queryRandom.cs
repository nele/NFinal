﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebMvc.App.Web.Default.SqliteTransStruct
{
    public class queryRandomAction  : Controller
	{
		public queryRandomAction(System.IO.TextWriter tw):base(tw){}
		public queryRandomAction(string fileName) : base(fileName) {}
		public queryRandomAction(HttpContext context):base(context){}
        
        
        
        
        
        
        public void queryRandom()
        {
            var con = new System.Data.SQLite.SQLiteConnection(NFinal.ConfigurationManager.ConnectionStrings["Common"].ConnectionString);
			con.Open();
            var trans = con.BeginTransaction();
            List<Models.Entity.Common.users> users = new List<Models.Entity.Common.users>();
            try
            {
                #region	var users; 随机选取前N行
			var __queryRandom_users_command__ = new System.Data.SQLite.SQLiteCommand(string.Format("select * from users order by random() limit {0}",3), con);
			__queryRandom_users_command__.Transaction=trans;
			var __queryRandom_users_reader__= __queryRandom_users_command__.ExecuteReader();
			if (__queryRandom_users_reader__.HasRows)
			{
				while (__queryRandom_users_reader__.Read())
				{
					var __queryRandom_users_temp__ = new Models.Entity.Common.users();
					__queryRandom_users_temp__.id = __queryRandom_users_reader__.GetInt64(0);
					__queryRandom_users_temp__.name =__queryRandom_users_reader__.IsDBNull(1)?null: __queryRandom_users_reader__.GetString(1);
					__queryRandom_users_temp__.pwd =__queryRandom_users_reader__.IsDBNull(2)?null: __queryRandom_users_reader__.GetString(2);
					users.Add(__queryRandom_users_temp__);
				}
			}
			__queryRandom_users_reader__.Dispose();
			__queryRandom_users_command__.Dispose();
			#endregion
			
                trans.Commit();
                AjaxReturn(users);
            }
            catch
            {
                trans.Rollback();
            }
            con.Close();
        }
        
        
    }
}