﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebMvc.App.Web.Default.SqliteTransStruct
{
    public class insertAction  : Controller
	{
		public insertAction(System.IO.TextWriter tw):base(tw){}
		public insertAction(string fileName) : base(fileName) {}
		public insertAction(HttpContext context):base(context){}
        
        public void insert(string name,string pwd)
        {
            var con = new System.Data.SQLite.SQLiteConnection(NFinal.ConfigurationManager.ConnectionStrings["Common"].ConnectionString);
			con.Open();
            var trans = con.BeginTransaction();
            int id = 0;
            try
            {
                #region	var id; 插入并返回ID
			var __insert_id_command__ = new System.Data.SQLite.SQLiteCommand("insert into users(name,pwd) values(@name,@pwd);select last_insert_rowid();", con);
			__insert_id_command__.Transaction=trans;
			var __insert_id_parameters__=new System.Data.SQLite.SQLiteParameter[2];
			__insert_id_parameters__[0] = new System.Data.SQLite.SQLiteParameter("@name",System.Data.DbType.String);
			__insert_id_parameters__[0].Value = name;
			__insert_id_parameters__[1] = new System.Data.SQLite.SQLiteParameter("@pwd",System.Data.DbType.String);
			__insert_id_parameters__[1].Value = pwd;
			__insert_id_command__.Parameters.AddRange(__insert_id_parameters__);
				id = int.Parse(__insert_id_command__.ExecuteScalar().ToString());
			__insert_id_command__.Dispose();
			#endregion
			
                trans.Commit();
            }
            catch
            {
                trans.Rollback();
            }
            con.Close();
            Write(id);
        }
        
        
        
        
        
        
        
    }
}