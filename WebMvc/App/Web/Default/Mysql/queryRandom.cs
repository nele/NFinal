﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebMvc.App.Web.Default.Mysql
{
    public class queryRandomAction  : Controller
	{
		public queryRandomAction(System.IO.TextWriter tw):base(tw){}
		public queryRandomAction(string fileName) : base(fileName) {}
		public queryRandomAction(HttpContext context):base(context){}
        Models.Tips.MySql.users users = new Models.Tips.MySql.users();
        
        
        
        
        
        
        public class __queryRandom_us__:NFinal.Struct
		{
			public System.Int32 id;
			public System.String name;
			public System.String pwd;
			#region 写Json字符串
			public override void WriteJson(System.IO.TextWriter tw)
			{
				tw.Write("{");
						tw.Write("\"id\":");
						tw.Write(id.ToString());
						tw.Write(",");
						tw.Write("\"name\":");
						if(name==null)
						{
							tw.Write("null");
						}
						else
						{
							tw.Write("\"");
							tw.Write(name);
							tw.Write("\"");
						}
						tw.Write(",");
						tw.Write("\"pwd\":");
						if(pwd==null)
						{
							tw.Write("null");
						}
						else
						{
							tw.Write("\"");
							tw.Write(pwd);
							tw.Write("\"");
						}
				tw.Write("}");
			}
			#endregion
		}
		public void queryRandom()
        {
            var con = new MySql.Data.MySqlClient.MySqlConnection(NFinal.ConfigurationManager.ConnectionStrings["MySql"].ConnectionString);
			con.Open();
            	#region	var us; 随机选取前N行
			var us = new NFinal.List<__queryRandom_us__>();
			var __queryRandom_us_command__ = new MySql.Data.MySqlClient.MySqlCommand(string.Format("select users.id,users.name,users.pwd from users order by rand() limit {0}",3), con);
			var __queryRandom_us_reader__= __queryRandom_us_command__.ExecuteReader();
			if (__queryRandom_us_reader__.HasRows)
			{
				while (__queryRandom_us_reader__.Read())
				{
					var __queryRandom_us_temp__ = new __queryRandom_us__();
					__queryRandom_us_temp__.id = __queryRandom_us_reader__.GetInt32(0);
					__queryRandom_us_temp__.name =__queryRandom_us_reader__.IsDBNull(1)?null: __queryRandom_us_reader__.GetString(1);
					__queryRandom_us_temp__.pwd =__queryRandom_us_reader__.IsDBNull(2)?null: __queryRandom_us_reader__.GetString(2);
					us.Add(__queryRandom_us_temp__);
				}
			}
			__queryRandom_us_reader__.Dispose();
			__queryRandom_us_command__.Dispose();
	#endregion
			
            con.Close();
            AjaxReturn(us);
        }
        
        
    }
}