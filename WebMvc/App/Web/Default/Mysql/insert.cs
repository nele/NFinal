﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebMvc.App.Web.Default.Mysql
{
    public class insertAction  : Controller
	{
		public insertAction(System.IO.TextWriter tw):base(tw){}
		public insertAction(string fileName) : base(fileName) {}
		public insertAction(HttpContext context):base(context){}
        Models.Tips.MySql.users users = new Models.Tips.MySql.users();
        
        public void insert(string name,string pwd)
        {
            var con = new MySql.Data.MySqlClient.MySqlConnection(NFinal.ConfigurationManager.ConnectionStrings["MySql"].ConnectionString);
			con.Open();
            #region	var id; 插入并返回ID
			var __insert_id_command__ = new MySql.Data.MySqlClient.MySqlCommand("insert into users(users.name,users.pwd) values(?name,?pwd);select @@IDENTITY;", con);
			var __insert_id_parameters__=new MySql.Data.MySqlClient.MySqlParameter[2];
			__insert_id_parameters__[0] = new MySql.Data.MySqlClient.MySqlParameter("?name",MySql.Data.MySqlClient.MySqlDbType.VarChar,255);
			__insert_id_parameters__[0].Value = name;
			__insert_id_parameters__[1] = new MySql.Data.MySqlClient.MySqlParameter("?pwd",MySql.Data.MySqlClient.MySqlDbType.VarChar,255);
			__insert_id_parameters__[1].Value = pwd;
			__insert_id_command__.Parameters.AddRange(__insert_id_parameters__);
				var id = int.Parse(__insert_id_command__.ExecuteScalar().ToString());
			__insert_id_command__.Dispose();
			#endregion
			
            con.Close();
            Write(id);
        }
        
        
        
        
        
        
        
    }
}