﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebMvc.App.Web.Default.Mysql
{
    public class deleteAction  : Controller
	{
		public deleteAction(System.IO.TextWriter tw):base(tw){}
		public deleteAction(string fileName) : base(fileName) {}
		public deleteAction(HttpContext context):base(context){}
        Models.Tips.MySql.users users = new Models.Tips.MySql.users();
        
        
        public void delete(int id)
        {
            var con = new MySql.Data.MySqlClient.MySqlConnection(NFinal.ConfigurationManager.ConnectionStrings["MySql"].ConnectionString);
			con.Open();
            #region	var count; 删除记录,返回受影响行数
			var __delete_count_command__ = new MySql.Data.MySqlClient.MySqlCommand("delete from users where users.id=?id;", con);
			var __delete_count_parameters__=new MySql.Data.MySqlClient.MySqlParameter[1];
			__delete_count_parameters__[0] = new MySql.Data.MySqlClient.MySqlParameter("?id",MySql.Data.MySqlClient.MySqlDbType.Int32);
			__delete_count_parameters__[0].Value = id;
			__delete_count_command__.Parameters.AddRange(__delete_count_parameters__);
			var count = __delete_count_command__.ExecuteNonQuery();
			__delete_count_command__.Dispose();
			#endregion
			
            con.Close();
            Write(count);
        }
        
        
        
        
        
        
    }
}