﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebMvc.App.Web.Default.Mysql
{
    public class queryTopAction  : Controller
	{
		public queryTopAction(System.IO.TextWriter tw):base(tw){}
		public queryTopAction(string fileName) : base(fileName) {}
		public queryTopAction(HttpContext context):base(context){}
        Models.Tips.MySql.users users = new Models.Tips.MySql.users();
        
        
        
        
        
        
        
        
        public class __queryTop_us__:NFinal.Struct
		{
			public System.Int32 id;
			public System.String name;
			public System.String pwd;
			#region 写Json字符串
			public override void WriteJson(System.IO.TextWriter tw)
			{
				tw.Write("{");
						tw.Write("\"id\":");
						tw.Write(id.ToString());
						tw.Write(",");
						tw.Write("\"name\":");
						if(name==null)
						{
							tw.Write("null");
						}
						else
						{
							tw.Write("\"");
							tw.Write(name);
							tw.Write("\"");
						}
						tw.Write(",");
						tw.Write("\"pwd\":");
						if(pwd==null)
						{
							tw.Write("null");
						}
						else
						{
							tw.Write("\"");
							tw.Write(pwd);
							tw.Write("\"");
						}
				tw.Write("}");
			}
			#endregion
		}
		public void queryTop(int top)
        {
            var con = new MySql.Data.MySqlClient.MySqlConnection(NFinal.ConfigurationManager.ConnectionStrings["MySql"].ConnectionString);
			con.Open();
            #region	var us; 随机选取前N行
			var us = new NFinal.List<__queryTop_us__>();
			var __queryTop_us_command__ = new MySql.Data.MySqlClient.MySqlCommand(string.Format("select users.id,users.name,users.pwd from users limit {0}",top), con);
			var __queryTop_us_reader__= __queryTop_us_command__.ExecuteReader();
			if (__queryTop_us_reader__.HasRows)
			{
				while (__queryTop_us_reader__.Read())
				{
					var __queryTop_us_temp__ = new __queryTop_us__();
					__queryTop_us_temp__.id = __queryTop_us_reader__.GetInt32(0);
					__queryTop_us_temp__.name =__queryTop_us_reader__.IsDBNull(1)?null: __queryTop_us_reader__.GetString(1);
					__queryTop_us_temp__.pwd =__queryTop_us_reader__.IsDBNull(2)?null: __queryTop_us_reader__.GetString(2);
					us.Add(__queryTop_us_temp__);
				}
			}
			__queryTop_us_reader__.Dispose();
			__queryTop_us_command__.Dispose();
			#endregion
			
            con.Close();
            AjaxReturn(us);
        }
    }
}