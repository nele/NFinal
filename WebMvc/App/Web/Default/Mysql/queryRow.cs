﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebMvc.App.Web.Default.Mysql
{
    public class queryRowAction  : Controller
	{
		public queryRowAction(System.IO.TextWriter tw):base(tw){}
		public queryRowAction(string fileName) : base(fileName) {}
		public queryRowAction(HttpContext context):base(context){}
        Models.Tips.MySql.users users = new Models.Tips.MySql.users();
        
        
        
        
        
        
        
        public class __queryRow_user__:NFinal.Struct
		{
			public System.Int32 id;
			public System.String name;
			public System.String pwd;
			#region 写Json字符串
			public override void WriteJson(System.IO.TextWriter tw)
			{
				tw.Write("{");
						tw.Write("\"id\":");
						tw.Write(id.ToString());
						tw.Write(",");
						tw.Write("\"name\":");
						if(name==null)
						{
							tw.Write("null");
						}
						else
						{
							tw.Write("\"");
							tw.Write(name);
							tw.Write("\"");
						}
						tw.Write(",");
						tw.Write("\"pwd\":");
						if(pwd==null)
						{
							tw.Write("null");
						}
						else
						{
							tw.Write("\"");
							tw.Write(pwd);
							tw.Write("\"");
						}
				tw.Write("}");
			}
			#endregion
		}
		public void queryRow(int id)
        {
            var con = new MySql.Data.MySqlClient.MySqlConnection(NFinal.ConfigurationManager.ConnectionStrings["MySql"].ConnectionString);
			con.Open();
            #region	var user; 选取一行
			var user = new __queryRow_user__();
			var __queryRow_user_command__ = new MySql.Data.MySqlClient.MySqlCommand("select users.id,users.name,users.pwd from users where users.id=?id", con);
			var __queryRow_user_parameters__=new MySql.Data.MySqlClient.MySqlParameter[1];
			__queryRow_user_parameters__[0] = new MySql.Data.MySqlClient.MySqlParameter("?id",MySql.Data.MySqlClient.MySqlDbType.Int32,11);
			__queryRow_user_parameters__[0].Value = id;
			__queryRow_user_command__.Parameters.AddRange(__queryRow_user_parameters__);
			var __queryRow_user_reader__= __queryRow_user_command__.ExecuteReader();
			if (__queryRow_user_reader__.HasRows)
			{
				__queryRow_user_reader__.Read();
				user = new __queryRow_user__();
					user.id = __queryRow_user_reader__.GetInt32(0);
					user.name =__queryRow_user_reader__.IsDBNull(1)?null: __queryRow_user_reader__.GetString(1);
					user.pwd =__queryRow_user_reader__.IsDBNull(2)?null: __queryRow_user_reader__.GetString(2);
			}
			else
			{
				user = null;
			}
			__queryRow_user_reader__.Dispose();
			__queryRow_user_command__.Dispose();
	#endregion
			
            con.Close();
            AjaxReturn(user);
        }
        
    }
}