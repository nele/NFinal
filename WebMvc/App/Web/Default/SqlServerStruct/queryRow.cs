﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebMvc.App.Web.Default.SqlServerStruct
{
    public class queryRowAction  : Controller
	{
		public queryRowAction(System.IO.TextWriter tw):base(tw){}
		public queryRowAction(string fileName) : base(fileName) {}
		public queryRowAction(HttpContext context):base(context){}
        

        
        
        
        
        
        
        public void queryRow(int id)
        {
            var con = new System.Data.SqlClient.SqlConnection(NFinal.ConfigurationManager.ConnectionStrings["SqlServer"].ConnectionString);
			con.Open();
            Models.Entity.SqlServer.users user = new Models.Entity.SqlServer.users();
            #region	var user; 选取一行
			var __queryRow_user_command__ = new System.Data.SqlClient.SqlCommand("select * from users where id=@id", con);
			var __queryRow_user_parameters__=new System.Data.SqlClient.SqlParameter[1];
			__queryRow_user_parameters__[0] = new System.Data.SqlClient.SqlParameter("@id",System.Data.SqlDbType.Int,4);
			__queryRow_user_parameters__[0].Value = id;
			__queryRow_user_command__.Parameters.AddRange(__queryRow_user_parameters__);
			var __queryRow_user_reader__= __queryRow_user_command__.ExecuteReader();
			if (__queryRow_user_reader__.HasRows)
			{
				__queryRow_user_reader__.Read();
				user = new Models.Entity.SqlServer.users();
					user.id = __queryRow_user_reader__.GetInt32(0);
					user.name =__queryRow_user_reader__.IsDBNull(1)?null: __queryRow_user_reader__.GetString(1);
					user.pwd =__queryRow_user_reader__.IsDBNull(2)?null: __queryRow_user_reader__.GetString(2);
			}
			else
			{
				user = null;
			}
			__queryRow_user_reader__.Dispose();
			__queryRow_user_command__.Dispose();
			#endregion
			
            con.Close();
            AjaxReturn(user);
        }
        
    }
}