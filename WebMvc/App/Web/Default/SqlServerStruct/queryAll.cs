﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebMvc.App.Web.Default.SqlServerStruct
{
    public class queryAllAction  : Controller
	{
		public queryAllAction(System.IO.TextWriter tw):base(tw){}
		public queryAllAction(string fileName) : base(fileName) {}
		public queryAllAction(HttpContext context):base(context){}
        

        
        
        
        public void queryAll()
        {
            var con = new System.Data.SqlClient.SqlConnection(NFinal.ConfigurationManager.ConnectionStrings["SqlServer"].ConnectionString);
			con.Open();
            List<Models.Entity.SqlServer.users> users = new List<Models.Entity.SqlServer.users>();
            #region	var users;选取所有记录
			var __queryAll_users_command__ = new System.Data.SqlClient.SqlCommand("select * from users", con);
			var __queryAll_users_reader__= __queryAll_users_command__.ExecuteReader();
			if (__queryAll_users_reader__.HasRows)
			{
				while (__queryAll_users_reader__.Read())
				{
					var __queryAll_users_temp__ = new Models.Entity.SqlServer.users();
					if(!__queryAll_users_reader__.IsDBNull(0)){__queryAll_users_temp__.id = __queryAll_users_reader__.GetInt32(0);}
					__queryAll_users_temp__.name = __queryAll_users_reader__.GetString(1);
					__queryAll_users_temp__.pwd = __queryAll_users_reader__.GetString(2);
					users.Add(__queryAll_users_temp__);
				}
			}
			__queryAll_users_reader__.Dispose();
			__queryAll_users_command__.Dispose();
			#endregion
			
            con.Close();
            AjaxReturn(users);
        }
        
        
        
        
    }
}