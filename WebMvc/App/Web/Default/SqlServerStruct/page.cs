﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebMvc.App.Web.Default.SqlServerStruct
{
    public class pageAction  : Controller
	{
		public pageAction(System.IO.TextWriter tw):base(tw){}
		public pageAction(string fileName) : base(fileName) {}
		public pageAction(HttpContext context):base(context){}
        

        
        
        public void page(int p)
        {
            var con = new System.Data.SqlClient.SqlConnection(NFinal.ConfigurationManager.ConnectionStrings["SqlServer"].ConnectionString);
			con.Open();
            List<Models.Entity.SqlServer.users> users = new List<Models.Entity.SqlServer.users>();
            NFinal.Page page = new NFinal.Page(p, 2);
            #region	var users;分页
			var __page_users_command__ = new System.Data.SqlClient.SqlCommand("select count(*) from users", con);
			page.recordCount =System.Convert.ToInt32(__page_users_command__.ExecuteScalar());
			//令SqlParameter可以重复使用到不用的SqlCommand中
			__page_users_command__.Parameters.Clear();
			__page_users_command__.Dispose();
			page.count = (page.recordCount % page.size==0)? page.recordCount/page.size:page.recordCount/page.size+1;
			//传页码时用的变量名
			int __pageIndex__=_get["_page"]==null?1:int.Parse(_get["_page"]);
			if (page.index > page.count)
            {
                page.index = page.count;
            }
            if (page.index < 1)
            {
                page.index = 1;
            }
            //计算得到SQL语句
			__page_users_command__ = new System.Data.SqlClient.SqlCommand(string.Format("select  top {0} * from users where id not in (select top {1} id from users) ",page.size,(page.index-1)*page.size), con);
			var __page_users_reader__= __page_users_command__.ExecuteReader();
			if (__page_users_reader__.HasRows)
			{
				while (__page_users_reader__.Read())
				{
					var __page_users_temp__ = new Models.Entity.SqlServer.users();
					__page_users_temp__.id = __page_users_reader__.GetInt32(0);
					__page_users_temp__.name =__page_users_reader__.IsDBNull(1)?null: __page_users_reader__.GetString(1);
					__page_users_temp__.pwd =__page_users_reader__.IsDBNull(2)?null: __page_users_reader__.GetString(2);
					users.Add(__page_users_temp__);
				}
			}
			__page_users_reader__.Dispose();
			__page_users_command__.Dispose();
			#endregion
			
            con.Close();
            AjaxReturn(users);
        }
        
        
        
        
        
    }
}