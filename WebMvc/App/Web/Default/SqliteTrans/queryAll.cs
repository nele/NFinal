﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebMvc.App.Web.Default.SqliteTrans
{
    public class queryAllAction  : Controller
	{
		public queryAllAction(System.IO.TextWriter tw):base(tw){}
		public queryAllAction(string fileName) : base(fileName) {}
		public queryAllAction(HttpContext context):base(context){}
        
        
        
        
        public class __queryAll_users__:NFinal.Struct
		{
			/// <summary>
            /// 用户表ID
            /// </summary>
			public System.Int64 id;
			/// <summary>
            /// 名称
            /// </summary>
			public System.String name;
			/// <summary>
            /// 密码
            /// </summary>
			public System.String pwd;
			#region 写Json字符串
			public override void WriteJson(System.IO.TextWriter tw)
			{
				tw.Write("{");
						tw.Write("\"id\":");
						tw.Write(id.ToString());
						tw.Write(",");
						tw.Write("\"name\":");
						if(name==null)
						{
							tw.Write("null");
						}
						else
						{
							tw.Write("\"");
							tw.Write(name);
							tw.Write("\"");
						}
						tw.Write(",");
						tw.Write("\"pwd\":");
						if(pwd==null)
						{
							tw.Write("null");
						}
						else
						{
							tw.Write("\"");
							tw.Write(pwd);
							tw.Write("\"");
						}
				tw.Write("}");
			}
			#endregion
		}
		public void queryAll()
        {
            var con = new System.Data.SQLite.SQLiteConnection(NFinal.ConfigurationManager.ConnectionStrings["Common"].ConnectionString);
			con.Open();
            var trans = con.BeginTransaction();
            try
            {
                #region	var users;选取所有记录
			var users = new NFinal.List<__queryAll_users__>();
			var __queryAll_users_command__ = new System.Data.SQLite.SQLiteCommand("select * from users", con);
			__queryAll_users_command__.Transaction=trans;
			var __queryAll_users_reader__= __queryAll_users_command__.ExecuteReader();
			if (__queryAll_users_reader__.HasRows)
			{
				while (__queryAll_users_reader__.Read())
				{
					var __queryAll_users_temp__ = new __queryAll_users__();
					__queryAll_users_temp__.id = __queryAll_users_reader__.GetInt64(0);
					__queryAll_users_temp__.name =__queryAll_users_reader__.IsDBNull(1)?null: __queryAll_users_reader__.GetString(1);
					__queryAll_users_temp__.pwd =__queryAll_users_reader__.IsDBNull(2)?null: __queryAll_users_reader__.GetString(2);
					users.Add(__queryAll_users_temp__);
				}
			}
			__queryAll_users_reader__.Dispose();
			__queryAll_users_command__.Dispose();
			#endregion
			
                AjaxReturn(users);
                trans.Rollback();
            }
            catch
            {
                trans.Rollback();
            }
            con.Close();
        }
        
        
        
        
    }
}