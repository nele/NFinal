﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebMvc.App.Web.Default.SqliteTrans
{
    public class queryObjectAction  : Controller
	{
		public queryObjectAction(System.IO.TextWriter tw):base(tw){}
		public queryObjectAction(string fileName) : base(fileName) {}
		public queryObjectAction(HttpContext context):base(context){}
        
        
        
        
        
        public void queryObject()
        {
            var con = new System.Data.SQLite.SQLiteConnection(NFinal.ConfigurationManager.ConnectionStrings["Common"].ConnectionString);
			con.Open();
            var trans = con.BeginTransaction();
            try
            {
                #region	var count; 选取首行首列的值
			var __queryObject_count_command__ = new System.Data.SQLite.SQLiteCommand("select count(*) from users", con);
			__queryObject_count_command__.Transaction=trans;
			var count = __queryObject_count_command__.ExecuteScalar().ToInt32();
			__queryObject_count_command__.Dispose();
			#endregion
			
                trans.Commit();
                Write(count);
            }
            catch
            {
                trans.Rollback();
            }
            con.Close();
        }
        
        
        
    }
}