﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebMvc.App.Web.Default.SqliteTrans
{
    public class queryRowAction  : Controller
	{
		public queryRowAction(System.IO.TextWriter tw):base(tw){}
		public queryRowAction(string fileName) : base(fileName) {}
		public queryRowAction(HttpContext context):base(context){}
        
        
        
        
        
        
        
        public class __queryRow_user__:NFinal.Struct
		{
			/// <summary>
            /// 用户表ID
            /// </summary>
			public System.Int64 id;
			/// <summary>
            /// 名称
            /// </summary>
			public System.String name;
			/// <summary>
            /// 密码
            /// </summary>
			public System.String pwd;
			#region 写Json字符串
			public override void WriteJson(System.IO.TextWriter tw)
			{
				tw.Write("{");
						tw.Write("\"id\":");
						tw.Write(id.ToString());
						tw.Write(",");
						tw.Write("\"name\":");
						if(name==null)
						{
							tw.Write("null");
						}
						else
						{
							tw.Write("\"");
							tw.Write(name);
							tw.Write("\"");
						}
						tw.Write(",");
						tw.Write("\"pwd\":");
						if(pwd==null)
						{
							tw.Write("null");
						}
						else
						{
							tw.Write("\"");
							tw.Write(pwd);
							tw.Write("\"");
						}
				tw.Write("}");
			}
			#endregion
		}
		public void queryRow(int id)
        {
            var con = new System.Data.SQLite.SQLiteConnection(NFinal.ConfigurationManager.ConnectionStrings["Common"].ConnectionString);
			con.Open();
            var trans = con.BeginTransaction();
            try
            {
                #region	var user; 选取一行
			var user = new __queryRow_user__();
			var __queryRow_user_command__ = new System.Data.SQLite.SQLiteCommand("select * from users where id=@id", con);
			__queryRow_user_command__.Transaction=trans;
			var __queryRow_user_parameters__=new System.Data.SQLite.SQLiteParameter[1];
			__queryRow_user_parameters__[0] = new System.Data.SQLite.SQLiteParameter("@id",System.Data.DbType.Int64,8);
			__queryRow_user_parameters__[0].Value = id;
			__queryRow_user_command__.Parameters.AddRange(__queryRow_user_parameters__);
			var __queryRow_user_reader__= __queryRow_user_command__.ExecuteReader();
			if (__queryRow_user_reader__.HasRows)
			{
				__queryRow_user_reader__.Read();
				user = new __queryRow_user__();
					user.id = __queryRow_user_reader__.GetInt64(0);
					user.name =__queryRow_user_reader__.IsDBNull(1)?null: __queryRow_user_reader__.GetString(1);
					user.pwd =__queryRow_user_reader__.IsDBNull(2)?null: __queryRow_user_reader__.GetString(2);
			}
			else
			{
				user = null;
			}
			__queryRow_user_reader__.Dispose();
			__queryRow_user_command__.Dispose();
			#endregion
			
                trans.Commit();
                AjaxReturn(user);
            }
            catch
            {
                trans.Rollback();
            }
            con.Close();
        }
        
    }
}