﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebMvc.App.Web.Default.SqliteTrans
{
    public class updateAction  : Controller
	{
		public updateAction(System.IO.TextWriter tw):base(tw){}
		public updateAction(string fileName) : base(fileName) {}
		public updateAction(HttpContext context):base(context){}
        public void update(string name,string pwd)
        {
            var con = new System.Data.SQLite.SQLiteConnection(NFinal.ConfigurationManager.ConnectionStrings["Common"].ConnectionString);
			con.Open();
            var trans = con.BeginTransaction();
            int count = 0;
            try
            {
                #region	var count; 更新表,并返回受影响行数
			var __update_count_command__ = new System.Data.SQLite.SQLiteCommand("update users set name=@name,pwd=@pwd where id=1;", con);
			__update_count_command__.Transaction=trans;
			var __update_count_parameters__=new System.Data.SQLite.SQLiteParameter[2];
					__update_count_parameters__[0] = new System.Data.SQLite.SQLiteParameter("@name",System.Data.DbType.String);
					__update_count_parameters__[0].Value = name;
					__update_count_parameters__[1] = new System.Data.SQLite.SQLiteParameter("@pwd",System.Data.DbType.String);
					__update_count_parameters__[1].Value = pwd;
			__update_count_command__.Parameters.AddRange(__update_count_parameters__);
				count = __update_count_command__.ExecuteNonQuery();
			__update_count_command__.Dispose();
			#endregion
			
                trans.Commit();
            }
            catch
            {
                trans.Rollback();
            }
            con.Close();
            Write(count);
        }
        
        
        
        
        
        
        
        
    }
}