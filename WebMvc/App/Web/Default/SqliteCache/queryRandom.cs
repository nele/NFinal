﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebMvc.App.Web.Default.SqliteCache
{
    public class queryRandomAction  : Controller
	{
		public queryRandomAction(System.IO.TextWriter tw):base(tw){}
		public queryRandomAction(string fileName) : base(fileName) {}
		public queryRandomAction(HttpContext context):base(context){}
        
        public void queryRandom()
        {
            var client = NFinal.Cache.GetRedisClient();
            var client2 = client.As<Models.Entity.Common.users>();
            var users = (from u in client2.GetAll() orderby Guid.NewGuid() select u).ToList();
            if (users.Count == 0)
            {
                var con = new System.Data.SQLite.SQLiteConnection(NFinal.ConfigurationManager.ConnectionStrings["Common"].ConnectionString);
			con.Open();
                #region	var users;选取所有记录
			var __queryRandom_users_command__ = new System.Data.SQLite.SQLiteCommand("select * from users", con);
			var __queryRandom_users_reader__= __queryRandom_users_command__.ExecuteReader();
			if (__queryRandom_users_reader__.HasRows)
			{
				while (__queryRandom_users_reader__.Read())
				{
					var __queryRandom_users_temp__ = new Models.Entity.Common.users();
					__queryRandom_users_temp__.id = __queryRandom_users_reader__.GetInt64(0);
					__queryRandom_users_temp__.name =__queryRandom_users_reader__.IsDBNull(1)?null: __queryRandom_users_reader__.GetString(1);
					__queryRandom_users_temp__.pwd =__queryRandom_users_reader__.IsDBNull(2)?null: __queryRandom_users_reader__.GetString(2);
					users.Add(__queryRandom_users_temp__);
				}
			}
			__queryRandom_users_reader__.Dispose();
			__queryRandom_users_command__.Dispose();
			#endregion
			
                con.Close();
                client2.StoreAll(users);
            }
            client.Dispose();
        }
        
        
        
    }
}