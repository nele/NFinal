﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebMvc.App.Web.Default.SqliteCache
{
    public class queryTopAction  : Controller
	{
		public queryTopAction(System.IO.TextWriter tw):base(tw){}
		public queryTopAction(string fileName) : base(fileName) {}
		public queryTopAction(HttpContext context):base(context){}
        
        
        public void queryTop()
        {
            var client = NFinal.Cache.GetRedisClient();
            var client2 = client.As<Models.Entity.Common.users>();
            var users = client2.GetAll().Take(10).ToList();
            if (users.Count == 0)
            {
                var con = new System.Data.SQLite.SQLiteConnection(NFinal.ConfigurationManager.ConnectionStrings["Common"].ConnectionString);
			con.Open();
                #region	var users;选取所有记录
			var __queryTop_users_command__ = new System.Data.SQLite.SQLiteCommand("select * from users", con);
			var __queryTop_users_reader__= __queryTop_users_command__.ExecuteReader();
			if (__queryTop_users_reader__.HasRows)
			{
				while (__queryTop_users_reader__.Read())
				{
					var __queryTop_users_temp__ = new Models.Entity.Common.users();
					__queryTop_users_temp__.id = __queryTop_users_reader__.GetInt64(0);
					__queryTop_users_temp__.name =__queryTop_users_reader__.IsDBNull(1)?null: __queryTop_users_reader__.GetString(1);
					__queryTop_users_temp__.pwd =__queryTop_users_reader__.IsDBNull(2)?null: __queryTop_users_reader__.GetString(2);
					users.Add(__queryTop_users_temp__);
				}
			}
			__queryTop_users_reader__.Dispose();
			__queryTop_users_command__.Dispose();
			#endregion
			
                con.Close();
                client2.StoreAll(users);
            }
            client.Dispose();
        }
        
        
    }
}