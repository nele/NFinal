﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebMvc.App.Web.Default.SqliteCache
{
    public class queryAllAction  : Controller
	{
		public queryAllAction(System.IO.TextWriter tw):base(tw){}
		public queryAllAction(string fileName) : base(fileName) {}
		public queryAllAction(HttpContext context):base(context){}
        public void queryAll()
        {
            //选取所有的参数并缓存
            var client = NFinal.Cache.GetRedisClient();
            var client2 = client.As<Models.Entity.Common.users>();
            var users = client2.GetAll();
            if (users.Count==0)
            {
                var con = new System.Data.SQLite.SQLiteConnection(NFinal.ConfigurationManager.ConnectionStrings["Common"].ConnectionString);
			con.Open();
                #region	var users;选取所有记录
			var __queryAll_users_command__ = new System.Data.SQLite.SQLiteCommand("select * from users", con);
			var __queryAll_users_reader__= __queryAll_users_command__.ExecuteReader();
			if (__queryAll_users_reader__.HasRows)
			{
				while (__queryAll_users_reader__.Read())
				{
					var __queryAll_users_temp__ = new Models.Entity.Common.users();
					__queryAll_users_temp__.id = __queryAll_users_reader__.GetInt64(0);
					__queryAll_users_temp__.name =__queryAll_users_reader__.IsDBNull(1)?null: __queryAll_users_reader__.GetString(1);
					__queryAll_users_temp__.pwd =__queryAll_users_reader__.IsDBNull(2)?null: __queryAll_users_reader__.GetString(2);
					users.Add(__queryAll_users_temp__);
				}
			}
			__queryAll_users_reader__.Dispose();
			__queryAll_users_command__.Dispose();
			#endregion
			
                client2.StoreAll(users);
                con.Close();
            }
            client.Dispose();
        }
        
        
        
        
    }
}