﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebMvc.App.Web.Default.SqliteCache
{
    public class PageAction  : Controller
	{
		public PageAction(System.IO.TextWriter tw):base(tw){}
		public PageAction(string fileName) : base(fileName) {}
		public PageAction(HttpContext context):base(context){}
        
        
        
        
        public void Page(int p)
        {
            var client = NFinal.Cache.GetRedisClient();
            var client2 = client.As<Models.Entity.Common.users>();
            var users = (from u in client2.GetAll() select u).ToList();
            if (users.Count == 0)
            {
                var con = new System.Data.SQLite.SQLiteConnection(NFinal.ConfigurationManager.ConnectionStrings["Common"].ConnectionString);
			con.Open();
                #region	var users;选取所有记录
			var __Page_users_command__ = new System.Data.SQLite.SQLiteCommand("select * from users", con);
			var __Page_users_reader__= __Page_users_command__.ExecuteReader();
			if (__Page_users_reader__.HasRows)
			{
				while (__Page_users_reader__.Read())
				{
					var __Page_users_temp__ = new Models.Entity.Common.users();
					__Page_users_temp__.id = __Page_users_reader__.GetInt64(0);
					__Page_users_temp__.name =__Page_users_reader__.IsDBNull(1)?null: __Page_users_reader__.GetString(1);
					__Page_users_temp__.pwd =__Page_users_reader__.IsDBNull(2)?null: __Page_users_reader__.GetString(2);
					users.Add(__Page_users_temp__);
				}
			}
			__Page_users_reader__.Dispose();
			__Page_users_command__.Dispose();
			#endregion
			
                con.Close();
                client2.StoreAll(users);
            }
            client.Dispose();
        }
    }
}