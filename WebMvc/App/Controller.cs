﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Collections.Specialized;
using System.IO;

namespace WebMvc.App
{
    //Controller基类
    public class Controller : NFinal.BaseAction
    {
        public Controller() { }
        public Controller(string fileName)
            : base(fileName)
        {
        }
        public Controller(TextWriter tw)
            : base(tw)
        {
        }
        public Controller(HttpContext context) : base(context)
        {
            Cookie = new Code.Data.CookieManager();
            Session = new Code.Data.SessionManager();
        }
        public void Error(string msg, string url, int second)
        {
            Web.Default.Common.Public.ErrorAction errorAction = new Web.Default.Common.Public.ErrorAction(_tw);
            errorAction.Error(msg, url, second);
        }
        public void Success(string msg, string url, int second)
        {
            Web.Default.Common.Public.SuccessAction successAction = new Web.Default.Common.Public.SuccessAction(_tw);
            successAction.Success(msg, url, second);
        }
        public void AjaxReturn<T>(List<T> obj)
        {
            this.AjaxReturn(obj.ToJson(), obj!=null && obj.Count>0  ? 1 : 0);
        }
        public Code.Data.CookieManager Cookie = new Code.Data.CookieManager();
        public Code.Data.SessionManager Session = new Code.Data.SessionManager();
    }
}