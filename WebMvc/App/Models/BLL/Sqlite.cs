﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebMvc.App.Models.BLL
{
    public class Sqlite
    {
        public List<dynamic> GetAll(System.Data.IDbConnection con)
        {
            var con1 = Models.Common.GetConnection(con);
            var users = con1.QueryAll("select * from users");
            return users;
        }
        public List<dynamic> GetRandom(System.Data.IDbConnection con)
        {
            var con1 = Models.Common.GetConnection(con);
            var users = con1.QueryRandom("select * from users",2);
            return users;
        }
    }
}