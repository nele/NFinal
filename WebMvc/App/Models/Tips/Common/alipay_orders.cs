﻿﻿using System;
using System.Collections.Generic;
using System.Web;
namespace WebMvc.App.Models.Tips.Common
{
    public struct alipay_orders
    {
		public bool id;
		public bool config_id;
		public bool payment_type;
		public bool alipay_order_id;
		public bool subject;
		public bool total_fee;
		public bool body;
		public bool show_url;
		public bool time;
		public bool status;
		public bool ip;
    }
}