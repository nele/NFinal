﻿﻿using System;
using System.Collections.Generic;
using System.Web;
namespace WebMvc.App.Models.Tips.Common
{
    public struct sms_send
    {
		public bool id;
		public bool phone;
		public bool content;
		public bool time;
		public bool success;
		public bool code;
    }
}