﻿﻿using System;
using System.Collections.Generic;
using System.Web;
namespace WebMvc.App.Models.Tips.Common
{
    public struct article_attachment
    {
		public bool id;
		public bool article_id;
		public bool file_name;
		public bool file_path;
		public bool file_size;
		public bool file_ext;
		public bool down_qty;
		public bool point;
		public bool add_time;
		public bool user_id;
    }
}