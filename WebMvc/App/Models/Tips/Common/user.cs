﻿﻿using System;
using System.Collections.Generic;
using System.Web;
namespace WebMvc.App.Models.Tips.Common
{
    public struct user
    {
		public bool id;
		public bool group_id;
		public bool user_name;
		public bool salt;
		public bool password;
		public bool mobile;
		public bool email;
		public bool avatar;
		public bool nick_name;
		public bool sex;
		public bool birthday;
		public bool telphone;
		public bool area;
		public bool address;
		public bool qq;
		public bool msn;
		public bool amount;
		public bool point;
		public bool exp;
		public bool status;
		public bool reg_time;
		public bool reg_ip;
    }
}