﻿﻿using System;
using System.Collections.Generic;
using System.Web;
namespace WebMvc.App.Models.Tips.Common
{
    public struct feedback
    {
		public bool id;
		public bool title;
		public bool content;
		public bool user_name;
		public bool user_tel;
		public bool user_qq;
		public bool user_email;
		public bool add_time;
		public bool reply_content;
		public bool reply_time;
		public bool is_lock;
    }
}