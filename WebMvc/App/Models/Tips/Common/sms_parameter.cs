﻿﻿using System;
using System.Collections.Generic;
using System.Web;
namespace WebMvc.App.Models.Tips.Common
{
    public struct sms_parameter
    {
		public bool id;
		public bool name;
		public bool type;
		public bool length;
		public bool note;
		public bool template_id;
    }
}