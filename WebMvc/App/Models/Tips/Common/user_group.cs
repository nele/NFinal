﻿﻿using System;
using System.Collections.Generic;
using System.Web;
namespace WebMvc.App.Models.Tips.Common
{
    public struct user_group
    {
		public bool id;
		public bool title;
		public bool grade;
		public bool upgrade_exp;
		public bool amount;
		public bool point;
		public bool discount;
		public bool is_default;
		public bool is_upgrade;
		public bool is_lock;
    }
}