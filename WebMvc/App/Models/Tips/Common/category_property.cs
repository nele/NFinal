﻿﻿using System;
using System.Collections.Generic;
using System.Web;
namespace WebMvc.App.Models.Tips.Common
{
    public struct category_property
    {
		public bool id;
		public bool name;
		public bool channel_id;
		public bool parent_id;
		public bool category_id;
    }
}