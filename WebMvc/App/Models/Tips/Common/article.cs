﻿﻿using System;
using System.Collections.Generic;
using System.Web;
namespace WebMvc.App.Models.Tips.Common
{
    public struct article
    {
		public bool id;
		public bool channel_id;
		public bool category_id;
		public bool title;
		public bool link_url;
		public bool img_url;
		public bool seo_title;
		public bool seo_description;
		public bool tags;
		public bool zhaiyao;
		public bool content;
		public bool sort_id;
		public bool click;
		public bool status;
		public bool is_msg;
		public bool is_top;
		public bool is_red;
		public bool is_hot;
		public bool is_slide;
		public bool is_sys;
		public bool user_name;
		public bool add_time;
		public bool update_time;
		public bool user_id;
    }
}