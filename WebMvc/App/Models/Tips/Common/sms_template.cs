﻿﻿using System;
using System.Collections.Generic;
using System.Web;
namespace WebMvc.App.Models.Tips.Common
{
    public struct sms_template
    {
		public bool id;
		public bool tpl_id;
		public bool app_id;
		public bool app_secret;
		public bool access_token;
		public bool expire;
		public bool name;
		public bool content;
    }
}