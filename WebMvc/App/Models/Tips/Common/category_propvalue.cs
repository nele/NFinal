﻿﻿using System;
using System.Collections.Generic;
using System.Web;
namespace WebMvc.App.Models.Tips.Common
{
    public struct category_propvalue
    {
		public bool id;
		public bool name;
		public bool name_alias;
		public bool category_id;
		public bool property_id;
    }
}