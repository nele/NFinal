﻿﻿using System;
using System.Collections.Generic;
using System.Web;
namespace WebMvc.App.Models.Tips.Common
{
    public struct article_category
    {
		public bool id;
		public bool channel_id;
		public bool title;
		public bool parent_id;
		public bool class_list;
		public bool class_layer;
		public bool sort_id;
		public bool link_url;
		public bool img_url;
		public bool content;
		public bool seo_folder;
		public bool seo_title;
		public bool seo_keywords;
		public bool seo_descrition;
    }
}