﻿﻿using System;
using System.Collections.Generic;
using System.Web;
namespace WebMvc.App.Models.Tips.Common
{
    public struct artile_comment
    {
		public bool id;
		public bool channel_id;
		public bool article_id;
		public bool parent_id;
		public bool user_id;
		public bool user_name;
		public bool user_ip;
		public bool content;
		public bool add_time;
    }
}