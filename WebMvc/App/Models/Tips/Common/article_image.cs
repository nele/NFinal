﻿﻿using System;
using System.Collections.Generic;
using System.Web;
namespace WebMvc.App.Models.Tips.Common
{
    public struct article_image
    {
		public bool id;
		public bool article_id;
		public bool img_url;
		public bool original_url;
		public bool add_time;
		public bool user_id;
    }
}