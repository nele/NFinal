﻿﻿using System;
using System.Collections.Generic;
using System.Web;
namespace WebMvc.App.Models.Tips.Common
{
    public struct shop_goods
    {
		public bool id;
		public bool subject;
		public bool fee;
		public bool qty;
		public bool body;
		public bool show_url;
		public bool total_fee;
    }
}