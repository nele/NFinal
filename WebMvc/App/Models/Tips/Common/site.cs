﻿﻿using System;
using System.Collections.Generic;
using System.Web;
namespace WebMvc.App.Models.Tips.Common
{
    public struct site
    {
		public bool id;
		public bool name;
		public bool channel_id;
		public bool subdomain_name;
		public bool title;
		public bool company;
		public bool address;
		public bool tel;
		public bool fax;
		public bool email;
		public bool copyright;
		public bool seo_folder;
		public bool seo_title;
		public bool seo_keyword;
		public bool seo_description;
		public bool is_default;
    }
}