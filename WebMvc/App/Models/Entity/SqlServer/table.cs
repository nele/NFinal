﻿﻿using System;
using System.Collections.Generic;
using System.Text;
namespace WebMvc.App.Models.Entity.SqlServer
{
    public class table:NFinal.Struct
    {
		public System.Int64? a1 {get;set;}
		public System.Byte[] a2 {get;set;}
		public System.Boolean? a3 {get;set;}
		public System.String a4 {get;set;}
		public System.DateTime? a5 {get;set;}
		public System.DateTime? a6 {get;set;}
		public System.DateTime? a7 {get;set;}
		public System.DateTimeOffset? a8 {get;set;}
		public System.Decimal? a9 {get;set;}
		public System.Double? a10 {get;set;}
		public Microsoft.SqlServer.Types.SqlGeography a11 {get;set;}
		public Microsoft.SqlServer.Types.SqlGeometry a12 {get;set;}
		public Microsoft.SqlServer.Types.SqlHierarchyId? a13 {get;set;}
		public System.Byte[] a14 {get;set;}
		public System.Int32? a15 {get;set;}
		public System.Decimal? a16 {get;set;}
		public System.String a17 {get;set;}
		public System.String a18 {get;set;}
		public System.Decimal? a19 {get;set;}
		public System.String a20 {get;set;}
		public System.String a21 {get;set;}
		public System.Single? a22 {get;set;}
		public System.DateTime? a23 {get;set;}
		public System.Int16? a24 {get;set;}
		public System.Decimal? a25 {get;set;}
		public System.Object a26 {get;set;}
		public System.String a27 {get;set;}
		public System.TimeSpan? a28 {get;set;}
		public System.Byte[] a29 {get;set;}
		public System.Byte? a30 {get;set;}
		public System.Guid? a31 {get;set;}
		public System.Byte[] a32 {get;set;}
		public System.Byte[] a33 {get;set;}
		public System.String a34 {get;set;}
		public System.String a35 {get;set;}
		public System.String a36 {get;set;}
		#region 写Json字符串
		public override void WriteJson(System.IO.TextWriter tw)
		{
			tw.Write("{");
					tw.Write("\"a1\":");
					tw.Write(a1==null?"null" : a1.ToString());
					tw.Write(",");
					tw.Write("\"a2\":");
					tw.Write("\"");
					tw.Write(a2==null?"null":Convert.ToBase64String(a2));
					tw.Write("\"");
					tw.Write(",");
					tw.Write("\"a3\":");
					tw.Write(a3==null?"null":((bool)a3?"true":"false"));
					tw.Write(",");
					tw.Write("\"a4\":");
					tw.Write("\"");
					tw.Write(a4==null?"null":a4);
					tw.Write("\"");
					tw.Write(",");
					tw.Write("\"a5\":");
					tw.Write("\"");
					tw.Write(a5==null?"null":a5.ToString());
					tw.Write("\"");
					tw.Write(",");
					tw.Write("\"a6\":");
					tw.Write("\"");
					tw.Write(a6==null?"null":a6.ToString());
					tw.Write("\"");
					tw.Write(",");
					tw.Write("\"a7\":");
					tw.Write("\"");
					tw.Write(a7==null?"null":a7.ToString());
					tw.Write("\"");
					tw.Write(",");
					tw.Write("\"a8\":");
					tw.Write("\"");
					tw.Write(a8==null?"null":a8.ToString());
					tw.Write("\"");
					tw.Write(",");
					tw.Write("\"a9\":");
					tw.Write(a9==null?"null" : a9.ToString());
					tw.Write(",");
					tw.Write("\"a10\":");
					tw.Write(a10==null?"null" : a10.ToString());
					tw.Write(",");
					tw.Write("\"a11\":");
					tw.Write("\"");
					tw.Write(a11==null?"null":a11.ToString());
					tw.Write("\"");
					tw.Write(",");
					tw.Write("\"a12\":");
					tw.Write("\"");
					tw.Write(a12==null?"null":a12.ToString());
					tw.Write("\"");
					tw.Write(",");
					tw.Write("\"a13\":");
					tw.Write("\"");
					tw.Write(a13==null?"null":a13.ToString());
					tw.Write("\"");
					tw.Write(",");
					tw.Write("\"a14\":");
					tw.Write("\"");
					tw.Write(a14==null?"null":Convert.ToBase64String(a14));
					tw.Write("\"");
					tw.Write(",");
					tw.Write("\"a15\":");
					tw.Write(a15==null?"null" : a15.ToString());
					tw.Write(",");
					tw.Write("\"a16\":");
					tw.Write(a16==null?"null" : a16.ToString());
					tw.Write(",");
					tw.Write("\"a17\":");
					tw.Write("\"");
					tw.Write(a17==null?"null":a17);
					tw.Write("\"");
					tw.Write(",");
					tw.Write("\"a18\":");
					tw.Write("\"");
					tw.Write(a18==null?"null":a18);
					tw.Write("\"");
					tw.Write(",");
					tw.Write("\"a19\":");
					tw.Write(a19==null?"null" : a19.ToString());
					tw.Write(",");
					tw.Write("\"a20\":");
					tw.Write("\"");
					tw.Write(a20==null?"null":a20);
					tw.Write("\"");
					tw.Write(",");
					tw.Write("\"a21\":");
					tw.Write("\"");
					tw.Write(a21==null?"null":a21);
					tw.Write("\"");
					tw.Write(",");
					tw.Write("\"a22\":");
					tw.Write(a22==null?"null" : a22.ToString());
					tw.Write(",");
					tw.Write("\"a23\":");
					tw.Write("\"");
					tw.Write(a23==null?"null":a23.ToString());
					tw.Write("\"");
					tw.Write(",");
					tw.Write("\"a24\":");
					tw.Write(a24==null?"null" : a24.ToString());
					tw.Write(",");
					tw.Write("\"a25\":");
					tw.Write(a25==null?"null" : a25.ToString());
					tw.Write(",");
					tw.Write("\"a26\":");
					tw.Write("\"");
					tw.Write(a26==null?"null":a26.ToString());
					tw.Write("\"");
					tw.Write(",");
					tw.Write("\"a27\":");
					tw.Write("\"");
					tw.Write(a27==null?"null":a27);
					tw.Write("\"");
					tw.Write(",");
					tw.Write("\"a28\":");
					tw.Write("\"");
					tw.Write(a28==null?"null":a28.ToString());
					tw.Write("\"");
					tw.Write(",");
					tw.Write("\"a29\":");
					tw.Write("\"");
					tw.Write(a29==null?"null":Convert.ToBase64String(a29));
					tw.Write("\"");
					tw.Write(",");
					tw.Write("\"a30\":");
					tw.Write(a30==null?"null" : a30.ToString());
					tw.Write(",");
					tw.Write("\"a31\":");
					tw.Write("\"");
					tw.Write(a31==null?"null":a31.ToString());
					tw.Write("\"");
					tw.Write(",");
					tw.Write("\"a32\":");
					tw.Write("\"");
					tw.Write(a32==null?"null":Convert.ToBase64String(a32));
					tw.Write("\"");
					tw.Write(",");
					tw.Write("\"a33\":");
					tw.Write("\"");
					tw.Write(a33==null?"null":Convert.ToBase64String(a33));
					tw.Write("\"");
					tw.Write(",");
					tw.Write("\"a34\":");
					tw.Write("\"");
					tw.Write(a34==null?"null":a34);
					tw.Write("\"");
					tw.Write(",");
					tw.Write("\"a35\":");
					tw.Write("\"");
					tw.Write(a35==null?"null":a35);
					tw.Write("\"");
					tw.Write(",");
					tw.Write("\"a36\":");
					tw.Write("\"");
					tw.Write(a36==null?"null":a36);
					tw.Write("\"");
			tw.Write("}");
		}
		#endregion
    }
}
