﻿﻿using System;
using System.Collections.Generic;
using System.Text;
namespace WebMvc.App.Models.Entity.MySql
{
    public class users:NFinal.Struct
    {
		public System.Int32? id {get;set;}
		public System.String name {get;set;}
		public System.String pwd {get;set;}
		#region 写Json字符串
		public override void WriteJson(System.IO.TextWriter tw)
		{
			tw.Write("{");
					tw.Write("\"id\":");
					tw.Write(id==null?"null" : id.ToString());
					tw.Write(",");
					tw.Write("\"name\":");
					tw.Write("\"");
					tw.Write(name==null?"null":name);
					tw.Write("\"");
					tw.Write(",");
					tw.Write("\"pwd\":");
					tw.Write("\"");
					tw.Write(pwd==null?"null":pwd);
					tw.Write("\"");
			tw.Write("}");
		}
		#endregion
    }
}
