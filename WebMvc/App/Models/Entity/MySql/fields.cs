﻿﻿using System;
using System.Collections.Generic;
using System.Text;
namespace WebMvc.App.Models.Entity.MySql
{
    public class fields:NFinal.Struct
    {
		/// <summary>
        /// a1
        /// </summary>
		public System.SByte? a1 {get;set;}
		/// <summary>
        /// a2
        /// </summary>
		public System.Int16? a2 {get;set;}
		/// <summary>
        /// a3
        /// </summary>
		public System.Int32? a3 {get;set;}
		public System.Int32? a4 {get;set;}
		public System.Int32? a5 {get;set;}
		public System.Int64? a6 {get;set;}
		public System.UInt64? a7 {get;set;}
		public System.Double? a8 {get;set;}
		public System.Double? a9 {get;set;}
		public System.Single? a10 {get;set;}
		public System.Decimal? a11 {get;set;}
		public System.Decimal? a12 {get;set;}
		public System.String a13 {get;set;}
		public System.String a14 {get;set;}
		public System.DateTime? a15 {get;set;}
		public System.TimeSpan? a16 {get;set;}
		public System.Int32? a17 {get;set;}
		public System.DateTime? a18 {get;set;}
		public System.DateTime? a19 {get;set;}
		public System.Byte[] a20 {get;set;}
		public System.Byte[] a21 {get;set;}
		public System.Byte[] a22 {get;set;}
		public System.Byte[] a23 {get;set;}
		public System.String a24 {get;set;}
		public System.String a25 {get;set;}
		public System.String a26 {get;set;}
		public System.String a27 {get;set;}
		public System.String a28 {get;set;}
		public System.String a29 {get;set;}
		public System.Byte[] a30 {get;set;}
		public System.Byte[] a31 {get;set;}
		public System.Byte[] a32 {get;set;}
		public System.Byte[] a33 {get;set;}
		public System.Byte[] a34 {get;set;}
		public System.Byte[] a35 {get;set;}
		public System.Byte[] a36 {get;set;}
		public System.Byte[] a37 {get;set;}
		public System.Byte[] a38 {get;set;}
		public System.Byte[] a39 {get;set;}
		#region 写Json字符串
		public override void WriteJson(System.IO.TextWriter tw)
		{
			tw.Write("{");
					tw.Write("\"a1\":");
					tw.Write(a1==null?"null" : a1.ToString());
					tw.Write(",");
					tw.Write("\"a2\":");
					tw.Write(a2==null?"null" : a2.ToString());
					tw.Write(",");
					tw.Write("\"a3\":");
					tw.Write(a3==null?"null" : a3.ToString());
					tw.Write(",");
					tw.Write("\"a4\":");
					tw.Write(a4==null?"null" : a4.ToString());
					tw.Write(",");
					tw.Write("\"a5\":");
					tw.Write(a5==null?"null" : a5.ToString());
					tw.Write(",");
					tw.Write("\"a6\":");
					tw.Write(a6==null?"null" : a6.ToString());
					tw.Write(",");
					tw.Write("\"a7\":");
					tw.Write(a7==null?"null" : a7.ToString());
					tw.Write(",");
					tw.Write("\"a8\":");
					tw.Write(a8==null?"null" : a8.ToString());
					tw.Write(",");
					tw.Write("\"a9\":");
					tw.Write(a9==null?"null" : a9.ToString());
					tw.Write(",");
					tw.Write("\"a10\":");
					tw.Write(a10==null?"null" : a10.ToString());
					tw.Write(",");
					tw.Write("\"a11\":");
					tw.Write(a11==null?"null" : a11.ToString());
					tw.Write(",");
					tw.Write("\"a12\":");
					tw.Write(a12==null?"null" : a12.ToString());
					tw.Write(",");
					tw.Write("\"a13\":");
					tw.Write("\"");
					tw.Write(a13==null?"null":a13);
					tw.Write("\"");
					tw.Write(",");
					tw.Write("\"a14\":");
					tw.Write("\"");
					tw.Write(a14==null?"null":a14);
					tw.Write("\"");
					tw.Write(",");
					tw.Write("\"a15\":");
					tw.Write("\"");
					tw.Write(a15==null?"null":a15.ToString());
					tw.Write("\"");
					tw.Write(",");
					tw.Write("\"a16\":");
					tw.Write("\"");
					tw.Write(a16==null?"null":a16.ToString());
					tw.Write("\"");
					tw.Write(",");
					tw.Write("\"a17\":");
					tw.Write("\"");
					tw.Write(a17==null?"null":a17.ToString());
					tw.Write("\"");
					tw.Write(",");
					tw.Write("\"a18\":");
					tw.Write("\"");
					tw.Write(a18==null?"null":a18.ToString());
					tw.Write("\"");
					tw.Write(",");
					tw.Write("\"a19\":");
					tw.Write("\"");
					tw.Write(a19==null?"null":a19.ToString());
					tw.Write("\"");
					tw.Write(",");
					tw.Write("\"a20\":");
					tw.Write("\"");
					tw.Write(a20==null?"null":Convert.ToBase64String(a20));
					tw.Write("\"");
					tw.Write(",");
					tw.Write("\"a21\":");
					tw.Write("\"");
					tw.Write(a21==null?"null":Convert.ToBase64String(a21));
					tw.Write("\"");
					tw.Write(",");
					tw.Write("\"a22\":");
					tw.Write("\"");
					tw.Write(a22==null?"null":Convert.ToBase64String(a22));
					tw.Write("\"");
					tw.Write(",");
					tw.Write("\"a23\":");
					tw.Write("\"");
					tw.Write(a23==null?"null":Convert.ToBase64String(a23));
					tw.Write("\"");
					tw.Write(",");
					tw.Write("\"a24\":");
					tw.Write("\"");
					tw.Write(a24==null?"null":a24);
					tw.Write("\"");
					tw.Write(",");
					tw.Write("\"a25\":");
					tw.Write("\"");
					tw.Write(a25==null?"null":a25);
					tw.Write("\"");
					tw.Write(",");
					tw.Write("\"a26\":");
					tw.Write("\"");
					tw.Write(a26==null?"null":a26);
					tw.Write("\"");
					tw.Write(",");
					tw.Write("\"a27\":");
					tw.Write("\"");
					tw.Write(a27==null?"null":a27);
					tw.Write("\"");
					tw.Write(",");
					tw.Write("\"a28\":");
					tw.Write("\"");
					tw.Write(a28==null?"null":a28);
					tw.Write("\"");
					tw.Write(",");
					tw.Write("\"a29\":");
					tw.Write("\"");
					tw.Write(a29==null?"null":a29);
					tw.Write("\"");
					tw.Write(",");
					tw.Write("\"a30\":");
					tw.Write("\"");
					tw.Write(a30==null?"null":Convert.ToBase64String(a30));
					tw.Write("\"");
					tw.Write(",");
					tw.Write("\"a31\":");
					tw.Write("\"");
					tw.Write(a31==null?"null":Convert.ToBase64String(a31));
					tw.Write("\"");
					tw.Write(",");
					tw.Write("\"a32\":");
					tw.Write("\"");
					tw.Write(a32==null?"null":Convert.ToBase64String(a32));
					tw.Write("\"");
					tw.Write(",");
					tw.Write("\"a33\":");
					tw.Write("\"");
					tw.Write(a33==null?"null":Convert.ToBase64String(a33));
					tw.Write("\"");
					tw.Write(",");
					tw.Write("\"a34\":");
					tw.Write("\"");
					tw.Write(a34==null?"null":Convert.ToBase64String(a34));
					tw.Write("\"");
					tw.Write(",");
					tw.Write("\"a35\":");
					tw.Write("\"");
					tw.Write(a35==null?"null":Convert.ToBase64String(a35));
					tw.Write("\"");
					tw.Write(",");
					tw.Write("\"a36\":");
					tw.Write("\"");
					tw.Write(a36==null?"null":Convert.ToBase64String(a36));
					tw.Write("\"");
					tw.Write(",");
					tw.Write("\"a37\":");
					tw.Write("\"");
					tw.Write(a37==null?"null":Convert.ToBase64String(a37));
					tw.Write("\"");
					tw.Write(",");
					tw.Write("\"a38\":");
					tw.Write("\"");
					tw.Write(a38==null?"null":Convert.ToBase64String(a38));
					tw.Write("\"");
					tw.Write(",");
					tw.Write("\"a39\":");
					tw.Write("\"");
					tw.Write(a39==null?"null":Convert.ToBase64String(a39));
					tw.Write("\"");
			tw.Write("}");
		}
		#endregion
    }
}
