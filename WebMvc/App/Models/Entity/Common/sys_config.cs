﻿﻿using System;
using System.Collections.Generic;
using System.Text;
namespace WebMvc.App.Models.Entity.Common
{
    public class sys_config:NFinal.Struct
    {
		public System.Int64 id {get;set;}
		public System.String name {get;set;}
		public System.String info {get;set;}
		public System.String value {get;set;}
		#region 写Json字符串
		public override void WriteJson(System.IO.TextWriter tw)
		{
			tw.Write("{");
					tw.Write("\"id\":");
					tw.Write(id.ToString());
					tw.Write(",");
					tw.Write("\"name\":");
					tw.Write("\"");
					tw.Write(name==null?"null":name);
					tw.Write("\"");
					tw.Write(",");
					tw.Write("\"info\":");
					tw.Write("\"");
					tw.Write(info==null?"null":info);
					tw.Write("\"");
					tw.Write(",");
					tw.Write("\"value\":");
					tw.Write("\"");
					tw.Write(value==null?"null":value);
					tw.Write("\"");
			tw.Write("}");
		}
		#endregion
    }
}
