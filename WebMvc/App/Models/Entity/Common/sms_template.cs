﻿﻿using System;
using System.Collections.Generic;
using System.Text;
namespace WebMvc.App.Models.Entity.Common
{
    public class sms_template:NFinal.Struct
    {
		public System.Int64 id {get;set;}
		public System.Int64? tpl_id {get;set;}
		public System.Int64? app_id {get;set;}
		public System.String app_secret {get;set;}
		public System.String access_token {get;set;}
		public System.Int64? expire {get;set;}
		public System.String name {get;set;}
		public System.String content {get;set;}
		#region 写Json字符串
		public override void WriteJson(System.IO.TextWriter tw)
		{
			tw.Write("{");
					tw.Write("\"id\":");
					tw.Write(id.ToString());
					tw.Write(",");
					tw.Write("\"tpl_id\":");
					tw.Write(tpl_id==null?"null" : tpl_id.ToString());
					tw.Write(",");
					tw.Write("\"app_id\":");
					tw.Write(app_id==null?"null" : app_id.ToString());
					tw.Write(",");
					tw.Write("\"app_secret\":");
					tw.Write("\"");
					tw.Write(app_secret==null?"null":app_secret);
					tw.Write("\"");
					tw.Write(",");
					tw.Write("\"access_token\":");
					tw.Write("\"");
					tw.Write(access_token==null?"null":access_token);
					tw.Write("\"");
					tw.Write(",");
					tw.Write("\"expire\":");
					tw.Write(expire==null?"null" : expire.ToString());
					tw.Write(",");
					tw.Write("\"name\":");
					tw.Write("\"");
					tw.Write(name==null?"null":name);
					tw.Write("\"");
					tw.Write(",");
					tw.Write("\"content\":");
					tw.Write("\"");
					tw.Write(content==null?"null":content);
					tw.Write("\"");
			tw.Write("}");
		}
		#endregion
    }
}
