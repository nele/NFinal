﻿﻿using System;
using System.Collections.Generic;
using System.Text;
namespace WebMvc.App.Models.Entity.Common
{
    public class article_image:NFinal.Struct
    {
		public System.Int64 id {get;set;}
		public System.Int64? article_id {get;set;}
		public System.String img_url {get;set;}
		public System.String original_url {get;set;}
		public System.DateTime? add_time {get;set;}
		public System.Int64? user_id {get;set;}
		#region 写Json字符串
		public override void WriteJson(System.IO.TextWriter tw)
		{
			tw.Write("{");
					tw.Write("\"id\":");
					tw.Write(id.ToString());
					tw.Write(",");
					tw.Write("\"article_id\":");
					tw.Write(article_id==null?"null" : article_id.ToString());
					tw.Write(",");
					tw.Write("\"img_url\":");
					tw.Write("\"");
					tw.Write(img_url==null?"null":img_url);
					tw.Write("\"");
					tw.Write(",");
					tw.Write("\"original_url\":");
					tw.Write("\"");
					tw.Write(original_url==null?"null":original_url);
					tw.Write("\"");
					tw.Write(",");
					tw.Write("\"add_time\":");
					tw.Write("\"");
					tw.Write(add_time==null?"null":add_time.ToString());
					tw.Write("\"");
					tw.Write(",");
					tw.Write("\"user_id\":");
					tw.Write(user_id==null?"null" : user_id.ToString());
			tw.Write("}");
		}
		#endregion
    }
}
