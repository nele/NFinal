﻿﻿using System;
using System.Collections.Generic;
using System.Text;
namespace WebMvc.App.Models.Entity.Common
{
    public class article:NFinal.Struct
    {
		public System.Int64 id {get;set;}
		public System.Int64? channel_id {get;set;}
		public System.Int64? category_id {get;set;}
		public System.String title {get;set;}
		public System.String link_url {get;set;}
		public System.String img_url {get;set;}
		public System.String seo_title {get;set;}
		public System.String seo_description {get;set;}
		public System.String tags {get;set;}
		public System.String zhaiyao {get;set;}
		public System.String content {get;set;}
		public System.Int64? sort_id {get;set;}
		public System.Int64? click {get;set;}
		public System.Int64? status {get;set;}
		public System.Int16? is_msg {get;set;}
		public System.Int16? is_top {get;set;}
		public System.Int16? is_red {get;set;}
		public System.Int16? is_hot {get;set;}
		public System.Int16? is_slide {get;set;}
		public System.Int16? is_sys {get;set;}
		public System.String user_name {get;set;}
		public System.DateTime? add_time {get;set;}
		public System.DateTime? update_time {get;set;}
		public System.Int64? user_id {get;set;}
		#region 写Json字符串
		public override void WriteJson(System.IO.TextWriter tw)
		{
			tw.Write("{");
					tw.Write("\"id\":");
					tw.Write(id.ToString());
					tw.Write(",");
					tw.Write("\"channel_id\":");
					tw.Write(channel_id==null?"null" : channel_id.ToString());
					tw.Write(",");
					tw.Write("\"category_id\":");
					tw.Write(category_id==null?"null" : category_id.ToString());
					tw.Write(",");
					tw.Write("\"title\":");
					tw.Write("\"");
					tw.Write(title==null?"null":title);
					tw.Write("\"");
					tw.Write(",");
					tw.Write("\"link_url\":");
					tw.Write("\"");
					tw.Write(link_url==null?"null":link_url);
					tw.Write("\"");
					tw.Write(",");
					tw.Write("\"img_url\":");
					tw.Write("\"");
					tw.Write(img_url==null?"null":img_url);
					tw.Write("\"");
					tw.Write(",");
					tw.Write("\"seo_title\":");
					tw.Write("\"");
					tw.Write(seo_title==null?"null":seo_title);
					tw.Write("\"");
					tw.Write(",");
					tw.Write("\"seo_description\":");
					tw.Write("\"");
					tw.Write(seo_description==null?"null":seo_description);
					tw.Write("\"");
					tw.Write(",");
					tw.Write("\"tags\":");
					tw.Write("\"");
					tw.Write(tags==null?"null":tags);
					tw.Write("\"");
					tw.Write(",");
					tw.Write("\"zhaiyao\":");
					tw.Write("\"");
					tw.Write(zhaiyao==null?"null":zhaiyao);
					tw.Write("\"");
					tw.Write(",");
					tw.Write("\"content\":");
					tw.Write("\"");
					tw.Write(content==null?"null":content);
					tw.Write("\"");
					tw.Write(",");
					tw.Write("\"sort_id\":");
					tw.Write(sort_id==null?"null" : sort_id.ToString());
					tw.Write(",");
					tw.Write("\"click\":");
					tw.Write(click==null?"null" : click.ToString());
					tw.Write(",");
					tw.Write("\"status\":");
					tw.Write(status==null?"null" : status.ToString());
					tw.Write(",");
					tw.Write("\"is_msg\":");
					tw.Write(is_msg==null?"null" : is_msg.ToString());
					tw.Write(",");
					tw.Write("\"is_top\":");
					tw.Write(is_top==null?"null" : is_top.ToString());
					tw.Write(",");
					tw.Write("\"is_red\":");
					tw.Write(is_red==null?"null" : is_red.ToString());
					tw.Write(",");
					tw.Write("\"is_hot\":");
					tw.Write(is_hot==null?"null" : is_hot.ToString());
					tw.Write(",");
					tw.Write("\"is_slide\":");
					tw.Write(is_slide==null?"null" : is_slide.ToString());
					tw.Write(",");
					tw.Write("\"is_sys\":");
					tw.Write(is_sys==null?"null" : is_sys.ToString());
					tw.Write(",");
					tw.Write("\"user_name\":");
					tw.Write("\"");
					tw.Write(user_name==null?"null":user_name);
					tw.Write("\"");
					tw.Write(",");
					tw.Write("\"add_time\":");
					tw.Write("\"");
					tw.Write(add_time==null?"null":add_time.ToString());
					tw.Write("\"");
					tw.Write(",");
					tw.Write("\"update_time\":");
					tw.Write("\"");
					tw.Write(update_time==null?"null":update_time.ToString());
					tw.Write("\"");
					tw.Write(",");
					tw.Write("\"user_id\":");
					tw.Write(user_id==null?"null" : user_id.ToString());
			tw.Write("}");
		}
		#endregion
    }
}
