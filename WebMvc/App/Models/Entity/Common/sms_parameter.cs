﻿﻿using System;
using System.Collections.Generic;
using System.Text;
namespace WebMvc.App.Models.Entity.Common
{
    public class sms_parameter:NFinal.Struct
    {
		public System.Int64 id {get;set;}
		public System.String name {get;set;}
		public System.Int64? type {get;set;}
		public System.Double? length {get;set;}
		public System.String note {get;set;}
		public System.Int64? template_id {get;set;}
		#region 写Json字符串
		public override void WriteJson(System.IO.TextWriter tw)
		{
			tw.Write("{");
					tw.Write("\"id\":");
					tw.Write(id.ToString());
					tw.Write(",");
					tw.Write("\"name\":");
					tw.Write("\"");
					tw.Write(name==null?"null":name);
					tw.Write("\"");
					tw.Write(",");
					tw.Write("\"type\":");
					tw.Write(type==null?"null" : type.ToString());
					tw.Write(",");
					tw.Write("\"length\":");
					tw.Write(length==null?"null" : length.ToString());
					tw.Write(",");
					tw.Write("\"note\":");
					tw.Write("\"");
					tw.Write(note==null?"null":note);
					tw.Write("\"");
					tw.Write(",");
					tw.Write("\"template_id\":");
					tw.Write(template_id==null?"null" : template_id.ToString());
			tw.Write("}");
		}
		#endregion
    }
}
