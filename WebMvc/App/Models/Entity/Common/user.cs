﻿﻿using System;
using System.Collections.Generic;
using System.Text;
namespace WebMvc.App.Models.Entity.Common
{
    public class user:NFinal.Struct
    {
		public System.Int64 id {get;set;}
		public System.Int64? group_id {get;set;}
		public System.String user_name {get;set;}
		public System.String salt {get;set;}
		public System.String password {get;set;}
		public System.String mobile {get;set;}
		public System.String email {get;set;}
		public System.String avatar {get;set;}
		public System.String nick_name {get;set;}
		public System.Int16? sex {get;set;}
		public System.DateTime? birthday {get;set;}
		public System.String telphone {get;set;}
		public System.String area {get;set;}
		public System.String address {get;set;}
		public System.String qq {get;set;}
		public System.String msn {get;set;}
		public System.Decimal? amount {get;set;}
		public System.Int64? point {get;set;}
		public System.Int64? exp {get;set;}
		public System.Int64? status {get;set;}
		public System.DateTime? reg_time {get;set;}
		public System.String reg_ip {get;set;}
		#region 写Json字符串
		public override void WriteJson(System.IO.TextWriter tw)
		{
			tw.Write("{");
					tw.Write("\"id\":");
					tw.Write(id.ToString());
					tw.Write(",");
					tw.Write("\"group_id\":");
					tw.Write(group_id==null?"null" : group_id.ToString());
					tw.Write(",");
					tw.Write("\"user_name\":");
					tw.Write("\"");
					tw.Write(user_name==null?"null":user_name);
					tw.Write("\"");
					tw.Write(",");
					tw.Write("\"salt\":");
					tw.Write("\"");
					tw.Write(salt==null?"null":salt);
					tw.Write("\"");
					tw.Write(",");
					tw.Write("\"password\":");
					tw.Write("\"");
					tw.Write(password==null?"null":password);
					tw.Write("\"");
					tw.Write(",");
					tw.Write("\"mobile\":");
					tw.Write("\"");
					tw.Write(mobile==null?"null":mobile);
					tw.Write("\"");
					tw.Write(",");
					tw.Write("\"email\":");
					tw.Write("\"");
					tw.Write(email==null?"null":email);
					tw.Write("\"");
					tw.Write(",");
					tw.Write("\"avatar\":");
					tw.Write("\"");
					tw.Write(avatar==null?"null":avatar);
					tw.Write("\"");
					tw.Write(",");
					tw.Write("\"nick_name\":");
					tw.Write("\"");
					tw.Write(nick_name==null?"null":nick_name);
					tw.Write("\"");
					tw.Write(",");
					tw.Write("\"sex\":");
					tw.Write(sex==null?"null" : sex.ToString());
					tw.Write(",");
					tw.Write("\"birthday\":");
					tw.Write("\"");
					tw.Write(birthday==null?"null":birthday.ToString());
					tw.Write("\"");
					tw.Write(",");
					tw.Write("\"telphone\":");
					tw.Write("\"");
					tw.Write(telphone==null?"null":telphone);
					tw.Write("\"");
					tw.Write(",");
					tw.Write("\"area\":");
					tw.Write("\"");
					tw.Write(area==null?"null":area);
					tw.Write("\"");
					tw.Write(",");
					tw.Write("\"address\":");
					tw.Write("\"");
					tw.Write(address==null?"null":address);
					tw.Write("\"");
					tw.Write(",");
					tw.Write("\"qq\":");
					tw.Write("\"");
					tw.Write(qq==null?"null":qq);
					tw.Write("\"");
					tw.Write(",");
					tw.Write("\"msn\":");
					tw.Write("\"");
					tw.Write(msn==null?"null":msn);
					tw.Write("\"");
					tw.Write(",");
					tw.Write("\"amount\":");
					tw.Write(amount==null?"null" : amount.ToString());
					tw.Write(",");
					tw.Write("\"point\":");
					tw.Write(point==null?"null" : point.ToString());
					tw.Write(",");
					tw.Write("\"exp\":");
					tw.Write(exp==null?"null" : exp.ToString());
					tw.Write(",");
					tw.Write("\"status\":");
					tw.Write(status==null?"null" : status.ToString());
					tw.Write(",");
					tw.Write("\"reg_time\":");
					tw.Write("\"");
					tw.Write(reg_time==null?"null":reg_time.ToString());
					tw.Write("\"");
					tw.Write(",");
					tw.Write("\"reg_ip\":");
					tw.Write("\"");
					tw.Write(reg_ip==null?"null":reg_ip);
					tw.Write("\"");
			tw.Write("}");
		}
		#endregion
    }
}
