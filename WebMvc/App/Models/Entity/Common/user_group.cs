﻿﻿using System;
using System.Collections.Generic;
using System.Text;
namespace WebMvc.App.Models.Entity.Common
{
    public class user_group:NFinal.Struct
    {
		public System.Int64 id {get;set;}
		public System.String title {get;set;}
		public System.Int64? grade {get;set;}
		public System.Int64? upgrade_exp {get;set;}
		public System.Int64? amount {get;set;}
		public System.Int64? point {get;set;}
		public System.Double? discount {get;set;}
		public System.Int16? is_default {get;set;}
		public System.Int16? is_upgrade {get;set;}
		public System.Int16? is_lock {get;set;}
		#region 写Json字符串
		public override void WriteJson(System.IO.TextWriter tw)
		{
			tw.Write("{");
					tw.Write("\"id\":");
					tw.Write(id.ToString());
					tw.Write(",");
					tw.Write("\"title\":");
					tw.Write("\"");
					tw.Write(title==null?"null":title);
					tw.Write("\"");
					tw.Write(",");
					tw.Write("\"grade\":");
					tw.Write(grade==null?"null" : grade.ToString());
					tw.Write(",");
					tw.Write("\"upgrade_exp\":");
					tw.Write(upgrade_exp==null?"null" : upgrade_exp.ToString());
					tw.Write(",");
					tw.Write("\"amount\":");
					tw.Write(amount==null?"null" : amount.ToString());
					tw.Write(",");
					tw.Write("\"point\":");
					tw.Write(point==null?"null" : point.ToString());
					tw.Write(",");
					tw.Write("\"discount\":");
					tw.Write(discount==null?"null" : discount.ToString());
					tw.Write(",");
					tw.Write("\"is_default\":");
					tw.Write(is_default==null?"null" : is_default.ToString());
					tw.Write(",");
					tw.Write("\"is_upgrade\":");
					tw.Write(is_upgrade==null?"null" : is_upgrade.ToString());
					tw.Write(",");
					tw.Write("\"is_lock\":");
					tw.Write(is_lock==null?"null" : is_lock.ToString());
			tw.Write("}");
		}
		#endregion
    }
}
