﻿﻿using System;
using System.Collections.Generic;
using System.Text;
namespace WebMvc.App.Models.Entity.Common
{
    public class users:NFinal.Struct
    {
		/// <summary>
        /// 用户表ID
        /// </summary>
		public System.Int64 id {get;set;}
		/// <summary>
        /// 名称
        /// </summary>
		public System.String name {get;set;}
		/// <summary>
        /// 密码
        /// </summary>
		public System.String pwd {get;set;}
		#region 写Json字符串
		public override void WriteJson(System.IO.TextWriter tw)
		{
			tw.Write("{");
					tw.Write("\"id\":");
					tw.Write(id.ToString());
					tw.Write(",");
					tw.Write("\"name\":");
					tw.Write("\"");
					tw.Write(name==null?"null":name);
					tw.Write("\"");
					tw.Write(",");
					tw.Write("\"pwd\":");
					tw.Write("\"");
					tw.Write(pwd==null?"null":pwd);
					tw.Write("\"");
			tw.Write("}");
		}
		#endregion
    }
}
