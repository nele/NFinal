﻿﻿using System;
using System.Collections.Generic;
using System.Text;
namespace WebMvc.App.Models.Entity.Common
{
    public class article_category:NFinal.Struct
    {
		public System.Int64 id {get;set;}
		public System.Int64? channel_id {get;set;}
		public System.String title {get;set;}
		public System.Int64? parent_id {get;set;}
		public System.String class_list {get;set;}
		public System.Int64? class_layer {get;set;}
		public System.Int64? sort_id {get;set;}
		public System.String link_url {get;set;}
		public System.String img_url {get;set;}
		public System.String content {get;set;}
		public System.String seo_folder {get;set;}
		public System.String seo_title {get;set;}
		public System.String seo_keywords {get;set;}
		public System.String seo_descrition {get;set;}
		#region 写Json字符串
		public override void WriteJson(System.IO.TextWriter tw)
		{
			tw.Write("{");
					tw.Write("\"id\":");
					tw.Write(id.ToString());
					tw.Write(",");
					tw.Write("\"channel_id\":");
					tw.Write(channel_id==null?"null" : channel_id.ToString());
					tw.Write(",");
					tw.Write("\"title\":");
					tw.Write("\"");
					tw.Write(title==null?"null":title);
					tw.Write("\"");
					tw.Write(",");
					tw.Write("\"parent_id\":");
					tw.Write(parent_id==null?"null" : parent_id.ToString());
					tw.Write(",");
					tw.Write("\"class_list\":");
					tw.Write("\"");
					tw.Write(class_list==null?"null":class_list);
					tw.Write("\"");
					tw.Write(",");
					tw.Write("\"class_layer\":");
					tw.Write(class_layer==null?"null" : class_layer.ToString());
					tw.Write(",");
					tw.Write("\"sort_id\":");
					tw.Write(sort_id==null?"null" : sort_id.ToString());
					tw.Write(",");
					tw.Write("\"link_url\":");
					tw.Write("\"");
					tw.Write(link_url==null?"null":link_url);
					tw.Write("\"");
					tw.Write(",");
					tw.Write("\"img_url\":");
					tw.Write("\"");
					tw.Write(img_url==null?"null":img_url);
					tw.Write("\"");
					tw.Write(",");
					tw.Write("\"content\":");
					tw.Write("\"");
					tw.Write(content==null?"null":content);
					tw.Write("\"");
					tw.Write(",");
					tw.Write("\"seo_folder\":");
					tw.Write("\"");
					tw.Write(seo_folder==null?"null":seo_folder);
					tw.Write("\"");
					tw.Write(",");
					tw.Write("\"seo_title\":");
					tw.Write("\"");
					tw.Write(seo_title==null?"null":seo_title);
					tw.Write("\"");
					tw.Write(",");
					tw.Write("\"seo_keywords\":");
					tw.Write("\"");
					tw.Write(seo_keywords==null?"null":seo_keywords);
					tw.Write("\"");
					tw.Write(",");
					tw.Write("\"seo_descrition\":");
					tw.Write("\"");
					tw.Write(seo_descrition==null?"null":seo_descrition);
					tw.Write("\"");
			tw.Write("}");
		}
		#endregion
    }
}
