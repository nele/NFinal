﻿﻿using System;
using System.Collections.Generic;
using System.Text;
namespace WebMvc.App.Models.Entity.Common
{
    public class fields:NFinal.Struct
    {
		public System.Int32? a1 {get;set;}
		public System.Int64? a2 {get;set;}
		public System.Byte? a3 {get;set;}
		public System.Int16? a4 {get;set;}
		public System.Object a5 {get;set;}
		public System.Int64? a6 {get;set;}
		public System.Object a7 {get;set;}
		public System.Object a8 {get;set;}
		public System.SByte? a9 {get;set;}
		public System.Object a10 {get;set;}
		public System.String a11 {get;set;}
		public System.Object a12 {get;set;}
		public System.String a13 {get;set;}
		public System.Object a14 {get;set;}
		public System.String a15 {get;set;}
		public System.String a16 {get;set;}
		public System.String a17 {get;set;}
		public System.Byte[] a18 {get;set;}
		public System.Double? a19 {get;set;}
		public System.Double? a20 {get;set;}
		public System.Object a21 {get;set;}
		public System.Double? a22 {get;set;}
		public System.Decimal? a23 {get;set;}
		public System.Decimal? a24 {get;set;}
		public System.Boolean? a25 {get;set;}
		public System.DateTime? a26 {get;set;}
		public System.DateTime? a27 {get;set;}
		#region 写Json字符串
		public override void WriteJson(System.IO.TextWriter tw)
		{
			tw.Write("{");
					tw.Write("\"a1\":");
					tw.Write(a1==null?"null" : a1.ToString());
					tw.Write(",");
					tw.Write("\"a2\":");
					tw.Write(a2==null?"null" : a2.ToString());
					tw.Write(",");
					tw.Write("\"a3\":");
					tw.Write(a3==null?"null" : a3.ToString());
					tw.Write(",");
					tw.Write("\"a4\":");
					tw.Write(a4==null?"null" : a4.ToString());
					tw.Write(",");
					tw.Write("\"a5\":");
					tw.Write("\"");
					tw.Write(a5==null?"null":a5.ToString());
					tw.Write("\"");
					tw.Write(",");
					tw.Write("\"a6\":");
					tw.Write(a6==null?"null" : a6.ToString());
					tw.Write(",");
					tw.Write("\"a7\":");
					tw.Write("\"");
					tw.Write(a7==null?"null":a7.ToString());
					tw.Write("\"");
					tw.Write(",");
					tw.Write("\"a8\":");
					tw.Write("\"");
					tw.Write(a8==null?"null":a8.ToString());
					tw.Write("\"");
					tw.Write(",");
					tw.Write("\"a9\":");
					tw.Write(a9==null?"null" : a9.ToString());
					tw.Write(",");
					tw.Write("\"a10\":");
					tw.Write("\"");
					tw.Write(a10==null?"null":a10.ToString());
					tw.Write("\"");
					tw.Write(",");
					tw.Write("\"a11\":");
					tw.Write("\"");
					tw.Write(a11==null?"null":a11);
					tw.Write("\"");
					tw.Write(",");
					tw.Write("\"a12\":");
					tw.Write("\"");
					tw.Write(a12==null?"null":a12.ToString());
					tw.Write("\"");
					tw.Write(",");
					tw.Write("\"a13\":");
					tw.Write("\"");
					tw.Write(a13==null?"null":a13);
					tw.Write("\"");
					tw.Write(",");
					tw.Write("\"a14\":");
					tw.Write("\"");
					tw.Write(a14==null?"null":a14.ToString());
					tw.Write("\"");
					tw.Write(",");
					tw.Write("\"a15\":");
					tw.Write("\"");
					tw.Write(a15==null?"null":a15);
					tw.Write("\"");
					tw.Write(",");
					tw.Write("\"a16\":");
					tw.Write("\"");
					tw.Write(a16==null?"null":a16);
					tw.Write("\"");
					tw.Write(",");
					tw.Write("\"a17\":");
					tw.Write("\"");
					tw.Write(a17==null?"null":a17);
					tw.Write("\"");
					tw.Write(",");
					tw.Write("\"a18\":");
					tw.Write("\"");
					tw.Write(a18==null?"null":Convert.ToBase64String(a18));
					tw.Write("\"");
					tw.Write(",");
					tw.Write("\"a19\":");
					tw.Write(a19==null?"null" : a19.ToString());
					tw.Write(",");
					tw.Write("\"a20\":");
					tw.Write(a20==null?"null" : a20.ToString());
					tw.Write(",");
					tw.Write("\"a21\":");
					tw.Write("\"");
					tw.Write(a21==null?"null":a21.ToString());
					tw.Write("\"");
					tw.Write(",");
					tw.Write("\"a22\":");
					tw.Write(a22==null?"null" : a22.ToString());
					tw.Write(",");
					tw.Write("\"a23\":");
					tw.Write(a23==null?"null" : a23.ToString());
					tw.Write(",");
					tw.Write("\"a24\":");
					tw.Write(a24==null?"null" : a24.ToString());
					tw.Write(",");
					tw.Write("\"a25\":");
					tw.Write(a25==null?"null":((bool)a25?"true":"false"));
					tw.Write(",");
					tw.Write("\"a26\":");
					tw.Write("\"");
					tw.Write(a26==null?"null":a26.ToString());
					tw.Write("\"");
					tw.Write(",");
					tw.Write("\"a27\":");
					tw.Write("\"");
					tw.Write(a27==null?"null":a27.ToString());
					tw.Write("\"");
			tw.Write("}");
		}
		#endregion
    }
}
