﻿﻿using System;
using System.Collections.Generic;
using System.Text;
namespace WebMvc.App.Models.Entity.Common
{
    public class alipay_config:NFinal.Struct
    {
		public System.Int64 id {get;set;}
		public System.String partner {get;set;}
		public System.String seller_email {get;set;}
		public System.String key {get;set;}
		#region 写Json字符串
		public override void WriteJson(System.IO.TextWriter tw)
		{
			tw.Write("{");
					tw.Write("\"id\":");
					tw.Write(id.ToString());
					tw.Write(",");
					tw.Write("\"partner\":");
					tw.Write("\"");
					tw.Write(partner==null?"null":partner);
					tw.Write("\"");
					tw.Write(",");
					tw.Write("\"seller_email\":");
					tw.Write("\"");
					tw.Write(seller_email==null?"null":seller_email);
					tw.Write("\"");
					tw.Write(",");
					tw.Write("\"key\":");
					tw.Write("\"");
					tw.Write(key==null?"null":key);
					tw.Write("\"");
			tw.Write("}");
		}
		#endregion
    }
}
