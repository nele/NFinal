﻿﻿using System;
using System.Collections.Generic;
using System.Text;
namespace WebMvc.App.Models.Entity.Common
{
    public class category_property:NFinal.Struct
    {
		public System.Int64 id {get;set;}
		public System.String name {get;set;}
		public System.Int64? channel_id {get;set;}
		public System.Int64? parent_id {get;set;}
		public System.Int64? category_id {get;set;}
		#region 写Json字符串
		public override void WriteJson(System.IO.TextWriter tw)
		{
			tw.Write("{");
					tw.Write("\"id\":");
					tw.Write(id.ToString());
					tw.Write(",");
					tw.Write("\"name\":");
					tw.Write("\"");
					tw.Write(name==null?"null":name);
					tw.Write("\"");
					tw.Write(",");
					tw.Write("\"channel_id\":");
					tw.Write(channel_id==null?"null" : channel_id.ToString());
					tw.Write(",");
					tw.Write("\"parent_id\":");
					tw.Write(parent_id==null?"null" : parent_id.ToString());
					tw.Write(",");
					tw.Write("\"category_id\":");
					tw.Write(category_id==null?"null" : category_id.ToString());
			tw.Write("}");
		}
		#endregion
    }
}
