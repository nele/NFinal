﻿﻿using System;
using System.Collections.Generic;
using System.Text;
namespace WebMvc.App.Models.Entity.Common
{
    public class sign_up:NFinal.Struct
    {
		public System.Int64 id {get;set;}
		public System.String name {get;set;}
		public System.Int64? type {get;set;}
		public System.String mobile {get;set;}
		public System.String site_url {get;set;}
		public System.String attr1 {get;set;}
		public System.String attr2 {get;set;}
		public System.String attr3 {get;set;}
		public System.String attr4 {get;set;}
		#region 写Json字符串
		public override void WriteJson(System.IO.TextWriter tw)
		{
			tw.Write("{");
					tw.Write("\"id\":");
					tw.Write(id.ToString());
					tw.Write(",");
					tw.Write("\"name\":");
					tw.Write("\"");
					tw.Write(name==null?"null":name);
					tw.Write("\"");
					tw.Write(",");
					tw.Write("\"type\":");
					tw.Write(type==null?"null" : type.ToString());
					tw.Write(",");
					tw.Write("\"mobile\":");
					tw.Write("\"");
					tw.Write(mobile==null?"null":mobile);
					tw.Write("\"");
					tw.Write(",");
					tw.Write("\"site_url\":");
					tw.Write("\"");
					tw.Write(site_url==null?"null":site_url);
					tw.Write("\"");
					tw.Write(",");
					tw.Write("\"attr1\":");
					tw.Write("\"");
					tw.Write(attr1==null?"null":attr1);
					tw.Write("\"");
					tw.Write(",");
					tw.Write("\"attr2\":");
					tw.Write("\"");
					tw.Write(attr2==null?"null":attr2);
					tw.Write("\"");
					tw.Write(",");
					tw.Write("\"attr3\":");
					tw.Write("\"");
					tw.Write(attr3==null?"null":attr3);
					tw.Write("\"");
					tw.Write(",");
					tw.Write("\"attr4\":");
					tw.Write("\"");
					tw.Write(attr4==null?"null":attr4);
					tw.Write("\"");
			tw.Write("}");
		}
		#endregion
    }
}
