﻿﻿using System;
using System.Collections.Generic;
using System.Text;
namespace WebMvc.App.Models.Entity.Common
{
    public class article_property:NFinal.Struct
    {
		public System.Int64 id {get;set;}
		public System.Int64? article_id {get;set;}
		public System.Int64? property_id {get;set;}
		public System.Int64? propvalue_id {get;set;}
		public System.String name {get;set;}
		public System.String valuea {get;set;}
		#region 写Json字符串
		public override void WriteJson(System.IO.TextWriter tw)
		{
			tw.Write("{");
					tw.Write("\"id\":");
					tw.Write(id.ToString());
					tw.Write(",");
					tw.Write("\"article_id\":");
					tw.Write(article_id==null?"null" : article_id.ToString());
					tw.Write(",");
					tw.Write("\"property_id\":");
					tw.Write(property_id==null?"null" : property_id.ToString());
					tw.Write(",");
					tw.Write("\"propvalue_id\":");
					tw.Write(propvalue_id==null?"null" : propvalue_id.ToString());
					tw.Write(",");
					tw.Write("\"name\":");
					tw.Write("\"");
					tw.Write(name==null?"null":name);
					tw.Write("\"");
					tw.Write(",");
					tw.Write("\"valuea\":");
					tw.Write("\"");
					tw.Write(valuea==null?"null":valuea);
					tw.Write("\"");
			tw.Write("}");
		}
		#endregion
    }
}
