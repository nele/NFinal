﻿﻿using System;
using System.Collections.Generic;
using System.Text;
namespace WebMvc.App.Models.Entity.Common
{
    public class channel:NFinal.Struct
    {
		public System.Int64 id {get;set;}
		public System.Int64? type_id {get;set;}
		public System.Int64? site_id {get;set;}
		public System.String name {get;set;}
		public System.String title {get;set;}
		public System.Int64? sort_id {get;set;}
		public System.String seo_folder {get;set;}
		#region 写Json字符串
		public override void WriteJson(System.IO.TextWriter tw)
		{
			tw.Write("{");
					tw.Write("\"id\":");
					tw.Write(id.ToString());
					tw.Write(",");
					tw.Write("\"type_id\":");
					tw.Write(type_id==null?"null" : type_id.ToString());
					tw.Write(",");
					tw.Write("\"site_id\":");
					tw.Write(site_id==null?"null" : site_id.ToString());
					tw.Write(",");
					tw.Write("\"name\":");
					tw.Write("\"");
					tw.Write(name==null?"null":name);
					tw.Write("\"");
					tw.Write(",");
					tw.Write("\"title\":");
					tw.Write("\"");
					tw.Write(title==null?"null":title);
					tw.Write("\"");
					tw.Write(",");
					tw.Write("\"sort_id\":");
					tw.Write(sort_id==null?"null" : sort_id.ToString());
					tw.Write(",");
					tw.Write("\"seo_folder\":");
					tw.Write("\"");
					tw.Write(seo_folder==null?"null":seo_folder);
					tw.Write("\"");
			tw.Write("}");
		}
		#endregion
    }
}
