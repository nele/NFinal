﻿﻿using System;
using System.Collections.Generic;
using System.Text;
namespace WebMvc.App.Models.Entity.Common
{
    public class shop_goods:NFinal.Struct
    {
		public System.Int64 id {get;set;}
		public System.String subject {get;set;}
		public System.Double? fee {get;set;}
		public System.Int64? qty {get;set;}
		public System.String body {get;set;}
		public System.String show_url {get;set;}
		public System.String total_fee {get;set;}
		#region 写Json字符串
		public override void WriteJson(System.IO.TextWriter tw)
		{
			tw.Write("{");
					tw.Write("\"id\":");
					tw.Write(id.ToString());
					tw.Write(",");
					tw.Write("\"subject\":");
					tw.Write("\"");
					tw.Write(subject==null?"null":subject);
					tw.Write("\"");
					tw.Write(",");
					tw.Write("\"fee\":");
					tw.Write(fee==null?"null" : fee.ToString());
					tw.Write(",");
					tw.Write("\"qty\":");
					tw.Write(qty==null?"null" : qty.ToString());
					tw.Write(",");
					tw.Write("\"body\":");
					tw.Write("\"");
					tw.Write(body==null?"null":body);
					tw.Write("\"");
					tw.Write(",");
					tw.Write("\"show_url\":");
					tw.Write("\"");
					tw.Write(show_url==null?"null":show_url);
					tw.Write("\"");
					tw.Write(",");
					tw.Write("\"total_fee\":");
					tw.Write("\"");
					tw.Write(total_fee==null?"null":total_fee);
					tw.Write("\"");
			tw.Write("}");
		}
		#endregion
    }
}
