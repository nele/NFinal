﻿﻿using System;
using System.Collections.Generic;
using System.Text;
namespace WebMvc.App.Models.Entity.Common
{
    public class sms_send:NFinal.Struct
    {
		public System.Int64 id {get;set;}
		public System.String phone {get;set;}
		public System.String content {get;set;}
		public System.Int64? time {get;set;}
		public System.Int64? success {get;set;}
		public System.String code {get;set;}
		#region 写Json字符串
		public override void WriteJson(System.IO.TextWriter tw)
		{
			tw.Write("{");
					tw.Write("\"id\":");
					tw.Write(id.ToString());
					tw.Write(",");
					tw.Write("\"phone\":");
					tw.Write("\"");
					tw.Write(phone==null?"null":phone);
					tw.Write("\"");
					tw.Write(",");
					tw.Write("\"content\":");
					tw.Write("\"");
					tw.Write(content==null?"null":content);
					tw.Write("\"");
					tw.Write(",");
					tw.Write("\"time\":");
					tw.Write(time==null?"null" : time.ToString());
					tw.Write(",");
					tw.Write("\"success\":");
					tw.Write(success==null?"null" : success.ToString());
					tw.Write(",");
					tw.Write("\"code\":");
					tw.Write("\"");
					tw.Write(code==null?"null":code);
					tw.Write("\"");
			tw.Write("}");
		}
		#endregion
    }
}
