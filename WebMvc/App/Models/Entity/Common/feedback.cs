﻿﻿using System;
using System.Collections.Generic;
using System.Text;
namespace WebMvc.App.Models.Entity.Common
{
    public class feedback:NFinal.Struct
    {
		public System.Int64 id {get;set;}
		public System.String title {get;set;}
		public System.String content {get;set;}
		public System.String user_name {get;set;}
		public System.String user_tel {get;set;}
		public System.String user_qq {get;set;}
		public System.String user_email {get;set;}
		public System.DateTime? add_time {get;set;}
		public System.String reply_content {get;set;}
		public System.DateTime? reply_time {get;set;}
		public System.Int16? is_lock {get;set;}
		#region 写Json字符串
		public override void WriteJson(System.IO.TextWriter tw)
		{
			tw.Write("{");
					tw.Write("\"id\":");
					tw.Write(id.ToString());
					tw.Write(",");
					tw.Write("\"title\":");
					tw.Write("\"");
					tw.Write(title==null?"null":title);
					tw.Write("\"");
					tw.Write(",");
					tw.Write("\"content\":");
					tw.Write("\"");
					tw.Write(content==null?"null":content);
					tw.Write("\"");
					tw.Write(",");
					tw.Write("\"user_name\":");
					tw.Write("\"");
					tw.Write(user_name==null?"null":user_name);
					tw.Write("\"");
					tw.Write(",");
					tw.Write("\"user_tel\":");
					tw.Write("\"");
					tw.Write(user_tel==null?"null":user_tel);
					tw.Write("\"");
					tw.Write(",");
					tw.Write("\"user_qq\":");
					tw.Write("\"");
					tw.Write(user_qq==null?"null":user_qq);
					tw.Write("\"");
					tw.Write(",");
					tw.Write("\"user_email\":");
					tw.Write("\"");
					tw.Write(user_email==null?"null":user_email);
					tw.Write("\"");
					tw.Write(",");
					tw.Write("\"add_time\":");
					tw.Write("\"");
					tw.Write(add_time==null?"null":add_time.ToString());
					tw.Write("\"");
					tw.Write(",");
					tw.Write("\"reply_content\":");
					tw.Write("\"");
					tw.Write(reply_content==null?"null":reply_content);
					tw.Write("\"");
					tw.Write(",");
					tw.Write("\"reply_time\":");
					tw.Write("\"");
					tw.Write(reply_time==null?"null":reply_time.ToString());
					tw.Write("\"");
					tw.Write(",");
					tw.Write("\"is_lock\":");
					tw.Write(is_lock==null?"null" : is_lock.ToString());
			tw.Write("}");
		}
		#endregion
    }
}
