﻿﻿using System;
using System.Collections.Generic;
using System.Text;
namespace WebMvc.App.Models.Entity.Common
{
    public class site:NFinal.Struct
    {
		public System.Int64 id {get;set;}
		public System.String name {get;set;}
		public System.Int64? channel_id {get;set;}
		public System.String subdomain_name {get;set;}
		public System.String title {get;set;}
		public System.String company {get;set;}
		public System.String address {get;set;}
		public System.String tel {get;set;}
		public System.String fax {get;set;}
		public System.String email {get;set;}
		public System.String copyright {get;set;}
		public System.String seo_folder {get;set;}
		public System.String seo_title {get;set;}
		public System.String seo_keyword {get;set;}
		public System.String seo_description {get;set;}
		public System.Int16? is_default {get;set;}
		#region 写Json字符串
		public override void WriteJson(System.IO.TextWriter tw)
		{
			tw.Write("{");
					tw.Write("\"id\":");
					tw.Write(id.ToString());
					tw.Write(",");
					tw.Write("\"name\":");
					tw.Write("\"");
					tw.Write(name==null?"null":name);
					tw.Write("\"");
					tw.Write(",");
					tw.Write("\"channel_id\":");
					tw.Write(channel_id==null?"null" : channel_id.ToString());
					tw.Write(",");
					tw.Write("\"subdomain_name\":");
					tw.Write("\"");
					tw.Write(subdomain_name==null?"null":subdomain_name);
					tw.Write("\"");
					tw.Write(",");
					tw.Write("\"title\":");
					tw.Write("\"");
					tw.Write(title==null?"null":title);
					tw.Write("\"");
					tw.Write(",");
					tw.Write("\"company\":");
					tw.Write("\"");
					tw.Write(company==null?"null":company);
					tw.Write("\"");
					tw.Write(",");
					tw.Write("\"address\":");
					tw.Write("\"");
					tw.Write(address==null?"null":address);
					tw.Write("\"");
					tw.Write(",");
					tw.Write("\"tel\":");
					tw.Write("\"");
					tw.Write(tel==null?"null":tel);
					tw.Write("\"");
					tw.Write(",");
					tw.Write("\"fax\":");
					tw.Write("\"");
					tw.Write(fax==null?"null":fax);
					tw.Write("\"");
					tw.Write(",");
					tw.Write("\"email\":");
					tw.Write("\"");
					tw.Write(email==null?"null":email);
					tw.Write("\"");
					tw.Write(",");
					tw.Write("\"copyright\":");
					tw.Write("\"");
					tw.Write(copyright==null?"null":copyright);
					tw.Write("\"");
					tw.Write(",");
					tw.Write("\"seo_folder\":");
					tw.Write("\"");
					tw.Write(seo_folder==null?"null":seo_folder);
					tw.Write("\"");
					tw.Write(",");
					tw.Write("\"seo_title\":");
					tw.Write("\"");
					tw.Write(seo_title==null?"null":seo_title);
					tw.Write("\"");
					tw.Write(",");
					tw.Write("\"seo_keyword\":");
					tw.Write("\"");
					tw.Write(seo_keyword==null?"null":seo_keyword);
					tw.Write("\"");
					tw.Write(",");
					tw.Write("\"seo_description\":");
					tw.Write("\"");
					tw.Write(seo_description==null?"null":seo_description);
					tw.Write("\"");
					tw.Write(",");
					tw.Write("\"is_default\":");
					tw.Write(is_default==null?"null" : is_default.ToString());
			tw.Write("}");
		}
		#endregion
    }
}
