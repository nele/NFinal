﻿﻿using System;
using System.Collections.Generic;
using System.Text;
namespace WebMvc.App.Models.Entity.Common
{
    public class alipay_orders:NFinal.Struct
    {
		public System.Int64 id {get;set;}
		public System.String config_id {get;set;}
		public System.String payment_type {get;set;}
		public System.String alipay_order_id {get;set;}
		public System.String subject {get;set;}
		public System.String total_fee {get;set;}
		public System.String body {get;set;}
		public System.String show_url {get;set;}
		public System.Int64? time {get;set;}
		public System.Int64? status {get;set;}
		public System.String ip {get;set;}
		#region 写Json字符串
		public override void WriteJson(System.IO.TextWriter tw)
		{
			tw.Write("{");
					tw.Write("\"id\":");
					tw.Write(id.ToString());
					tw.Write(",");
					tw.Write("\"config_id\":");
					tw.Write("\"");
					tw.Write(config_id==null?"null":config_id);
					tw.Write("\"");
					tw.Write(",");
					tw.Write("\"payment_type\":");
					tw.Write("\"");
					tw.Write(payment_type==null?"null":payment_type);
					tw.Write("\"");
					tw.Write(",");
					tw.Write("\"alipay_order_id\":");
					tw.Write("\"");
					tw.Write(alipay_order_id==null?"null":alipay_order_id);
					tw.Write("\"");
					tw.Write(",");
					tw.Write("\"subject\":");
					tw.Write("\"");
					tw.Write(subject==null?"null":subject);
					tw.Write("\"");
					tw.Write(",");
					tw.Write("\"total_fee\":");
					tw.Write("\"");
					tw.Write(total_fee==null?"null":total_fee);
					tw.Write("\"");
					tw.Write(",");
					tw.Write("\"body\":");
					tw.Write("\"");
					tw.Write(body==null?"null":body);
					tw.Write("\"");
					tw.Write(",");
					tw.Write("\"show_url\":");
					tw.Write("\"");
					tw.Write(show_url==null?"null":show_url);
					tw.Write("\"");
					tw.Write(",");
					tw.Write("\"time\":");
					tw.Write(time==null?"null" : time.ToString());
					tw.Write(",");
					tw.Write("\"status\":");
					tw.Write(status==null?"null" : status.ToString());
					tw.Write(",");
					tw.Write("\"ip\":");
					tw.Write("\"");
					tw.Write(ip==null?"null":ip);
					tw.Write("\"");
			tw.Write("}");
		}
		#endregion
    }
}
