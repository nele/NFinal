﻿﻿using System;
using System.Collections.Generic;
using System.Text;
namespace WebMvc.App.Models.Entity.Common
{
    public class artile_comment:NFinal.Struct
    {
		public System.Int64 id {get;set;}
		public System.Int64? channel_id {get;set;}
		public System.Int64? article_id {get;set;}
		public System.Int64? parent_id {get;set;}
		public System.Int64? user_id {get;set;}
		public System.String user_name {get;set;}
		public System.String user_ip {get;set;}
		public System.String content {get;set;}
		public System.DateTime? add_time {get;set;}
		#region 写Json字符串
		public override void WriteJson(System.IO.TextWriter tw)
		{
			tw.Write("{");
					tw.Write("\"id\":");
					tw.Write(id.ToString());
					tw.Write(",");
					tw.Write("\"channel_id\":");
					tw.Write(channel_id==null?"null" : channel_id.ToString());
					tw.Write(",");
					tw.Write("\"article_id\":");
					tw.Write(article_id==null?"null" : article_id.ToString());
					tw.Write(",");
					tw.Write("\"parent_id\":");
					tw.Write(parent_id==null?"null" : parent_id.ToString());
					tw.Write(",");
					tw.Write("\"user_id\":");
					tw.Write(user_id==null?"null" : user_id.ToString());
					tw.Write(",");
					tw.Write("\"user_name\":");
					tw.Write("\"");
					tw.Write(user_name==null?"null":user_name);
					tw.Write("\"");
					tw.Write(",");
					tw.Write("\"user_ip\":");
					tw.Write("\"");
					tw.Write(user_ip==null?"null":user_ip);
					tw.Write("\"");
					tw.Write(",");
					tw.Write("\"content\":");
					tw.Write("\"");
					tw.Write(content==null?"null":content);
					tw.Write("\"");
					tw.Write(",");
					tw.Write("\"add_time\":");
					tw.Write("\"");
					tw.Write(add_time==null?"null":add_time.ToString());
					tw.Write("\"");
			tw.Write("}");
		}
		#endregion
    }
}
