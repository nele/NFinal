﻿﻿using System;
using System.Collections.Generic;
using System.Text;
namespace WebMvc.App.Models.Entity.Common
{
    public class admin:NFinal.Struct
    {
		public System.Int64 id {get;set;}
		public System.String user_name {get;set;}
		public System.String password {get;set;}
		public System.String salt {get;set;}
		public System.Int64? typea {get;set;}
		#region 写Json字符串
		public override void WriteJson(System.IO.TextWriter tw)
		{
			tw.Write("{");
					tw.Write("\"id\":");
					tw.Write(id.ToString());
					tw.Write(",");
					tw.Write("\"user_name\":");
					tw.Write("\"");
					tw.Write(user_name==null?"null":user_name);
					tw.Write("\"");
					tw.Write(",");
					tw.Write("\"password\":");
					tw.Write("\"");
					tw.Write(password==null?"null":password);
					tw.Write("\"");
					tw.Write(",");
					tw.Write("\"salt\":");
					tw.Write("\"");
					tw.Write(salt==null?"null":salt);
					tw.Write("\"");
					tw.Write(",");
					tw.Write("\"typea\":");
					tw.Write(typea==null?"null" : typea.ToString());
			tw.Write("}");
		}
		#endregion
    }
}
