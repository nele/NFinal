﻿﻿using System;
using System.Collections.Generic;
using System.Text;
namespace WebMvc.App.Models.Entity.Common
{
    public class article_attachment:NFinal.Struct
    {
		public System.Int64 id {get;set;}
		public System.Int64? article_id {get;set;}
		public System.String file_name {get;set;}
		public System.String file_path {get;set;}
		public System.Int64? file_size {get;set;}
		public System.String file_ext {get;set;}
		public System.Int64? down_qty {get;set;}
		public System.Int64? point {get;set;}
		public System.DateTime? add_time {get;set;}
		public System.Int64? user_id {get;set;}
		#region 写Json字符串
		public override void WriteJson(System.IO.TextWriter tw)
		{
			tw.Write("{");
					tw.Write("\"id\":");
					tw.Write(id.ToString());
					tw.Write(",");
					tw.Write("\"article_id\":");
					tw.Write(article_id==null?"null" : article_id.ToString());
					tw.Write(",");
					tw.Write("\"file_name\":");
					tw.Write("\"");
					tw.Write(file_name==null?"null":file_name);
					tw.Write("\"");
					tw.Write(",");
					tw.Write("\"file_path\":");
					tw.Write("\"");
					tw.Write(file_path==null?"null":file_path);
					tw.Write("\"");
					tw.Write(",");
					tw.Write("\"file_size\":");
					tw.Write(file_size==null?"null" : file_size.ToString());
					tw.Write(",");
					tw.Write("\"file_ext\":");
					tw.Write("\"");
					tw.Write(file_ext==null?"null":file_ext);
					tw.Write("\"");
					tw.Write(",");
					tw.Write("\"down_qty\":");
					tw.Write(down_qty==null?"null" : down_qty.ToString());
					tw.Write(",");
					tw.Write("\"point\":");
					tw.Write(point==null?"null" : point.ToString());
					tw.Write(",");
					tw.Write("\"add_time\":");
					tw.Write("\"");
					tw.Write(add_time==null?"null":add_time.ToString());
					tw.Write("\"");
					tw.Write(",");
					tw.Write("\"user_id\":");
					tw.Write(user_id==null?"null" : user_id.ToString());
			tw.Write("}");
		}
		#endregion
    }
}
