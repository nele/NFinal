﻿﻿using System;
using System.Collections.Generic;
using System.Text;
namespace WebMvc.App.Models.Entity.PostgreSql
{
    public class fields:NFinal.Struct
    {
		public NpgsqlTypes.BitString a1 {get;set;}
		public System.Boolean? a2 {get;set;}
		public NpgsqlTypes.NpgsqlBox? a3 {get;set;}
		public System.Byte[] a4 {get;set;}
		public System.String a5 {get;set;}
		public System.String a6 {get;set;}
		public NpgsqlTypes.NpgsqlCircle? a7 {get;set;}
		public System.DateTime? a8 {get;set;}
		public System.Decimal? a9 {get;set;}
		public System.Single? a10 {get;set;}
		public System.Double? a11 {get;set;}
		public System.Net.IPAddress a12 {get;set;}
		public System.Int16? a13 {get;set;}
		public System.Int32? a14 {get;set;}
		public System.Int64? a15 {get;set;}
		public System.TimeSpan? a16 {get;set;}
		public System.String a17 {get;set;}
		public NpgsqlTypes.NpgsqlLSeg? a18 {get;set;}
		public System.Net.NetworkInformation.PhysicalAddress a19 {get;set;}
		public System.Decimal? a20 {get;set;}
		public NpgsqlTypes.NpgsqlPath? a21 {get;set;}
		public NpgsqlTypes.NpgsqlPoint? a22 {get;set;}
		public NpgsqlTypes.NpgsqlPolygon? a23 {get;set;}
		public System.Int32? a24 {get;set;}
		public System.Int64? a25 {get;set;}
		public System.String a26 {get;set;}
		public System.DateTime? a27 {get;set;}
		public System.DateTime? a28 {get;set;}
		public System.DateTime? a29 {get;set;}
		public System.DateTime? a30 {get;set;}
		public System.String a31 {get;set;}
		public System.String a32 {get;set;}
		public System.String a33 {get;set;}
		public System.Guid? a34 {get;set;}
		public System.String a35 {get;set;}
		public System.String a36 {get;set;}
		public System.String a37 {get;set;}
		#region 写Json字符串
		public override void WriteJson(System.IO.TextWriter tw)
		{
			tw.Write("{");
					tw.Write("\"a1\":");
					tw.Write("\"");
					tw.Write(a1==null?"null":a1.ToString());
					tw.Write("\"");
					tw.Write(",");
					tw.Write("\"a2\":");
					tw.Write(a2==null?"null":((bool)a2?"true":"false"));
					tw.Write(",");
					tw.Write("\"a3\":");
					tw.Write("\"");
					tw.Write(a3==null?"null":a3.ToString());
					tw.Write("\"");
					tw.Write(",");
					tw.Write("\"a4\":");
					tw.Write("\"");
					tw.Write(a4==null?"null":Convert.ToBase64String(a4));
					tw.Write("\"");
					tw.Write(",");
					tw.Write("\"a5\":");
					tw.Write("\"");
					tw.Write(a5==null?"null":a5);
					tw.Write("\"");
					tw.Write(",");
					tw.Write("\"a6\":");
					tw.Write("\"");
					tw.Write(a6==null?"null":a6);
					tw.Write("\"");
					tw.Write(",");
					tw.Write("\"a7\":");
					tw.Write("\"");
					tw.Write(a7==null?"null":a7.ToString());
					tw.Write("\"");
					tw.Write(",");
					tw.Write("\"a8\":");
					tw.Write("\"");
					tw.Write(a8==null?"null":a8.ToString());
					tw.Write("\"");
					tw.Write(",");
					tw.Write("\"a9\":");
					tw.Write(a9==null?"null" : a9.ToString());
					tw.Write(",");
					tw.Write("\"a10\":");
					tw.Write(a10==null?"null" : a10.ToString());
					tw.Write(",");
					tw.Write("\"a11\":");
					tw.Write(a11==null?"null" : a11.ToString());
					tw.Write(",");
					tw.Write("\"a12\":");
					tw.Write("\"");
					tw.Write(a12==null?"null":a12.ToString());
					tw.Write("\"");
					tw.Write(",");
					tw.Write("\"a13\":");
					tw.Write(a13==null?"null" : a13.ToString());
					tw.Write(",");
					tw.Write("\"a14\":");
					tw.Write(a14==null?"null" : a14.ToString());
					tw.Write(",");
					tw.Write("\"a15\":");
					tw.Write(a15==null?"null" : a15.ToString());
					tw.Write(",");
					tw.Write("\"a16\":");
					tw.Write("\"");
					tw.Write(a16==null?"null":a16.ToString());
					tw.Write("\"");
					tw.Write(",");
					tw.Write("\"a17\":");
					tw.Write("\"");
					tw.Write(a17==null?"null":a17);
					tw.Write("\"");
					tw.Write(",");
					tw.Write("\"a18\":");
					tw.Write("\"");
					tw.Write(a18==null?"null":a18.ToString());
					tw.Write("\"");
					tw.Write(",");
					tw.Write("\"a19\":");
					tw.Write("\"");
					tw.Write(a19==null?"null":a19.ToString());
					tw.Write("\"");
					tw.Write(",");
					tw.Write("\"a20\":");
					tw.Write(a20==null?"null" : a20.ToString());
					tw.Write(",");
					tw.Write("\"a21\":");
					tw.Write("\"");
					tw.Write(a21==null?"null":a21.ToString());
					tw.Write("\"");
					tw.Write(",");
					tw.Write("\"a22\":");
					tw.Write("\"");
					tw.Write(a22==null?"null":a22.ToString());
					tw.Write("\"");
					tw.Write(",");
					tw.Write("\"a23\":");
					tw.Write("\"");
					tw.Write(a23==null?"null":a23.ToString());
					tw.Write("\"");
					tw.Write(",");
					tw.Write("\"a24\":");
					tw.Write(a24==null?"null" : a24.ToString());
					tw.Write(",");
					tw.Write("\"a25\":");
					tw.Write(a25==null?"null" : a25.ToString());
					tw.Write(",");
					tw.Write("\"a26\":");
					tw.Write("\"");
					tw.Write(a26==null?"null":a26);
					tw.Write("\"");
					tw.Write(",");
					tw.Write("\"a27\":");
					tw.Write("\"");
					tw.Write(a27==null?"null":a27.ToString());
					tw.Write("\"");
					tw.Write(",");
					tw.Write("\"a28\":");
					tw.Write("\"");
					tw.Write(a28==null?"null":a28.ToString());
					tw.Write("\"");
					tw.Write(",");
					tw.Write("\"a29\":");
					tw.Write("\"");
					tw.Write(a29==null?"null":a29.ToString());
					tw.Write("\"");
					tw.Write(",");
					tw.Write("\"a30\":");
					tw.Write("\"");
					tw.Write(a30==null?"null":a30.ToString());
					tw.Write("\"");
					tw.Write(",");
					tw.Write("\"a31\":");
					tw.Write("\"");
					tw.Write(a31==null?"null":a31);
					tw.Write("\"");
					tw.Write(",");
					tw.Write("\"a32\":");
					tw.Write("\"");
					tw.Write(a32==null?"null":a32);
					tw.Write("\"");
					tw.Write(",");
					tw.Write("\"a33\":");
					tw.Write("\"");
					tw.Write(a33==null?"null":a33);
					tw.Write("\"");
					tw.Write(",");
					tw.Write("\"a34\":");
					tw.Write("\"");
					tw.Write(a34==null?"null":a34.ToString());
					tw.Write("\"");
					tw.Write(",");
					tw.Write("\"a35\":");
					tw.Write("\"");
					tw.Write(a35==null?"null":a35);
					tw.Write("\"");
					tw.Write(",");
					tw.Write("\"a36\":");
					tw.Write("\"");
					tw.Write(a36==null?"null":a36);
					tw.Write("\"");
					tw.Write(",");
					tw.Write("\"a37\":");
					tw.Write("\"");
					tw.Write(a37==null?"null":a37);
					tw.Write("\"");
			tw.Write("}");
		}
		#endregion
    }
}
