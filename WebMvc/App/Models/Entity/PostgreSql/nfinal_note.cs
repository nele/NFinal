﻿﻿using System;
using System.Collections.Generic;
using System.Text;
namespace WebMvc.App.Models.Entity.PostgreSql
{
    public class nfinal_note:NFinal.Struct
    {
		public System.String table_name {get;set;}
		public System.String field_name {get;set;}
		public System.String field_note {get;set;}
		#region 写Json字符串
		public override void WriteJson(System.IO.TextWriter tw)
		{
			tw.Write("{");
					tw.Write("\"table_name\":");
					tw.Write("\"");
					tw.Write(table_name==null?"null":table_name);
					tw.Write("\"");
					tw.Write(",");
					tw.Write("\"field_name\":");
					tw.Write("\"");
					tw.Write(field_name==null?"null":field_name);
					tw.Write("\"");
					tw.Write(",");
					tw.Write("\"field_note\":");
					tw.Write("\"");
					tw.Write(field_note==null?"null":field_note);
					tw.Write("\"");
			tw.Write("}");
		}
		#endregion
    }
}
