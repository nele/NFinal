﻿﻿using System;
using System.Collections.Generic;
using System.Text;
namespace WebMvc.App.Models.Entity.Oracle
{
    public class fields:NFinal.Struct
    {
		public System.String column1 {get;set;}
		public System.Double? column2 {get;set;}
		public System.Single? column3 {get;set;}
		public System.Byte[] column4 {get;set;}
		public System.String column5 {get;set;}
		public System.String column6 {get;set;}
		public System.String column7 {get;set;}
		public System.String column8 {get;set;}
		public System.String column9 {get;set;}
		public System.DateTime? column10 {get;set;}
		public System.Decimal? column11 {get;set;}
		public System.Decimal? column12 {get;set;}
		public System.Decimal? column13 {get;set;}
		public System.Decimal? column14 {get;set;}
		public System.Decimal? column15 {get;set;}
		public System.Decimal? column16 {get;set;}
		public System.TimeSpan? column17 {get;set;}
		public System.Int64? column18 {get;set;}
		public System.String column19 {get;set;}
		public System.String column20 {get;set;}
		public System.String column21 {get;set;}
		public System.String column22 {get;set;}
		public System.String column23 {get;set;}
		public System.String column24 {get;set;}
		public System.String column25 {get;set;}
		public System.String column26 {get;set;}
		public System.Decimal? column27 {get;set;}
		public System.Decimal? column28 {get;set;}
		public System.String column29 {get;set;}
		public System.Byte[] column30 {get;set;}
		public System.Decimal? column31 {get;set;}
		public System.String column32 {get;set;}
		public System.Decimal? column33 {get;set;}
		public System.DateTime? column34 {get;set;}
		public System.String column35 {get;set;}
		public System.String column36 {get;set;}
		public System.String column37 {get;set;}
		public System.Byte[] column38 {get;set;}
		#region 写Json字符串
		public override void WriteJson(System.IO.TextWriter tw)
		{
			tw.Write("{");
					tw.Write("\"column1\":");
					tw.Write("\"");
					tw.Write(column1==null?"null":column1);
					tw.Write("\"");
					tw.Write(",");
					tw.Write("\"column2\":");
					tw.Write(column2==null?"null" : column2.ToString());
					tw.Write(",");
					tw.Write("\"column3\":");
					tw.Write(column3==null?"null" : column3.ToString());
					tw.Write(",");
					tw.Write("\"column4\":");
					tw.Write("\"");
					tw.Write(column4==null?"null":Convert.ToBase64String(column4));
					tw.Write("\"");
					tw.Write(",");
					tw.Write("\"column5\":");
					tw.Write("\"");
					tw.Write(column5==null?"null":column5);
					tw.Write("\"");
					tw.Write(",");
					tw.Write("\"column6\":");
					tw.Write("\"");
					tw.Write(column6==null?"null":column6);
					tw.Write("\"");
					tw.Write(",");
					tw.Write("\"column7\":");
					tw.Write("\"");
					tw.Write(column7==null?"null":column7);
					tw.Write("\"");
					tw.Write(",");
					tw.Write("\"column8\":");
					tw.Write("\"");
					tw.Write(column8==null?"null":column8);
					tw.Write("\"");
					tw.Write(",");
					tw.Write("\"column9\":");
					tw.Write("\"");
					tw.Write(column9==null?"null":column9);
					tw.Write("\"");
					tw.Write(",");
					tw.Write("\"column10\":");
					tw.Write("\"");
					tw.Write(column10==null?"null":column10.ToString());
					tw.Write("\"");
					tw.Write(",");
					tw.Write("\"column11\":");
					tw.Write(column11==null?"null" : column11.ToString());
					tw.Write(",");
					tw.Write("\"column12\":");
					tw.Write(column12==null?"null" : column12.ToString());
					tw.Write(",");
					tw.Write("\"column13\":");
					tw.Write(column13==null?"null" : column13.ToString());
					tw.Write(",");
					tw.Write("\"column14\":");
					tw.Write(column14==null?"null" : column14.ToString());
					tw.Write(",");
					tw.Write("\"column15\":");
					tw.Write(column15==null?"null" : column15.ToString());
					tw.Write(",");
					tw.Write("\"column16\":");
					tw.Write(column16==null?"null" : column16.ToString());
					tw.Write(",");
					tw.Write("\"column17\":");
					tw.Write("\"");
					tw.Write(column17==null?"null":column17.ToString());
					tw.Write("\"");
					tw.Write(",");
					tw.Write("\"column18\":");
					tw.Write(column18==null?"null" : column18.ToString());
					tw.Write(",");
					tw.Write("\"column19\":");
					tw.Write("\"");
					tw.Write(column19==null?"null":column19);
					tw.Write("\"");
					tw.Write(",");
					tw.Write("\"column20\":");
					tw.Write("\"");
					tw.Write(column20==null?"null":column20);
					tw.Write("\"");
					tw.Write(",");
					tw.Write("\"column21\":");
					tw.Write("\"");
					tw.Write(column21==null?"null":column21);
					tw.Write("\"");
					tw.Write(",");
					tw.Write("\"column22\":");
					tw.Write("\"");
					tw.Write(column22==null?"null":column22);
					tw.Write("\"");
					tw.Write(",");
					tw.Write("\"column23\":");
					tw.Write("\"");
					tw.Write(column23==null?"null":column23);
					tw.Write("\"");
					tw.Write(",");
					tw.Write("\"column24\":");
					tw.Write("\"");
					tw.Write(column24==null?"null":column24);
					tw.Write("\"");
					tw.Write(",");
					tw.Write("\"column25\":");
					tw.Write("\"");
					tw.Write(column25==null?"null":column25);
					tw.Write("\"");
					tw.Write(",");
					tw.Write("\"column26\":");
					tw.Write("\"");
					tw.Write(column26==null?"null":column26);
					tw.Write("\"");
					tw.Write(",");
					tw.Write("\"column27\":");
					tw.Write(column27==null?"null" : column27.ToString());
					tw.Write(",");
					tw.Write("\"column28\":");
					tw.Write(column28==null?"null" : column28.ToString());
					tw.Write(",");
					tw.Write("\"column29\":");
					tw.Write("\"");
					tw.Write(column29==null?"null":column29);
					tw.Write("\"");
					tw.Write(",");
					tw.Write("\"column30\":");
					tw.Write("\"");
					tw.Write(column30==null?"null":Convert.ToBase64String(column30));
					tw.Write("\"");
					tw.Write(",");
					tw.Write("\"column31\":");
					tw.Write(column31==null?"null" : column31.ToString());
					tw.Write(",");
					tw.Write("\"column32\":");
					tw.Write("\"");
					tw.Write(column32==null?"null":column32);
					tw.Write("\"");
					tw.Write(",");
					tw.Write("\"column33\":");
					tw.Write(column33==null?"null" : column33.ToString());
					tw.Write(",");
					tw.Write("\"column34\":");
					tw.Write("\"");
					tw.Write(column34==null?"null":column34.ToString());
					tw.Write("\"");
					tw.Write(",");
					tw.Write("\"column35\":");
					tw.Write("\"");
					tw.Write(column35==null?"null":column35);
					tw.Write("\"");
					tw.Write(",");
					tw.Write("\"column36\":");
					tw.Write("\"");
					tw.Write(column36==null?"null":column36);
					tw.Write("\"");
					tw.Write(",");
					tw.Write("\"column37\":");
					tw.Write("\"");
					tw.Write(column37==null?"null":column37);
					tw.Write("\"");
					tw.Write(",");
					tw.Write("\"column38\":");
					tw.Write("\"");
					tw.Write(column38==null?"null":Convert.ToBase64String(column38));
					tw.Write("\"");
			tw.Write("}");
		}
		#endregion
    }
}
