﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebMvc.App.Models.DAL
{
    public class Sqlite
    {
        public class __GetAll_users__:NFinal.Struct
		{
			/// <summary>
            /// 用户表ID
            /// </summary>
			public System.Int64 id;
			/// <summary>
            /// 名称
            /// </summary>
			public System.String name;
			/// <summary>
            /// 密码
            /// </summary>
			public System.String pwd;
			#region 写Json字符串
			public override void WriteJson(System.IO.TextWriter tw)
			{
				tw.Write("{");
						tw.Write("\"id\":");
						tw.Write(id.ToString());
						tw.Write(",");
						tw.Write("\"name\":");
						if(name==null)
						{
							tw.Write("null");
						}
						else
						{
							tw.Write("\"");
							tw.Write(name);
							tw.Write("\"");
						}
						tw.Write(",");
						tw.Write("\"pwd\":");
						if(pwd==null)
						{
							tw.Write("null");
						}
						else
						{
							tw.Write("\"");
							tw.Write(pwd);
							tw.Write("\"");
						}
				tw.Write("}");
			}
			#endregion
		}
		public NFinal.List<__GetAll_users__> GetAll(System.Data.IDbConnection con)
        {
            var con1=(System.Data.SQLite.SQLiteConnection)con;
            #region	var users;选取所有记录
			var users = new NFinal.List<__GetAll_users__>();
			var __GetAll_users_command__ = new System.Data.SQLite.SQLiteCommand("select * from users", con1);
			var __GetAll_users_reader__= __GetAll_users_command__.ExecuteReader();
			if (__GetAll_users_reader__.HasRows)
			{
				while (__GetAll_users_reader__.Read())
				{
					var __GetAll_users_temp__ = new __GetAll_users__();
					__GetAll_users_temp__.id = __GetAll_users_reader__.GetInt64(0);
					__GetAll_users_temp__.name =__GetAll_users_reader__.IsDBNull(1)?null: __GetAll_users_reader__.GetString(1);
					__GetAll_users_temp__.pwd =__GetAll_users_reader__.IsDBNull(2)?null: __GetAll_users_reader__.GetString(2);
					users.Add(__GetAll_users_temp__);
				}
			}
			__GetAll_users_reader__.Dispose();
			__GetAll_users_command__.Dispose();
			#endregion
			
            return users;
        }
        public class __GetRandom_users__:NFinal.Struct
		{
			/// <summary>
            /// 用户表ID
            /// </summary>
			public System.Int64 id;
			/// <summary>
            /// 名称
            /// </summary>
			public System.String name;
			/// <summary>
            /// 密码
            /// </summary>
			public System.String pwd;
			#region 写Json字符串
			public override void WriteJson(System.IO.TextWriter tw)
			{
				tw.Write("{");
						tw.Write("\"id\":");
						tw.Write(id.ToString());
						tw.Write(",");
						tw.Write("\"name\":");
						if(name==null)
						{
							tw.Write("null");
						}
						else
						{
							tw.Write("\"");
							tw.Write(name);
							tw.Write("\"");
						}
						tw.Write(",");
						tw.Write("\"pwd\":");
						if(pwd==null)
						{
							tw.Write("null");
						}
						else
						{
							tw.Write("\"");
							tw.Write(pwd);
							tw.Write("\"");
						}
				tw.Write("}");
			}
			#endregion
		}
		public NFinal.List<__GetRandom_users__> GetRandom(System.Data.IDbConnection con)
        {
            var con1=(System.Data.SQLite.SQLiteConnection)con;
            #region	var users; 随机选取前N行
			var users = new NFinal.List<__GetRandom_users__>();
			var __GetRandom_users_command__ = new System.Data.SQLite.SQLiteCommand(string.Format("select * from users order by random() limit {0}",2), con1);
			var __GetRandom_users_reader__= __GetRandom_users_command__.ExecuteReader();
			if (__GetRandom_users_reader__.HasRows)
			{
				while (__GetRandom_users_reader__.Read())
				{
					var __GetRandom_users_temp__ = new __GetRandom_users__();
					__GetRandom_users_temp__.id = __GetRandom_users_reader__.GetInt64(0);
					__GetRandom_users_temp__.name =__GetRandom_users_reader__.IsDBNull(1)?null: __GetRandom_users_reader__.GetString(1);
					__GetRandom_users_temp__.pwd =__GetRandom_users_reader__.IsDBNull(2)?null: __GetRandom_users_reader__.GetString(2);
					users.Add(__GetRandom_users_temp__);
				}
			}
			__GetRandom_users_reader__.Dispose();
			__GetRandom_users_command__.Dispose();
			#endregion
			
            return users;
        }
    }
}