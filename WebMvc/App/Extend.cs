﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebMvc.App
{
    public static class Extend
    {
        public static string ToJson<T>(this IList<T> structs,bool addBracket=true)
        {
            string result = Newtonsoft.Json.JsonConvert.SerializeObject(structs);
            if (!addBracket)
            {
                result= result.Trim('{','}');
            }
            return result;
        }
        public static Byte ToByte(this object obj)
        {
            return Convert.ToByte(obj);
        }
        public static SByte ToSByte(this object obj)
        {
            return Convert.ToSByte(obj);
        }
        public static Char ToChar(this object obj)
        {
            return Convert.ToChar(obj);
        }
        public static DateTime ToDateTime(this object obj)
        {
            return Convert.ToDateTime(obj);
        }
        public static decimal ToDecimal(this object obj)
        {
            return Convert.ToDecimal(obj);
        }
        public static double ToDouble(this object obj)
        {
            return Convert.ToDouble(obj);
        }
        public static float ToSingle(this object obj)
        {
            return Convert.ToSingle(obj);
        }
        public static Boolean ToBoolean(this object obj)
        {
            return Convert.ToBoolean(obj);
        }
        public static Int16 ToInt16(this object obj)
        {
            return Convert.ToInt16(obj);
        }
        public static Int32 ToInt32(this object obj)
        {
            return Convert.ToInt32(obj);
        }
        public static Int64 ToInt64(this object obj)
        {
            return Convert.ToInt64(obj);
        }
        public static UInt16 ToUInt16(this object obj)
        {
            return Convert.ToUInt16(obj);
        }
        public static UInt32 ToUInt32(this object obj)
        {
            return Convert.ToUInt32(obj);
        }
        public static UInt64 ToUInt64(this object obj)
        {
            return Convert.ToUInt64(obj);
        }
    }
}