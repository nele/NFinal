﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebMvc.App.Controllers
{
    public class SqliteBLL : Controller
    {
        public void queryAll()
        {
            var con = Models.Common.OpenConnection();
            Models.DAL.Sqlite sqlite = new Models.DAL.Sqlite();
            var users= sqlite.GetAll(con);
            con.Close();
            AjaxReturn(users);
        }
        public void queryRandom()
        {
            var con = Models.Common.OpenConnection();
            Models.DAL.Sqlite sqlite = new Models.DAL.Sqlite();
            var users = sqlite.GetRandom(con);
            con.Close();
            AjaxReturn(users);
        }
    }
}