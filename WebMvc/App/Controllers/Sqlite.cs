﻿using System;
using System.Collections.Generic;
using System.Web;
using NFinal.Advanced;


namespace WebMvc.App.Controllers
{
    [BaseUrl("/Sqlite")]
    public class Sqlite : Controller
    {
        Models.Tips.Common.users users = new Models.Tips.Common.users();
        [GetHtml("/update.html")]
        public void update(
            [maxLength(12),required()]
            string name,
            
            string pwd)
        {
            var con = Models.Common.OpenConnection();
            var count = con.Update($"update {users} set {users.name}=@name,{users.pwd}=@pwd where {users.id}=1");
            con.Close();
            Write(count);
        }
        public void insert(string name, string pwd)
        {
            var con = Models.Common.OpenConnection();
            var id = con.Insert($"insert into {users}({users.name},{users.pwd}) values(@name,@pwd)");
            con.Close();
            Write(id);
        }
        public void delete(int id)
        {
            var con = Models.Common.OpenConnection();
            var count = con.Delete($"delete from {users} where {users.id}=@id");
            con.Close();
            Write(count);
        }
        [GetJson("/page-{p}.json")]
        public void page(int p)
        {
            var con = Models.Common.OpenConnection();
            NFinal.Page page = new NFinal.Page(p, 2);
            var us = con.Page($"select {users.id},{users.name},{users.pwd} from {users}",page);
            con.Close();
            AjaxReturn(us);
        }
        public void queryAll()
        {
            var con = Models.Common.OpenConnection();
            var us= con.QueryAll($"select {users.id},{users.name},{users.pwd} from {users}");
            con.Close();
            AjaxReturn(us);
        }
        public void queryObject()
        {
            var con = Models.Common.OpenConnection();
            var count = con.QueryObject($"select count(*) from {users}").ToInt32();
            con.Close();
            Write(count);
        }
        public void queryRandom()
        {
            var con = Models.Common.OpenConnection();
            var us = con.QueryRandom($"select {users.id},{users.name},{users.pwd} from {users}",3);
            con.Close();
            AjaxReturn(us);
        }
        public void queryRow(int id)
        {
            var con = Models.Common.OpenConnection();
            var user = con.QueryRow($"select {users.id},{users.name},{users.pwd} from {users} where {users.id}=@id");
            con.Close();
            AjaxReturn(user);
        }
        public void queryTop(int top)
        {
            var con = Models.Common.OpenConnection();
            var us = con.QueryTop($"select {users.id},{users.name},{users.pwd} from {users}",top);
            con.Close();
            AjaxReturn(us);
        }
    }
}