﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebMvc.App.Controllers
{
    public class Mysql : Controller
    {
        Models.Tips.MySql.users users = new Models.Tips.MySql.users();
        public void update(
            string name,
            string pwd)
        {
            var con = Models.MySql.OpenConnection();
            var count = con.Update($"update {users} set {users.name}=@name,{users.pwd}=@pwd where {users.id}=1");
            con.Close();
            Write(count);
        }
        public void insert(string name, string pwd)
        {
            var con = Models.MySql.OpenConnection();
            var id = con.Insert($"insert into {users}({users.name},{users.pwd}) values(@name,@pwd)");
            con.Close();
            Write(id);
        }
        public void delete(int id)
        {
            var con = Models.MySql.OpenConnection();
            var count = con.Delete($"delete from {users} where {users.id}=@id");
            con.Close();
            Write(count);
        }
        public void page(int p)
        {
            var con = Models.MySql.OpenConnection();
            NFinal.Page page = new NFinal.Page(p, 2);
            var us = con.Page($"select {users.id},{users.name},{users.pwd} from {users}", page);
            con.Close();
            AjaxReturn(us);
        }
        public void queryAll()
        {
            var con = Models.MySql.OpenConnection();
            var us = con.QueryAll($"select {users.id},{users.name},{users.pwd} from {users}");
            con.Close();
            AjaxReturn(us);
        }
        public void queryObject()
        {
            var con = Models.MySql.OpenConnection();
            var count = con.QueryObject($"select count(*) from {users}").ToInt32();
            con.Close();
            Write(count);
        }
        public void queryRandom()
        {
            var con = Models.MySql.OpenConnection();
            var us = con.QueryRandom($"select {users.id},{users.name},{users.pwd} from {users}", 3);
            con.Close();
            AjaxReturn(us);
        }
        public void queryRow(int id)
        {
            var con = Models.MySql.OpenConnection();
            var user = con.QueryRow($"select {users.id},{users.name},{users.pwd} from {users} where {users.id}=@id");
            con.Close();
            AjaxReturn(user);
        }
        public void queryTop(int top)
        {
            var con = Models.MySql.OpenConnection();
            var us = con.QueryTop($"select {users.id},{users.name},{users.pwd} from {users}", top);
            con.Close();
            AjaxReturn(us);
        }
    }
}