﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebMvc.App.Controllers
{
    public class SqliteCache:Controller
    {
        public void queryAll()
        {
            //选取所有的参数并缓存
            var client = NFinal.Cache.GetRedisClient();
            var client2 = client.As<Models.Entity.Common.users>();
            var users = client2.GetAll();
            if (users.Count==0)
            {
                var con = Models.Common.OpenConnection();
                users = con.QueryAll<Models.Entity.Common.users>("select * from users");
                client2.StoreAll(users);
                con.Close();
            }
            client.Dispose();
        }
        public void queryRandom()
        {
            var client = NFinal.Cache.GetRedisClient();
            var client2 = client.As<Models.Entity.Common.users>();
            var users = (from u in client2.GetAll() orderby Guid.NewGuid() select u).ToList();
            if (users.Count == 0)
            {
                var con = Models.Common.OpenConnection();
                users = con.QueryAll<Models.Entity.Common.users>("select * from users");
                con.Close();
                client2.StoreAll(users);
            }
            client.Dispose();
        }
        public void queryTop()
        {
            var client = NFinal.Cache.GetRedisClient();
            var client2 = client.As<Models.Entity.Common.users>();
            var users = client2.GetAll().Take(10).ToList();
            if (users.Count == 0)
            {
                var con = Models.Common.OpenConnection();
                users = con.QueryAll<Models.Entity.Common.users>("select * from users");
                con.Close();
                client2.StoreAll(users);
            }
            client.Dispose();
        }
        public void queryRow(int id)
        {
            var client = NFinal.Cache.GetRedisClient();
            var client2 = client.As<Models.Entity.Common.users>();
            var user = (from u in client2.GetAll() where u.id==id select u).Single<Models.Entity.Common.users>();
            if (user==null)
            {
                var con = Models.Common.OpenConnection();
                user = con.QueryRow<Models.Entity.Common.users>("select * from users where id=@id");
                con.Close();
                client2.Store(user);
            }
            client.Dispose();
        }
        public void Page(int p)
        {
            var client = NFinal.Cache.GetRedisClient();
            var client2 = client.As<Models.Entity.Common.users>();
            var users = (from u in client2.GetAll() select u).ToList();
            if (users.Count == 0)
            {
                var con = Models.Common.OpenConnection();
                users = con.QueryAll<Models.Entity.Common.users>("select * from users");
                con.Close();
                client2.StoreAll(users);
            }
            client.Dispose();
        }
    }
}