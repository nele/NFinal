﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NFinal.Advanced;

namespace WebMvc.App.Controllers
{
    public class IndexController:Controller
    {
        [GetHtml("~/Index.html")]
        public void Index()
        {
            View("Index.aspx");
        }
    }
}