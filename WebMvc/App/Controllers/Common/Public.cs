﻿using System;
using System.Collections.Generic;
using System.Web;

namespace WebMvc.App.Controllers.Common
{
    public class Public: Controller
    {
        public new void Success(string message,string url,int second)
        {
            View("Public/Success.aspx");
        }
        public new void Error(string message, string url, int second)
        {
            View("Public/Error.aspx");
        }
        public void Header()
        {
            string message=null;
            View("Public/Header.ascx");
        }
        public void Footer()
        {
            View("Public/Footer.ascx");
        }
        public void Navigator()
        {
            NFinal.Page page = new NFinal.Page(1,2);
            View("Public/Navigator.ascx");
        }
        
    }
}