﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebMvc.App.Controllers.Common
{
    public class VCode:Controller
    {
        private string GenerateCheckCode()
        {
            //创建整型型变量   
            int number;
            //创建字符型变量   
            char code;
            //创建字符串变量并初始化为空   
            string checkCode = String.Empty;
            //创建Random对象   
            Random random = new Random();
            //使用For循环生成4个数字   
            for (int i = 0; i < 4; i++)
            {
                //生成一个随机数   
                number = random.Next();
                //将数字转换成为字符型   
                code = (char)('0' + (char)(number % 10));

                checkCode += code.ToString();
            }
            //返回字符串   
            return checkCode;
        }
        private System.Drawing.Image CreateCheckCodeImage(string checkCode)
        {
            //判断字符串不等于空和null   
            if (checkCode == null || checkCode.Trim() == String.Empty)
                return null;
            //创建一个位图对象   
            System.Drawing.Bitmap image = new System.Drawing.Bitmap((int)Math.Ceiling((checkCode.Length * 12.5)), 22);
            //创建Graphics对象   
            System.Drawing.Graphics g = System.Drawing.Graphics.FromImage(image);

            try
            {
                //生成随机生成器   
                Random random = new Random();

                //清空图片背景色   
                g.Clear(System.Drawing.Color.White);

                //画图片的背景噪音线   
                for (int i = 0; i < 2; i++)
                {
                    int x1 = random.Next(image.Width);
                    int x2 = random.Next(image.Width);
                    int y1 = random.Next(image.Height);
                    int y2 = random.Next(image.Height);

                    g.DrawLine(new System.Drawing.Pen(System.Drawing.Color.Black), x1, y1, x2, y2);
                }

                System.Drawing.Font font = new System.Drawing.Font("Arial", 12, (System.Drawing.FontStyle.Bold));
                System.Drawing.Drawing2D.LinearGradientBrush brush = new System.Drawing.Drawing2D.LinearGradientBrush(
                    new System.Drawing.Rectangle(0, 0, image.Width, image.Height),
                    System.Drawing.Color.Blue,
                    System.Drawing.Color.DarkRed,
                    1.2f, true);
                g.DrawString(checkCode, font, brush, 2, 2);

                //画图片的前景噪音点   
                for (int i = 0; i < 100; i++)
                {
                    int x = random.Next(image.Width);
                    int y = random.Next(image.Height);

                    image.SetPixel(x, y, System.Drawing.Color.FromArgb(random.Next()));
                }

                //画图片的边框线   
                g.DrawRectangle(new System.Drawing.Pen(System.Drawing.Color.Silver), 0, 0, image.Width - 1, image.Height - 1);
            }
            finally
            {
                g.Dispose();
            }
            return image;
        }
        public void GetVerifyImage()
        {
            if (!this._isStatic)
            {
                //将图片输出到页面上   
                System.IO.MemoryStream ms = new System.IO.MemoryStream();
                string _vocde = GenerateCheckCode();
                using (System.Security.Cryptography.MD5CryptoServiceProvider md5 = new System.Security.Cryptography.MD5CryptoServiceProvider())
                {
                    string _vcheck = BitConverter.ToString(md5.ComputeHash(System.Text.Encoding.UTF8.GetBytes(_vocde + _salt))).Replace("-", "");
                    Cookie.vcheck = _vcheck;
                }
                System.Drawing.Image image = CreateCheckCodeImage(_vocde);
                image.Save(ms, System.Drawing.Imaging.ImageFormat.Gif);
                _context.Response.ContentType = "image/gif";
                try
                {
                    Write(ms.ToArray());
                }
                finally
                {
                    ms.Dispose();
                    image.Dispose();
                }
            }
        }
        public void VCheck()
        {
            this.AjaxReturn(Verify());
        }
    }
}