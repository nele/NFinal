﻿using System;
using System.Collections.Generic;
using System.Web;
using NFinal.Advanced;


namespace WebMvc.App.Controllers
{
    public class SqliteStruct : Controller
    {
        public void update(string name,string pwd)
        {
            var con = Models.Common.OpenConnection();
            
            var count= con.Update("update users set name=@name,pwd=@pwd where id=1");
            con.Close();
            Write(count);
        }

        public void insert(string name, string pwd)
        {
            var con = Models.Common.OpenConnection();
            var id = con.Insert("insert into users(name,pwd) values(@name,@pwd)");
            con.Close();
            Write(id);
        }
        public void delete(int id)
        {
            var con = Models.Common.OpenConnection();
            var count = con.Delete("delete from users where id=@id");
            con.Close();
            Write(count);
        }
        public void page(int p)
        {
            var con = Models.Common.OpenConnection();
            List<Models.Entity.Common.users> users = new List<Models.Entity.Common.users>();
            NFinal.Page page = new NFinal.Page(p, 2);
            users = con.Page<Models.Entity.Common.users>("select * from users",page);
            con.Close();
            AjaxReturn(users);
        }
        public void queryAll()
        {
            var con = Models.Common.OpenConnection();
            List<Models.Entity.Common.users> users = new List<Models.Entity.Common.users>();
            users= con.QueryAll<Models.Entity.Common.users>("select * from users");
            con.Close();
            AjaxReturn(users);
        }
        public void queryObject()
        {
            var con = Models.Common.OpenConnection();
            var count = con.QueryObject("select count(*) from users").ToInt32();
            con.Close();
            Write(count);
        }
        public void queryRandom()
        {
            var con = Models.Common.OpenConnection();
            List<Models.Entity.Common.users> users = new List<Models.Entity.Common.users>();
            users = con.QueryRandom<Models.Entity.Common.users>("select * from users",3);
            con.Close();
            AjaxReturn(users);
        }
        public void queryRow(int id)
        {
            var con = Models.Common.OpenConnection();
            Models.Entity.Common.users user = new Models.Entity.Common.users();
            user = con.QueryRow<Models.Entity.Common.users>("select * from users where id=@id");
            con.Close();
            AjaxReturn(user);
        }
        public void queryTop(int top)
        {
            var con = Models.Common.OpenConnection();
            List<Models.Entity.Common.users> users = new List<Models.Entity.Common.users>();
            users = con.QueryTop<Models.Entity.Common.users>("select * from users",top);
            con.Close();
            AjaxReturn(users);
        }
    }
}