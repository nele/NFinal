﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebMvc.App.Controllers
{
    public class SqliteTransStruct:Controller
    {
        public void update(string name, string pwd)
        {
            var con = Models.Common.OpenConnection();
            var trans = con.BeginTransaction();
            int count = 0;
            try
            {
                count = trans.Update("update users set name=@name,pwd=@pwd where id=1");
                trans.Commit();
            }
            catch
            {
                trans.Rollback();
            }
            con.Close();
            Write(count);
        }
        public void insert(string name, string pwd)
        {
            var con = Models.Common.OpenConnection();
            var trans = con.BeginTransaction();
            int id = 0;
            try
            {
                id = trans.Insert("insert into users(name,pwd) values(@name,@pwd)");
                trans.Commit();
            }
            catch
            {
                trans.Rollback();
            }
            con.Close();
            Write(id);
        }
        public void delete(int id)
        {
            var con = Models.Common.OpenConnection();
            var trans = con.BeginTransaction();
            int count = 0;
            try
            {
                count = trans.Delete("delete from users where id=@id");
                trans.Commit();
            }
            catch
            {
                trans.Rollback();
            }
            con.Close();
            Write(count);
        }
        public void page(int p)
        {
            var con = Models.Common.OpenConnection();
            var trans = con.BeginTransaction();
            List<Models.Entity.Common.users> users = new List<Models.Entity.Common.users>();
            NFinal.Page page = new NFinal.Page(p, 2);
            try
            {
                users = trans.Page<Models.Entity.Common.users>("select * from users", page);
                AjaxReturn(users);
                trans.Commit();
            }
            catch
            {
                trans.Rollback();
            }
            con.Close();
        }
        public void queryAll()
        {
            var con = Models.Common.OpenConnection();
            var trans = con.BeginTransaction();
            List<Models.Entity.Common.users> users = new List<Models.Entity.Common.users>();
            try
            {
                users = trans.QueryAll<Models.Entity.Common.users>("select * from users");
                AjaxReturn(users);
                trans.Rollback();
            }
            catch
            {
                trans.Rollback();
            }
            con.Close();
        }
        public void queryObject()
        {
            var con = Models.Common.OpenConnection();
            var trans = con.BeginTransaction();
            try
            {
                var count = trans.QueryObject("select count(*) from users").ToInt32();
                trans.Commit();
                Write(count);
            }
            catch
            {
                trans.Rollback();
            }
            con.Close();
        }
        public void queryRandom()
        {
            var con = Models.Common.OpenConnection();
            var trans = con.BeginTransaction();
            List<Models.Entity.Common.users> users = new List<Models.Entity.Common.users>();
            try
            {
                users = trans.QueryRandom<Models.Entity.Common.users>("select * from users", 3);
                trans.Commit();
                AjaxReturn(users);
            }
            catch
            {
                trans.Rollback();
            }
            con.Close();
        }
        public void queryRow(int id)
        {
            var con = Models.Common.OpenConnection();
            var trans = con.BeginTransaction();
            Models.Entity.Common.users user = new Models.Entity.Common.users();
            try
            {
                user = trans.QueryRow<Models.Entity.Common.users>("select * from users where id=@id");
                trans.Commit();
                AjaxReturn(user);
            }
            catch
            {
                trans.Rollback();
            }
            con.Close();
        }
        public void queryTop(int top)
        {
            var con = Models.Common.OpenConnection();
            var trans = con.BeginTransaction();
            List<Models.Entity.Common.users> users = new List<Models.Entity.Common.users>();
            try
            {
                users = trans.QueryTop<Models.Entity.Common.users>("select * from users", top);
                AjaxReturn(users);
                trans.Commit();
            }
            catch
            {
                trans.Rollback();
            }
            con.Close();
        }
    }
}