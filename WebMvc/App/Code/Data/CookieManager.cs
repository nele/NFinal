﻿﻿//======================================================================
//
//        Copyright : Zhengzhou Strawberry Computer Technology Co.,LTD.
//        All rights reserved
//        
//        Application:NFinal MVC framework
//        Filename :CookieManager.cs
//        Description :Cookie管理类
//
//        created by Lucas at  2015-6-30`
//     
//        WebSite:http://www.nfinal.com
//
//======================================================================
using System;
using System.Collections.Generic;
using System.Web;
namespace WebMvc.App.Code.Data
{
    public class CookieManager
    {
        private HttpContext _context;
        private CookieInfo _cookieInfo;
        private bool _isStatic;
        public CookieManager()
        {
            this._context = null;
            this._isStatic = true;
            _cookieInfo = new CookieInfo();
        }
        public CookieManager(HttpContext context)
        {
            this._context = context;
            if (context == null)
            {
                this._isStatic = true;
                _cookieInfo = new CookieInfo();
            }
            else
            {
                this._isStatic = false;
            }
        }
		public string vcheck
        {
            get
            {
                if (!_isStatic && _context.Request.Cookies["vcheck"] != null)
                {
                    _cookieInfo.vcheck=_context.Request.Cookies["vcheck"].Value;
                }
                return _cookieInfo.vcheck;
            }
            set
            {
                _cookieInfo.vcheck = value;
                if (_context.Response.Cookies["vcheck"] == null)
                {
                    HttpCookie cookie = new HttpCookie("vcheck", value.ToString());
                    _context.Response.Cookies.Add(cookie);
                }
                else
                {
                    _context.Response.Cookies["vcheck"].Value = value.ToString();
                }
            }
        }
		public string session_id
        {
            get
            {
                if (!_isStatic && _context.Request.Cookies["session_id"] != null)
                {
                    _cookieInfo.session_id = _context.Request.Cookies["session_id"].Value;
                }
                return _cookieInfo.session_id;
            }
            set
            {
                _cookieInfo.session_id = value;
                if (_context.Response.Cookies["session_id"] == null)
                {
                    HttpCookie cookie = new HttpCookie("session_id", value);
                    _context.Response.Cookies.Add(cookie);
                }
                else
                {
                    _context.Response.Cookies["session_id"].Value = value;
                }
            }
        }
        public string name
        {
            get
            {
                if (!_isStatic && _context.Request.Cookies["App_name"] != null)
                {
						_cookieInfo.name=_context.Request.Cookies["App_name"].Value;
				}
				return _cookieInfo.name;
            }
            set
            {
                _cookieInfo.name = value;
                if (_context.Response.Cookies["App_name"] == null)
                {
						HttpCookie cookie = new HttpCookie("App_name", value);
					_context.Response.Cookies.Add(cookie);
                }
                else
                {
						_context.Response.Cookies["App_name"].Value = value;
                }
            }
        }
        public bool islogin
        {
            get
            {
                if (!_isStatic && _context.Request.Cookies["App_islogin"] != null)
                {
						bool.TryParse(_context.Request.Cookies["App_islogin"].Value,out _cookieInfo.islogin);
				}
				return _cookieInfo.islogin;
            }
            set
            {
                _cookieInfo.islogin = value;
                if (_context.Response.Cookies["App_islogin"] == null)
                {
						HttpCookie cookie = new HttpCookie("App_islogin", value.ToString());
					_context.Response.Cookies.Add(cookie);
                }
                else
                {
						_context.Response.Cookies["App_islogin"].Value = value.ToString();
                }
            }
        }
        public int age
        {
            get
            {
                if (!_isStatic && _context.Request.Cookies["App_age"] != null)
                {
						int.TryParse(_context.Request.Cookies["App_age"].Value,out _cookieInfo.age);
				}
				return _cookieInfo.age;
            }
            set
            {
                _cookieInfo.age = value;
                if (_context.Response.Cookies["App_age"] == null)
                {
						HttpCookie cookie = new HttpCookie("App_age", value.ToString());
					_context.Response.Cookies.Add(cookie);
                }
                else
                {
						_context.Response.Cookies["App_age"].Value = value.ToString();
                }
            }
        }
    }
}