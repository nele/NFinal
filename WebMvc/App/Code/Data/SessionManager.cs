﻿﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
namespace WebMvc.App.Code.Data
{
    public class SessionManager
    {
        private HttpContext _context;
        private SessionInfo _sessionInfo;
        private bool _isStatic;
        public SessionManager()
        {
            this._context = null;
            this._isStatic = true;
            _sessionInfo = new SessionInfo();
        }
        public SessionManager(HttpContext context)
        {
            this._context = context;
            if (context == null)
            {
                this._isStatic = true;
                _sessionInfo = new SessionInfo();
            }
            else
            {
                this._isStatic = false;
            }
        }
        public int userId
        {
            get {
                if (!_isStatic && _context.Session["App_userId"] != null)
                {
						_sessionInfo.userId=(int)_context.Session["App_userId"];
                }
                return _sessionInfo.userId;
            }
            set {
                _sessionInfo.userId = value;
                if(!_isStatic)
                {
                    _context.Session["App_userId"] = _sessionInfo.userId;
                }
            }
        }
    }
}