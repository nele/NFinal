﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Navigator.ascx.cs" Inherits="WebMvc.App.Views.Default.Common.Public.Navigator" db="page" %>
当前页:<%=ViewBag.page.index %> / <%=ViewBag.page.count%>
<a href="<%=ViewBag.page.GetUrlFunction(1); %>">首页</a>
<a href="<%=ViewBag.page.index <= 1 ? "#" : ViewBag.page.GetUrlFunction(ViewBag.page.index - 1) %>">上一页</a>
<for start="<%int i = ((ViewBag.page.index - 1) / ViewBag.page.navigatorSize) * ViewBag.page.navigatorSize + 1;%>" condition="<%=i <= ViewBag.page.count && i <= ((ViewBag.page.index - 1) / ViewBag.page.navigatorSize + 1) * ViewBag.page.navigatorSize;%>" step="<%i++; %>">
    <if condition="<%=i == ViewBag.page.index%>">
    <a style="color:red;" href="<%=ViewBag.page.GetUrlFunction(i); %>"><%=i %></a>
    <else />
    <a href="<%=ViewBag.page.GetUrlFunction(i); %>"><%=i %></a>
    </if>
</for>
<a href="<%=ViewBag.page.index >= ViewBag.page.count ? "#" : ViewBag.page.GetUrlFunction(ViewBag.page.index + 1) %>">下一页</a>
<a href="<%=ViewBag.page.GetUrlFunction(ViewBag.page.count); %>">末页</a>