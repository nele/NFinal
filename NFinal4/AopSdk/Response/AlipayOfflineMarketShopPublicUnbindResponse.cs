using System;
using System.Xml.Serialization;

namespace Aop.Api.Response
{
    /// <summary>
    /// AlipayOfflineMarketShopPublicUnbindResponse.
    /// </summary>
    public class AlipayOfflineMarketShopPublicUnbindResponse : AopResponse
    {
    }
}
