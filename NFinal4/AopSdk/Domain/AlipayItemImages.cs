using System;
using System.Xml.Serialization;
using System.Collections.Generic;

namespace Aop.Api.Domain
{
    /// <summary>
    /// AlipayItemImages Data Structure.
    /// </summary>
    [Serializable]
    public class AlipayItemImages : AopObject
    {
        /// <summary>
        /// 详情图片，支付宝图片id
        /// </summary>
        [XmlArray("detail_images")]
        [XmlArrayItem("string")]
        public List<string> DetailImages { get; set; }

        /// <summary>
        /// 支付宝图片id
        /// </summary>
        [XmlElement("top_image")]
        public string TopImage { get; set; }
    }
}
