﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using Aliyun.OpenServices.OpenStorageService;

namespace NFinal.Common.CloudStorage
{
    public class AliStorage:StorageInterface
    {
        private StorageInfo info;
        private OssClient ossclient;

        public bool GetStoreInfo(StorageInfo info)
        {
            this.info = info;
            this.ossclient = new OssClient(info.ak, info.sk);
            IEnumerable<Bucket> bucklist = ossclient.ListBuckets();
            if (bucklist == null)
            {
                return false;
            }
            return true;
        }

        /// <summary>
        /// 创建bucket 
        /// </summary>
        public bool CreatePartition()
        {
            try
            {
                OssClient ossclient = new OssClient(info.ak, info.sk);
                IEnumerable<Bucket> partitionList = ossclient.ListBuckets();
                Bucket createRes;
                foreach (object bucket in partitionList)
                {
                    Bucket buck = (Bucket)bucket;

                    if (!buck.Name.Equals(info.bucket))
                        continue;
                    else
                        return true;
                }
                createRes = ossclient.CreateBucket(info.bucket);
                ossclient.SetBucketAcl(info.bucket, CannedAccessControlList.PublicRead);
                if (createRes != null)
                    return true;
                else
                    return false; // 分区建立失败，需要继续建立

            }
            catch (Exception) {

                return false;
            }
        
        }



        /// <summary>
        /// 上传文件 
        /// </summary>
        /// <param name="fileName">文件在阿里云上的名字</param>
        /// <param name="localFileName">本地要上传的文件地址</param>
        /// <returns></returns>
        public string UploadFile(string fileName, string localFileName)
        {
            
            PutObjectResult putObjectRes;
            OssClient ossclient = new OssClient(info.ak, info.sk);
            string resUrl;
            if (CreatePartition())
            {
                FileStream fs = new FileStream(localFileName, FileMode.Open, FileAccess.Read, FileShare.Read);
                ObjectMetadata omd = new ObjectMetadata();
                putObjectRes = ossclient.PutObject(info.bucket, fileName, fs, omd);

                if (putObjectRes != null)
                {
                    resUrl = string.Format("http://{0}.oss-cn-hangzhou.aliyuncs.com/{1}",info.bucket,fileName);
                    return resUrl;
                }
                else
                    return "上传文件失败";

            }
            else
            {
                return "创建分区失败";
            }

        }

        /// <summary>
        /// 删除阿里云存储上面的文件
        /// </summary>
        /// <param name="fileName">要删除的文件名字 http://detmftest123.oss-cn-hangzhou.aliyuncs.com/test.jpg </param>
        /// <returns></returns>
        public bool DeleteFile(string fileName)
        {
            string deleteName = null;
            OssClient ossclient = new OssClient(info.ak, info.sk);
            if (fileName.IndexOf("aliyuncs") != -1)
            {
                string[] name = fileName.Split('/');

                for (int i = 0; i < name.Length; i++)
                {
                    if (i < 3)
                    {
                        continue;
                    }
                    else
                    {
                        if (i == name.Length - 1)
                        {
                            deleteName += name[i];
                        }
                        else
                        {
                            deleteName += name[i] + "/";
                        }

                    }
                }
                fileName = deleteName;
            }
            try
            {
                ossclient.DeleteObject(info.bucket, fileName);
                return true;
            }
            catch(Exception){

                return false;
            }

        }
        
    }
}
