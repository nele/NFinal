﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net;

namespace NFinal.Common.CloudStorage
{
    public class BaiduStorage:StorageInterface
    {
        private StorageInfo info;
        private CloudStorage.BaiduCloud.BaiduCloudBase bcb;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="userPassword"></param>
        /// <param name="userAK"></param>
        /// <param name="userSK"></param>
        /// <param name="wxAccountId"></param>
        public bool GetStoreInfo(StorageInfo info)
        {
            this.info =info;
            this.bcb = new CloudStorage.BaiduCloud.BaiduCloudBase(info.ak, info.sk);
            List<BaiduCloud.JsonPartition> partitionList = bcb.GetPartitions();
            bool hasBucket = false;
            if (partitionList != null)
            {
                foreach (var partition in partitionList)
                {
                    if (partition.PartitionName == info.bucket)
                    {
                        hasBucket = true;
                    }
                }
            }
            if (!hasBucket)
            {
                return bcb.CreatePartition(info.name, BaiduCloud.PartitionRight.PublicRead);
            }
            else
            {
                return true;
            }

        }


        /// <summary>
        /// 创建新分区
        /// </summary>
        /// <returns></returns>
        public bool CreatePartition()
        {
            List<BaiduCloud.JsonPartition> partitionList = bcb.GetPartitions();
            bool hasBucket = false;
            if (partitionList != null)
            {
                foreach (var partition in partitionList)
                {
                    if (partition.PartitionName == info.bucket)
                    {
                        hasBucket = true;
                    }
                }
            }
            if (!hasBucket)
            {
                return bcb.CreatePartition(info.name, BaiduCloud.PartitionRight.PublicRead);
            }
            else
            {
                return true;
            }
        }

        /// <summary>
        /// 上传文件 
        /// </summary>
        /// <param name="fileName">文件在百度空间的名字</param>
        /// <param name="localFileName">本地要上传的文件地址</param>
        /// <returns></returns>
        public string UploadFile(string fileName, string localFileName)
        {
            string UploadRes;
            string fileExist;
            if (CreatePartition())
            {
                fileExist = bcb.GetFileMd5(info.bucket, fileName);
                if (fileExist != string.Empty)
                {
                    return "文件已经存在";
                }
                UploadRes = bcb.UploadFile(info.bucket, fileName, localFileName);

                if (UploadRes.Length > 1)
                {

                    return UploadRes;
                }
                else
                    return "上传文件失败";

            }
            else
            {
                return "创建分区失败";
            }
           
        }



        /// <summary>
        /// 删除百度云存储上面的文件
        /// </summary>
        /// <param name="fileName">要删除的文件名字 http://bcs.duapp.com/weixiaobao/2218/image/20140325112415_2303.jpg </param>
        /// <returns></returns>
        public bool DeleteFile(string fileName) 
        {
            string deleteName = null;
            if (fileName.IndexOf("bcs.duapp") != -1)
            {
                string[] name = fileName.Split('/');
                for (int i = 0; i < name.Length; i++)
                {
                    if (i < 4)
                    {
                        continue;
                    }
                    else
                    {
                        if (i == name.Length - 1)
                        {
                            deleteName += name[i];
                        }
                        else
                        {
                            deleteName += name[i] + "/";
                        }

                    }
                }
                fileName = deleteName;
            }
 
            return bcb.DeleteFile(info.bucket, fileName);
          
      
        }



        public static string GetString(int length)
        {
            string[] source = { "0", "1", "1", "3", "4", "5", "6", "7", "8", "9", "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z" };
            string checkCode = String.Empty;
            Random random = new Random();
            for (int i = 0; i < length; i++)
            {
                checkCode += source[random.Next(0, source.Length)];
            }
            return checkCode;
        }
        
    }


}
