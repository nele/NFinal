﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NFinal
{
    public static class Cache
    {
        public static ServiceStack.Redis.PooledRedisClientManager clientManager = null;
        public static ServiceStack.Redis.IRedisClient GetRedisClient()
        {
            return clientManager.GetClient();
        }
    }
}
