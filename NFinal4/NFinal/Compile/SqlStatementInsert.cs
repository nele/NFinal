﻿//======================================================================
//
//        Copyright : Zhengzhou Strawberry Computer Technology Co.,LTD.
//        All rights reserved
//
//        Application:NFinal MVC framework
//        Filename :InsertStatement.cs
//        Description :插入声明类(匹配插入语句)
//
//        created by Lucas at  2015-6-30`
//     
//        WebSite:http://www.nfinal.com
//
//======================================================================
using System;
using System.Collections.Generic;
using System.Web;
using System.Text;
using System.Text.RegularExpressions;

namespace NFinal.Compile
{
    public class SqlStatementInsert : SqlStatement
    {
        //INSERT INTO 表名称 VALUES (值1, 值2,....)
        public static string sqlReg = @"insert\s+into\s+([^,\(\)\s]+)(?:\s*\(\s*((?:[^,\s\(\)]+)(?:\s*,\s*[^,\s\(\)]+)*)\s*\))?\s*values\s*\(\s*((?:[^,\s\(\)]+)(?:\s*,\s*[^,\s\(\)]+)*)\s*\)";
        public string varNameReg = @"@([^,\s]+)";
        public SqlStatementInsert(string sql, NFinal.DB.DBType dbType)
            : base(sql, dbType)
        { 
            
        }
        public static new string FormatSql(string sql)
        {
            Regex reg = new Regex(sqlReg, RegexOptions.IgnoreCase);
            Match mat = reg.Match(sql);
            if (mat.Success)
            {
                int indexPos = 0;
                string columns=mat.Groups[2].Value;
                if (columns.IndexOf('.') > -1)
                {
                    columns = string.Empty;
                    string[] varNames = mat.Groups[2].Value.Split(',');
                    for (int i = 0; i < varNames.Length; i++)
                    {
                        indexPos = varNames[i].LastIndexOf('.');
                        if (i != 0)
                        {
                            columns += ",";
                        }
                        if (indexPos > -1)
                        {
                            columns += varNames[i].Substring(indexPos + 1);
                        }
                        else
                        {
                            columns += varNames[i];
                        }
                    }
                    sql= sql.Remove(mat.Groups[2].Index, mat.Groups[2].Length);
                    sql = sql.Insert(mat.Groups[2].Index,columns);
                }
            }
            return sql;
        }
        public void Parse()
        {
            Regex reg=new Regex(sqlReg,RegexOptions.IgnoreCase);
            Match mat = reg.Match(sqlInfo.sql);
            SqlTable table =null;
            if (mat.Success)
            {
                table = GetTable(mat.Groups[1].Value);
                this.sqlInfo.Tables.Add(table);

                this.sqlInfo.ColumnsSql = mat.Groups[2].Value;
                this.sqlInfo.Columns= ParseColumn(mat.Groups[2].Value);
                string[] varNames = mat.Groups[3].Value.Split(',');
                if (sqlInfo.Columns.Count == varNames.Length)
                {
                    SqlVarParameter sqlVarParameter = null;
                    Regex varReg = new Regex(varNameReg);
                    Match varMat = null;
                    for (int i = 0; i < sqlInfo.Columns.Count; i++)
                    {
                        varMat= varReg.Match(varNames[i]);
                        if (varMat.Success)
                        {
                            sqlVarParameter = new SqlVarParameter();
                            sqlVarParameter.sql=varNames[i];
                            sqlVarParameter.name= varMat.Groups[1].Value;
                            sqlVarParameter.columnName = sqlInfo.Columns[i].name;
                            sqlInfo.sqlVarParameters.Add(sqlVarParameter);
                        }
                    }
                }
            }
        }
    }
}