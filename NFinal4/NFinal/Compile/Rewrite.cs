﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NFinal.Compile
{
    public struct RewriteFile
    {
        public string from;
        public string to;
    }
    public struct RewriteDirectory
    {
        public string from;
        public string to;
    }
}
