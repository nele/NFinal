﻿//======================================================================
//
//        Copyright : Zhengzhou Strawberry Computer Technology Co.,LTD.
//        All rights reserved
//
//        Application:NFinal MVC framework
//        Filename :DeleteStatement.cs
//        Description :删除声明类(匹配删除语句)
//
//        created by Lucas at  2015-6-30`
//     
//        WebSite:http://www.nfinal.com
//
//======================================================================
using System;
using System.Collections.Generic;
using System.Web;
using System.Text.RegularExpressions;

namespace NFinal.Compile
{
    public class SqlStatementDelete : SqlStatement
    {
        public string deleteReg = @"delete\s+from\s+(\S+)";
        public SqlStatementDelete(string sql, NFinal.DB.DBType dbType)
            : base(sql, dbType)
        { 
            
        }
        public void Parse()
        {
            Regex reg = new Regex(deleteReg, RegexOptions.IgnoreCase);
            Match mat = reg.Match(this.sqlInfo.sql);
            if (mat.Success)
            {
                SqlTable tab = GetTable(mat.Groups[1].Value);
                this.sqlInfo.Tables.Add(tab);
                this.sqlInfo.sqlVarParameters = ParseVarName(sqlInfo.sql);
            }
        }
    }
}