﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NFinal.Compile
{
    public enum VerbMethod
    {
        Request = 0,
        Post = 2,
        Get = 1
    }
    /// <summary>
    /// 方法实体类
    /// </summary>
    public class ControllerMethodData
    {
        //方法头开始,及长度
        public int start = 0;
        public int length = 0;
        public string AttributeString = string.Empty;
        public System.Collections.Generic.List<ControllerAttributeData> Attributes;
        public VerbMethod verbMethod = 0;
        public string contentType = "text/html";
        public string url = string.Empty;
        public string formatUrl = string.Empty;
        public string formatParameters = string.Empty;
        public string actionUrl = string.Empty;
        public string parameterRegex = string.Empty;
        public System.Collections.Generic.List<RewriteFile> rewriteFileList = new System.Collections.Generic.List<RewriteFile>();
        public System.Collections.Generic.List<RewriteDirectory> rewriteDirectoryList = new System.Collections.Generic.List<RewriteDirectory>();
        public int optimizing = 0;
        public string seconds = "50";

        public string CommitString = string.Empty;
        //方法体开始,及内容
        public int position = 0;
        public string Content = string.Empty;
        public StringBuilder verifyCode = new StringBuilder();

        public string publicStr = string.Empty;
        public bool isPublic = false;
        public string returnType = string.Empty;
        public int returnTypeIndex = 0;
        public int returnTypeLength = 0;
        public string returnVarName = string.Empty;
        public string name = string.Empty;
        public bool hasParameters = false;
        public string parameters = string.Empty;
        public string parameterNames = string.Empty;
        public string parameterTypeAndNames = string.Empty;
        public string formatParameterNames = string.Empty;
        public string formatParameterTypeAndNames = string.Empty;
        public int parametersIndex = 0;
        public int parametersLength = 0;

        //数据库方法
        public System.Collections.Generic.List<DbFunctionData> dbFunctions = new System.Collections.Generic.List<DbFunctionData>();
        //View方法
        public System.Collections.Generic.List<ViewData> views = new System.Collections.Generic.List<ViewData>();
        //函数参数
        public System.Collections.Generic.List<ControllerParameterData> parameterDataList = new System.Collections.Generic.List<ControllerParameterData>();
    }
}
