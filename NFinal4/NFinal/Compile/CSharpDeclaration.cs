﻿//======================================================================
//
//        Copyright : Zhengzhou Strawberry Computer Technology Co.,LTD.
//        All rights reserved
//
//        Application:NFinal MVC framework
//        Filename :CSharpDeclaration.cs
//        Description :Csharp代码分析类
//
//        created by Lucas at  2015-6-30`
//     
//        WebSite:http://www.nfinal.com
//
//======================================================================
using System;
using System.Collections.Generic;
using System.Web;
using System.Text.RegularExpressions;

namespace NFinal.Compile
{
    public class CSharpDeclaration
    {
        //类型
        public string typeName;
        //是否是字符串类型
        public bool isString;
        //编译器内部类型
        public string compileTypeName;
        //变量名
        public string varName;
        //表达式
        public string expression;
        //转换函数
        public string covertMethod;
    }
}