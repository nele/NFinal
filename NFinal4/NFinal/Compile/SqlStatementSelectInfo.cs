﻿//======================================================================
//
//        Copyright : Zhengzhou Strawberry Computer Technology Co.,LTD.
//        All rights reserved
//
//        Application:NFinal MVC framework
//        Filename :SelectStatementInfo.cs
//        Description :select语句解析结果
//
//        created by Lucas at  2015-6-30`
//     
//        WebSite:http://www.nfinal.com
//
//======================================================================
using System;
using System.Collections.Generic;
using System.Web;

namespace NFinal.Compile
{
    public class SqlStatementSelectInfo
    {
        public string sql;
        public string selectClause;
        public string fromClause;
        public string whereClause;
        public string otherClause;
        public string sqlWithOutSubSelect;
        public System.Collections.Generic.List<SqlStatementSelectInfo> selects;
        public SqlStatementSelectInfo(string sql)
        {
            this.sql = sql;
            this.sqlWithOutSubSelect = sql;
        }
    }
}