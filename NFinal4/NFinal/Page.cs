﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NFinal
{
    public delegate string GetUrlDelegate(int index);
    public class Page:NFinal.UserControl
    {
        /// <summary>
        /// 当前页码
        /// </summary>
        public int index;
        /// <summary>
        /// 总页数
        /// </summary>
        public int count;
        /// <summary>
        /// 每页多少行记录
        /// </summary>
        public int size;
        /// <summary>
        /// 总记录总
        /// </summary>
        public int recordCount;
        /// <summary>
        /// 控件最多显示页码标签数
        /// </summary>
        public int navigatorSize;
        public string prevFormatString = "上{0}页";
        public string nextFormatString = "上{0}页";
        public string firstString = "首页";
        public string lastString = "末页";
        public string currentSting = "当前页";
        public GetUrlDelegate GetUrlFunction;
        public Page(int index, int size)
        {
            this.index = index;
            this.size = size;
            this.navigatorSize = 5;
        }
    }
}
