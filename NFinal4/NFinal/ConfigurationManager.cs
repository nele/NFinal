﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace NFinal
{
    public class ConnectionStringSettings
    {
        public string Name;
        public string ConnectionString;
        public string ProviderName;
        public ConnectionStringSettings()
        { }
        public ConnectionStringSettings(string name, string connectionString)
        {
            this.Name = name;
        }
        public ConnectionStringSettings(string name, string connectionString, string providerName)
        {
            this.Name = name;
            this.ConnectionString = connectionString;
            this.ProviderName = providerName;
        }
    }
    public class ConnectionStringSettingsCollection : Dictionary<string,NFinal.ConnectionStringSettings>
    {
        public ConnectionStringSettingsCollection()
        {

        }
        public void Add(ConnectionStringSettings settings)
        {
            this.Add(settings.Name, settings);
        }
    }
    public class ConfigurationManager
    {
        public static NFinal.ConnectionStringSettingsCollection ConnectionStrings = null;
        public static Config GetConfig(string appName)
        {
            string configFileName = Frame.MapPath(appName + "/config.json");
            return GetConfig(appName, configFileName);
        }
        public static void Load(string appRoot)
        {
            if (ConnectionStrings == null)
            {
                string[] dirs = Directory.GetDirectories(appRoot);
                ConnectionStrings =new NFinal.ConnectionStringSettingsCollection();
                NFinal.ConnectionStringSettings setting = null;
                if (dirs.Length > 0)
                {
                    for (int i = 0; i < dirs.Length; i++)
                    {
                        string app = Path.GetFileName(dirs[i]);
                        string configFileName = Path.Combine(dirs[i], "config.json");
                        if (File.Exists(configFileName))
                        {
                            Config config = GetConfig(app, configFileName);
                            for (int j = 0; j < config.connectionStrings.Count; j++)
                            {
                                DB.ConnectionString conStr = config.connectionStrings[j];

                                if (!ConnectionStrings.ContainsKey(conStr.name))
                                {
                                    setting = new NFinal.ConnectionStringSettings();
                                    setting.Name = conStr.name;
                                    setting.ConnectionString = conStr.value;
                                    setting.ProviderName = conStr.provider;
                                    ConnectionStrings.Add(setting);
                                }
                            }

                        }
                    }
                }
            }
        }
        //配置初始化
        public static Config GetConfig(string appName, string configFileName)
        {
            Config config = new Config();
            if (File.Exists(configFileName))
            {
                StreamReader sr = new StreamReader(configFileName, System.Text.Encoding.UTF8);
                string json = sr.ReadToEnd();
                sr.Dispose();
                LitJson.JsonData configData = LitJson.JsonMapper.ToObject(json);
                config.defaultStyle = configData["defaultStyle"].ToString();
                int.TryParse(configData["urlMode"].ToString(), out config.UrlMode);
                config.connectionStrings = new System.Collections.Generic.List<DB.ConnectionString>();
                LitJson.JsonData connectionStringArray = configData["connectionStrings"];
                bool.TryParse(configData["compressHTML"].ToString(), out config.CompressHTML);
                config.cookiePrefix = configData["cookiePrefix"].ToString();
                config.sessionPrefix = configData["sessionPrefix"].ToString();
                config.urlPrefix = configData["urlPrefix"].ToString();
                config.urlExtension = configData["urlExtension"].ToString();
                NFinal.DB.ConnectionString conStr = null;
                for (int i = 0; i < connectionStringArray.Count; i++)
                {
                    conStr = new DB.ConnectionString();
                    conStr.name = connectionStringArray[i]["name"].ToString();
                    conStr.provider = connectionStringArray[i]["providerName"].ToString();
                    conStr.value = connectionStringArray[i]["connectionString"].ToString();
                    config.connectionStrings.Add(conStr);
                }
                LitJson.JsonData redisConfig = configData["redis"];
                bool.TryParse(redisConfig["autoStart"].ToString(),out config.redisConfigAutoStart);
                int.TryParse(redisConfig["maxReadPoolSize"].ToString(),out config.redisConfigMaxReadPoolSize);
                int.TryParse(redisConfig["maxWritePoolSize"].ToString(),out config.redisConfigMaxWritePoolSize);
                LitJson.JsonData readWriteHosts = redisConfig["readWriteHosts"];
                if (readWriteHosts == null || readWriteHosts.Count < 1)
                {
                    config.redisReadWriteHosts = null;
                }
                else
                {
                    config.redisReadWriteHosts = new string[readWriteHosts.Count];
                }
                for (int i = 0; i < readWriteHosts.Count; i++)
                {
                    config.redisReadWriteHosts[i] = readWriteHosts[i].ToString();
                }
                LitJson.JsonData readOnlyHosts = redisConfig["readOnlyHosts"];
                if (readOnlyHosts == null || readOnlyHosts.Count < 1)
                {
                    config.redisReadOnlyHosts = null;
                }
                else
                {
                    config.redisReadOnlyHosts = new string[readWriteHosts.Count];
                }
                for (int i = 0; i < readOnlyHosts.Count; i++)
                {
                    config.redisReadOnlyHosts[i] = readOnlyHosts[i].ToString();
                }
                if (NFinal.Cache.clientManager == null)
                {
                    ServiceStack.Redis.RedisClientManagerConfig redisClientManagerConfig = new ServiceStack.Redis.RedisClientManagerConfig();
                    redisClientManagerConfig.AutoStart = config.redisConfigAutoStart;
                    redisClientManagerConfig.MaxReadPoolSize = config.redisConfigMaxReadPoolSize;
                    redisClientManagerConfig.MaxWritePoolSize = config.redisConfigMaxWritePoolSize;
                    NFinal.Cache.clientManager = new ServiceStack.Redis.PooledRedisClientManager(config.redisReadWriteHosts, config.redisReadOnlyHosts, redisClientManagerConfig);
                }
            }
            
            if (string.IsNullOrEmpty(appName))
            {
                config.APP = "/";
            }
            else
            {
                config.APP = "/" + appName.Trim(new char[] { '/' }) + "/";
            }
            config.Controller = config.APP + config.Controller;
            config.Code = config.APP + config.Code;
            config.Views = config.APP + config.Views;
            config.Models = config.APP + config.Models;
            config.BLL = config.Models + config.BLL;
            config.DAL = config.Models + config.DAL;
            config.Web = config.APP + config.Web;
            config.Content = config.APP + config.Content;
            config.ContentCss = config.Content + config.ContentCss;
            config.ContentJS = config.Content + config.ContentJS;
            config.ContentImages = config.Content + config.ContentImages;
            return config;
        }
    }
}
