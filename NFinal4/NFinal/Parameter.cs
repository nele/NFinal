﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NFinal
{
    public interface Parameter
    {
        bool TryParse(string val, out Parameter par);
    }
}
