﻿//======================================================================
//
//        Copyright : Zhengzhou Strawberry Computer Technology Co.,LTD.
//        All rights reserved
//        
//        Application:NFinal MVC framework
//        Filename :Config.cs
//        Description :陪置
//
//        created by Lucas at  2015-6-30`
//     
//        WebSite:http://www.nfinal.com
//
//======================================================================
using System;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using System.Collections.Specialized;

namespace NFinal
{
    //配置文件
    public class Config
    {
        public string APP = "/";
        public string Controller = "Controllers/";
        public string Code = "Code/";
        public string Views = "Views/";
        public string Models = "Models/";
        public string BLL = "BLL/";
        public string DAL = "DAL/";
        public string Web = "Web/";
        public string Content = "Content/";
        public string ContentJS = "js/";
        public string ContentCss = "css/";
        public string ContentImages = "images/";

        public string urlPrefix = "";
        public string urlExtension = "";
        public string cookiePrefix = "";
        public string sessionPrefix = "";
        public string defaultStyle = "Default/";

        public bool redisConfigAutoStart = false;
        public int redisConfigMaxReadPoolSize = 60;
        public int redisConfigMaxWritePoolSize = 60;
        public string[] redisReadWriteHosts = new string[] { "127.0.0.1:6379"};
        public string[] redisReadOnlyHosts = new string[] { "127.0.0.1:6379"};

        public int UrlMode = 1;
        public System.Collections.Generic.List<NFinal.DB.ConnectionString> connectionStrings =null;
        public bool CompressHTML = false;
        //控制器的后缀名
        public string ControllerSuffix = "Controller";

        public Config()
        { }
        
        //获取命名空间
        public string GetFullName(string projectName,string name)
        {
            name = projectName + name;
            return name.Trim('/').Replace('/','.');
        }
        public string ChangeControllerName(string projectName, string controllerFullName)
        {
            string controllerName = Frame.AssemblyTitle + Controller;
            return projectName + controllerFullName.Substring(controllerName.Length,
                controllerFullName.Length - controllerName.Length);
        }
        public string ChangeBLLName(string projectName, string BLLFullName)
        {
            string BLLName = (Frame.AssemblyTitle + BLL).TrimEnd('/');
            return projectName + BLLFullName.Substring(BLLName.Length,BLLFullName.Length-BLLName.Length);
        }
        public static string MapPath(string url)
        {
            return Frame.MapPath(url);
        }
       
    }
}