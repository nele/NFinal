﻿//======================================================================
//
//        Copyright : Zhengzhou Strawberry Computer Technology Co.,LTD.
//        All rights reserved
//        
//        Application:NFinal MVC framework
//        Filename :List.cs
//        Description :数据库表对象类
//
//        created by Lucas at  2015-6-30`
//     
//        WebSite:http://www.nfinal.com
//
//======================================================================
using System;
using System.Collections.Generic;
using System.Web;

namespace NFinal
{
    public class List<T> : System.Collections.Generic.List<T>
    {
        public static bool operator true(List<T> list)
        {
            if (list != null && list.Count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public static bool operator false(List<T> list)
        {
            if (list != null && list.Count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public void WriteJson(System.IO.TextWriter tw,bool addBracket=true)
        {
            if (this == null)
            {
                tw.Write("null");
            }
            else
            {
                if (addBracket)
                {
                    tw.Write("[");
                }
                bool isFirst = true;
                foreach (NFinal.JsonInterface str in this)
                {
                    if (!isFirst)
                    {
                        tw.Write(",");
                    }
                    if (isFirst)
                    {
                        isFirst = false;
                    }
                    str.WriteJson(tw);
                }
                if (addBracket)
                {
                    tw.Write("]");
                }
            }
        }
        public string ToJson(bool addBracket = true)
        {
            if (this)
            {
                System.IO.StringWriter tw = new System.IO.StringWriter();
                this.WriteJson(tw,addBracket);
                string result = tw.ToString();
                tw.Close();
                return result;
            }
            else
            {
                return "null";
            }
        }
    }

}