﻿#region	var ${varName}; 选取一行
			${if(hasGenericType)}
			${if(isDeclaration)}
			${type} ${varName}=new ${type}();
			${end}
			${else}
			var ${varName} = new __${functionName}_${varName}__();
            ${end}
			var __${functionName}_${varName}_command__ = new System.Data.SQLite.SQLiteCommand("${sql}", ${connectionVarName});
			${if(isTransaction)}
			__${functionName}_${varName}_command__.Transaction=${transactionVarName};
			${end}
			${if(sqlVarParameters.Count>0)}
			var __${functionName}_${varName}_parameters__=new System.Data.SQLite.SQLiteParameter[${sqlVarParameters.Count}];
			${end}
			${set(i=0)}
			${foreach(sqlVarParameter in sqlVarParameters)}
			${if(sqlVarParameter.field.length>0)}
			__${functionName}_${varName}_parameters__[${i}] = new System.Data.SQLite.SQLiteParameter("@${sqlVarParameter.name}",System.Data.DbType.${sqlVarParameter.field.dbType},${sqlVarParameter.field.length});
			__${functionName}_${varName}_parameters__[${i}].Value = ${sqlVarParameter.name};
			${else}
			__${functionName}_${varName}_parameters__[${i}] = new System.Data.SQLite.SQLiteParameter("@${sqlVarParameter.name}",System.Data.DbType.${sqlVarParameter.field.dbType});
			__${functionName}_${varName}_parameters__[${i}].Value = ${sqlVarParameter.name};
			${end}
			${set(i=i+1)}
			${end}
			${if(sqlVarParameters.Count>0)}
			__${functionName}_${varName}_command__.Parameters.AddRange(__${functionName}_${varName}_parameters__);
			${end}
			var __${functionName}_${varName}_reader__= __${functionName}_${varName}_command__.ExecuteReader();
			if (__${functionName}_${varName}_reader__.HasRows)
			{
				__${functionName}_${varName}_reader__.Read();
				${if(hasGenericType)}
				${varName} = new ${type}();
				${else}
				${varName} = new __${functionName}_${varName}__();
				${end}
				${set(i=0)}
				${foreach(field in fields)}
				${if(field.allowNull)}
					${if(field.isValueType)}
					if(!__${functionName}_${varName}_reader__.IsDBNull(${i})){${varName}.${field.structFieldName} =${field.GetMethodConvert} __${functionName}_${varName}_reader__.${field.GetMethodName}(${i})${field.GetMethodValue};
					${else}
					${varName}.${field.structFieldName} =__${functionName}_${varName}_reader__.IsDBNull(${i})?null:${field.GetMethodConvert} __${functionName}_${varName}_reader__.${field.GetMethodName}(${i})${field.GetMethodValue};
					${end}
					${else}
					${varName}.${field.structFieldName} =${field.GetMethodConvert} __${functionName}_${varName}_reader__.${field.GetMethodName}(${i})${field.GetMethodValue};
					${end}
				${set(i=i+1)}
				${end}
			}
			else
			{
				${varName} = null;
			}
			__${functionName}_${varName}_reader__.Dispose();
			__${functionName}_${varName}_command__.Dispose();
			#endregion
			