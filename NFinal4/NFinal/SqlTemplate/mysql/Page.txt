﻿#region	var ${varName};分页
			var __${functionName}_${varName}_command__ = new MySql.Data.MySqlClient.MySqlCommand("${countSql}", ${connectionVarName});
			${if(isTransaction)}
			__${functionName}_${varName}_command__.Transaction=${transactionVarName};
			${end}
			${if(sqlVarParameters.Count>0)}
			var __${functionName}_${varName}_parameters__=new MySql.Data.MySqlClient.MySqlParameter[${sqlVarParameters.Count}];
			${end}
			${set(i=0)}
			${foreach(sqlVarParameter in sqlVarParameters)}
			${if(sqlVarParameter.field.length>0)}
			__${functionName}_${varName}_parameters__[${i}] = new MySql.Data.MySqlClient.MySqlParameter("?${sqlVarParameter.name}",MySql.Data.MySqlClient.MySqlDbType.${sqlVarParameter.field.dbType},${sqlVarParameter.field.length});
			__${functionName}_${varName}_parameters__[${i}].Value = ${sqlVarParameter.name};
			${else}
			__${functionName}_${varName}_parameters__[${i}] = new MySql.Data.MySqlClient.MySqlParameter("?${sqlVarParameter.name}",MySql.Data.MySqlClient.MySqlDbType.${sqlVarParameter.field.dbType});
			__${functionName}_${varName}_parameters__[${i}].Value = ${sqlVarParameter.name};
			${end}
			${set(i=i+1)}
			${end}
			${if(sqlVarParameters.Count>0)}
			__${functionName}_${varName}_command__.Parameters.AddRange(__${functionName}_${varName}_parameters__);
			${end}
			${pageVarName}.recordCount =System.Convert.ToInt32(__${functionName}_${varName}_command__.ExecuteScalar());
			__${functionName}_${varName}_command__.Dispose();
			${pageVarName}.count = (${pageVarName}.recordCount % ${pageVarName}.size==0)? ${pageVarName}.recordCount/${pageVarName}.size:${pageVarName}.recordCount/${pageVarName}.size+1;
			//传页码时用的变量名
			if (${pageVarName}.index > ${pageVarName}.count)
            {
                ${pageVarName}.index = ${pageVarName}.count;
            }
            if (${pageVarName}.index < 1)
            {
                ${pageVarName}.index = 1;
            }
			${if(hasGenericType)}
			${if(isDeclaration)}
			NFinal.List<${type}> ${varName}=new NFinal.List<${type}>();
			${end}
			${else}
			var ${varName} = new NFinal.List<__${functionName}_${varName}__>();
            ${end}
            //计算得到SQL语句
			__${functionName}_${varName}_command__ = new MySql.Data.MySqlClient.MySqlCommand(string.Format("${pageSql}",${pageVarName}.size,(${pageVarName}.index-1)*${pageVarName}.size), ${connectionVarName});
			${if(isTransaction)}
			__${functionName}_${varName}_command__.Transaction=${transactionVarName};
			${end}
			${if(sqlVarParameters.Count>0)}
			__${functionName}_${varName}_command__.Parameters.AddRange(__${functionName}_${varName}_parameters__);
			${end}
			var __${functionName}_${varName}_reader__= __${functionName}_${varName}_command__.ExecuteReader();
			if (__${functionName}_${varName}_reader__.HasRows)
			{
				while (__${functionName}_${varName}_reader__.Read())
				{
					${if(hasGenericType)}
					var __${functionName}_${varName}_temp__ = new ${type}();
					${else}
					var __${functionName}_${varName}_temp__ = new __${functionName}_${varName}__();
					${end}
					${set(i=0)}
					${foreach(field in fields)}
					${if(field.allowNull)}
					${if(field.isValueType)}
					if(!__${functionName}_${varName}_reader__.IsDBNull(${i})){__${functionName}_${varName}_temp__.${field.structFieldName} =${field.GetMethodConvert} __${functionName}_${varName}_reader__.${field.GetMethodName}(${i})${field.GetMethodValue};
					${else}
					__${functionName}_${varName}_temp__.${field.structFieldName} =__${functionName}_${varName}_reader__.IsDBNull(${i})?null:${field.GetMethodConvert} __${functionName}_${varName}_reader__.${field.GetMethodName}(${i})${field.GetMethodValue};
					${end}
					${else}
					__${functionName}_${varName}_temp__.${field.structFieldName} =${field.GetMethodConvert} __${functionName}_${varName}_reader__.${field.GetMethodName}(${i})${field.GetMethodValue};
					${end}
					${set(i=i+1)}
					${end}
					${varName}.Add(__${functionName}_${varName}_temp__);
				}
			}
			__${functionName}_${varName}_reader__.Dispose();
			__${functionName}_${varName}_command__.Dispose();
			#endregion
			