﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NFinal
{
    public abstract class BaseModule:IHttpModule
    {
        public virtual void Init(HttpApplication app)
        {
            app.BeginRequest += new EventHandler(this.BaseModuleRewriter_BeginRequest);
        }
        public virtual void Dispose()
        { }
        protected virtual void BaseModuleRewriter_BeginRequest(object sender, EventArgs e)
        {
            HttpApplication app = (HttpApplication)sender;
            Rewrite(app.Request.Path, app);
        }
        protected abstract void Rewrite(string requestedPath, HttpApplication app);
    }
}