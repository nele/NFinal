﻿//======================================================================
//
//        Copyright : Zhengzhou Strawberry Computer Technology Co.,LTD.
//        All rights reserved
//        
//        Application:NFinal MVC framework
//        Filename :Transation.cs
//        Description :事务处理类
//
//        created by Lucas at  2015-6-30`
//     
//        WebSite:http://www.nfinal.com
//
//======================================================================
using System;
using System.Collections.Generic;
using System.Data;
using System.Web;

namespace NFinal
{
    /// <summary>
    /// SQL事务魔法类
    /// </summary>
    public class Transaction:NFinal.DB.DBBase,System.Data.IDbTransaction
    {
        public void Commit()
        { 
            
        }
        public void Rollback()
        { 
        
        }
        public void Dispose()
        {
        }

        public DBException dbExcetion;

        public IDbConnection Connection
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public IsolationLevel IsolationLevel
        {
            get
            {
                throw new NotImplementedException();
            }
        }
    }
    public class DBException : Exception
    { 
    
    }
}