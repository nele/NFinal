﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ${project}
{
    public class Url
    {
		private static string __ParseUrl__(string __url__)
        {
            switch (__url__)
            {
			${set(i=1)}
			${foreach(fileData in controllerFileDataList)}
			${set(fileDataClassDataList=fileData.ClassDataList)}
			${foreach(classData in fileDataClassDataList)}
			${set(classDataMethodDataList=classData.MethodDataList)}
			${foreach(methodData in classDataMethodDataList)}
			${if(methodData.isPublic)}
				${set(rewriteFileList=methodData.rewriteFileList)}
				${foreach(rewriteFile in rewriteFileList)}
				case "${rewriteFile.to}":__url__ = "${rewriteFile.from}"; break;
				${end}
			${set(i=i+1)}
			${end}
			${end}
			${end}
			${end}
				default: break;
            }
			${set(i=1)}
			${foreach(fileData in controllerFileDataList)}
			${set(fileDataClassDataList=fileData.ClassDataList)}
			${foreach(classData in fileDataClassDataList)}
			${set(classDataMethodDataList=classData.MethodDataList)}
			${foreach(methodData in classDataMethodDataList)}
			${if(methodData.isPublic)}
				${set(rewriteDirectoryList=methodData.rewriteDirectoryList)}
				${foreach(rewriteDirectory in rewriteDirectoryList)}
			if(__url__.StartsWith("${rewriteDirectory.to}"))
			{
				__url__ = "${rewriteDirectory.from}" + __url__.Remove(0,"${rewriteDirectory.to}".Length);
			}
				${end}
			${set(i=i+1)}
			${end}
			${end}
			${end}
			${end}
            return __url__;
        }
        ${set(i=1)}
		${foreach(fileData in controllerFileDataList)}
		${set(fileDataClassDataList=fileData.ClassDataList)}
		${foreach(classData in fileDataClassDataList)}
		${set(classDataMethodDataList=classData.MethodDataList)}
		${foreach(methodData in classDataMethodDataList)}
        ${if(methodData.isPublic)}
		public static string ${fileData.appName}_${classData.name}_${methodData.name}(${methodData.formatParameterTypeAndNames})
        {
            string __url__=string.Format("${methodData.formatUrl}"${methodData.formatParameters});
			__url__ = __ParseUrl__(__url__);
            return __url__;
        }
		${set(i=i+1)}
		${end}
		${end}
		${end}
		${end}
    }
}
