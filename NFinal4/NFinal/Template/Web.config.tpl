<?xml version="1.0" encoding="utf-8"?>
<!--
  有关如何配置 ASP.NET 应用程序的详细信息，请访问
  http://go.microsoft.com/fwlink/?LinkId=169433
  -->
<configuration>
  <!--禁用Browser Link功能-->
  <appSettings>
    <add key="vs:EnableBrowserLink" value="false" />
  </appSettings>
  <system.web>
    <httpRuntime enableVersionHeader="false" />
    <!--session重命名-->
    <sessionState cookieName="session_id" mode="InProc">
    </sessionState>
    <compilation debug="true" />
    <!--http handler重写-->
    <httpHandlers>
      <add verb="*" path="*.nf" type="NFinal.Handler" validate="false" />
    </httpHandlers>
    <!--http module重写-->
    <httpModules>
      <add name="NFinalMoudle" type="NFinal.Module" />
    </httpModules>
  </system.web>
  <system.webServer>
    <validation validateIntegratedModeConfiguration="false" />
    <!--http handler重写-->
    <handlers>
      <add name="NFinalHandler" verb="*" path="*.nf" type="NFinal.Handler" preCondition="integratedMode" />
    </handlers>
    <!--http module重写-->
    <modules>
      <add name="NFinalMoudle" type="NFinal.Module" preCondition="integratedMode" />
      <add name="NFinalModule" type="NFinal.Module" />
    </modules>
    <!--删除无用的http头-->
    <httpProtocol>
      <customHeaders>
        <remove name="X-Powered-By" />
      </customHeaders>
    </httpProtocol>
  </system.webServer>
</configuration>