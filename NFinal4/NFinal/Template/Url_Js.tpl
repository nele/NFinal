﻿function StringFormat() {
         if (arguments.length == 0)
             return null;
         var str = arguments[0];
         for (var i = 1; i < arguments.length; i++) {
             var re = new RegExp('\\{' + (i - 1) + '\\}', 'gm');
             str = str.replace(re, arguments[i]);
         }
         return str;
     }
String.prototype.endWith = function (s) {
    if (s == null || s == "" || this.length == 0 || s.length > this.length)
        return false;
    if (this.substring(this.length - s.length) == s)
        return true;
    else
        return false;
    return true;
}
String.prototype.startWith = function (s) {
    if (s == null || s == "" || this.length == 0 || s.length > this.length)
        return false;
    if (this.substr(0, s.length) == s)
        return true;
    else
        return false;
    return true;
}
function __ParseUrl__(__url__)
{
	this.next=function()
    {
	${set(i=1)}
	${foreach(fileData in controllerFileDataList)}
	${set(fileDataClassDataList=fileData.ClassDataList)}
	${foreach(classData in fileDataClassDataList)}
	${set(classDataMethodDataList=classData.MethodDataList)}
	${foreach(methodData in classDataMethodDataList)}
    ${if(methodData.isPublic)}
		${set(rewriteDirectoryList=methodData.rewriteDirectoryList)}
		${foreach(rewriteDirectory in rewriteDirectoryList)}
		if(__url__.startWith("${rewriteDirectory.to}"))
		{
			__url__ = "${rewriteDirectory.from}" + __url__.substring("${rewriteDirectory.to}".length);
		}
		${end}
	${set(i=i+1)}
	${end}
	${end}
	${end}
	${end}
		return __url__;
	}
	${set(i=1)}
	${foreach(fileData in controllerFileDataList)}
	${set(fileDataClassDataList=fileData.ClassDataList)}
	${foreach(classData in fileDataClassDataList)}
	${set(classDataMethodDataList=classData.MethodDataList)}
	${foreach(methodData in classDataMethodDataList)}
    ${if(methodData.isPublic)}
		${set(rewriteFileList=methodData.rewriteFileList)}
		${foreach(rewriteFile in rewriteFileList)}
		if(__url__=="${rewriteFile.to}")
		{
			__url__ = "${rewriteFile.from}"; return this.next();
		}
		${end}
	${set(i=i+1)}
	${end}
	${end}
	${end}
	${end}
	return __url__;
}
var Url=new function()
{
		${set(i=1)}
		${foreach(fileData in controllerFileDataList)}
		${set(fileDataClassDataList=fileData.ClassDataList)}
		${foreach(classData in fileDataClassDataList)}
		${set(classDataMethodDataList=classData.MethodDataList)}
		${foreach(methodData in classDataMethodDataList)}
        ${if(methodData.isPublic)}
		this.${fileData.appName}_${classData.name}_${methodData.name}= function(${methodData.formatParameterNames})
        {
            var __url__=StringFormat("${methodData.formatUrl}"${methodData.formatParameters});
			__url__ = __ParseUrl__(__url__);
            return __url__;
        };
		${set(i=i+1)}
		${end}
		${end}
		${end}
		${end}
}