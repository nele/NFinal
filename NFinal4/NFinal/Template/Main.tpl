﻿//======================================================================
//
//        Copyright : Zhengzhou Strawberry Computer Technology Co.,LTD.
//        All rights reserved
//        
//        Application:NFinal MVC framework
//        Filename :Main.cs
//        Description :入口类
//
//        created by Lucas at  2015-6-30`
//     
//        WebSite:http://www.nfinal.com
//
//======================================================================
using System;
using System.Collections.Generic;
using System.Web;
using System.Threading;
using System.Globalization;
using System.Collections.Specialized;
using System.Text.RegularExpressions;
#if NET2
namespace System.Runtime.CompilerServices
{
    [AttributeUsage(AttributeTargets.Method | AttributeTargets.Class | AttributeTargets.Assembly)]
    public sealed class ExtensionAttribute : Attribute { }
}
#endif
namespace ${project}
{
    public static class Main
    {
        public static string GetString(string val, string de)
        {
            return string.IsNullOrEmpty(val) ? de : val;
        }
        public static string UCFirst(string val)
        {
            CultureInfo cultureInfo = Thread.CurrentThread.CurrentCulture;
            return cultureInfo.TextInfo.ToTitleCase(val);
        }
		  //第一个是app/第二个是Controller/第三个是Folder/第四个是Controller_Short/第五个是方法名/第六个是参数
        private static Regex reg = new Regex(@"(/[^/\s]+)(((?:/[^/\s]+)*)/([^/\s]+)${ControllerSuffix})/([^/\s.]+)((?:/[^/\s.]+)*)",  RegexOptions.IgnoreCase);
        public static void Run(HttpContext context,string url)
        {
			string[] hostName=context.Request.Url.Host.ToString().Split('.');
            string subdomain=hostName.Length==3?hostName[0]:"www";
            string folder = "/Manage";
            string controller = "/Manage/Index";
            string app = "/App";
            string controller_short = "Index";
            string action = "Index";
            string[] parameters = null;
			System.Text.StringBuilder urlWithOutAppAndExt = new System.Text.StringBuilder();

            NameValueCollection get = new NameValueCollection();
            Match mat= reg.Match(url);
            if (mat.Success)
            {
                app =GetString(mat.Groups[1].Value,app);
                controller = GetString(mat.Groups[2].Value, folder);
                folder =mat.Groups[3].Value;
                controller_short =GetString(mat.Groups[4].Value,controller_short);
                action =GetString(mat.Groups[5].Value,action);
                if(mat.Groups[6].Success)
                {
                    parameters = mat.Groups[6].Value.Split('/');
                    //如果长度为奇数说明正确
                    if((parameters.Length&1)==1)
                    {
                        int count=parameters.Length>>1;
						if (count > 0)
                        {
                            for (int i = 0; i < count; i++)
                            {
                                get.Add(parameters[((i << 1)+1)], parameters[(i << 1) + 2]);
                            }
                        }
                    }
                }

                if (context.Request.Form.Count > 0)
                {
                    for (int i = 0; i < context.Request.Form.Count; i++)
                    {
                        get.Add(context.Request.Form.Keys[i], context.Request.Form[i]);
                    }
                }
				//获取没有APP和后缀的URL
                urlWithOutAppAndExt.Append(controller);
                urlWithOutAppAndExt.Append("/");
                urlWithOutAppAndExt.Append(action);
                if (get.Count > 0)
                {
                    for (int i = 0; i < get.Count; i++)
                    {
                        urlWithOutAppAndExt.Append("/");
                        urlWithOutAppAndExt.Append(get.AllKeys[i]);
                        urlWithOutAppAndExt.Append(get[i]);
                    }
                }
				switch (app)
				{
					$foreach(app in apps)
                    case "/${app}":
					{ 
                        ${project}.${app}.Router.Run(context,subdomain, app, urlWithOutAppAndExt.ToString(), controller, action,get); break;
					}
					$end
                    default: break;
                }
            }
            else
            {
                context.Response.Write("wrong url");
            }
        }
    }
}