﻿//======================================================================
//
//        Copyright : Zhengzhou Strawberry Computer Technology Co.,LTD.
//        All rights reserved
//        
//        Application:NFinal MVC framework
//        Filename :IndexController.cs
//        Description :首页控制器
//
//        created by Lucas at  2015-6-30`
//     
//        WebSite:http://www.nfinal.com
//
//======================================================================
using System;
using System.Collections.Generic;
using System.Web;

namespace ${project}.${app}.Controllers
{
    public class IndexController: Controller
    {
        public void Index()
        {
            View("Index.aspx");
        }
    }
}