﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ${project}.${app}.Models.Entity.${database}
{
    public class ${table.name}:NFinal.Struct
    {
		${foreach(field in table.fields)}
		${if(field.description!=null)}
		/// <summary>
        /// ${field.description}
        /// </summary>
		${end}
		${if(field.allowNull && field.isValueType)}
		public ${field.csharpType}? ${field.structFieldName} {get;set;}
		${else}
		public ${field.csharpType} ${field.structFieldName} {get;set;}
		${end}
		${end}
		#region 写Json字符串
		public override void WriteJson(System.IO.TextWriter tw)
		{
			tw.Write("{");
			${set(i=0)}
			${foreach(field in table.fields)}
				${if(i!=0)}
					tw.Write(",");
				${end}
				${if(field.jsonType=="Number")}
					tw.Write("\"${field.structFieldName}\":");
					${if(field.allowNull)}
					tw.Write(${field.structFieldName}==null?"null" : ${field.structFieldName}.ToString());
					${else}
					tw.Write(${field.structFieldName}.ToString());
					${end}
				${elseif(field.jsonType=="Bool")}
					tw.Write("\"${field.structFieldName}\":");
					${if(field.allowNull)}
					tw.Write(${field.structFieldName}==null?"null":((bool)${field.structFieldName}?"true":"false"));
					${else}
					tw.Write(${field.structFieldName}?"true":"false");
					${end}
				${elseif(field.jsonType=="Time")}
					tw.Write("\"${field.structFieldName}\":");
					tw.Write("\"");
					${if(field.allowNull)}
					tw.Write(${field.structFieldName}==null?"null":${field.structFieldName}.ToString());
					${else}
					tw.Write(${field.structFieldName}.ToString());
					${end}
					tw.Write("\"");
				${elseif(field.jsonType=="String")}
					tw.Write("\"${field.structFieldName}\":");
					tw.Write("\"");
					${if(field.allowNull)}
					tw.Write(${field.structFieldName}==null?"null":${field.structFieldName});
					${else}
					tw.Write(${field.structFieldName});
					${end}
					tw.Write("\"");
				${elseif(field.jsonType=="Object")}
					tw.Write("\"${field.structFieldName}\":");
					tw.Write("\"");
					${if(field.allowNull)}
					tw.Write(${field.structFieldName}==null?"null":${field.structFieldName}.ToString());
					${else}
					tw.Write(${field.structFieldName}.ToString());
					${end}
					tw.Write("\"");
				${elseif(field.jsonType=="Base64")}
					tw.Write("\"${field.structFieldName}\":");
					tw.Write("\"");
					${if(field.allowNull)}
					tw.Write(${field.structFieldName}==null?"null":Convert.ToBase64String(${field.structFieldName}));
					${else}
					tw.Write(Convert.ToBase64String(${field.structFieldName}));
					${end}
					tw.Write("\"");
				${end}
			${set(i=i+1)}
			${end}
			tw.Write("}");
		}
		#endregion
    }
}
