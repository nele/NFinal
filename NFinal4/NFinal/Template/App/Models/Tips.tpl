﻿using System;
using System.Collections.Generic;
using System.Web;

namespace ${project}.${app}.Models.Tips.${database}
{
    public struct ${table.name}
    {
		${foreach(field in table.fields)}
		public bool ${field.name};
		${end}
    }
}