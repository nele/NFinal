﻿//======================================================================
//
//        Copyright : Zhengzhou Strawberry Computer Technology Co.,LTD.
//        All rights reserved
//        
//        Application:NFinal MVC framework
//        Filename :CookieInfo.cs
//        Description :Cookie实体对象
//
//        created by Lucas at  2015-6-30`
//     
//        WebSite:http://www.nfinal.com
//
//======================================================================
using System;
using System.Collections.Generic;
using System.Web;

namespace ${project}.${app}.Code.Data
{
    public class CookieInfo : NFinal.CookieInfo
    {
        public string name="";
        public bool islogin=false;
        public int age=0;
    }
}