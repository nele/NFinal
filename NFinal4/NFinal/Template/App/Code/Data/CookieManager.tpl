﻿//======================================================================
//
//        Copyright : Zhengzhou Strawberry Computer Technology Co.,LTD.
//        All rights reserved
//        
//        Application:NFinal MVC framework
//        Filename :CookieManager.cs
//        Description :Cookie管理类
//
//        created by Lucas at  2015-6-30`
//     
//        WebSite:http://www.nfinal.com
//
//======================================================================
using System;
using System.Collections.Generic;
using System.Web;

namespace ${project}.${app}.Code.Data
{
    public class CookieManager
    {
        private HttpContext _context;
        private CookieInfo _cookieInfo;
        private bool _isStatic;
        public CookieManager()
        {
            this._context = null;
            this._isStatic = true;
            _cookieInfo = new CookieInfo();
        }
        public CookieManager(HttpContext context)
        {
            this._context = context;
            if (context == null)
            {
                this._isStatic = true;
                _cookieInfo = new CookieInfo();
            }
            else
            {
                this._isStatic = false;
            }
        }
		public string vcheck
        {
            get
            {
                if (!_isStatic && _context.Request.Cookies["vcheck"] != null)
                {
                    _cookieInfo.vcheck=_context.Request.Cookies["vcheck"].Value;
                }
                return _cookieInfo.vcheck;
            }
            set
            {
                _cookieInfo.vcheck = value;
                if (_context.Response.Cookies["vcheck"] == null)
                {
                    HttpCookie cookie = new HttpCookie("vcheck", value.ToString());
                    _context.Response.Cookies.Add(cookie);
                }
                else
                {
                    _context.Response.Cookies["vcheck"].Value = value.ToString();
                }
            }
        }
		public string session_id
        {
            get
            {
                if (!_isStatic && _context.Request.Cookies["session_id"] != null)
                {
                    _cookieInfo.session_id = _context.Request.Cookies["session_id"].Value;
                }
                return _cookieInfo.session_id;
            }
            set
            {
                _cookieInfo.session_id = value;
                if (_context.Response.Cookies["session_id"] == null)
                {
                    HttpCookie cookie = new HttpCookie("session_id", value);
                    _context.Response.Cookies.Add(cookie);
                }
                else
                {
                    _context.Response.Cookies["session_id"].Value = value;
                }
            }
        }
        $foreach(declaraton in declarations)
        public ${declaraton.typeName} ${declaraton.varName}
        {
            get
            {
                if (!_isStatic && _context.Request.Cookies["${cookiePrefix}${declaraton.varName}"] != null)
                {
					${if(declaraton.isString==true)}
						_cookieInfo.${declaraton.varName}=_context.Request.Cookies["${cookiePrefix}${declaraton.varName}"].Value;
					${else}
						${declaraton.typeName}.TryParse(_context.Request.Cookies["${cookiePrefix}${declaraton.varName}"].Value,out _cookieInfo.${declaraton.varName});
					${end}
				}
				return _cookieInfo.${declaraton.varName};
            }
            set
            {
                _cookieInfo.${declaraton.varName} = value;
                if (_context.Response.Cookies["${cookiePrefix}${declaraton.varName}"] == null)
                {
					${if(declaraton.isString==true)}
						HttpCookie cookie = new HttpCookie("${cookiePrefix}${declaraton.varName}", value);
					${else}
						HttpCookie cookie = new HttpCookie("${cookiePrefix}${declaraton.varName}", value.ToString());
                    ${end}
					_context.Response.Cookies.Add(cookie);
                }
                else
                {
					${if(declaraton.isString==true)}
						_context.Response.Cookies["${cookiePrefix}${declaraton.varName}"].Value = value;
					${else}
						_context.Response.Cookies["${cookiePrefix}${declaraton.varName}"].Value = value.ToString();
					${end}
                }
            }
        }
        $end
    }
}