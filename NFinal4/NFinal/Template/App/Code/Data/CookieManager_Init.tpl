﻿//======================================================================
//
//        Copyright : Zhengzhou Strawberry Computer Technology Co.,LTD.
//        All rights reserved
//        
//        Application:NFinal MVC framework
//        Filename :CookieManager.cs
//        Description :Cookie管理类
//
//        created by Lucas at  2015-6-30`
//     
//        WebSite:http://www.nfinal.com
//
//======================================================================
using System;
using System.Collections.Generic;
using System.Web;

namespace ${project}.${app}.Code.Data
{
    public class CookieManager
    {
        private HttpContext _context;
        private CookieInfo _cookieInfo;
        private bool _isStatic;
        public CookieManager()
        {
            this._context = null;
            this._isStatic = true;
            _cookieInfo = new CookieInfo();
        }
        public CookieManager(HttpContext context)
        {
            this._context = context;
            if (context == null)
            {
                this._isStatic = true;
                _cookieInfo = new CookieInfo();
            }
            else
            {
                this._isStatic = false;
            }
        }
		public string vcheck
        {
            get
            {
                if (_isStatic)
                {
                    return _cookieInfo.vcheck;
                }
                else
                {
                    if (_context.Request.Cookies["vcheck"] == null)
                    {
                        return _cookieInfo.vcheck;;
                    }
                    else
                    {
                        return _context.Request.Cookies["vcheck"].Value;
                    }
                }
            }
            set
            {
                if (_isStatic)
                {
                    _cookieInfo.vcheck = value;
                }
                else
                {
                    if (_context.Response.Cookies["vcheck"] == null)
                    {
                        HttpCookie cookie = new HttpCookie("vcheck", value.ToString());
                        _context.Response.Cookies.Add(cookie);
                    }
                    else
                    {
                        _context.Response.Cookies["vcheck"].Value = value.ToString();
                    }
                }
            }
        }
		public string session_id
        {
            get
            {
                if (_isStatic)
                {
                    return _cookieInfo.session_id;
                }
                else
                {
                    if (_context.Request.Cookies["session_id"] == null)
                    {
                        return _cookieInfo.session_id;;
                    }
                    else
                    {
                        return _context.Request.Cookies["session_id"].Value;
                    }
                }
            }
            set
            {
                if (_isStatic)
                {
                    _cookieInfo.session_id = value;
                }
                else
                {
                    if (_context.Response.Cookies["session_id"] == null)
                    {
                        HttpCookie cookie = new HttpCookie("session_id", value.ToString());
                        _context.Response.Cookies.Add(cookie);
                    }
                    else
                    {
                        _context.Response.Cookies["session_id"].Value = value.ToString();
                    }
                }
            }
        }
        public string name
        {
            get
            {
                if (_isStatic)
                {
                    return _cookieInfo.name;
                }
                else
                {
                    if (_context.Request.Cookies["name"] == null)
                    {
                        return _cookieInfo.name;;
                    }
                    else
                    {
                        return _context.Request.Cookies["name"].Value;
                    }
                }
            }
            set
            {
                if (_isStatic)
                {
                    _cookieInfo.name = value;
                }
                else
                {
                    if (_context.Response.Cookies["name"] == null)
                    {
                        HttpCookie cookie = new HttpCookie("name", value.ToString());
                        _context.Response.Cookies.Add(cookie);
                    }
                    else
                    {
                        _context.Response.Cookies["name"].Value = value.ToString();
                    }
                }
            }
        }
        
        public bool islogin
        {
            get
            {
                if (_isStatic)
                {
                    return _cookieInfo.islogin;
                }
                else
                {
                    if (_context.Request.Cookies["islogin"] == null)
                    {
                        return _cookieInfo.islogin;;
                    }
                    else
                    {
                        return Convert.ToBoolean(_context.Request.Cookies["islogin"].Value);
                    }
                }
            }
            set
            {
                if (_isStatic)
                {
                    _cookieInfo.islogin = value;
                }
                else
                {
                    if (_context.Response.Cookies["islogin"] == null)
                    {
                        HttpCookie cookie = new HttpCookie("islogin", value.ToString());
                        _context.Response.Cookies.Add(cookie);
                    }
                    else
                    {
                        _context.Response.Cookies["islogin"].Value = value.ToString();
                    }
                }
            }
        }
        
        public int age
        {
            get
            {
                if (_isStatic)
                {
                    return _cookieInfo.age;
                }
                else
                {
                    if (_context.Request.Cookies["age"] == null)
                    {
                        return _cookieInfo.age;;
                    }
                    else
                    {
                        return Convert.ToInt32(_context.Request.Cookies["age"].Value);
                    }
                }
            }
            set
            {
                if (_isStatic)
                {
                    _cookieInfo.age = value;
                }
                else
                {
                    if (_context.Response.Cookies["age"] == null)
                    {
                        HttpCookie cookie = new HttpCookie("age", value.ToString());
                        _context.Response.Cookies.Add(cookie);
                    }
                    else
                    {
                        _context.Response.Cookies["age"].Value = value.ToString();
                    }
                }
            }
        }
    }
}