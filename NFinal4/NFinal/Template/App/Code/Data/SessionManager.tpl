﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebMvc.App.Code.Data
{
    public class SessionManager
    {
        private HttpContext _context;
        private SessionInfo _sessionInfo;
        private bool _isStatic;
        public SessionManager()
        {
            this._context = null;
            this._isStatic = true;
            _sessionInfo = new SessionInfo();
        }
        public SessionManager(HttpContext context)
        {
            this._context = context;
            if (context == null)
            {
                this._isStatic = true;
                _sessionInfo = new SessionInfo();
            }
            else
            {
                this._isStatic = false;
            }
        }
        $foreach(declaraton in declarations)
        public ${declaraton.typeName} ${declaraton.varName}
        {
            get {
                if (!_isStatic && _context.Session["${sessionPrefix}${declaraton.varName}"] != null)
                {
                    ${if(declaraton.isString==true)}
						_sessionInfo.${declaraton.varName}=_context.Session["${sessionPrefix}${declaraton.varName}"].ToString();
					${else}
						_sessionInfo.${declaraton.varName}=(${declaraton.typeName})_context.Session["${sessionPrefix}${declaraton.varName}"];
					${end}
                }
                return _sessionInfo.${declaraton.varName};
            }
            set {
                _sessionInfo.${declaraton.varName} = value;
                if(!_isStatic)
                {
                    _context.Session["${sessionPrefix}${declaraton.varName}"] = _sessionInfo.${declaraton.varName};
                }
            }
        }
        ${end}
    }
}