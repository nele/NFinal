﻿//======================================================================
//
//        Copyright : Zhengzhou Strawberry Computer Technology Co.,LTD.
//        All rights reserved
//        
//        Application:NFinal MVC framework
//        Filename :Controller.cs
//        Description :控制器
//
//        created by Lucas at  2015-6-30`
//     
//        WebSite:http://www.nfinal.com
//
//======================================================================
using System;
using System.Collections.Generic;
using System.Web;
using System.Collections.Specialized;
using System.IO;

namespace ${project}.${app}
{
    //Controller基类
    public class Controller : NFinal.BaseAction
    {
        public Controller() { }
        public Controller(string fileName)
            : base(fileName)
        {
        }
        public Controller(TextWriter tw)
            : base(tw)
        {
        }
		public void Error(string msg, string url, int second)
        {
            Web.Default.Common.Public.ErrorAction errorAction = new Web.Default.Common.Public.ErrorAction(_tw);
            errorAction.Error(msg, url, second);
        }
        public void Success(string msg, string url, int second)
        {
            Web.Default.Common.Public.SuccessAction successAction = new Web.Default.Common.Public.SuccessAction(_tw);
            successAction.Success(msg, url, second);
        }
        public Code.Data.CookieManager _cookies;
    }
}