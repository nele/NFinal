﻿//======================================================================
//
//        Copyright : Zhengzhou Strawberry Computer Technology Co.,LTD.
//        All rights reserved
//        
//        Application:NFinal MVC framework
//        Filename :WebCompile .cs
//        Description :生成器
//
//        created by Lucas at  2015-6-30`
//     
//        WebSite:http://www.nfinal.com
//
//======================================================================
using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ${project}.${app}
{
    public partial class WebCompiler : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Response.Write("生成开始<br/>");
            Response.Flush();

            string appRoot = Server.MapPath("/");
            //获取WebConfig中的配置信息
            NFinal.Frame frame = new NFinal.Frame(appRoot);
            string[] Apps = frame.GetApps();
            //创建主路由
            frame.CreateMain(Apps);
            List<NFinal.Compile.ControllerFileData> fileDataList = frame.GetAllControllers();
            frame.CreateModuleAndRouter(fileDataList);
            frame.CreateURLHelper(fileDataList);
            //创建并生成Web站点
            NFinal.Config config = NFinal.ConfigurationManager.GetConfig("${app}");
            NFinal.Application application = new NFinal.Application(config);
			frame.GetDB(config);
            application.CreateApp();
            application.CreateModelsFile();
            application.CreateRouter();
			application.CreateCookieManager();
			application.CreateSessionManager();
            application.CreateDAL(true);
            application.CreateCompile(true);
			frame.ClearDB();
            Response.Write("生成结束<br/>");

            Response.Flush();
            Response.End();
        }
    }
}