﻿﻿//======================================================================
//
//        Copyright : Zhengzhou Strawberry Computer Technology Co.,LTD.
//        All rights reserved
//        
//        Application:NFinal MVC framework
//        Filename :{Header}.cs
//        Description :模板提示类
//
//        created by Lucas at  2015-6-30`
//     
//        WebSite:http://www.nfinal.com
//
//======================================================================
using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
namespace ${project}.${app}.Views.Default.Common.Public
{
    public partial class Header : System.Web.UI.UserControl
    {
		//数据库模型
		//函数内变量
        public class Header_AutoComplete:Controller
        {
			//参数变量
			//数据库变量
			//一般变量
				public string message;
			//DAL函数声明变量
        }
		//变量存储类,用于自动完成.
        public Header_AutoComplete ViewBag = new Header_AutoComplete();
    }
}