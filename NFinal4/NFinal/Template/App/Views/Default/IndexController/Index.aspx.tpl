﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Index.aspx.cs" Inherits="${project}.${app}.Views.Default.IndexController.Index" %>
<%@ Register Src="~/${app}/Views/Default/Common/Public/Header.ascx" TagPrefix="uc1" TagName="Header" %>
<%@ Register Src="~/${app}/Views/Default/Common/Public/Footer.ascx" TagPrefix="uc1" TagName="Footer" %>
<!doctype html>
<html>
<head>
	<title>nfinal框架</title>
    <script src="/${app}/Content/js/jquery-1.11.2.min.js"></script>
    <link href="/${app}/Content/css/frame.css" type="text/css" rel="stylesheet" />
    <style type="text/css">
        .auto-style1 {
            width: 140px;
        }
        .auto-style2 {
            width: 197px;
        }
    </style>
</head>
<body>
    <uc1:Header runat="server" />   
    <article>
        Hello,NFinal!
    </article>
    <uc1:Footer runat="server" />
</body>
</html>