﻿﻿//======================================================================
//
//        Copyright : Zhengzhou Strawberry Computer Technology Co.,LTD.
//        All rights reserved
//        
//        Application:NFinal MVC framework
//        Filename :config.json
//        Description :模块配置文件
//
//        created by Lucas at  2015-10-9`
//     
//        WebSite:http://www.nfinal.com
//
//======================================================================
{
  //默认样式
  "defaultStyle": "Default/",
  //自动生成
  "AutoGeneration": false,
  //是否调试
  "IsDebug": true,
  //模板使用Replace函数进行字符串替换所需的参数
  "TemplateReplaceParameters": [
    {
      "oldValue": "__APP__",
      "newValue": "/${app}"
    },
    {
      "oldValue": "__CONTENT__",
      "newValue": "/${app}/Content"
    },
    {
      "oldValue": "__JS__",
      "newValue": "/${app}/Content/js"
    },
    {
      "oldValue": "__CSS__",
      "newValue": "/${app}/Content/css"
    },
    {
      "oldValue": "__IMAGE__",
      "newValue": "/${app}/Content/images"
    },
    {
      "oldValue": "__VIEWS__",
      "newValue": "/${app}/Default"
    }
  ]
}
