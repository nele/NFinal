﻿//======================================================================
//
//        Copyright : Zhengzhou Strawberry Computer Technology Co.,LTD.
//        All rights reserved
//        
//        Application:NFinal MVC framework
//        Filename :Router.cs
//        Description :路由类
//
//        created by Lucas at  2015-6-30`
//     
//        WebSite:http://www.nfinal.com
//
//======================================================================
using System;
using System.Collections.Generic;
using System.Web;
using System.Threading;
using System.Globalization;
using System.Collections.Specialized;

namespace ${namespace}
{
    public static class Router
    {
		private static string GetString(string val, string de)
        {
            return string.IsNullOrEmpty(val) ? de : val;
        }
        private static string UCFirst(string val)
        {
            CultureInfo cultureInfo = Thread.CurrentThread.CurrentCulture;
            return cultureInfo.TextInfo.ToTitleCase(val);
        }
        private static void Callback(IAsyncResult result)
        {
            //结束异步写入
            System.IO.FileStream stream = (System.IO.FileStream)result.AsyncState;
            stream.EndWrite(result);
            stream.Close();
        }
        public static void Run(System.Web.HttpContext context,string subdomain,string app,string urlWithOutAppAndExt,string controller,string action,NameValueCollection get)
        {
			switch (controller)
			{
				case "/IndexController":
				{  
					switch (action)
					{
						case "Index": 
						{
							Web.Default.IndexController.IndexAction control= new Web.Default.IndexController.IndexAction(context.Response.Output);
							control._cookies = new Code.Data.CookieManager(context);	
							control._context = context;
							control._subdomain = subdomain;
							control._url=context.Request.RawUrl;
							control._get = get;
							control._app=app;
							control._controller = controller;
                            control._action = action;
							control.Before();
							control.Index();
							control.After();
							break;
						}
							
						default: context.Response.Write("找不到类" + controller + "下的" + action + "方法");context.Response.End(); break;
					}
					break;
				}
				default: context.Response.Write("找不到类" + controller); context.Response.End(); break; 
			}
        }
        
    }
}