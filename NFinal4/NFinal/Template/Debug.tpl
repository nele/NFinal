﻿<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
    <script src="/Url.js"></script>
</head>
<body>
    <script>
		function jump()
		{
			${foreach(parameter in parameterDataList)}
			${if(parameter.isUrlParameter)}
			${if(parameter.type=="int")}
			var ${parameter.name}=1;
			${else}
			var ${parameter.name}="";
			${end}
			${end}
			${end}
			window.location.href = Url.${methodName}(${methodParameters});
		}
		jump();
	</script>
</body>
</html>
