﻿//======================================================================
//
//        Copyright : Zhengzhou Strawberry Computer Technology Co.,LTD.
//        All rights reserved
//        
//        Application:NFinal MVC framework
//        Filename :{$aspxPageClassName}.cs
//        Description :模板提示类
//
//        created by Lucas at  2015-6-30`
//     
//        WebSite:http://www.nfinal.com
//
//======================================================================
using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ${aspxPageNameSpace}
{
    public partial class ${aspxPageClassName} : System.Web.UI.${BaseClassName}
    {
		//数据库模型
        ${StructDatas}
		//函数内变量
        public class ${aspxPageClassName}_AutoComplete:${baseName}
        {
			//参数变量
			$foreach(parameterData in parameterDataList)
				${if(parameterData.isArray)}
					public ${parameterData.type}[] ${parameterData.name};
				${else}
					public ${parameterData.type} ${parameterData.name};
				${end}
			$end
			//数据库变量
			$foreach(functionData in functionDataList)
				${if(functionData.type=="var")}
					${if(functionData.functionName=="Delete")}
						public int ${functionData.varName};
					${end}
					${if(functionData.functionName=="ExecuteNonQuery")}
						public int ${functionData.varName};
					${end}
					${if(functionData.functionName=="Insert")}
						public int ${functionData.varName};
					${end}
					${if(functionData.functionName=="Page")}
						public NFinal.List<__${methodName}_${functionData.varName}__> ${functionData.varName};
					${end}
					${if(functionData.functionName=="QueryAll")}
						public NFinal.List<__${methodName}_${functionData.varName}__> ${functionData.varName};
					${end}
					${if(functionData.functionName=="QueryObject")}
						public ${functionData.type} ${functionData.varName};
					${end}
					${if(functionData.functionName=="QueryRandom")}
						public NFinal.List<__${methodName}_${functionData.varName}__> ${functionData.varName};
					${end}
					${if(functionData.functionName=="QueryTop")}
						public NFinal.List<__${methodName}_${functionData.varName}__> ${functionData.varName};
					${end}
					${if(functionData.functionName=="QueryRow")}
						public __${methodName}_${functionData.varName}__ ${functionData.varName};
					${end}
					${if(functionData.functionName=="Update")}
						public int ${functionData.varName};
					${end}
				${end}
			$end
			//一般变量
			$foreach(csharpDeclaration in csharpDeclarationList)
				public ${csharpDeclaration.typeName} ${csharpDeclaration.varName};
			$end
			//DAL函数声明变量
        }
		//变量存储类,用于自动完成.
        public ${aspxPageClassName}_AutoComplete ViewBag = new ${aspxPageClassName}_AutoComplete();
    }
}