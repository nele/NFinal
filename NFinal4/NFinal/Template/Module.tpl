﻿﻿//======================================================================
//
//        Copyright : Zhengzhou Strawberry Computer Technology Co.,LTD.
//        All rights reserved
//        
//        Application:NFinal MVC framework
//        Filename :Module.cs
//        Description :路由类
//
//        created by Lucas at  2015-10-15`
//     
//        WebSite:http://www.nfinal.com
//
//======================================================================
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NFinal
{
    public class Module:NFinal.BaseModule
    {
        protected override void Rewrite(string requestedPath, HttpApplication app)
        {
			//去掉Server头，没有任何作用，且浪费资源
			app.Context.Response.Headers.Remove("Server");
            string requestPath = app.Context.Request.Path;
            string pathInfo = app.Context.Request.PathInfo;
            string paramers = app.Context.Request.Params.ToString();
            string filePath = app.Context.Request.PhysicalPath;
			string domainName = app.Context.Request.Url.Host;
            //执行自定义重写函数
            if (!RewriteUrl.Rewrite(requestedPath,app.Context))
            {
				//重写虚拟目录
				${set(i=1)}
				${foreach(fileData in controllerFileDataList)}
				${set(fileDataClassDataList=fileData.ClassDataList)}
				${foreach(classData in fileDataClassDataList)}
				${set(classDataMethodDataList=classData.MethodDataList)}
				${foreach(methodData in classDataMethodDataList)}
                ${if(methodData.isPublic)}
					${set(rewriteDirectoryList=methodData.rewriteDirectoryList)}
					${foreach(rewriteDirectory in rewriteDirectoryList)}
				if(requestedPath.StartsWith("${rewriteDirectory.from}"))
				{
					requestedPath = "${rewriteDirectory.to}" + requestedPath.Remove(0,"${rewriteDirectory.from}".Length);
				}
					${end}
				${set(i=i+1)}
				${end}
				${end}
				${end}
				${end}

                //重写虚拟路径,也可以把此部分放到find中
				switch (requestedPath)
                {
				${set(i=1)}
				${foreach(fileData in controllerFileDataList)}
				${set(fileDataClassDataList=fileData.ClassDataList)}
				${foreach(classData in fileDataClassDataList)}
				${set(classDataMethodDataList=classData.MethodDataList)}
				${foreach(methodData in classDataMethodDataList)}
                ${if(methodData.isPublic)}
					${set(rewriteFileList=methodData.rewriteFileList)}
					${foreach(rewriteFile in rewriteFileList)}
					case "${rewriteFile.from}":requestedPath = "${rewriteFile.to}"; break;
					${end}
				${set(i=i+1)}
				${end}
				${end}
				${end}
				${end}
					default: break;
                }
                //获取actionUrl
                string actionUrl = GetActionUrl(requestedPath);
                int find = FindAction(domainName, actionUrl, requestedPath);
                if (find > 0)
                {

                    //重写url使其能够让NFinal.Handler进行处理
                    app.Context.RewritePath(string.Format("{0}{1}{2}", requestedPath, find.ToString("00000"), ".nf"));
                }
            }
        }
        /// <summary>
        /// 计算actionUrl
        /// </summary>
        /// <param name="requestedPath"></param>
        /// <returns></returns>
        private string GetActionUrl(string requestedPath)
        {
			//找到最后一个.的位置
            int position = requestedPath.LastIndexOf('.');
            bool hasExtension = false;
            char ch = requestedPath[position + 1];
            //判断是否有后缀（后缀首字母不能为数字，且长度必须小于4）
            //.后面不是数字
            if (ch > '9' || ch < '0')
            {
                //.后缀长度必须小于4
                if( requestedPath.Length-position<6)
                {
                    hasExtension = true;
                }
            }
			//指向ActionUrl结尾的位置
            int pointer = 0;
            string actionUrl = requestedPath;
            bool hasGet = false;
			//如果有后缀
            if (hasExtension)
            {
                hasGet = int.TryParse(requestedPath.Substring(position - 2, 2), out pointer);
            }
            //如果没有后缀
            else
            {
                hasGet = int.TryParse(requestedPath.Substring(requestedPath.Length - 2, 2), out pointer);
            }
			//取出ActionUrl
            if (hasGet)
            {
                if (pointer > -1 && pointer < requestedPath.Length)
                {
                    actionUrl = requestedPath.Substring(0, pointer + 1);
                }
            }
            return actionUrl;
        }
        /// <summary>
        /// 查询actionUrl
        /// </summary>
        /// <param name="app"></param>
        /// <param name="actionUrl"></param>
        /// <param name="requestedPath"></param>
        /// <returns></returns>
        public int FindAction(string domainName, string actionUrl, string requestedPath)
        {
            int find = 0;
            //无参数
            switch (requestedPath)
            {
                ${set(i=1)}
				${foreach(fileData in controllerFileDataList)}
				${set(fileDataClassDataList=fileData.ClassDataList)}
				${foreach(classData in fileDataClassDataList)}
				${set(classDataMethodDataList=classData.MethodDataList)}
				${foreach(methodData in classDataMethodDataList)}
                ${if(methodData.isPublic)}
					${if(methodData.hasParameters==false)}
				case "${methodData.actionUrl}":
                    find = ${i}; break;
					${end}
				${set(i=i+1)}
				${end}
				${end}
				${end}
				${end}
                default: find = 0; break;
            }
			//有参数
			if(find==0)
			{
				switch (actionUrl)
				{
					${set(i=1)}
					${foreach(fileData in controllerFileDataList)}
					${set(fileDataClassDataList=fileData.ClassDataList)}
					${foreach(classData in fileDataClassDataList)}
					${set(classDataMethodDataList=classData.MethodDataList)}
					${foreach(methodData in classDataMethodDataList)}
					${if(methodData.isPublic)}
						${if(methodData.hasParameters==true)}
					case "${methodData.actionUrl}":
						find = ${i}; break;
						${end}
					${set(i=i+1)}
					${end}
					${end}
					${end}
					${end}
					default: find = 0; break;
				}
			}
            return find;
        }
    }
}