﻿﻿//======================================================================
//
//        Copyright : Zhengzhou Strawberry Computer Technology Co.,LTD.
//        All rights reserved
//        
//        Application:NFinal MVC framework
//        Filename :Router.cs
//        Description :路由类
//
//        created by Lucas at  2015-10-15`
//     
//        WebSite:http://www.nfinal.com
//
//======================================================================
using System;
using System.Collections.Generic;
using System.Web;
using System.Threading;
using System.Globalization;
using System.Collections.Specialized;
using System.Text.RegularExpressions;

namespace ${project}
{
    public static class Router
    {
        public static string GetString(string val, string de)
        {
            return string.IsNullOrEmpty(val) ? de : val;
        }
        public static string UCFirst(string val)
        {
            CultureInfo cultureInfo = Thread.CurrentThread.CurrentCulture;
            return cultureInfo.TextInfo.ToTitleCase(val);
        }
		 private static void Callback(IAsyncResult result)
        {
            //结束异步写入
            System.IO.FileStream stream = (System.IO.FileStream)result.AsyncState;
            stream.EndWrite(result);
            stream.Close();
        }
		//设置浏览器缓存
        public static void SetBrowerCache(HttpContext context,int optimizing,int seconds)
        {
            //浏览器缓存
            if ((optimizing & 224) != 0)
            {
                context.Response.Cache.SetCacheability(HttpCacheability.Public);
                //NotModify
                if ((optimizing & 32) != 0)
                {
                    context.Response.Cache.SetLastModifiedFromFileDependencies();
                }
                //Expires
                else if ((optimizing & 64) != 0)
                {
                    context.Response.Cache.SetExpires(DateTime.Now.AddSeconds(seconds));
                }
                //NoExpires
                else if ((optimizing & 128) != 0)
                {
                }
            }
            else
            {
                //不进行缓存
                context.Response.Cache.SetNoStore();
            }
        }
        //设置服务器缓存
        public static void SetServerCacheAndOutPut(HttpContext context, string app, string urlWithOutAppAndExt, string url, byte[] buffer, int optimizing, int seconds)
        {
            //服务器缓存
            if ((optimizing & 14) != 0)
            {
                //FileDependency
                if ((optimizing & 2) != 0)
                {
                    string fileName = context.Server.MapPath(app + "/HTML" + urlWithOutAppAndExt + ".html");
                    //将HTML写入静态文件
                    if (!System.IO.File.Exists(fileName))
                    {
                        string dir = System.IO.Path.GetDirectoryName(fileName);
                        if (!System.IO.Directory.Exists(dir))
                        {
                            System.IO.Directory.CreateDirectory(dir);
                        }
                        System.IO.FileStream file = new System.IO.FileStream(fileName, System.IO.FileMode.Create, System.IO.FileAccess.Write, System.IO.FileShare.ReadWrite, 1024, true);
                        file.BeginWrite(buffer, 0, buffer.Length, new AsyncCallback(Callback), file);
                    }
                    //将HTML插入新的缓存
                    System.Web.Caching.CacheDependency dep = new System.Web.Caching.CacheDependency(fileName);
                    HttpRuntime.Cache.Insert(url, buffer, dep, System.Web.Caching.Cache.NoAbsoluteExpiration, System.Web.Caching.Cache.NoSlidingExpiration);
                }
                //AbsoluteExpiration
                else if ((optimizing & 4) != 0)
                {
                    HttpRuntime.Cache.Insert(url, buffer, null, DateTime.Now.AddSeconds(seconds), System.Web.Caching.Cache.NoSlidingExpiration);
                }
                //SlidingExpiration
                else if ((optimizing & 8) != 0)
                {
                    HttpRuntime.Cache.Insert(url, buffer, null, System.Web.Caching.Cache.NoAbsoluteExpiration, new TimeSpan(0, 0, seconds));
                }
            }
            //直接输出
            context.Response.OutputStream.Write(buffer, 0, buffer.Length);
            context.Response.OutputStream.Close();
        }
        //写入缓存
        public static bool WriteCache(HttpContext context,string url,int optimizing)
        {
            object cache = null;
            //服务器缓存
            if ((optimizing & 14) != 0)
            {
                cache = HttpRuntime.Cache.Get(url);
                //如果缓存存在则直接输出.
                if (cache != null)
                {
                    System.IO.Stream stream = null;
                    if ((optimizing & 1536) != 0)
                    {
                        if ((optimizing & 512) != 0)
                        {
                            context.Response.AppendHeader("Content-encoding", "gzip");
                        }
                        else if ((optimizing & 1024) != 0)
                        {
                            context.Response.AppendHeader("Content-encoding", "deflate");
                        }
                    }
                    byte[] buffer= (byte[])cache;
                    context.Response.OutputStream.Write(buffer, 0, buffer.Length);
                    context.Response.OutputStream.Close();
                    return true;
                }
            }
            return false;
        }
        /// <summary>
        /// 计算actionUrl
        /// </summary>
        /// <param name="requestedPath"></param>
        /// <returns></returns>
        private static string GetActionUrl(string requestedPath)
        {
            int position = requestedPath.LastIndexOf('.');
            int pointer = 0;
            string actionUrl = "";
            if (position > 0)
            {
                bool hasGet = int.TryParse(requestedPath.Substring(position - 2, 2), out pointer);
                if (hasGet)
                {
                    actionUrl = requestedPath.Substring(0, pointer + 1);
                }
                else
                {
                    actionUrl = requestedPath;
                }
            }
            return actionUrl;
        }
        public static void Run(HttpContext context, string requestedPath)
        {
            //取出action标识
            int find = 0;
            string f = requestedPath.Substring(requestedPath.Length - 8, 5);
            int.TryParse(requestedPath.Substring(requestedPath.Length - 8, 5), out find);
            //去掉00000.htm后缀,还原requestPath
            requestedPath = requestedPath.Substring(0, requestedPath.Length - 8);
            string[] hostName = context.Request.Url.Host.ToString().Split('.');
            string subdomain = hostName.Length == 3 ? hostName[0] : "www";
            string[] parameters = null;
            NameValueCollection get = new NameValueCollection();
			//提取form参数
            if (context.Request.Form.Count > 0)
            {
                for (int i = 0; i < context.Request.Form.Count; i++)
                {
                    get.Add(context.Request.Form.Keys[i], context.Request.Form[i]);
                }
            }
			//提取URL?后的参数
			if (context.Request.QueryString.Count > 0)
            {
                for (int i = 0; i < context.Request.QueryString.Count; i++)
                {
                    get.Add(context.Request.QueryString.Keys[i], context.Request.QueryString[i]);
                }
            }
			//提取MethodType
			NFinal.MethodType methodType = NFinal.MethodType.NONE;
            switch (context.Request.HttpMethod)
            {
                case "POST":methodType = NFinal.MethodType.POST; break;
                case "GET":methodType = NFinal.MethodType.GET; break;
                case "DELETE":methodType = NFinal.MethodType.DELETE; break;
                case "PUT":methodType = NFinal.MethodType.PUT; break;
                case "AJAX":methodType = NFinal.MethodType.AJAX; break;
                default:methodType = NFinal.MethodType.NONE; break;
            }
            //获取actionUrl,用于获取参数
            string actionUrl = GetActionUrl(requestedPath);
            switch (find)
            {
				${set(i=1)}
				${foreach(fileData in controllerFileDataList)}
				${set(config=fileData.config)}
				${set(fileDataClassDataList=fileData.ClassDataList)}
                ${foreach(classData in fileDataClassDataList)}
				${set(classDataMethodDataList=classData.MethodDataList)}
				${foreach(method in classDataMethodDataList)}
					${if(method.isPublic)}
					case ${i}:
					{
						context.Response.ContentType = "${method.contentType}";
						Regex regParameters = new Regex("${method.parameterRegex}");
                        Match matParameters = regParameters.Match(requestedPath);
						${set(methodparameterDataList=method.parameterDataList)}
						${set(j=1)}
						${foreach(parameterData in methodparameterDataList)}
                        ${if(parameterData.isUrlParameter)}
						if (get["${parameterData.name}"] == null)
                        {
                            get.Add("${parameterData.name}", matParameters.Groups[${j}].Value);
                        }
                        else
                        {
                            get["${parameterData.name}"] = matParameters.Groups[${j}].Value;
                        }
						${set(j=j+1)}
						${end}
						${end}
						${method.verifyCode}
						${if(method.optimizing==0)}
						${fileData.appName}.Web.${config.defaultStyle}${classData.relativeDotName}.${method.name}Action control= new ${fileData.appName}.Web.${config.defaultStyle}${classData.relativeDotName}.${method.name}Action(context);
						${set(methodparameterDataList=method.parameterDataList)}
						$foreach(parameterData in methodparameterDataList)
							${parameterData.getParamterCode}
						$end
						control._methodType = methodType;
						control._context = context;
						control._subdomain = subdomain;
						control._url=requestedPath;
						control._get = get;
						control._app="/${fileData.appName}";
						control.Before();
						control.${method.name}(${method.parameterNames});
						control.After();
						control.Close();
						context.Response.End();
						${else}
							//优化选项
                        int optimizing = ${method.optimizing};
                        int seconds = ${method.seconds};
                        string url = string.Format("{0}{1}.htm", app, urlWithOutAppAndExt);
                        bool compress = (optimizing & 1536)!=0;
                        //设置浏览器缓存
                        SetBrowerCache(context, optimizing, seconds);
                        //是否启用了服务器缓存
                        bool serverCahce = (optimizing & 14) != 0;
                        bool hasCache = false;
                        //服务器缓存
                        if (serverCahce)
                        {
                            hasCache=WriteCache(context,url,optimizing);
                        }
                        //如果没有缓存
                        if (!hasCache)
                        {
                            System.IO.MemoryStream htmlStream = new System.IO.MemoryStream();
                                    
                            ${fileData.appName}.Web.${config.defaultStyle}${classData.relativeDotName}.${method.name}Action control = null;
                            System.IO.Stream stream = null;
                            System.IO.StreamWriter sw = null;
                            //如果有压缩或缓存
                            if (compress || serverCahce)
                            {
                                if ((optimizing & 1536) != 0)
                                {
                                    if ((optimizing & 512) != 0)
                                    {
                                        stream = new System.IO.Compression.GZipStream(htmlStream, System.IO.Compression.CompressionMode.Compress, true);
                                        context.Response.AppendHeader("Content-Encoding", "gzip");
                                    }
                                    else if ((optimizing & 1024) != 0)
                                    {
                                        stream = new System.IO.Compression.DeflateStream(htmlStream, System.IO.Compression.CompressionMode.Compress, true);
                                        context.Response.AppendHeader("Content-Encoding", "deflate");
                                    }
                                    sw = new System.IO.StreamWriter(stream);
                                }
                                else
                                {
                                    sw = new System.IO.StreamWriter(htmlStream);
                                }
                                control = new ${fileData.appName}.Web.${config.defaultStyle}${classData.relativeDotName}.${method.name}Action(sw);
                            }
                            else
                            {
                                control = new ${fileData.appName}.Web.${config.defaultStyle}${classData.relativeDotName}.${method.name}Action(context);
                            }
							${set(methodparameterDataList=method.parameterDataList)}
							$foreach(parameterData in methodparameterDataList)
								${parameterData.getParamterCode}
							$end
                            control._context = context;
                            control._subdomain = subdomain;
                            control._url = requestedPath;
                            control._get = get;
                            control._app="/${fileData.appName}";
                            control.Before();
                            control.${method.name}(${method.parameterNames});
                            control.After();
                            control.Close();
                            byte[] buffer = htmlStream.ToArray();
							htmlStream.Close();
                            //如果有压缩或缓存
                            if (compress || serverCahce)
                            {
                                SetServerCacheAndOutPut(context, app, urlWithOutAppAndExt, url, buffer, optimizing, seconds);
                            }	
						}
						context.Response.End();
						${end}
					}break;
					${set(i=i+1)}
					${end}
				${end}
				${end}
				${end}
                default: break;
            }
        }
    }
}