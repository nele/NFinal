﻿using System;
//======================================================================
//
//        Copyright : Zhengzhou Strawberry Computer Technology Co.,LTD.
//        All rights reserved
//        
//        Application:NFinal MVC framework
//        Filename :UserControl.cs
//        Description :用户控件父类
//
//        created by Lucas at  2015-6-30`
//     
//        WebSite:http://www.nfinal.com
//
//======================================================================
using System.Collections.Generic;
using System.Web;

namespace NFinal
{
    public class UserControl : System.Web.UI.UserControl
    {
        public delegate void __Render__();
        public __Render__ __render__ = null;
        public string _subdomain;//二级域名
        public string _action;
        public string _controller;
        public string _app;
        public string _url;
    }
}