﻿//======================================================================
//
//        Copyright : Zhengzhou Strawberry Computer Technology Co.,LTD.
//        All rights reserved
//        
//        Application:NFinal MVC framework
//        Filename :DBBase.cs
//        Description : 数据库魔法函数类,此类只为自动提示时使用,并不编译执行
//
//        created by Lucas at  2015-6-30`
//     
//        WebSite:http://www.nfinal.com
//
//======================================================================
using System;
using System.Data;
using System.Web;

namespace NFinal.DB
{
    /// <summary>
    /// 数据库魔法函数类,此类只为自动提示时使用,并不编译执行
    /// </summary>
    public class DBBase
    {
        /// <summary>
        /// 执行SQL并返回相应行数
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="top"></param>
        /// <returns></returns>
        public List<dynamic> QueryRandom(string sql,int top)
        {
            return new List<dynamic>();
        }
        /// <summary>
        /// 执行SQL并返回相应行数
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="top"></param>
        /// <returns></returns>
        public List<T> QueryRandom<T>(string sql, int top)
        {
            return new List<T>();
        }
        /// <summary>
        /// 执行SQL并返回行对象数组
        /// </summary>
        /// <param name="sql"></param>
        /// <returns></returns>
        public List<dynamic> QueryAll(string sql)
        {
            return new List<dynamic>();
        }
        /// <summary>
        /// 执行SQL并返回行对象数组
        /// </summary>
        /// <param name="sql"></param>
        /// <returns></returns>
        public List<T> QueryAll<T>(string sql)
        {
            return new List<T>();
        }
        /// <summary>
        /// 执行SQL并返回行对象数组
        /// </summary>
        /// <param name="sql"></param>
        /// <returns></returns>
        public List<dynamic> QueryTop(string sql,int topQty)
        {
            return new List<dynamic>();
        }
        /// <summary>
        /// 执行SQL并返回行对象数组
        /// </summary>
        /// <param name="sql"></param>
        /// <returns></returns>
        public List<T> QueryTop<T>(string sql, int topQty)
        {
            return new List<T>();
        }
        /// <summary>
        /// 分页
        /// </summary>
        /// <param name="sql">选取所有记录的SQL语句</param>
        /// <param name="pageIndexVarName">传送页码所需的变量,默认为pageIndex</param>
        /// <param name="pageSize">每页显示条数</param>
        /// <returns></returns>
        public List<dynamic> Page(string sql,NFinal.Page page)
        {
            return new List<dynamic>();
        }
        /// <summary>
        /// 分页
        /// </summary>
        /// <param name="sql">选取所有记录的SQL语句</param>
        /// <param name="pageIndexVarName">传送页码所需的变量,默认为pageIndex</param>
        /// <param name="pageSize">每页显示条数</param>
        /// <returns></returns>
        public List<T> Page<T>(string sql, NFinal.Page page)
        {
            return new List<T>();
        }
        /// <summary>
        /// 执行SQL并返回行对象
        /// </summary>
        /// <param name="sql"></param>
        /// <returns></returns>
        public dynamic QueryRow(string sql)
        {
            dynamic obj=null;
            return obj;
        }
        /// <summary>
        /// 执行SQL并返回行对象
        /// </summary>
        /// <param name="sql"></param>
        /// <returns></returns>
        public T QueryRow<T>(string sql)
        {
            dynamic obj = null;
            return obj;
        }
        public object QueryObject(string sql)
        {
            return null;
        }
        /// <summary>
        /// 执行SQL并返回ID
        /// </summary>
        /// <param name="sql"></param>
        /// <returns></returns>
        public int Insert(string sql)
        {
            return 0;
        }
        public int Update(string sql)
        {
            return 0;
        }
        public int Delete(string sql)
        {
            return 0;
        }
        /// <summary>
        /// 执行SQL并返回受影响行数
        /// </summary>
        /// <param name="sql"></param>
        /// <returns></returns>
        public int ExecuteNonQuery(string sql)
        {
            return 0;
        } 
    }
}