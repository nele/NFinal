﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NFinal.DB.Coding
{
    public enum JsonType
    {
        String=1,
        Bool=2,
        Time=3,
        Object=4,
        Number=5,
        Base64=6
    }
}
