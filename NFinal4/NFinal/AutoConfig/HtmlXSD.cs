﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NFinal.AutoConfig
{
    public class HtmlXSD
    {
        public static string[] GetVSTools()
        {
            string vsTool = null;
            System.Collections.Generic.List<string> vsTools = new System.Collections.Generic.List<string>();
            for (int i = 60; i < 150; i = i + 10)
            {
                vsTool = System.Environment.GetEnvironmentVariable(string.Format("VS{0}COMNTOOLS", i), EnvironmentVariableTarget.Machine);
                if (!string.IsNullOrEmpty(vsTool))
                {
                    vsTools.Add(vsTool);
                }
            }
            return vsTools.ToArray();
        }
        public static void RewriteXSD(string root)
        {
            string[] vsTools = GetVSTools();
            string commonPath = null;
            string xsdFolder = null;
            for (int i = 0; i < vsTools.Length; i++)
            {
                commonPath = System.IO.Path.GetDirectoryName(System.IO.Path.GetDirectoryName(vsTools[i]));
                xsdFolder = System.IO.Path.Combine(commonPath, "Packages\\schemas\\html");
                System.IO.File.Copy(System.IO.Path.Combine(root, "NFinal\\Resource\\schemas\\html\\html_401.xsd"), System.IO.Path.Combine(xsdFolder, "html_401.xsd"), true);
                System.IO.File.Copy(System.IO.Path.Combine(root, "NFinal\\Resource\\schemas\\html\\html_5.xsd"), System.IO.Path.Combine(xsdFolder, "html_5.xsd"), true);
                System.IO.File.Copy(System.IO.Path.Combine(root, "NFinal\\Resource\\schemas\\html\\nfinal.xsd"), System.IO.Path.Combine(xsdFolder, "nfinal.xsd"), true);
                System.IO.File.Copy(System.IO.Path.Combine(root, "NFinal\\Resource\\schemas\\html\\xhtml_5.xsd"), System.IO.Path.Combine(xsdFolder, "xhtml_5.xsd"), true);
            }
        }
    }
}
