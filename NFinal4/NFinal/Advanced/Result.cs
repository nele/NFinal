﻿using System;
using System.Collections.Generic;
using System.Web;

namespace NFinal.Advanced
{
    public enum Result
    {
        Html,
        HtmlZip,
        HtmlDeflate,
        Empty,
        Redirect,
        Json,
        JsonZip,
        JsonDeflate,
        JavaScript,
        JavaScriptZip,
        JavaScriptDeflate,
        Css,
        CssZip,
        CssDeflate,
        File,
        Image,
        Xml,
        XmlZip,
        XmlDeflate,
        Text,
        TextZip,
        TextDeflate
    }
}