﻿using System;
using System.Collections.Generic;
using System.Web;

namespace NFinal.Advanced
{
    [AttributeUsage(AttributeTargets.Method)]
    public class ActionAttribute : Attribute
    {
        public ActionAttribute(Result type)
        { }
    }
}