﻿using System;
using System.Collections.Generic;
using System.Web;

namespace NFinal.Advanced
{
    public enum Server
    {
        /// <summary>
        /// 不缓存
        /// </summary>
        NoCache = 1,
        /// <summary>
        /// 文件依赖
        /// </summary>
        FileDependency = 2,
        /// <summary>
        /// 绝对超时
        /// </summary>
        AbsoluteExpiration = 4,
        /// <summary>
        /// 滑动超时
        /// </summary>
        SlidingExpiration = 8
    }
}