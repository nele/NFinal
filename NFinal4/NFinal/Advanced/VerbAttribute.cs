﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NFinal.Advanced
{
    [AttributeUsage(AttributeTargets.Method,AllowMultiple=false,Inherited=false)]
    public class VerbAttribute:Attribute
    {
    
    }
    [AttributeUsage(AttributeTargets.Method, AllowMultiple = true, Inherited = false)]
    public class RewriteFile : VerbAttribute
    {
        public RewriteFile(string from,string to)
        { }
    }
    [AttributeUsage(AttributeTargets.Method, AllowMultiple = true, Inherited = false)]
    public class RewriteDirectory : VerbAttribute
    {
        public RewriteDirectory(string from, string to)
        { }
    }
    public class FormHtml : VerbAttribute
    {
        public FormHtml()
        { }
        public FormHtml(string path)
        { }
    }

    public class Html : VerbAttribute
    {
        public Html()
        { }
        public Html(string path)
        { }
    }

    public class GetHtml : Attribute
    {
        public GetHtml()
        { }
        public GetHtml(string path)
        { }
    }
    public class PostHtml : VerbAttribute
    {
        public PostHtml()
        { }
        public PostHtml(string path)
        { }
    }
    public class HtmlZip : VerbAttribute
    {
        public HtmlZip()
        { }
        public HtmlZip(string path)
        { }
    }
    public class GetHtmlZip : VerbAttribute
    {
        public GetHtmlZip()
        { }
        public GetHtmlZip(string path)
        { }
    }
    public class PostHtmlZip : VerbAttribute
    {
        public PostHtmlZip()
        { }
        public PostHtmlZip(string path)
        { }
    }
    public class HtmlDeflate : VerbAttribute
    {
        public HtmlDeflate()
        { }
        public HtmlDeflate(string path)
        { }
    }
    public class GetHtmlDeflate : VerbAttribute
    {
        public GetHtmlDeflate()
        { }
        public GetHtmlDeflate(string path)
        { }
    }
    public class PostHtmlDeflate : VerbAttribute
    {
        public PostHtmlDeflate()
        { }
        public PostHtmlDeflate(string path)
        { }
    }
    public class Empty : VerbAttribute
    {
        public Empty()
        { }
        public Empty(string path)
        { }
    }
    public class GetEmpty : VerbAttribute
    {
        public GetEmpty()
        { }
        public GetEmpty(string path)
        { }
    }
    public class PostEmpty : VerbAttribute
    {
        public PostEmpty()
        { }
        public PostEmpty(string path)
        { }
    }
    public class Redirect : VerbAttribute
    {
        public Redirect()
        {}
        public Redirect(string path)
        { }
    }
    public class GetRedirect : VerbAttribute
    {
        public GetRedirect()
        { }
        public GetRedirect(string path)
        { }
    }
    public class PostRedirect : VerbAttribute
    {
        public PostRedirect()
        { }
        public PostRedirect(string path)
        { }
    }
    public class Json : VerbAttribute
    {
        public Json()
        { }
        public Json(string path)
        { }
    }
    public class GetJson : VerbAttribute
    {
        public GetJson()
        { }
        public GetJson(string path)
        { }
    }
    public class PostJson : VerbAttribute
    {
        public PostJson()
        { }
        public PostJson(string path)
        { }
    }
    public class JsonZip : VerbAttribute
    {
        public JsonZip()
        { }
        public JsonZip(string path)
        { }
    }
    public class GetJsonZip : VerbAttribute
    {
        public GetJsonZip()
        { }
        public GetJsonZip(string path)
        { }
    }
    public class PostJsonZip : VerbAttribute
    {
        public PostJsonZip()
        { }
        public PostJsonZip(string path)
        { }
    }
    public class JsonDeflate : VerbAttribute
    {
        public JsonDeflate()
        { }
        public JsonDeflate(string path)
        { }
    }
    public class GetJsonDeflate : VerbAttribute
    {
        public GetJsonDeflate()
        { }
        public GetJsonDeflate(string path)
        { }
    }
    public class PostJsonDeflate : VerbAttribute
    {
        public PostJsonDeflate()
        { }
        public PostJsonDeflate(string path)
        { }
    }
    public class JavaScript : VerbAttribute
    {
        public JavaScript() 
        { }
        public JavaScript(string path)
        { }
    }
    public class GetJavaScript : VerbAttribute
    {
        public GetJavaScript()
        { }
        public GetJavaScript(string path)
        { }
    }
    public class PostJavaScript : VerbAttribute
    {
        public PostJavaScript()
        { }
        public PostJavaScript(string path)
        { }
    }
    public class JavaScriptZip : VerbAttribute
    {
        public JavaScriptZip()
        {}
        public JavaScriptZip(string path)
        { }
    }
    public class GetJavaScriptZip : VerbAttribute
    {
        public GetJavaScriptZip()
        { }
        public GetJavaScriptZip(string path)
        { }
    }
    public class PostJavaScriptZip : VerbAttribute
    {
        public PostJavaScriptZip()
        { }
        public PostJavaScriptZip(string path)
        { }
    }
    public class JavaScriptDeflate : VerbAttribute
    {
        public JavaScriptDeflate()
        { }
        public JavaScriptDeflate(string path)
        { }
    }
    public class GetJavaScriptDeflate : VerbAttribute
    {
        public GetJavaScriptDeflate()
        { }
        public GetJavaScriptDeflate(string path)
        { }
    }
    public class PostJavaScriptDeflate : VerbAttribute
    {
        public PostJavaScriptDeflate()
        { }
        public PostJavaScriptDeflate(string path)
        { }
    }
    public class Css : VerbAttribute
    {
        public Css()
        { }
        public Css(string path)
        { }
    }
    public class GetCss : VerbAttribute
    {
        public GetCss()
        { }
        public GetCss(string path)
        { }
    }
    public class PostCss : VerbAttribute
    {
        public PostCss()
        { }
        public PostCss(string path)
        { }
    }
    public class CssZip : VerbAttribute
    {
        public CssZip()
        { }
        public CssZip(string path)
        { }
    }
    public class GetCssZip : VerbAttribute
    {
        public GetCssZip()
        { }
        public GetCssZip(string path)
        { }
    }
    public class PostCssZip : VerbAttribute
    {
        public PostCssZip()
        { }
        public PostCssZip(string path)
        { }
    }
    public class CssDeflate : VerbAttribute
    {
        public CssDeflate()
        { }
        public CssDeflate(string path)
        { }
    }
    public class GetCssDeflate : VerbAttribute
    {
        public GetCssDeflate()
        { }
        public GetCssDeflate(string path)
        { }
    }
    public class PostCssDeflate : VerbAttribute
    {
        public PostCssDeflate()
        { }
        public PostCssDeflate(string path)
        { }
    }
    public class File : VerbAttribute
    {
        public File()
        { }
        public File(string path)
        { }
    }
    public class GetFile : VerbAttribute
    {
        public GetFile()
        { }
        public GetFile(string path)
        { }
    }
    public class PostFile : VerbAttribute
    {
        public PostFile()
        { }
        public PostFile(string path)
        { }
    }
    public class Jpeg : VerbAttribute
    {
        public Jpeg()
        { }
        public Jpeg(string path)
        { }
    }
	public class GetJpeg : VerbAttribute
    {
		public GetJpeg()
        { }
		public GetJpeg(string path)
        { }
    }
	public class PostJpeg : VerbAttribute
    {
		public PostJpeg()
        { }
		public PostJpeg(string path)
        { }
    }
	public class Gif : VerbAttribute
	{
		public Gif()
		{ }
		public Gif(string path)
		{ }
	}
	public class GetGif : VerbAttribute
	{
		public GetGif()
		{ }
		public GetGif(string path)
		{ }
	}
	public class PostGif : VerbAttribute
	{
		public PostGif()
		{ }
		public PostGif(string path)
		{ }
	}
	public class Png : VerbAttribute
	{
		public Png()
		{ }
		public Png(string path)
		{ }
	}
	public class GetPng : VerbAttribute
	{
		public GetPng()
		{ }
		public GetPng(string path)
		{ }
	}
	public class PostPng : VerbAttribute
	{
		public PostPng()
		{ }
		public PostPng(string path)
		{ }
	}
    public class Xml : VerbAttribute
    {
        public Xml()
        { }
        public Xml(string path)
        { }
    }
    public class GetXml : VerbAttribute
    {
        public GetXml()
        { }
        public GetXml(string path)
        { }
    }
    public class PostXml : VerbAttribute
    {
        public PostXml()
        { }
        public PostXml(string path)
        { }
    }
    public class XmlZip : VerbAttribute
    {
        public XmlZip()
        { }
        public XmlZip(string path)
        { }
    }
    public class GetXmlZip : VerbAttribute
    {
        public GetXmlZip()
        { }
        public GetXmlZip(string path)
        { }
    }
    public class PostXmlZip : VerbAttribute
    {
        public PostXmlZip()
        { }
        public PostXmlZip(string path)
        { }
    }
    public class XmlDeflate : VerbAttribute
    {
        public XmlDeflate()
        { }
        public XmlDeflate(string path)
        { }
    }
    public class GetXmlDeflate : VerbAttribute
    {
        public GetXmlDeflate()
        { }
        public GetXmlDeflate(string path)
        { }
    }
    public class PostXmlDeflate : VerbAttribute
    {
        public PostXmlDeflate()
        { }
        public PostXmlDeflate(string path)
        { }
    }
    public class Text : VerbAttribute
    {
        public Text()
        { }
        public Text(string path)
        { }
    }
    public class GetText : VerbAttribute
    {
        public GetText()
        { }
        public GetText(string path)
        { }
    }
    public class PostText : VerbAttribute
    {
        public PostText()
        { }
        public PostText(string path)
        { }
    }
    public class TextZip : VerbAttribute
    {
        public TextZip()
        { }
        public TextZip(string path)
        { }
    }
    public class GetTextZip : VerbAttribute
    {
        public GetTextZip()
        { }
        public GetTextZip(string path)
        { }
    }
    public class PostTextZip : VerbAttribute
    {
        public PostTextZip()
        { }
        public PostTextZip(string path)
        { }
    }
    public class TextDeflate : VerbAttribute
    {
        public TextDeflate()
        { }
        public TextDeflate(string path)
        { }
    }
    public class GetTextDeflate : VerbAttribute
    {
        public GetTextDeflate()
        { }
        public GetTextDeflate(string path)
        { }
    }
    public class PostTextDeflate : VerbAttribute
    {
        public PostTextDeflate()
        { }
        public PostTextDeflate(string path)
        { }
    }
}
