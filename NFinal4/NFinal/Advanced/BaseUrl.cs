﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NFinal.Advanced
{
    [AttributeUsage(AttributeTargets.Class,AllowMultiple=false,Inherited=false)]
    public class BaseUrl:Attribute
    {
        public BaseUrl(string url)
        { }
    }
}
