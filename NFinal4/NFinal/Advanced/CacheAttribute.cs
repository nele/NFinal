﻿using System;
using System.Collections.Generic;
using System.Web;

namespace NFinal.Advanced
{
    [AttributeUsage(AttributeTargets.Method)]
    public class CacheAttribute:Attribute
    {
        public CacheAttribute(Server server,Browser browser,int second)
        { }
        public CacheAttribute(Standard standard,int second)
        { }
        public CacheAttribute(Standard standard)
        { }
    }
    [AttributeUsage(AttributeTargets.Method)]
    public class CacheFileAttribute : Attribute
    {
        public CacheFileAttribute(int second)
        { }
    }
    [AttributeUsage(AttributeTargets.Method)]
    public class CacheNormalAttribute : Attribute
    {
        public CacheNormalAttribute(int second)
        { }
    }
}