﻿//======================================================================
//
//        Copyright : Zhengzhou Strawberry Computer Technology Co.,LTD.
//        All rights reserved
//        
//        Application:NFinal MVC framework
//        Filename :Frame.cs
//        Description :总框架
//
//        created by Lucas at  2015-6-30`
//     
//        WebSite:http://www.nfinal.com
//
//======================================================================
using System;
using System.Collections.Generic;
using System.Web;
using System.IO;
using System.Reflection;
using System.Text;
using System.Xml;
using NFinal.Compile;

namespace NFinal
{
    public class Frame
    {
        public static string appRoot;
        public static string AssemblyTitle;

        public static System.Collections.Generic.List<NFinal.DB.ConnectionString> ConnectionStrings = new System.Collections.Generic.List<NFinal.DB.ConnectionString>();

        public Frame(string appRoot)
        {
            Frame.appRoot = appRoot;
            string[] fileNames = Directory.GetFiles(appRoot, "*.csproj");
            if (fileNames.Length > 0)
            {
                AssemblyTitle = Path.GetFileNameWithoutExtension(fileNames[0]);
            }
            else
            {
                string temp;
                temp = appRoot.Trim('\\');
                AssemblyTitle = temp.Substring(temp.LastIndexOf('\\') + 1);
            }
        }

        /// <summary>
        /// 把基于网站根目录的绝对路径改为相对路径
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public static string MapPath(string url)
        {
            return appRoot + url.Trim('/').Replace('/', '\\');
        }
        //释放数据库资源
        public void ClearDB()
        {
            if (NFinal.DB.Coding.DB.DbStore.Count > 0)
            {
                foreach (KeyValuePair<string, NFinal.DB.Coding.DataUtility> da  in NFinal.DB.Coding.DB.DbStore)
                {
                    da.Value.con.Close();
                }
                NFinal.DB.Coding.DB.DbStore.Clear();
            }
        }
        public void GetDB(Config config)
        {
            //获取WebConfig中的连接字符串信息
            Frame.ConnectionStrings.Clear();
            if (config.connectionStrings.Count > 0)
            {
                foreach (DB.ConnectionString connectionString in config.connectionStrings)
                {
                    if (connectionString.provider.ToLower().IndexOf("mysql") > -1)
                    {
                        connectionString.type = NFinal.DB.DBType.MySql;
                    }
                    else if (connectionString.provider.ToLower().IndexOf("sqlclient") > -1)
                    {
                        connectionString.type = NFinal.DB.DBType.SqlServer;
                    }
                    else if (connectionString.provider.ToLower().IndexOf("sqlite") > -1)
                    {
                        connectionString.type = NFinal.DB.DBType.Sqlite;
                    }
                    else if (connectionString.provider.ToLower().IndexOf("oracle") > -1)
                    {
                        connectionString.type = NFinal.DB.DBType.Oracle;
                    }
                    else if(connectionString.provider.ToLower().IndexOf("npgsql")>-1)
                    {
                        connectionString.type = NFinal.DB.DBType.PostgreSql;
                    }
                    Frame.ConnectionStrings.Add(connectionString);
                }
            }
            //读取数据库信息
            NFinal.DB.Coding.DataUtility dataUtility = null;
            if (Frame.ConnectionStrings.Count > 0)
            {
                NFinal.DB.ConnectionString conStr;
                NFinal.DB.Coding.DB.DbStore.Clear();
                for (int i = 0; i < Frame.ConnectionStrings.Count; i++)
                {
                    conStr = Frame.ConnectionStrings[i];
                    if (conStr.type == NFinal.DB.DBType.MySql)
                    {
                        dataUtility = new NFinal.DB.Coding.MySQLDataUtility(conStr.value);
                        dataUtility.GetAllTables(dataUtility.con.Database);
                        NFinal.DB.Coding.DB.DbStore.Add(conStr.name, dataUtility);
                    }
                    else if (conStr.type == NFinal.DB.DBType.Sqlite)
                    {
                        dataUtility = new NFinal.DB.Coding.SQLiteDataUtility(conStr.value);
                        dataUtility.GetAllTables(dataUtility.con.Database);
                        NFinal.DB.Coding.DB.DbStore.Add(conStr.name, dataUtility);
                    }
                    else if (conStr.type == NFinal.DB.DBType.SqlServer)
                    {
                        dataUtility = new NFinal.DB.Coding.SQLDataUtility(conStr.value);
                        dataUtility.GetAllTables(dataUtility.con.Database);
                        NFinal.DB.Coding.DB.DbStore.Add(conStr.name, dataUtility);
                    }
                    else if (conStr.type == NFinal.DB.DBType.Oracle)
                    {
                        dataUtility = new NFinal.DB.Coding.OracleDataUtility(conStr.value);
                        dataUtility.GetAllTables(dataUtility.con.Database);
                        NFinal.DB.Coding.DB.DbStore.Add(conStr.name, dataUtility);
                    }
                    else if (conStr.type == NFinal.DB.DBType.PostgreSql)
                    {
                        dataUtility = new NFinal.DB.Coding.PostgreSqlDataUtility(conStr.value);
                        dataUtility.GetAllTables(dataUtility.con.Database);
                        NFinal.DB.Coding.DB.DbStore.Add(conStr.name,dataUtility);
                    }
                }
            }
        }
        /// <summary>
        /// 创建主路由
        /// </summary>
        /// <param name="apps"></param>
        public void CreateMain(string[] apps)
        {
            JinianNet.JNTemplate.ITemplate template =new JinianNet.JNTemplate.Template(
                System.Text.Encoding.UTF8.GetString(NFinal.Template.Template.Main));
            template.Context.TempData["project"]=NFinal.Frame.AssemblyTitle;
            template.Context.TempData["apps"]=apps;
            template.Context.TempData["ControllerSuffix"]="Controller";
            template.Render(MapPath("/Main.cs"),System.Text.Encoding.UTF8);
            template =new JinianNet.JNTemplate.Template(
                System.Text.Encoding.UTF8.GetString(NFinal.Template.Template.Handler));
            template.Context.TempData["project"] = NFinal.Frame.AssemblyTitle;
            template.Render(MapPath("/Handler.cs"), System.Text.Encoding.UTF8);
        }

        /// <summary>
        /// 获取所有的应用模块
        /// </summary>
        /// <returns></returns>
        public string[] GetApps()
        {
            string[] dirs = Directory.GetDirectories(MapPath("/"));
            string configJsonFileName = string.Empty;
            System.Collections.Generic.List<string> apps = new System.Collections.Generic.List<string>();
            for (int i = 0; i < dirs.Length; i++)
            {
                configJsonFileName = System.IO.Path.Combine(dirs[i], "config.json");
                if (File.Exists(configJsonFileName))
                {
                    apps.Add(System.IO.Path.GetFileName(dirs[i]));
                }
            }
            return apps.ToArray();
        }
        //获取所有的控制器
        public System.Collections.Generic.List<ControllerFileData> GetAllControllers()
        {
            string[] apps = GetApps();
            System.Collections.Generic.List<ControllerFileData> fileDataList = new System.Collections.Generic.List<ControllerFileData>();
            ControllerAnalyse com = new ControllerAnalyse();
            ControllerFileData fileData = null;
            string[] fileNames = null;
            string controllerPath = string.Empty;
            Config config = null;
            for (int i = 0; i < apps.Length; i++)
            {
                controllerPath = Frame.MapPath("/" + apps[i].Trim('/') + "/Controllers");
                if (!Directory.Exists(controllerPath))
                {
                    continue;
                }
                fileNames = Directory.GetFiles(controllerPath, "*.cs", SearchOption.AllDirectories);
                if (fileNames == null || fileNames.Length < 1)
                {
                    continue;
                }
                config = ConfigurationManager.GetConfig(apps[i]);
                for (int j = 0; j < fileNames.Length; j++)
                {
                    fileData = com.GetFileData(Frame.appRoot, apps[i], fileNames[j], System.Text.Encoding.UTF8,config);
                    fileDataList.Add(fileData);
                }
            }
            return fileDataList;
        }
        //URL解析
        public void CreateModuleAndRouter(System.Collections.Generic.List<ControllerFileData> controllerFileDataList)
        {
            JinianNet.JNTemplate.ITemplate template= new JinianNet.JNTemplate.Template(
                System.Text.Encoding.UTF8.GetString(NFinal.Template.Template.Module));
            template.Context.TempData["controllerFileDataList"] = controllerFileDataList;
            template.Render(MapPath("/NFinal/Module.cs"), System.Text.Encoding.UTF8);
            template = new JinianNet.JNTemplate.Template(
                System.Text.Encoding.UTF8.GetString(NFinal.Template.Template.Router));
            template.Context.TempData["controllerFileDataList"] = controllerFileDataList;
            template.Context.TempData["project"] = Frame.AssemblyTitle;
            template.Render(MapPath("/Router.cs"),System.Text.Encoding.UTF8);
        }
        //URL生成
        public void CreateURLHelper(System.Collections.Generic.List<ControllerFileData> controllerFileDataList)
        {
            JinianNet.JNTemplate.ITemplate template = new JinianNet.JNTemplate.Template(
                System.Text.Encoding.UTF8.GetString(NFinal.Template.Template.Url));
            template.Context.TempData["controllerFileDataList"] = controllerFileDataList;
            template.Context.TempData["project"] = Frame.AssemblyTitle;
            template.Render(MapPath("/Url.cs"), System.Text.Encoding.UTF8);
            template = new JinianNet.JNTemplate.Template(
                System.Text.Encoding.UTF8.GetString(NFinal.Template.Template.Url_Js));
            template.Context.TempData["controllerFileDataList"] = controllerFileDataList;
            template.Context.TempData["project"] = Frame.AssemblyTitle;
            template.Render(MapPath("/Url.js"), System.Text.Encoding.UTF8);
        }
    }

}